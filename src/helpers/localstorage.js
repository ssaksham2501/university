
export function getStoredItem(key)
{
    let item = localStorage.getItem(key);
    item = JSON.parse(item);
    return item ? item.value : "";
}


export function storeItem(key, value)
{
    let payload = {
        value: value
    };
    localStorage.setItem(key, JSON.stringify(payload));
}

export function removeItem(key) {
    localStorage.removeItem(key);
}