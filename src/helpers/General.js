import parse from "html-react-parser"; 
import { v4 as uuidv4 } from "uuid";  
import moment from "moment";
import nl2br from "nl2br";
import Constant from "../apis/constants";

export function baseUrl(path) {
    if (path && path !== null && path !== "") return Constant.image + path;
    else return Constant.image;
}

export function baseUrlImage(path) {
    if (path && path !== null && path !== "") return Constant.image + path;
    else return Constant.image;
}
export function sharableUrl(path) {
    return encodeURI(Constant.host + path);
}

export function getUrlParam(key) {
    let urlParams = new URLSearchParams();
    return urlParams.has(key) ? urlParams.get(key) : null;
}

export function stringLimit(string, limit) {
    return string !== null
        ? string.length >= limit
            ? string.substr(0, limit) + "..."
            : string
        : "";
}

export function toHtml(string) {
    return parse(string);
}

export function stripTags(html) {
    return html.replace(/(<([^>]+)>)/gi, "");
}

export function uuId() {
    return uuidv4();
}

export function _date(timestamp) {
    let date = new Date(timestamp);
    let d = date.getDate();
    d = d < 10 ? "0" + d : d;

    let m = date.getMonth() + 1;
    m = m < 10 ? "0" + m : m;

    let y = date.getFullYear();

    return d + "/" + m + "/" + y;
}

export function _time(date) {
    return moment(date).format("LT");
}

export function _msdate(timestamp) {
    let date = new Date(timestamp);
    let d = date.getDate();
    d = d < 10 ? "0" + d : d;

    let m = date.getMonth() + 1;
    m = m < 10 ? "0" + m : m;

    let y = date.getFullYear();

    return y + "-" + m + "-" + d;
}

export function dateText(timestamp) {
    var d = new Date(timestamp);
    var days = ["Sun", "Mon", "Tues", "Wed", "Thur", "Fri", "Sat"];
    var months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sept",
        "Oct",
        "Nov",
        "Dec",
    ];

    return (
        days[d.getDay()] +
        ", " +
        d.getDate() +
        " " +
        months[d.getMonth()] +
        (new Date().getFullYear() !== d.getFullYear()
            ? ` ` + d.getFullYear()
            : ``)
    );
}

export function timeAgo(time) {
    let today = moment().format("L");
    let expected = moment(time).format("L");
        return moment(time).fromNow(true) + ' ago';
}

export function enterToBR(string) {
    return nl2br(string);
}

export function renderImage(image, size = "small") {
    if (typeof image === "string") {
        return image ? baseUrlImage(image) : '';
    } else if (size == "large") {
        return image && image.large && image.large !== ``
            ? baseUrlImage(image.large)
            : image && image.original && image.original !== ``
            ? baseUrlImage(image.original)
            : null;
    } else if (size === "medium") {
        return image && image.medium && image.medium !== ``
            ? baseUrlImage(image.medium)
            : image && image.original && image.original !== ``
            ? baseUrlImage(image.original)
            : null;
    } else {
        console.log(`image.small`, image.small);
        return image && image.small && image.small !== ``
            ? baseUrlImage(image.small)
            : image && image.original && image.original !== ``
            ? baseUrlImage(image.original)
            : '';
    }
}

export function respondMessage(mins) {
    if (mins * 1 >= 1440) {
        return `Typically responds in days`;
    } else if (mins * 1 >= 60) {
        return `Typically reponds in hours`;
    } else if (mins * 1 >= 0) {
        return `Typically reponds in minutes`;
    } else {
        return ``;
    }
}

export function getMonths(name = false) {
    // let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    return name ? months[name] : months
}

export function getYear() {
    let date = new Date();
    return date.getFullYear();
}

export function renderVideoThumb(video, path) {
    video = video.replace(/\\/g, '/').split('/');
    video = video[video.length - 1];
    return baseUrl( (path ? path : '/uploads/posts/thumbs/') + video.replace('.mp4', '.png') );
}

export function range(start, end, mZero = false) {
    var ans = [];
    for (let i = start; i <= end; i++) {
        if (mZero) {
            ans.push(i <= 9 ? '0' + i : i);
        }
        else {
            ans.push(i);
        }
    }
    return ans;
}

export function leapYear(year)
{
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

export function copyToClipboard(text)
{
    let copyText = document.querySelector("#clipboard-input");
    if (!copyText || copyText.length < 1)  {
        let elemDiv = document.createElement('input');
        elemDiv.setAttribute("id", 'clipboard-input');
        elemDiv.style.cssText = 'position:absolute;top:-1000px; left: -1000px;';
        elemDiv.value = text;
        document.body.appendChild(elemDiv);
        copyText = document.querySelector("#clipboard-input");
    }
    else {
        copyText.value = text;
    }
    copyText.select();
    navigator.clipboard
      .writeText(copyText.value)
      .then(() => {
      })
      .catch(() => {
      });
}

export function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');
    return replacedText;
}

export function getFlagIcon(code) {
    return `https://flagpedia.net/data/flags/h60/` + code.toLowerCase() + '.webp'
}

export function nFormatter(num, digits) {
  const lookup = [
    { value: 1, symbol: "" },
    { value: 1e3, symbol: "k" },
    { value: 1e6, symbol: "M" },
    { value: 1e9, symbol: "G" },
    { value: 1e12, symbol: "T" },
    { value: 1e15, symbol: "P" },
    { value: 1e18, symbol: "E" }
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var item = lookup.slice().reverse().find(function(item) {
    return num >= item.value;
  });
  return item ? (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol : "0";
}


export function ucfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


//Check Variables
export const isEmpty = (item) => {
    
    if(item === null){
        return true
    }
    else if( item === ''){
        return true;
    }
    else if( item === undefined){
        return true;
    }
    else{
        return false;
    }     
}

//Check array
export const isEmptyArray = (item) => {
   // console.log(item);
    if(item.length <= 0)
        return true
    else 
        return false;
}

export const isEmptyObj = (item) => {
    if(Object.keys(item).length === 0)
        return true
    else 
        return false;
}

export function parseYoutubeUrl(url)
{
    let id = url.replace('https://www.youtube.com/watch?v=', '').replace('https://youtu.be/', '').replace('https://www.youtube.com/embed/', '');
    return 'https://www.youtube.com/embed/' + id;
}

export function intToString(value) {
    value = value * 1;
    var suffixes = ["", "k", "m", "b","t"];
    var suffixNum = Math.floor((""+value).length/3);
    var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000,suffixNum)) : value).toPrecision(2));
    if (shortValue % 1 != 0) {
        shortValue = shortValue.toFixed(1);
    }
    return shortValue+suffixes[suffixNum];
}

export function renderNoAvatar(name)
{
    return `https://ui-avatars.com/api/?background=ebd0d9&font-size=0.3&name=` + name.substr(0, 1);
}

export function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}