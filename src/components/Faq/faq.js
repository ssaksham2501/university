import React, { useState, useEffect } from "react";
import faq_left from '../../assets/images/faq_left.png';
import Accordion from 'react-bootstrap/Accordion';
import PagesController from "../../apis/controllers/pages.controller";

function Faq() {
    const [question, setQuestion] = useState(null);
    const [leftheading, setLeftheading] = useState(null);
    useEffect(() => {
        getQuestionsList()
    }, []);

    const getQuestionsList = async () => {
        let response = await new PagesController().faqs();
        if (response.status) {
            setQuestion(response.listing)
            setLeftheading(response.page)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }
    return (
        <div>
            <div className="cms_banner_area top_space">
                <div className="bg_area">
                    <div className="heading_area">
                        <h1>
                            {leftheading !== null ? leftheading.main_title : ''}
                        </h1>
                    </div>
                </div>
            </div>
            <div className="faq_section top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="faq_wrap">
                                <div className="row">
                                    <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                        <div className="left_wrap sticky-top">
                                            <div className="image_area">
                                                <img src={faq_left} />
                                            </div>
                                            <div className="detail_wrap">
                                                <h1>{leftheading !== null ? leftheading.faq_title : ''}</h1>
                                                <h1>{leftheading !== null ? leftheading.faq_sub_title : ''}</h1>
                                                <p> {leftheading !== null ? leftheading.description : ''} </p>
                                                <div className="btn_area">
                                                    <button className="btn-primary-4" >Contact us</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                        <div className="right_wrap">
                                            <div className="faq_detail_area">
                                                {
                                                    question !== null
                                                    &&
                                                    question.map((listing, i) => {
                                                        return <> <Accordion defaultActiveKey={0} flush>
                                                            <Accordion.Item eventKey={i}>
                                                                <Accordion.Header>{listing.title}</Accordion.Header>
                                                                <Accordion.Body>
                                                                    <div dangerouslySetInnerHTML=
                                                                        {{ __html: listing.description }} />
                                                                </Accordion.Body>
                                                            </Accordion.Item>
                                                        </Accordion> </>
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Faq;