import React from "react";
import "./../../assets/sass/pages/home.scss";
import Banner from "./components/banner";
import News from "./components/news";
import Aboutus from "./components/about";
import MultipleItems from "./components/slider";
import Heading from "./components/heading";
import MultipleItems2 from "./components/top_courses";
import MultipleItems3 from "./components/top_agent";
import TopUniversities from "./components/top_university";
import Testimonial from "./components/testimonial"
import NewsUpdates from "./components/news_update"
import GetStarted from "./components/get_started";
import StudentsReview from "./components/student_review";

function Home() {
  return (
    <div className="top_space">
      <Banner />
      <MultipleItems />
      <Aboutus />
      <TopUniversities />
      <MultipleItems3 />
      <GetStarted />
      <MultipleItems2 />
      <StudentsReview />
      {/* <NewsUpdates />
      <Testimonial /> */}
    </div>
  );
}

export default Home;
