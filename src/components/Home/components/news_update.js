import React from "react";
import Heading from "./heading";
import news1 from "../../../assets/images/news1.png"
import news2 from "../../../assets/images/news2.png"
import news3 from "../../../assets/images/news3.png"
import news4 from "../../../assets/images/news4.png"
import news5 from "../../../assets/images/news5.png"
import news6 from "../../../assets/images/news6.png"
import { Link, withRouter } from "react-router-dom";

function NewsUpdates() {
    return (
        <div>
            <div className="news_update_section">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="news_detail_area">
                                <div className="heading_area">
                                    <Heading title="News & updates" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut 
                                        aliquam, purus sit amet luctus venenatis, " />
                                </div>
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="row">
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div className="image_detail_area">
                                                    <div className="image_area">
                                                        <img src={news1} alt="" />
                                                        <div className="info_area">
                                                            <h4>lorem ipsum sit dolor </h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                                ut aliquam, purus sit amet luctus venenatis, </p>
                                                        </div>
                                                        <div className="arrow_area">
                                                            <Link to='/#'><i className="fal fa-long-arrow-up"></i></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div className="image_detail_area">
                                                    <div className="image_area">
                                                        <img src={news2} alt="" />
                                                        <div className="info_area">
                                                            <h4>lorem ipsum sit dolor </h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                                ut aliquam, purus sit amet luctus venenatis, </p>
                                                        </div>
                                                        <div className="arrow_area">
                                                            <Link to='/#'><i className="fal fa-long-arrow-up"></i></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div className="image_detail_area">
                                                    <div className="image_area">
                                                        <img src={news3} alt="" />
                                                        <div className="info_area">
                                                            <h4>lorem ipsum sit dolor </h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                                ut aliquam, purus sit amet luctus venenatis, </p>
                                                        </div>
                                                        <div className="arrow_area">
                                                            <Link to='/#'><i className="fal fa-long-arrow-up"></i></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                        <div className="row">
                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div className="image_detail_area">
                                                    <div className="image_area">
                                                        <img src={news4} alt="" />
                                                        <div className="info_area">
                                                            <h4>lorem ipsum sit dolor </h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                                ut aliquam, purus sit amet luctus venenatis, </p>
                                                        </div>
                                                        <div className="arrow_area">
                                                        <Link to='/#'><i className="fal fa-long-arrow-up"></i></Link>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div className="image_detail_area">
                                                    <div className="image_area">
                                                        <img src={news5} alt="" />
                                                        <div className="info_area">
                                                            <h4>lorem ipsum sit dolor </h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                                ut aliquam, purus sit amet luctus venenatis, </p>
                                                        </div>
                                                        <div className="arrow_area">
                                                        <Link to='/#'><i className="fal fa-long-arrow-up"></i></Link>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div className="image_detail_area">
                                                    <div className="image_area">
                                                        <img src={news6} alt="" />
                                                        <div className="info_area">
                                                            <h4>lorem ipsum sit dolor </h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit
                                                                ut aliquam, purus sit amet luctus venenatis, </p>
                                                        </div>
                                                        <div className="arrow_area">
                                                        <Link to='/#'><i className="fal fa-long-arrow-up"></i></Link>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default NewsUpdates;
