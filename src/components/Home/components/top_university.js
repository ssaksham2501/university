import React, { Component, useState } from "react";
import top_uni1 from "../../../assets/images/top_uni1.png"
import top_uni2 from "../../../assets/images/top_uni2.png"
import top_uni3 from "../../../assets/images/top_uni3.png"
import top_uni4 from "../../../assets/images/top_uni4.png"
import uni_big from "../../../assets/images/uni_big.png"
import logo_top from "../../../assets/images/logo_top.png"
import { Rating } from 'react-simple-star-rating'
import { Link, withRouter } from "react-router-dom";
import Heading from "./heading";
import Slider from "react-slick";

    const TopUniversities = () => {
        const [nav1, setNav1] = useState();
        const [nav2, setNav2] = useState();

       
        return (
            <div>
                <div className="top_university_section">
                    <div className="container">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="heading_area el text-start">
                                    <Heading title="Top Universities" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet
                                 luctus venenatis, lectus magna fringilla urna, porttitor " />
                                </div>
                                <div className="top_university_detail_area">
                                    
                                    <Slider asNavFor={nav2} ref={(slider1) => setNav1(slider1)} arrows={true} slidesToShow={1}  >
                                        <div className="university_inner_area">
                                            <div className="left_area">
                                                <div className="img_area">
                                                    <img src={uni_big} alt="" />
                                                </div>
                                                <div className="slider_detail_area">
                                                    <p>02<sup>/12</sup></p>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="logo_area">
                                                    <div className="img_area">
                                                        <img src={logo_top} alt="" />
                                                    </div>
                                                </div>
                                                <div className="rating_area">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star"></i>
                                                        </li>
                                                        <li>
                                                            (4.1)
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="university_detail_area">
                                                    <h4>Penn State University</h4>
                                                    <div className="university_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i className="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area">
                                                                State College, PA 16801, United States
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <p>
                                                        The Pennsylvania State University is a public state-related
                                                        land-grant research university with campuses and facilities
                                                        throughout Pennsylvania
                                                        <Link to='/#'>...more</Link>
                                                    </p>
                                                </div>
                                                <div className="uni_detail_area">
                                                    <div className="box_area el">
                                                        <div className="dt_left_area">
                                                            Avg cost after aid
                                                        </div>
                                                        <div className="dt_right_area">
                                                            $27k
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Graduation rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            68%
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Acceptance rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            <i className="fal fa-horizontal-rule"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="university_inner_area">
                                            <div className="left_area">
                                                <div className="img_area">
                                                    <img src={uni_big} alt="" />
                                                </div>
                                               
                                                <div className="slider_detail_area">
                                                    <p>02<sup>/12</sup></p>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="logo_area">
                                                    <div className="img_area">
                                                        <img src={logo_top} alt="" />
                                                    </div>
                                                </div>
                                                <div className="rating_area">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            (4.1)
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="university_detail_area">
                                                    <h4>Penn State University</h4>
                                                    <div className="university_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i className="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area">
                                                                State College, PA 16801, United States
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <p>
                                                        The Pennsylvania State University is a public state-related
                                                        land-grant research university with campuses and facilities
                                                        throughout Pennsylvania
                                                        <Link to='/#'>...more</Link>
                                                    </p>
                                                </div>
                                                <div className="uni_detail_area">
                                                    <div className="box_area el">
                                                        <div className="dt_left_area">
                                                            Avg cost after aid
                                                        </div>
                                                        <div className="dt_right_area">
                                                            $27k
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Graduation rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            68%
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Acceptance rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            <i className="fal fa-horizontal-rule"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="university_inner_area">
                                            <div className="left_area">
                                                <div className="img_area">
                                                    <img src={uni_big} alt="" />
                                                </div>
                                               
                                                <div className="slider_detail_area">
                                                    <p>02<sup>/12</sup></p>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="logo_area">
                                                    <div className="img_area">
                                                        <img src={logo_top} alt="" />
                                                    </div>
                                                </div>
                                                <div className="rating_area">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            (4.1)
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="university_detail_area">
                                                    <h4>Penn State University</h4>
                                                    <div className="university_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i className="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area">
                                                                State College, PA 16801, United States
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <p>
                                                        The Pennsylvania State University is a public state-related
                                                        land-grant research university with campuses and facilities
                                                        throughout Pennsylvania
                                                        <Link to='/#'>...more</Link>
                                                    </p>
                                                </div>
                                                <div className="uni_detail_area">
                                                    <div className="box_area el">
                                                        <div className="dt_left_area">
                                                            Avg cost after aid
                                                        </div>
                                                        <div className="dt_right_area">
                                                            $27k
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Graduation rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            68%
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Acceptance rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            <i className="fal fa-horizontal-rule"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="university_inner_area">
                                            <div className="left_area">
                                                <div className="img_area">
                                                    <img src={uni_big} alt="" />
                                                </div>
                                               
                                                <div className="slider_detail_area">
                                                    <p>02<sup>/12</sup></p>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="logo_area">
                                                    <div className="img_area">
                                                        <img src={logo_top} alt="" />
                                                    </div>
                                                </div>
                                                <div className="rating_area">
                                                    <ul>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            <i class="fas fa-star active"></i>
                                                        </li>
                                                        <li>
                                                            (4.1)
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div className="university_detail_area">
                                                    <h4>Penn State University</h4>
                                                    <div className="university_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i className="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area">
                                                                State College, PA 16801, United States
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <p>
                                                        The Pennsylvania State University is a public state-related
                                                        land-grant research university with campuses and facilities
                                                        throughout Pennsylvania
                                                        <Link to='/#'>...more</Link>
                                                    </p>
                                                </div>
                                                <div className="uni_detail_area">
                                                    <div className="box_area el">
                                                        <div className="dt_left_area">
                                                            Avg cost after aid
                                                        </div>
                                                        <div className="dt_right_area">
                                                            $27k
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Graduation rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            68%
                                                        </div>
                                                    </div>
                                                    <div className="box_area">
                                                        <div className="dt_left_area">
                                                            Acceptance rate
                                                        </div>
                                                        <div className="dt_right_area">
                                                            <i className="fal fa-horizontal-rule"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Slider>
                                    
                                   
                                   
                                    <div className="side_university_area">
                                        <div className="side_strange">
                                            <Slider
                                            asNavFor={nav1}
                                            ref={(slider2) => setNav2(slider2)}
                                            slidesToShow={3}
                                            arrows={false}
                                            verticalSwiping= {false}
                                            vertical={true}
                                            margin='70'
                                            swipeToSlide={false}
                                            focusOnSelect={true}
                                            >
                                            <div className="uni_box_area">
                                                <div className="uni_left_area">
                                                    <div className="img_area">
                                                        <img src={top_uni1} alt="" />
                                                    </div>
                                                </div>
                                                <div className="uni_right_area">
                                                    <div className="detail_area">
                                                        <h4>Penn State University</h4>
                                                        <Link to='/#'>
                                                            <div className="location_detail">
                                                                <div className="left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="right_area">
                                                                    PA 16801, United States
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="side_rating_area">
                                                        <div className="rating">
                                                            <ul>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="icon_area">
                                                        <Link to='/#'> <i className="fal fa-bookmark"></i></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="uni_box_area">
                                                <div className="uni_left_area">
                                                    <div className="img_area">
                                                        <img src={top_uni2} alt="" />
                                                    </div>
                                                </div>
                                                <div className="uni_right_area">
                                                    <div className="detail_area">
                                                        <h4>Penn State University</h4>
                                                        <Link to='/#'>
                                                            <div className="location_detail">
                                                                <div className="left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="right_area">
                                                                    PA 16801, United States
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="side_rating_area">
                                                        <div className="rating">
                                                            <ul>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="icon_area">
                                                        <Link to='/#'> <i className="fal fa-bookmark"></i></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="uni_box_area">
                                                <div className="uni_left_area">
                                                    <div className="img_area">
                                                        <img src={top_uni3} alt="" />
                                                    </div>
                                                </div>
                                                <div className="uni_right_area">
                                                    <div className="detail_area">
                                                        <h4>Penn State University</h4>
                                                        <Link to='/#'>
                                                            <div className="location_detail">
                                                                <div className="left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="right_area">
                                                                    PA 16801, United States
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="side_rating_area">
                                                        <div className="rating">
                                                            <ul>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="icon_area">
                                                        <Link to='/#'> <i className="fal fa-bookmark"></i></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="uni_box_area">
                                                <div className="uni_left_area">
                                                    <div className="img_area">
                                                        <img src={top_uni4} alt="" />
                                                    </div>
                                                </div>
                                                <div className="uni_right_area">
                                                    <div className="detail_area">
                                                        <h4>Penn State University</h4>
                                                        <Link to='/#'>
                                                            <div className="location_detail">
                                                                <div className="left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="right_area">
                                                                    PA 16801, United States
                                                                </div>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="side_rating_area">
                                                        <div className="rating">
                                                            <ul>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star active"></i>
                                                                </li>
                                                                <li>
                                                                    <i class="fas fa-star"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="icon_area">
                                                        <Link to='/#'> <i className="fal fa-bookmark"></i></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            </Slider>

                                        </div>
                                    </div>
                                    
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    
}

export default TopUniversities;


