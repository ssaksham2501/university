import React, { Component, useState } from "react";
import getstartedbg from "../../../assets/images/get_started_bg.png";
import getbg from "../../../assets/images/get_bg.png";
import getperson from "../../../assets/images/get_person.png";
import { Link, withRouter } from "react-router-dom";

const GetStarted = () => {
    return (
        <div>
            <div className="get_started_section top-space">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="get_started_area">
                                <div className="background_area">
                                    <div className="bg_img_area">
                                        <img src={getstartedbg} />
                                    </div>
                                    <div className="inner_bg_area">
                                        <div className="bg_img">
                                            <img src={getbg} />
                                            <div className="inner_detail">
                                                <div className="info_area">
                                                    <div className="left_area">
                                                        <h2>Get started</h2>
                                                        <p>Lectus magna fringilla urna, porttitor Lectus magna
                                                            fringilla urna, porttitorLorem ipsum dolor sit amet.</p>
                                                        <div className="btn_area">
                                                            <Link to='/agent-signup'>
                                                                <button className="btn-primary-4">Register now</button>
                                                            </Link>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="person_img">
                                                        <img src={getperson} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GetStarted;