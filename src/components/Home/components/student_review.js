import React, { Component } from "react";
import Slider from "react-slick";
import Heading from "./heading";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import reviews1 from "../../../assets/images/reviews1.png"
import canadaicon from "../../../assets/images/canada_icon.png"
import releft from "../../../assets/images/re_left.png"
import reright from "../../../assets/images/re_right.png"
import reviewbg from "../../../assets/images/review_bg.png"




const StudentsReview = () => {

  
        // const settings = {
        //     dots: false,
        //     arrows: false,
        //     infinite: false,
        //     autoplay: true,
        //     cssEase: 'linear',
        //     variableWidth: true,
        //     speed: 2000,
        //     slidesToShow: true,
        //     adaptiveHeight: true,
        //     ltl: true,
        //     rows: 1,
        //     responsive: [
        //         {
        //             breakpoint: 1367,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 3,
        //                 initialSlide: 3,
        //             }
        //         },
        //         {
        //             breakpoint: 1025,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 2,
        //                 initialSlide: 2,
        //             }
        //         },
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 2,
        //                 initialSlide: 2,
        //             }
        //         },
        //         {
        //             breakpoint: 770,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 3,
        //                 initialSlide: 3
        //             }
        //         },
        //         {
        //             breakpoint: 480,
        //             settings: {
        //                 slidesToShow: 1,
        //                 slidesToScroll: 1
        //             }
        //         },
        //         {
        //             breakpoint: 700,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 2
        //             }
        //         }
        //     ]

        // };
        // const settings2 = {
        //     dots: false,
        //     arrows: false,
        //     infinite: false,
        //     autoplay: true,
        //     cssEase: 'linear',
        //     variableWidth: true,
        //     slidesToScroll:true,
        //     speed: 2000,
        //     adaptiveHeight: true,
        //     rtl: true,
        //     rows: 1,
        //     responsive: [
        //         {
        //             breakpoint: 1367,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 3,
        //                 initialSlide: 3,
        //             }
        //         },
        //         {
        //             breakpoint: 1025,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 2,
        //                 initialSlide: 2,
        //             }
        //         },
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 2,
        //                 initialSlide: 2,
        //             }
        //         },
        //         {
        //             breakpoint: 770,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 3,
        //                 initialSlide: 3
        //             }
        //         },
        //         {
        //             breakpoint: 480,
        //             settings: {
        //                 slidesToShow: 1,
        //                 slidesToScroll: 1
        //             }
        //         },
        //         {
        //             breakpoint: 700,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 2
        //             }
        //         }
        //     ]

        // };
        return (
            <div>
                <div className="student_review_section">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="review_area">
                                    <div className="bg_img_area">
                                        <div className="main_img_set">
                                            <img src={reviewbg} />
                                        </div>
                                        <div className="slider_wrap">
                                            <div className="heading_area">
                                                <Heading title="Lorem ipsum" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut 
                                                aliquam, purus sit amet luctus venenatis, " />
                                            </div>
                                            <div className="slider_marque">
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div className="slider_marque reverse">
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="slider_img_wrap">
                                                    <div className="slider_inner_area">
                                                        <div className="re_left_area">
                                                            <img src={releft} alt="" />
                                                        </div>
                                                        <div className="re_right_area">
                                                            <img src={reright} alt="" />
                                                        </div>
                                                        <div className="detail_area">
                                                            <div className="left_area">
                                                                <div className="image_area">
                                                                    <img src={reviews1} />
                                                                </div>
                                                                <div className="info_area">
                                                                    <h3>Leslie Alexander</h3>
                                                                    <p>Penn State University</p>
                                                                </div>
                                                            </div>
                                                            <div className="right_area">
                                                                <div className="image_area">
                                                                    <img src={canadaicon} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }


export default StudentsReview;





