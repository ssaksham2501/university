import React from "react";
import Form from 'react-bootstrap/Form'

function News() {
     return (
          <div>
               <div className="newswrap">
                    <div className="container">
                         <div className="row">
                              <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                   <div className="head_label">
                                        <h1>Newsletter</h1>
                                   </div>
                              </div>
                              <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                   <div className="subscribe">
                                        <Form>
                                             <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                  <Form.Control type="email" placeholder="Enter your email" />
                                             </Form.Group>
                                             <div className="subscribe_btn">
                                                  <button className="btn-primary-2" type="submit">
                                                       Subscribe
                                                  </button>
                                             </div>
                                        </Form>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     );
}

export default News;
