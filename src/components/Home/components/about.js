import React from "react";

import about_video from "../../../assets/images/about_video.mp4"
import AboutHeading from "./aboutheading";
import Heading from "./heading";


function Aboutus() {

    return (
        <div>
            <div className="about_section">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="heading_area el text-start">
                                <AboutHeading title="About Us" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet
                                 luctus venenatis, lectus magna fringilla urna, porttitor " />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="video_area text-center">
                    <div className="video_iner_area">
                        <video width="100%" height="500" controls >
                            <source src={about_video} type="video/mp4" />
                            {/* <source src="https://s3.amazonaws.com/codecademy-content/courses/React/react_video-cute.mp4" /> */}
                        </video>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Aboutus;