import React, { Component, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Heading from "./heading";
import testimonial1 from "../../../assets/images/testimonial1.png";
import testimonial2 from "../../../assets/images/testimonial2.png";
import testimonial3 from "../../../assets/images/testimonial3.png";
import { Rating } from 'react-simple-star-rating'


const Testimonial = () => {


  const [ratingValue, setRatingValue] = useState(0) // initial rating v

  const handleRating = (rate) => {
    setRatingValue(rate)
  }
  // Catch Rating value
  const handleReset = () => {
    setRatingValue(4)
    // other logic
  }


  const settings = {
    dots: false,
    arrows: true,
    infinite: true,
    autoplay: true,
    
    speed: 2000,
    slidesToShow: 3,
    adaptiveHeight: true,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 770,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      }
    ]
  };


  return (
    <div>
      <div className="testimonial_area">
        <div className="container">
          <div className="row">
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div className="slider_wrap">
                <div className="heading_area">
                  <Heading title="Testimonials" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut 
                                        aliquam, purus sit amet luctus venenatis, " />
                </div>
                <Slider {...settings}>
                  <div className="testimonial_slider">
                    <div className="slider_info_area">
                      <div className="slider_inner_area">
                        <div className="testimonial_top_area">
                          <div className="left_area">
                            <div className="img_area">
                              <img src={testimonial1} alt="" />
                            </div>
                          </div>
                          <div className="right_area">
                            <div className="rating_area">
                              <Rating
                                // onClick={}
                                ratingValue={ratingValue}
                                showTooltip={false}
                                initialValue={3.8}
                                readonly

                                tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                              />
                            </div>
                            <h4>Bessie Cooper</h4>
                          </div>
                        </div>
                        <div className="des_area">
                          <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit ut
                            aliquam, purus sit amet luctus venenatis, consectetur adipiscing
                            elit ut purus sit amet luctus venenatis, </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="testimonial_slider">
                    <div className="slider_info_area">
                      <div className="slider_inner_area">
                        <div className="testimonial_top_area">
                          <div className="left_area">
                            <div className="img_area">
                              <img src={testimonial2} alt="" />
                            </div>
                          </div>
                          <div className="right_area">
                            <div className="rating_area">
                              <Rating
                                //   onClick={handleRating}
                                ratingValue={ratingValue}
                                showTooltip={false}
                                initialValue={3.8}
                                readonly

                                tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                              />
                            </div>
                            <h4>Jane Cooper</h4>
                          </div>
                        </div>
                        <div className="des_area">
                          <p>Lorem ipsum dolor sit amet, con sectetur
                            adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                            consectetur adipiscing elit ut purus sit amet luctus venenatis, </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="testimonial_slider">
                    <div className="slider_info_area">
                      <div className="slider_inner_area">
                        <div className="testimonial_top_area">
                          <div className="left_area">
                            <div className="img_area">
                              <img src={testimonial2} alt="" />
                            </div>
                          </div>
                          <div className="right_area">
                            <div className="rating_area">
                              <Rating
                                //   onClick={handleRating}
                                ratingValue={ratingValue}
                                showTooltip={false}
                                initialValue={3.8}
                                readonly

                                tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                              />
                            </div>
                            <h4>Jane Cooper</h4>
                          </div>
                        </div>
                        <div className="des_area">
                          <p>Lorem ipsum dolor sit amet, con sectetur
                            adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                            consectetur adipiscing elit ut purus sit amet luctus venenatis, </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="testimonial_slider">
                    <div className="slider_info_area">
                      <div className="slider_inner_area">
                        <div className="testimonial_top_area">
                          <div className="left_area">
                            <div className="img_area">
                              <img src={testimonial2} alt="" />
                            </div>
                          </div>
                          <div className="right_area">
                            <div className="rating_area">
                              <Rating
                                //   onClick={handleRating}
                                ratingValue={ratingValue}
                                showTooltip={false}
                                initialValue={3.8}
                                readonly
                                tooltipDefaultText="0"
                                tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                              />
                            </div>
                            <h4>Jane Cooper</h4>
                          </div>
                        </div>
                        <div className="des_area">
                          <p>Lorem ipsum dolor sit amet, con sectetur
                            adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                            consectetur adipiscing elit ut purus sit amet luctus venenatis, </p>
                        </div>
                      </div>
                    </div>
                  </div>

                </Slider>
                <div className="count">
                  <p>03<sup>  /12</sup></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}



export default Testimonial;







































