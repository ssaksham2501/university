
import React from "react";


function Heading(props) {
    return (
      <>
          <div className="heading_wrap_set">
                <h1>{props.title}</h1>
                <p>{props.description}</p>
          </div>
      </>
    );
  }


  export default  Heading;