import React, { Component } from "react";
import Slider from "react-slick";
import Heading from "./heading";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import courses from "../../../assets/images/courses.png"



export class MultipleItems2 extends Component {
    render() {
        var settings = {
            dots: false,
            arrows: true,
            infinite: false,
            autoplay: false,
            speed: 2000,
            slidesToShow: 4,
            adaptiveHeight: true,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 770,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        initialSlide: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 700,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]

        };
        return (
            <div>
                <div className="top_courses">
                    <div className="container">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="slider_wrap">
                                    <div className="heading_area">
                                        <Heading title="Top Courses" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut 
                                        aliquam, purus sit amet luctus venenatis, " />
                                    </div>
                                    <Slider {...settings}>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="slider_inner_area">
                                                {/* <div className="img_area">
                                                    <img src={courses} alt="" />
                                                </div> */}
                                                <div className="detail_area">
                                                    <h4>MBA In Banking & lorem</h4>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut,
                                                        purus sit amet luctus venenatis, </p>
                                                    <ul>
                                                        <li>
                                                            <p>24 Month</p>
                                                        </li>
                                                        <li>
                                                            <p>1-1 Mentorship & job support</p>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div className="btn_area">
                                                <button className="btn-primary-2 w-100" >View Detail</button>
                                            </div>
                                        </div>
                                        
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MultipleItems2
