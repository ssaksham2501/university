import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import photo from "../../../assets/images/UofT-Logo1.png"
import photo1 from "../../../assets/images/Penn-State-University-Logo1.png"
import photo2 from "../../../assets/images/university-of-cambridge-logo1.png"
import photo3 from "../../../assets/images/harvard.png"

export class MultipleItems extends Component {
    render() {
        var settings = {
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            speed: 2000,
            slidesToShow: 4,
            adaptiveHeight: true,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 770,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
            ]
        };
        return (
            <div>
                <div className="Slider_logo">
                    <div className="container">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="slider_wrap">
                                    <Slider {...settings}>
                                        <div className="slider_img_wrap">
                                            <div className="set">
                                                <div className="img_area">
                                                    <img src={photo} alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="set">
                                                <div className="img_area">
                                                    <img src={photo1} alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="set">
                                                <div className="img_area">
                                                    <img src={photo2} alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="set">
                                                <div className="img_area">
                                                    <img src={photo3} alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="set">
                                                <div className="img_area">
                                                    <img src={photo} alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MultipleItems
