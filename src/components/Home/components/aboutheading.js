
import React from "react";


function AboutHeading(props) {
    return (
      <>
          <div className="heading_wrap_set el text-start">
                <h1>{props.title}</h1>
                <p>{props.description}</p>
          </div>
      </>
    );
  }


  export default  AboutHeading;