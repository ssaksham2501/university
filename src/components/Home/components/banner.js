import React from "react";
import Form from 'react-bootstrap/Form'
import { useHistory } from "react-router-dom";
import FilterController from "../../../apis/controllers/filter.controller";
import home_banner from "../../../assets/images/home_banner.png";
import { storeItem } from "../../../helpers/localstorage";
import Search from "../../listing/Search";
function Banner() {
  const history = useHistory()
  const [text, setText] = React.useState(``);
  const [county, setCountry] = React.useState(``);

  const submitSearch = (text, country) => {
    storeItem('search', {
        country: country,
        search: text
    });
    if (text && country) {
      history.push(`/colleges`);
    }
  }
  return (
    <div>
      <div className="banner_section">
        <div className="img_area">
          <img src={home_banner} />
        </div>
        <div className="container">
          <div className="row">
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <Search
                text={text}
                country={county}
                setSearchText={(text) => {
                  setText(text);
                }}
                setCountry={(countryId) => {
                  setCountry(countryId);
                }}
                submitSearch={submitSearch}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Banner;
