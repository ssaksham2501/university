import React, { Component, useState , useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Heading from "./heading";
import top_agent1 from "../../../assets/images/top_agent1.png";
import top_agent2 from "../../../assets/images/top_agent2.png";


const MultipleItems3 = () => {
    // const[currentSLide1 , currentSLide] = useState(0);
    




    const settings = {
        dots: false,
        arrows: true,
        infinite: false,
        autoplay: false,
        speed: 1500,
        slidesToShow: 2,
        adaptiveHeight: true,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 770,
            settings: {
              slidesToShow: 1.2,
            }
          },
          {
            breakpoint: 680,
            settings: {
              slidesToShow: 1,
            }
          },
          {
            breakpoint: 400,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 1
            }
          }
        ]
      };
     
        return (
            <div>
                <div className="top_agent">
                    <div className="container">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="slider_wrap">
                                    <div className="heading_area">
                                        <Heading title="Top Agents" description="Lorem ipsum dolor sit amet, consectetur adipiscing elit ut 
                                        aliquam, purus sit amet luctus venenatis, " />
                                    </div>
                                    <Slider {...settings}>
                                        <div className="slider_img_wrap">
                                            <div className="agent_box">
                                                <div className="slider_box_area">
                                                    <div className="slider_inner_area">
                                                        <div className="left_area">
                                                            <div className="img_area">
                                                                <img src={top_agent1} alt="" />
                                                            </div>
                                                        </div>
                                                        <div className="right_area">
                                                            <div className="name">
                                                                Cody Fisher
                                                            </div>
                                                            <div className="desc">
                                                                Lorem ipsum sit dolor amet ipsum dor lorem sit. Lorem sit dolor.
                                                            </div>
                                                            <div className="rating_area">
                                                                <div className="rating_met">
                                                                    <ul>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                       
                                                                    </ul>
                                                                    <span className="show_rating">
                                                                        <i class="fas fa-star pe-2"></i>3.0
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="location_area">
                                                                <div className="location_left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="location_right_area">
                                                                    <p> Lorem ipsum sit, canada</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bottom_area">
                                                    <p>90% Sucess Rate</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="agent_box">
                                                <div className="slider_box_area">
                                                    <div className="slider_inner_area">
                                                        <div className="left_area">
                                                            <div className="img_area">
                                                                <img src={top_agent2} alt="" />
                                                            </div>
                                                        </div>
                                                        <div className="right_area">
                                                            <div className="name">
                                                                Cody Fisher
                                                            </div>
                                                            <div className="desc">
                                                                Lorem ipsum sit dolor amet ipsum dor lorem sit. Lorem sit dolor.
                                                            </div>
                                                            <div className="rating_area">
                                                                <div className="rating_met">
                                                                    <ul>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                       
                                                                    </ul>
                                                                    <span className="show_rating">
                                                                        <i class="fas fa-star pe-2"></i>3.0
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="location_area">
                                                                <div className="location_left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="location_right_area">
                                                                    <p> Lorem ipsum sit, canada</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bottom_area">
                                                    <p>90% Sucess Rate</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="slider_img_wrap">
                                            <div className="agent_box">
                                                <div className="slider_box_area">
                                                    <div className="slider_inner_area">
                                                        <div className="left_area">
                                                            <div className="img_area">
                                                                <img src={top_agent1} alt="" />
                                                            </div>
                                                        </div>
                                                        <div className="right_area">
                                                            <div className="name">
                                                                Cody Fisher
                                                            </div>
                                                            <div className="desc">
                                                                Lorem ipsum sit dolor amet ipsum dor lorem sit. Lorem sit dolor.
                                                            </div>
                                                            <div className="rating_area">
                                                                <div className="rating_met">
                                                                    <ul>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                       
                                                                    </ul>
                                                                    <span className="show_rating">
                                                                        <i class="fas fa-star pe-2"></i>3.0
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="location_area">
                                                                <div className="location_left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="location_right_area">
                                                                    <p> Lorem ipsum sit, canada</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bottom_area">
                                                    <p>90% Sucess Rate</p>
                                                </div>
                                            </div>
                                        </div> 
                                        <div className="slider_img_wrap">
                                            <div className="agent_box">
                                                <div className="slider_box_area">
                                                    <div className="slider_inner_area">
                                                        <div className="left_area">
                                                            <div className="img_area">
                                                                <img src={top_agent2} alt="" />
                                                            </div>
                                                        </div>
                                                        <div className="right_area">
                                                            <div className="name">
                                                                Cody Fisher
                                                            </div>
                                                            <div className="desc">
                                                                Lorem ipsum sit dolor amet ipsum dor lorem sit. Lorem sit dolor.
                                                            </div>
                                                            <div className="rating_area">
                                                                <div className="rating_met">
                                                                    <ul>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star active`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star`}></i>
                                                                        </li>
                                                                       
                                                                    </ul>
                                                                    <span className="show_rating">
                                                                        <i class="fas fa-star pe-2"></i>3.0
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div className="location_area">
                                                                <div className="location_left_area">
                                                                    <i className="fas fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="location_right_area">
                                                                    <p> Lorem ipsum sit, canada</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="bottom_area">
                                                    <p>90% Sucess Rate</p>
                                                </div>
                                            </div>
                                        </div>   
                                    </Slider>
                                    
                                    <div className="count">
                                        <p>02<sup>  /12</sup></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }


export default MultipleItems3




