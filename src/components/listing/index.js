import React, { useState, useEffect } from "react";
import FilterController from "../../apis/controllers/filter.controller";
import { getStoredItem, storeItem } from "../../helpers/localstorage";
import Rightside from "./components/right";
import Sidebar from "./components/sidebar";

function Listing() {
    let defaultFilters = {
        tuition_cost: null,
        cost_of_living: null,
        scholarship: null,
        stream: null,
        program: null,
        gpa: null,
        duration: null,
        ielts: false,
        pte: false,
        toefl: false,
        rating: null,
        search: ``,
        country: null
    };
    const [sidebarOpen, setSideBarOpen] = useState(false);
    const [filters, setFilters] = useState(null);
    const [streams, setStreams] = useState(null);
    const [maxFees, setMaxFees] = useState(null);
    const [maxGpa, setMaxGpa] = useState(null);
    const [maxDuration, setMaxDuration] = useState(null);

    useEffect(() => {
        window.scrollTo(0, 0);
        init();
    }, []);

    const init = async () => {
        let limits = await new FilterController().getUniversityFilters();
        if (limits && limits.status) {
            setStreams(limits.streams);
            let d = { ...defaultFilters };
            if (limits.max_fee !== null && limits.max_fee > 0) {
                d.tuition_cost = { min: 0, max: limits.max_fee.toFixed(2) };
                setMaxFees(limits.max_fee.toFixed(2));
            }
            if (limits.max_duration !== null && limits.max_duration > 0) {
                d.duration = { min: 0, max: limits.max_duration };
                setMaxDuration(limits.max_duration.toFixed(2));
            }
            if (limits.max_gpa !== null && limits.max_gpa > 0) {
                d.gpa = { min: 0, max: limits.max_gpa };
                setMaxGpa(5)
            }

            let search = getStoredItem('search');
            if (search) {
                setFilters(search);
            }
            else {
                setFilters(d);
            }
        }
    }

    return (
      <div>
          <div className="listing_section top_space">
               <div className="container">
                    <div className="row">
                          <div className="col-xxl-12 col-xxl-12 col-xxl-12 col-xxl-12 col-xxl-12 col-xxl-12">
                              <div className="listing_area_wrap">
                                    <Sidebar
                                        filters={filters}
                                        streams={streams}
                                        show={sidebarOpen}
                                        maxDuration={maxDuration}
                                        maxFees={maxFees}
                                        maxGpa={maxGpa}
                                        closeSideBar={() => setSideBarOpen(false)}
                                        setFilters={(records) => {
                                            records.search = ``;
                                            setFilters(records);
                                            storeItem('search', records);
                                        }}
                                    />
                                    <Rightside 
                                        filters={filters}
                                        showSideBar={() => setSideBarOpen(true)}
                                        setFilters={(records) => {
                                            setFilters(records);
                                        }}
                                        submitSearch={(text, country) => {
                                            if (text && text.trim() && country) {
                                                setFilters({ ...defaultFilters, search: text.trim(), country: country ? country : `` });
                                                storeItem('search', { ...defaultFilters, search: text.trim(), country: country ? country : `` });
                                            }
                                            else if(!text && country) {
                                                setFilters({ ...filters, search: ``, country: country ? country : `` });
                                                storeItem('search', { ...filters, search: ``, country: country ? country : `` });
                                            }
                                        }}
                                    />
                              </div>
                          </div>
                    </div>
               </div>
          </div>
      </div>
    );
}

export default Listing;
