import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import FilterController from "../../apis/controllers/filter.controller";
import { storeItem } from "../../helpers/localstorage";
import AutoComplete from "../Layout/components/AutoComplete";
import Selectable from "../Layout/components/Selectable";
const Search = (props) => {
    const history = useHistory();
    const [countries, setCountries] = useState([]);
    const [selectedItem, setSelectedItem] = useState(null);

    useEffect(() => {
        if (props.countries && props.countries.length > 0) {
            setCountries(props.countries);
        }
        else {
            getCountries();
        }
    }, []);

    useEffect(() => {
        setSelectedItem(selectedItem)
    }, [selectedItem]);

    const getCountries = async () => {
        let response = await new FilterController().getCountries();
        setCountries(response);
    }

    const submitSearch = (text, country, selectedItem) => {
        if (selectedItem && selectedItem.type === 'university') {
            history.push('/college-detail/' + selectedItem.slug);
        }
        else if (selectedItem && selectedItem.type === 'agent') {
            history.push('/agent/' + selectedItem.slug);
        }
        else {            
            storeItem('search', {
                country: country,
                search: text
            });
            setTimeout(() => {
                props.submitSearch(text, country);
            }, 300);
        }
    }

    const filleddCountryValue = () => {
        if (props.country && countries && countries.length > 0) {
            let c = countries.filter((item) => (item.id == props.country));
            if (c && c.length > 0) {
                return c[0].value;
            }
        }
        return ``;
    }

    return (
    <div className="subscribe">
        <form
            onSubmit={(e) => { e.preventDefault(); submitSearch(props.text, props.country) }}
            >
                <div className="left">
                <div className="form_search form-group college-search">
                    <AutoComplete
                        id="text-search"
                        className="form-control search-autocomplete"
                        placeholder="Search for colleges, programs and more..."
                        value={props.text ? props.text : ``}
                        onChange={async (val) => {
                            if (!val.trim() && typeof props.setSearchText !== undefined) {
                                props.setSearchText(``);
                            }
                            let resp = await new FilterController().performSearch(val);
                            if (resp && resp.status) {
                                return resp.search;
                            }
                            else {
                                return [];
                            }
                        }}
                        onSelect={(val, item) => {
                            props.setSearchText(val);
                            if (item.country_id) {
                                props.setCountry(item.country_id);
                            }
                            else {
                                document.getElementById("autocomplete-input-country-search").focus();
                            }
                            
                            if (val && item.country_id) {
                                submitSearch(val, item.country_id, item);
                            }
                            else {
                                submitSearch(val, props.country, item);
                            }
                        }}
                    />
                </div>
            </div>
            <div className="right">
                <div className="form-group form_search">
                    <AutoComplete
                        id="country-search"
                        className="form-control search-autocomplete"
                        placeholder="Country"
                        value={filleddCountryValue()}
                        items={countries}
                        onChange={async (val) => {
                            props.setCountry(null);
                            if (countries && countries.length > 0) {
                                return countries.filter((item) => (item.value.toLowerCase().indexOf(val.toLowerCase()) > -1) );
                            }
                            else {
                                return [];
                            }
                        }}
                        onSelect={(val, item) => {
                            props.setCountry(item.id);
                            if (val && props.text) {
                                submitSearch(props.text, item.id);
                            }
                            else {
                                submitSearch(props.text, item.id);
                            }
                        }}
                    />
                    <div className="cross d-none">
                        <a href="/"><i class="fal fa-times"></i></a>
                    </div>
                </div>
                <div className="subscribe_btn">
                    <button
                        className="btn-primary-2  d-sm-block d-none"
                        onClick={() => {
                            submitSearch(props.text, props.country);
                        }}
                    >Search</button>
                    <button
                        className="mobile_search d-sm-none d-block"
                        onClick={() => {
                            console.log(props);
                            submitSearch(props.text, props.country);
                        }}
                    ><i className="fal fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>);
}
export default Search;