import React, { useEffect, useState } from "react";
import { isMobile, isTablet } from 'react-device-detect';
import InfiniteLoader from "react-infinite-loader";
import { Link } from "react-router-dom";
import FilterController from "../../../apis/controllers/filter.controller";
import UniversityController from "../../../apis/controllers/university.controllers";
import nocollege from '../../../assets/images/nocollege.jpg';
import Dropdown from "../../Layout/components/Dropdown";
import Search from "../Search";
import ListingGrids from "./ListingGrids";

let page = 1;
let listing = [];
let timeOut = null;
const Rightside = (props) => {  
  const [university, setUniversity] = useState([]);
  const [total, setTotal] = useState(0)
  const [loader, setLoader] = useState(false);
  const [isEmpty, setIsEmpty] = useState(false);
  const [filtering, setFiltering] = useState(false);
  const [list, setList] = useState(true);
  const [done, setDone] = useState(false);
  const [sorting, setSorting] = useState({
    title: "Global Ranking",
    value: "ranking"
  });
  const [searchText, setSearchText] = useState(``);
  const [countryId, setCountryId] = useState(null);
  const [searchLocation, setSearchLocation] = useState('')
  const [searchUniversityCourse, setUniversityCourse] = useState('');
  const [countries, setCountries] = useState([]);
  const loaderRef = React.useRef(loader);
  
  useEffect(() => {
    getCountries()
    return () => {
      window.removeEventListener('scroll', scrollSidebar);
    }
  }, []);

  useEffect(() => {
    if (props.filters) {
      applyFilters();
    }
  }, [props.filters, sorting]);

  useEffect(() => {
    setLoader(loader);
    loaderRef.current = { loader, done };
  }, [loader]);

  const applyFilters = () => {
      if (timeOut) {
        clearTimeout(timeOut);
      }

      timeOut = setTimeout(() => {
        setSorting(sorting);
        setLoader(false);
        setDone(false);
        scrollSidebar();
        getUniversity(1);
      }, 500);
  }

  const getCountries = async () => {
    let response = await new FilterController().getCountries();
    setCountries(response);
  }

  let lastDownAxis = 0;
  var lastScrollTop = 0;
  const scrollSidebar = async () => {
    if (!isMobile && !isTablet) {
      var st = window.pageYOffset || document.documentElement.scrollTop;
      let sideBarEle = document.querySelector('.side-bar');
      let rightGrids = document.querySelector('.listing_side_view');
      if (listing.length > 9) {  
        sideBarEle.classList.add('fixed-side-prop');
        let sidebarChilds = sideBarEle.children;
        let sidebarHeight = 0;
        for (let i in sidebarChilds) {
          sidebarHeight = sidebarHeight + (sidebarChilds[i].offsetHeight > 0 ? sidebarChilds[i].offsetHeight * 1 : 0);
        }
        sidebarHeight = (sidebarHeight > 0 ? sidebarHeight / 2 : 0) + 50;

        if (st < sidebarHeight) {
          
        }
        else {
          if (st >= rightGrids.offsetHeight - window.outerHeight) {
            let sHt = rightGrids.offsetHeight - window.outerHeight - sidebarHeight
            sideBarEle.style = "position:absolute;top:" + sHt + "px;transition:unset;";
          }
          else {
            if (st > lastScrollTop) {
              if (window.pageYOffset > sidebarHeight) {
                sideBarEle.style = "margin-top:-" + sidebarHeight + "px;";
              }
              lastDownAxis = window.pageYOffset;
            } else {
              let t = (sidebarHeight / 2) - (lastDownAxis - window.pageYOffset);
              sideBarEle.style = "margin-top:" + (t > 0 ? '-' + t : 0) + "px;";
            }
            lastScrollTop = st <= 0 ? 0 : st; // For Mobile or negative scrolling
          }
        }

        if (st >= rightGrids.offsetHeight - window.outerHeight - 500 && loaderRef.current && !loaderRef.current.loader && !loaderRef.current.done) {
          await paginateRecords();
        }
      }
      else {
        sideBarEle.classList.remove('fixed-side-prop');
        sideBarEle.style = "";
      }

      /** Search bar fix at top **/
      let rightSideTop = document.querySelector('.lsiting_show_wrap');
      if (st >= rightGrids.offsetHeight - 310) {
        document.querySelector('.listing_side_main_set_wrap.two').classList.add('d-none');
      }
      else if(st > rightSideTop.offsetTop + 50) {
        document.querySelector('.listing_side_main_set_wrap.two').classList.remove('d-none');
      }
      else {
        document.querySelector('.listing_side_main_set_wrap.two').classList.add('d-none');
      }
    }
  }

  const paginateRecords = async () => {
    if (!loader && !done) {
      await getUniversity(page + 1, true);
    }
  }

  const getUniversity = async (p = null, noOpacity = false) => {
    if (!loader && !done) {
      p = p > 1 ? p : 1;
      setLoader(true);
      if (!noOpacity) {
        setFiltering(true);
      }
      setIsEmpty(false);
      let filters = props.filters ? props.filters : {};
      filters.sort = sorting && sorting.value ? sorting.value : null;
      filters.page = p;
      let res = await new UniversityController().getUniversity(filters);
      
      if (res && res.status) {
        page = p;
        setLoader(false);
        setTotal(res.total)
        if (listing.length > 0 && page > 1) {
          listing = [...listing, ...res.listing];
          setUniversity(listing);
        }
        else {
          listing = res.listing;
          setUniversity(res.listing);
        }

        if (res.listing < 1 && page === 1) {
          setIsEmpty(true);
        }
        else if (res.listing < 1) {
          setDone(true);
          setIsEmpty(false);
        }
        else {
          setIsEmpty(false);
        }
      }
      scrollSidebar();
      setFiltering(false);
      if (page === 1) {
        window.addEventListener('scroll', scrollSidebar);
        window.scrollTo(0, 0);
      }
    }
  }

  const favourite = async (slug, index) => {
    console.log(listing[index])
    let response = await new UniversityController().favUniversity(slug);
    if (response && response.status && listing[index]) {
      listing[index].favourite = response.fav ? 1 : 0;
      setUniversity(listing);
    }
  }
  
  const applyNow  = async (id) =>{
    // console.log(id, 'id from list view')
    let response = await  UniversityController().appliedUniversity(id)
    if(response && response.status) {
      alert(response.statusMessage)
    }
    else{
      alert('erroe message')
    }
  }

  return (
    <div className="listing_side_view">
      <div className="listing_side_main_set_wrap">
        {
          countries && countries.length > 0
          &&
          <Search
            text={props.filters && props.filters.search}
            country={props.filters && props.filters.country}
            countries={countries}
            setSearchText={(text) => {
              setSearchText(text);
              if (!text) {
                props.submitSearch(``, props.filters && props.filters.country ? props.filters.country : ``);
              }
            }}
            setCountry={(countryId) => {
              setCountryId(countryId);
            }}
            submitSearch={async (text, country) => {
              props.submitSearch(text, country);
            }}
          />
        }
        <div className="listing_head_wrap">
          <div className="left d-sm-block d-none">
            <p>{total > 0 ? <>{total}<span> listing found</span></> : `` }</p>
          </div>
          <div className="right">
            <div className="sort_by_warp">
              <ul>
                <li>
                  <div className="sort_by_swap">
                    <div className="left">
                      <p>Sort by</p>
                    </div>

                    <div className="right">
                      <Dropdown
                        id="dropdown-listing"
                        title={sorting ? sorting.title : `Global Ranking`}
                        selected={sorting}
                        items={[
                          {
                            title: "Global Ranking",
                            value: "ranking"
                          },
                          {
                            title: "Popular",
                            value: "popular"
                          },
                          
                          {
                            title: "Rating",
                            value: "rating"
                          }
                        ]}
                        onSelect={(item) => {
                          setSorting(item);
                        }}
                      />
                    </div>
                  </div>
                </li>
                <li>
                  <div className="listing_view">
                    <Link to="" className={!list ? 'inactive' : "active"} onClick={(e) => {
                      e.preventDefault();
                      setList(true)
                    }}>
                      <i className="fal fa-bars"></i>
                    </Link>
                    <Link to="" className={list ? 'inactive' : "active"} onClick={(e) => {
                      e.preventDefault();
                      setList(false);
                    }}>
                      <i className="fal fa-border-all"></i>
                    </Link>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div className="listing_side_main_set_wrap two d-none">
          <div className="left_area">
            {
              countries && countries.length > 0
              &&
              <Search
                text={props.filters && props.filters.search}
                country={props.filters && props.filters.country}
                countries={countries}
                setSearchText={(text) => {
                  setSearchText(text);
                }}
                setCountry={(countryId) => {
                  setCountryId(countryId);
                }}
                submitSearch={async (text, country) => {
                  props.submitSearch(text, country);
                }}
              />
            }
              <div className="toal_list">
                <p>{total > 0 ? <>{total}<span> listing found</span></> : `` }</p>
              </div>
          </div>
          <div className="right_area">
            <div className="listing_head_wrap">
                <div className="left d-sm-block d-none">
                  
                </div>
                <div className="right">
                  <div className="sort_by_warp">
                    <ul>
                      <li>
                        <div className="sort_by_swap">
                          {/* <div className="left">
                            <p>Sort by</p>
                          </div> */}

                          <div className="right">
                            <Dropdown
                              id="dropdown-listing"
                              title={sorting ? sorting.title : `Global Ranking`}
                              selected={sorting}
                              items={[
                                {
                                  title: "Global Ranking",
                                  value: "ranking"
                                },
                                {
                                  title: "Popular",
                                  value: "popular"
                                },
                                
                                {
                                  title: "Rating",
                                  value: "rating"
                                }
                              ]}
                              onSelect={(item) => {
                                setSorting(item);
                              }}
                            />
                          </div>
                        </div>
                      </li>
                      <li>
                        <div className="listing_view">
                          <Link to="" className={!list ? 'inactive' : "active"} onClick={(e) => {
                            e.preventDefault();
                            setList(true)
                          }}>
                            <i className="fal fa-bars"></i>
                          </Link>
                          <Link to="" className={list ? 'inactive' : "active"} onClick={(e) => {
                            e.preventDefault();
                            setList(false);
                          }}>
                            <i className="fal fa-border-all"></i>
                          </Link>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>  
            </div>
          </div>
          
        </div>
        
        <div className="Filter_mobile d-xl-none d-flex">
          <Link
            to=""
            onClick={(e) => {
              e.preventDefault(); props.showSideBar()
            }}
          >Filter<i className="fas fa-filter ps-2"></i></Link>
          <div className="left d-sm-none d-block">
            <p>{total > 0 ? <>{total}<span>listing found</span></> : `` }</p>
          </div>
        </div>
        <div className="lsiting_show_wrap">
          <div className="list_grid" style={filtering ? { opacity: 0.6 } : {}}>
            <div className={list ? "list_grid_wrap list_single" : "list_grid_wrap"}>
              {
                university && university.length > 0 &&
                  university && university.map((item, index) => {
                    return (<ListingGrids
                      index={index}
                      key={`listin-grid` + index}
                      listView={list}
                      university={item}
                      setFavourite={async (slug, index) => {
                        await favourite(slug, index);
                      }}
                      applyNow={applyNow}
                    />)
                  })
              }
              {
                isEmpty
                &&
                  <div className="border_box_area">
                    <div className="tags_area">
                        <div className="left">
                          <p>No record available. Try different filters for results.</p>
                        </div>
                        <div className="right">
                              <div className="img_area">
                                  <img src={nocollege}></img>
                              </div>
                        </div>
                    </div>
                  </div>
              }
              {
                loader && !isEmpty
                &&
                <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Rightside;
