import MultiRangeSlider from "multi-range-slider-react";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating';
import FilterController from "../../../apis/controllers/filter.controller";

const Sidebar = (props) => {
  const [timeoutFilters, setTimeoutFilters] = useState(null);
  const [streamSearch, setStreamSearch] = useState(``);
  const [streamAccordian, setStreamAccordian] = useState(false);
  const [programs, setPrograms] = useState([]);

  /** Mobile Sidebar **/
  useEffect(() => {
    if (props.show) {
      document.body.classList.add('listing_flow');
    }
    else {
      document.body.classList.remove('listing_flow');
    }
  }, [props.show]);
  /** Mobile Sidebar **/

  const handleStreams = (item) => {
    let s = props.filters && props.filters.stream ? props.filters.stream: [];
    s = s && s.length > 0 ? s : [];
    if (s && s.includes(item.slug)) {
      let index = s.indexOf(item.slug);
      s.splice(index, 1);
      props.setFilters({ ...props.filters, stream: s });
    }
    else {
      s.push(item.slug);
      props.setFilters({ ...props.filters, stream: s });
    }
  }

  const handleRating = (rate) => {
    props.setFilters({ ...props.filters, rating: rate });
  }

  // const getPrograms = async (streams) => {
  //   let response = await new FilterController().getPrograms({
  //     streams: streams
  //   });

  //   if (response && response.status) {
  //     setPrograms(response.programs);
  //   }
  //   else {
  //     setPrograms([]);
  //   }
  // }

  return (
    <div className="side_bar_met">
      <div className={props && props.show ? "side-bar open" : "side-bar "}>
        <div className="filter_head_label">
          <h5>Filter</h5>
        </div>
        <div className="option_set_">
          <div className="accordion">
            <div className="accordion-item">
              <h2 className="accordion-header">
                <button
                  type="button"
                  aria-expanded="false"
                  className={`accordion-button` + (streamAccordian ? ` collapsed` : `` )}
                  onClick={() => {
                    setStreamAccordian(!streamAccordian);
                  }}
                >Stream</button>
              </h2>
              <div className={`accordion-collapse collapse` + (streamAccordian ? ` hide` : ` show` )}>
                <div className="accordion-body">
                  <div className="serach_bar">
                    <input className="form-control" type="text" placeholder="Search" onChange={(e) => {
                      setStreamSearch(e.target.value);
                    }} />
                  </div>
                  <div className="check_type">
                    <div key={`default-checkbox`} className="mb-3">
                      {
                        props.streams && props.streams.length > 0 && props.streams.filter((item) => (item.title.toLowerCase().indexOf(streamSearch.toLowerCase()) > -1)).map((item, i) => {
                          return (<div className="form-check" key={`stram` + i}>
                            <input
                              type="checkbox"
                              id={`default-checkbox-` + i}
                              className="form-check-input"
                              value={item.slug}
                              checked={props.filters && props.filters.stream && props.filters.stream.includes(item.slug) ? true : false}
                              onChange={(e) => {
                                handleStreams(item);
                              }}
                            />
                            <label title="" htmlFor={`default-checkbox-` + i} className="form-check-label">{item.title}</label>
                          </div>)
                        })
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="fees">
          <div className="head">
            <h5>GPA Scale</h5>
          </div>
          <div className="fees_manage_range">
            <MultiRangeSlider
              min={0}
              max={5}
              step={0.1}
              ruler={false}
              label={false}
              preventWheel={false}
              minValue={props.filters && props.filters.gpa && props.filters.gpa.min ? props.filters.gpa.min : `0`}
              maxValue={props.filters && props.filters.gpa && props.filters.gpa.max ? props.filters.gpa.max : props.maxGpa}
              onInput={(e) => {
                props.setFilters({ ...props.filters, gpa: { min: e.minValue.toFixed(1), max: e.maxValue.toFixed(1) } });
              }}
            />
          </div>
          <div className="min_max">
            <div className="min">
              <p>Min</p>
              <input className="form-control" type="text" placeholder="" value={props.filters && props.filters.gpa && props.filters.gpa.min ? props.filters.gpa.min : `0`} />
            </div>
            <div className="max">
              <p>Max</p>
              <input className="form-control" type="text" placeholder="" value={props.filters && props.filters.gpa && props.filters.gpa.max ? props.filters.gpa.max : props.maxGpa} />
            </div>
          </div>
        </div>
        <div className="fees">
          <div className="head">
            <h5>Fees</h5>
          </div>
          <div className="fees_manage_range">
            <MultiRangeSlider
              min={0.00}
              max={props.maxFees}
              step={100}
              ruler={false}
              label={false}
              preventWheel={false}
              minValue={props.filters && props.filters.tuition_cost && props.filters.tuition_cost.min ? props.filters.tuition_cost.min : 0}
              maxValue={props.filters && props.filters.tuition_cost && props.filters.tuition_cost.max ? props.filters.tuition_cost.max : props.maxFees}
              onInput={(e) => {
                props.setFilters({ ...props.filters, tuition_cost: { min: e.minValue.toFixed(2), max: e.maxValue.toFixed(2) } });
              }}
            />
          </div>
          <div className="min_max">
            <div className="min">
              <p>Min</p>
              <input className="form-control" type="text" placeholder="" value={'$' + (props.filters && props.filters.tuition_cost && props.filters.tuition_cost.min ? props.filters.tuition_cost.min : '0') } />
            </div>
            <div className="max">
              <p>Max</p>
              <input className="form-control" type="text" placeholder="" value={'$' + (props.filters && props.filters.tuition_cost && props.filters.tuition_cost.max ? props.filters.tuition_cost.max : props.maxFees) } />
            </div>
          </div>
        </div>
        <div className="fees">
          <div className="study_btn">
            <button
              className={`btn-primary-1` + (props.filters && props.filters.ielts ? ' active' : '')}
              onClick={() => {
                props.setFilters({ ...props.filters, ielts: !props.filters.ielts });
              }}
            > IELTS</button>
            <button
              className={`btn-primary-1` + (props.filters && props.filters.toefl ? ' active' : '')}
              onClick={() => {
                props.setFilters({ ...props.filters, toefl: !props.filters.toefl });
              }}
            >TOEFL</button>
            <button
              className={`btn-primary-1` + (props.filters && props.filters.pte ? ' active' : '')}
              onClick={() => {
                props.setFilters({ ...props.filters, pte: !props.filters.pte });
              }}
            >PTE</button>
          </div>
          <div className="head">
            <div className="switch_detail">
              <h5>Scholarship</h5>
              <div className="switch_data">
                <label className="switch">
                  <input
                    type="checkbox"
                    checked={props.filters && props.filters.scholarship ? true : false}
                    onChange={() => {
                      props.setFilters({ ...props.filters, scholarship: props.filters && props.filters.scholarship ? false : true });
                    }}
                  />
                  <span className="slider round"></span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div className="fees">
          <div className="head">
            <h5>Course Duration</h5>
          </div>
          <div className="fees_manage_range">
            <MultiRangeSlider
              min={0}
              max={props.maxDuration}
              step={1}
              ruler={false}
              label={false}
              preventWheel={false}
              minValue={props.filters && props.filters.duration && props.filters.duration.min ? props.filters.duration.min : 0}
              maxValue={props.filters && props.filters.duration && props.filters.duration.max ? props.filters.duration.max : props.maxDuration}
              onInput={(e) => {
                props.setFilters({ ...props.filters, duration: {min: e.minValue, max: e.maxValue} });
              }}
            />
          </div>
          <div className="min_max">
            <div className="min">
              <p>Min</p>
              <input className="form-control" type="text" placeholder="" value={(props.filters && props.filters.duration && props.filters.duration.min > 0 ? (props.filters.duration.min + ` Years`) : '0 Year' )} />
            </div>
            <div className="max">
              <p>Max</p>
              <input className="form-control" type="text" placeholder="" value={(props.filters && props.filters.duration && props.filters.duration.max > 1 ? (props.filters.duration.max + ` Years`) : (props.filters && props.filters.duration && props.filters.duration.max === 1 ? `1 Year` : props.maxDuration + ` Years`) )} />
            </div>
          </div>
        </div>
        <div className="fees">
          <div className="head">
            <h5>Ratings & Review</h5>
          </div>
          <div className="rating_accdn">
            <Rating
              onClick={handleRating}
              ratingValue={props.filters && props.filters.rating}
              iconsCount={5}
              showTooltip={false}
              tooltipDefaultText="ABCD"
              tooltipArray={["0.1", "0.3", "5.7", "7.8", "EXC"]}
            />
          </div>
        </div>
        <div className="closer d-xl-none d-block">
          <Link to="#" onClick={(e) => { e.preventDefault(); props.closeSideBar() }}><i className="fal fa-times"></i></Link>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
