import React, { useEffect, useState } from "react";
import { EqualHeight , EqualHeightElement , EqualHeightContext} from "react-equal-height";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating';
import ModalController from "../../../apis/controllers/modal.controller";
import photo from '../../../assets/images/top_uni3.png';
import { intToString, isEmptyObj, nFormatter, renderImage, stringLimit, stripTags, ucfirst } from "../../../helpers/General";

const ListingGrids = (props) => {
    const [favouriteId, setfavouriteId] = useState([])
    const [applied, setApplied] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setfavouriteId([])
    }, [])

    const getFavourite = (id) => {
        setfavouriteId([...favouriteId, id])
        props.getFavourite(id)
    }

    const applyNow = (id) => {
        setApplied([...applied, id])
        props.applyNow(id)
    }

    const renderTags = (item) => {
        return (
            <>
                {
                    item.university_type
                    &&
                    <li className="">
                        <Link to='#' className="listing_button">{ucfirst(item.university_type)} </Link>
                    </li>
                }
                {
                    item.gpa
                    &&
                    <li className="">
                        <Link to='#' className="listing_button">GPA: {item.gpa} </Link>
                    </li>
                }
                {
                    item.course_duration && item.course_duration > 0
                    &&
                    <li className="other">
                        <Link to='#' className="listing_button">Duration: {item.course_duration + (item.course_duration > 1 ? ' Years' : ' Year')} </Link>
                    </li>
                }

                {
                    item.ielts_overall_score
                    &&
                    <li className="other">
                        <Link to='#' className="listing_button">IELTS: {item.ielts_overall_score} </Link>
                    </li>
                }
                {
                    item.tofel_score
                    &&
                    <li className="other">
                        <Link to='#' className="listing_button">TOEFL: {item.tofel_score} </Link>
                    </li>
                }
                {
                    item.pte_score
                    &&
                    <li className="other">
                        <Link to='#' className="listing_button">PTE: {item.pte_score} </Link>
                    </li>
                }
                {
                    item.total_applicants
                    &&
                    <li className="other">
                        <Link to='#' className="listing_button"> &nbsp; {nFormatter(item.total_applicants)}+ applicants</Link>
                    </li>
                }
                {
                    item.total_enrolled
                    &&
                    <li className="other">
                        <Link to='#' className="listing_button"> &nbsp; {nFormatter(item.total_enrolled)}+ enrolled</Link>
                    </li>
                }

            </>
        );
    }


    const handleWishlist = async (e) => {
        e.preventDefault()
        if (!loading) {
            setLoading(true);
            if (isEmptyObj(props.user)) {
                new ModalController().setStartModal(true);
            } else {
                new ModalController().setStartModal(false);
                await props.setFavourite(props.university.slug, props.index)
            }
            setLoading(false);
        }
    }

    return (
        <>
            <div className="listing_box">
                <div className="listing_one_third">
                    <div className="space_sprate">
                        <div className="img_area_parent">
                            <div className="img_area">
                                <img src={
                                    props.university.image && props.university.image.length > 0
                                        ? renderImage(props.university.image[0], 'medium')
                                        : photo
                                } alt=""
                                />
                                 <div className={`wishlist single`} style={loading ? { opacity: `0.7` } : {}}>
                                    <Link to="" onClick={handleWishlist}><i className={props.university && parseInt(props.university.favourite) === 1 ? `fas fa-bookmark` : `fal fa-bookmark`}></i></Link>
                                </div>
                            </div>
                        </div>
                        <div className="detail_below">
                            <div className="single_view">
                           
                            <EqualHeight>
                                <div className="name">
                                    <div className="left">
                                        <Link to={`/college-detail/${props.university.slug}`}><p>{props.university.title}</p></Link>
                                    </div>
                                   
                                        
                                   
                                    {
                                        props.university.rating === null || props.university.rating > 0 ? null :
                                            <div className="right">
                                                <p>
                                                    <i className="fas fa-star"></i>
                                                    {props.university.rating}
                                                </p>
                                            </div>
                                    }
                                </div>
                                {
                                    props.university.rating_user_count > 0
                                    &&
                                    <div className=' my-2'>
                                        <div className="rating_met">
                                        <ul>
                                            <li>
                                                <i className={`fas fa-star` + (props.university.rating >= 0.1 || props.university.rating >= 1 ? ' active' : '')}></i>
                                            </li>
                                            <li>
                                                <i className={`fas fa-star` + (props.university.rating >= 2 ? ' active' : '')}></i>
                                            </li>
                                            <li>
                                                <i className={`fas fa-star` + (props.university.rating >= 3 ? ' active' : '')}></i>
                                            </li>
                                            <li>
                                                <i className={`fas fa-star` + (props.university.rating >= 4 ? ' active' : '')}></i>
                                            </li>
                                            <li>
                                                <i className={`fas fa-star` + (props.university.rating >= 5 ? ' active' : '')}></i>
                                            </li>
                                        </ul>
                                        {
                                            props.university.rating_user_count > 0
                                            &&
                                                <span className="show_rating">
                                                    <i class="fas fa-user pe-1"></i> {intToString(props.university.rating_user_count)}
                                                </span>
                                        }
                                        </div>
                                    </div>
                                }
                                </EqualHeight>
                               
                                
                                <div className="descrpt">
                                    <p>
                                        {stringLimit(stripTags(props.university.description), props.listView ? 140 : 60)}
                                    </p>
                                </div>
                               
                                <div className="location">
                                   
                                        <div className="left">
                                            <i className="fas fa-map-marker-alt"></i>
                                        </div>
                                        <div className="right">
                                            <p>
                                                {props.university.address}
                                            </p>
                                        </div>
                                    
                                </div>
                                
                                <div className="courses">
                                    <ul>
                                        {renderTags(props.university)}
                                    </ul>
                                </div>
                              
                            </div>
                            <div className="apply">
                                {
                                    applied && applied.length > 0 && applied.includes(props.university.id) ?
                                        <Link to="" className="btn-primary-3" onClick={(e) => { e.preventDefault(); applyNow(props.university.id) }}>Applied</Link>
                                        :
                                        (props.university && <Link to="" className="btn-primary-3" onClick={(e) => { e.preventDefault(); applyNow(props.university.id) }}>Apply now</Link>)

                                }
                            </div>
                            <div className={`wishlist`} style={loading ? { opacity: `0.7` } : {}}>
                                <Link to="" onClick={handleWishlist}><i className={props.university && parseInt(props.university.favourite) === 1 ? `fas fa-bookmark` : `fal fa-bookmark`}></i></Link>
                            </div>
                           
                        </div>
                    </div>
                    <div className="apply">
                        {
                            applied && applied.length > 0 && applied.includes(props.university.id) ?
                                <Link to="" className="btn-primary-3 w-100 " onClick={(e) => { e.preventDefault(); applyNow(props.university.id) }}>Applied</Link>
                                :
                                (props.university && props.university.id && <Link to="" className="btn-primary-3 w-100 " onClick={(e) => { e.preventDefault(); applyNow(props.university.id) }}>Apply now</Link>)
                        }

                    </div>
                </div>
            </div>
        </>
    )
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
});
export default connect(mapStateToProps)(ListingGrids);