import React, { useState } from "react";
import AboutHeading from "../Home/components/aboutheading";
import processleftbg from "../../assets/images/process_left_bg.png"
import processrightbg from "../../assets/images/process_right_bg.png"
import aboutleft from "../../assets/images/about_left.png"
import aboutright from "../../assets/images/about_right.png"
import reviewbg from "../../assets/images/review_bg.png"
import joinstudent from "../../assets/images/join_stu.png"
import aboutbg from "../../assets/images/about_bg.png"
import aboutinnerbg from "../../assets/images/aboutinner_bg.png"
import aboutstu from "../../assets/images/about_stu.png"
import aboutbottombg from "../../assets/images/about_bottom_bg.png"
import abouttopbg from "../../assets/images/about_top_bg.png"
import { Link, withRouter } from "react-router-dom";

function StudentAbout() {
    return (
        <div>
            <div className="cms_banner_area top_space">
                <div className="bg_area ul">
                    <div className="heading_area">
                        <h1>
                            About us
                        </h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
                            purus sit amet luctus venenatis, lectus magna</p>
                    </div>
                </div>
            </div>
            <div className="about_detail_section">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="about_bg_area">
                                <div className="bg_img_area d-lg-block d-none">
                                    <div className="main_img_set">
                                        <img src={aboutbg} />
                                    </div>
                                </div>
                                <div className="center_img_area">
                                    <div className="bg_img">
                                        <img src={aboutinnerbg} />
                                        <div className="image_detail">
                                            <div className="img_set">
                                                <img src={aboutstu} />
                                            </div>
                                        </div>

                                        <div className="bottom_area text-center">
                                            <h1>Lorem ipsum dolor sit amet. </h1>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                                                aliquam, purus sit amet luctus venenatis, lectus magna fringilla
                                                urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing
                                                elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla
                                                urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit
                                                ut aliquam, purus sit amet luctus venenatis.</p>
                                        </div>
                                        <div className="row">
                                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                                <div className="bottom_listing_detail">
                                                    <div className="top_box_detail">
                                                        <div className="img_area">
                                                            <img src={abouttopbg} />
                                                            <div className="info_area">
                                                                <i class="fal fa-university"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="bottom_box_detail">
                                                        <div className="img_area">
                                                            <img src={aboutbottombg} />
                                                            <div className="info_area">
                                                                <h3>2500+</h3>
                                                                <p>Colleges</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                                <div className="bottom_listing_detail">
                                                    <div className="top_box_detail">
                                                        <div className="img_area">
                                                            <img src={abouttopbg} />
                                                            <div className="info_area">
                                                                <i class="fal fa-user-friends"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="bottom_box_detail">
                                                        <div className="img_area">
                                                            <img src={aboutbottombg} />
                                                            <div className="info_area">
                                                                <h3>10k+</h3>
                                                                <p>Students </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                                <div className="bottom_listing_detail">
                                                    <div className="top_box_detail">
                                                        <div className="img_area">
                                                            <img src={abouttopbg} />
                                                            <div className="info_area">
                                                                <i class="fal fa-user"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="bottom_box_detail">
                                                        <div className="img_area">
                                                            <img src={aboutbottombg} />
                                                            <div className="info_area">
                                                                <h3>2000+</h3>
                                                                <p>Agents</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                                <div className="bottom_listing_detail">
                                                    <div className="top_box_detail">
                                                        <div className="img_area">
                                                            <img src={abouttopbg} />
                                                            <div className="info_area">
                                                                <i class="fal fa-thumbs-up"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="bottom_box_detail">
                                                        <div className="img_area">
                                                            <img src={aboutbottombg} />
                                                            <div className="info_area">
                                                                <h3>99%</h3>
                                                                <p>Sucess rate</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="our_mission_section top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="mission_detail_wrap text-center">
                                <h1>Our Mission</h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus
                                    sit amet luctus venenatis, lectus magna fringilla urna, porttitorLorem ipsum
                                    dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus
                                    venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet,
                                    consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="about_student_left_section">
                <div className="banner_img">
                    <div className="bg_image_area d-lg-block d-none">
                        <img src={processleftbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_left_img_detail ">
                                            <div className="image_area">
                                                <img src={aboutleft} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_right_text_detail">
                                            <div className="heading_area el">
                                                <AboutHeading title="For Students" description="Lorem ipsum dolor sit amet, 
                                                consectetur adipiscing elit ut aliquam, purus sit amet luctus 
                                                venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus 
                                                 magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit
                                                  ut aliquam, purus sit amet luctus venenatis. " />
                                            </div>
                                            <div className="right_area">
                                                <Link to='/'> <button className="btn-primary-4" >Read more<i class="far fa-arrow-right"></i></button></Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="about_student_right_section">
                <div className="banner_img">
                    <div className="bg_image_area d-lg-block d-none">
                        <img src={processrightbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_left_text_detail">
                                            <div className="heading_area">
                                                <div className="heading_wrap_set">
                                                    <AboutHeading title="For Agents" description="Lorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                                                  lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur 
                                                  adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                                                  fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                                                  elit ut aliquam, purus sit amet luctus venenatis.Lectus magna fringilla urna, 
                                                  porttitorLorem ipsum dolor" />
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <Link to='/'> <button className="btn-primary-4" >Read more<i class="far fa-arrow-right"></i></button></Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_right_img_detail">
                                            <div className="image_area">
                                                <img src={aboutright} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="join_us_section" style={{
                            backgroundImage: `url(${reviewbg})`
                        }}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="review_area">
                                <div className="bg_img_area">
                                    <div className="main_img_set">
                                        <div className="agent_process_wrap text-center">
                                            <div className="agent_left_text_detail">
                                                <h1>Join us </h1>
                                                <p>Lectus magna fringilla urna, porttitor Lectus magna fringilla urna,
                                                    porttitorLorem ipsum dolor sit amet.</p>
                                                <div className="right_area">
                                                    <Link to='/'> <button className="btn-primary-4">Register now</button></Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="agent_right_img_detail">
                                            <div className="image_area">
                                                <img src={joinstudent} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default StudentAbout;