import React, { useState, useEffect } from "react";
import Select from 'react-select';
import Form from 'react-bootstrap/Form';
import { Link, useHistory } from "react-router-dom";
import Validation from "../../../helpers/vaildation";
import AuthController from "../../../apis/controllers/auth.controller";
import { getStoredItem, removeItem, storeItem } from "../../../helpers/localstorage";
import ActionController from "../../../apis/controllers/action.controller";

function SignUpThree() {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(``);
    const [user, setUser] = useState(null);
    const [states, setStates] = useState([]);


    //validations  -- start
    const [isError, setError] = useState({
        first_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        last_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        email: {
            rules: ["required", "email"],
            isValid: true,
            message: "",
        },
        password: {
            rules: ["required", 'password'],
            isValid: true,
            message: "",
        },
        state_id: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        zipcode: {
            rules: ["numeric", "required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        first_name: null,
        last_name: null,
        email: null,
        password: null,
        state_id: null,
        zipcode: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        setValues(defaultValues);
        init();
    }, []);

    const init = async () => {
        let user = await getStoredItem("user")
        if (user && user.token) {
            setUser(user);
            getSingupStepThree()
        }
        else {
            history.push('/');
        }
    }

    const getSingupStepThree = async () => {
        let response = await new ActionController().getSingupStepThree();
        if (response.status) {
            setStates(response.listing)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    const handleStates = (field, value) => {

        /** Validate each field on change */
        let node = validation.validateField(field, value);
        console.log(node, 'node data')
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };

    const handleEmail = async (e) => {
        handleStates(
            "email",
            e.target.value
        );
        let node = await validation.emailExist('email', e.target.value);
        console.log(node);
        setError({ ...isError, email: node });
    }

    //validations  -- end

    //signup  --start
    const signupFinalStep = async () => {
        if (isLoading === false) {
            setErrMsg(``);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            let node = await validation.emailExist('email', values.email);
            setError({ ...isValid.errors, email: node });
            if (isValid && !isValid.haveError && node.isValid) {
                setIsLoading(true);
                let response = await new AuthController().studentSignupFinalStep(user.token, values);
                setIsLoading(false);
                if (response && response.status) {
                    new AuthController().setUpLogin(response.user);
                    setValues(defaultValues);
                    removeItem(`user`);
                    history.push('/signup/success');
                } else {
                    setErrMsg(response.message);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //signup --end

    return (
        <>
            <div className="Sign_up_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form3">
                                <div className="heading_area">
                                    <h1>
                                        Sign up
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>
                                <div className="bar_wrap">
                                    <div className="top_bar">
                                        <div className="line_bar">
                                            <div className="bar_inn" style={{ width: "100%" }}></div>
                                            <div className="bar_wrap">
                                                <div className="bar_circle circle_1"><p>1</p></div>
                                                <div className="bar_circle circle_2"><p>2</p></div>
                                                <div className="bar_circle circle_3"><p>3</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="heading_area ul">
                                    <h2>
                                        Please enter your details
                                    </h2>
                                </div>
                                <Form
                                    noValidate
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        signupFinalStep();
                                    }}
                                >
                                    <div className="input_fields_area">
                                        <div className="row">
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                                                <div className="input_fields_area">
                                                    <div className="row">
                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                            <div className="expect_education_area  ul">
                                                                <div className="form_search">
                                                                    <label className={
                                                                        !isError
                                                                            .first_name
                                                                            .isValid
                                                                            ? "valid-box form_controls form-label"
                                                                            : "form_controls form-label"
                                                                    }>First name</label>
                                                                    <input type="text" placeholder="First name" className="form-control"
                                                                        onChange={(
                                                                            e
                                                                        ) => {
                                                                            handleStates(
                                                                                "first_name",
                                                                                e.target
                                                                                    .value);
                                                                        }}
                                                                        value={
                                                                            values.first_name
                                                                                ? values.first_name
                                                                                : ``
                                                                        } />
                                                                    {isError.first_name
                                                                        .message && !isError
                                                                            .first_name
                                                                            .isValid? (
                                                                        <p className="valid-helper">
                                                                            {
                                                                                isError
                                                                                    .first_name
                                                                                    .message
                                                                            }
                                                                        </p>
                                                                    ) : null}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                            <div className="expect_education_area  ul">
                                                                <div className="form_search" >
                                                                    <label
                                                                        className={
                                                                            !isError.last_name.isValid
                                                                                ? "valid-box form_controls form-label"
                                                                                : "form_controls form-label"
                                                                        }>Last name</label>
                                                                    <input type="text" placeholder="Last name" className="form-control"
                                                                        onChange={(e) => {
                                                                            handleStates(
                                                                                "last_name",
                                                                                e.target.value
                                                                            );
                                                                        }}
                                                                        value={
                                                                            values.last_name
                                                                                ? values.last_name
                                                                                : ``
                                                                        } />
                                                                    {isError.last_name.message && !isError.last_name.isValid ? (
                                                                        <p className="valid-helper">
                                                                            {isError.last_name.message}
                                                                        </p>
                                                                    ) : null}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="expect_education_area  ul">
                                                                <div className="form_search">
                                                                    <label className={
                                                                        !isError.email
                                                                            .isValid
                                                                            ? "valid-box form_controls  form-label"
                                                                            : "form_controls  form-label"
                                                                    }

                                                                    >Email address</label>
                                                                    <input type="email" placeholder="Email address" className="form-control"
                                                                        onChange={handleEmail}
                                                                        value={
                                                                            values.email
                                                                                ? values.email
                                                                                : ``
                                                                        } />
                                                                    {isError.email
                                                                        .message && !isError.email
                                                                            .isValid ? (
                                                                        <p className="valid-helper">
                                                                            {
                                                                                isError
                                                                                    .email
                                                                                    .message
                                                                            }
                                                                        </p>
                                                                    ) : null}

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="expect_education_area  ul">
                                                                <div className="form_search">
                                                                    <label className={
                                                                        !isError.password
                                                                            .isValid
                                                                            ? "valid-box form_controls  form-label"
                                                                            : "form_controls  form-label"
                                                                    }

                                                                    >Password</label>
                                                                    <input type="password" placeholder="Password" className="form-control"
                                                                        onChange={(
                                                                            e
                                                                        ) => {
                                                                            handleStates(
                                                                                "password",
                                                                                e.target
                                                                                    .value
                                                                            );
                                                                        }}
                                                                        value={
                                                                            values.password
                                                                                ? values.password
                                                                                : ``
                                                                        } />
                                                                    {isError.email
                                                                        .message && !isError.password
                                                                            .isValid ? (
                                                                        <p className="valid-helper">
                                                                            {
                                                                                isError
                                                                                    .password
                                                                                    .message
                                                                            }
                                                                        </p>
                                                                    ) : null}

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="select_area">
                                                                <div className="graduation_area ul ">
                                                                    <Form.Label>State</Form.Label>
                                                                    <Select
                                                                        value={values.state_id}
                                                                        onChange={(item) => {
                                                                            handleStates(
                                                                                "state_id",
                                                                                item
                                                                            );
                                                                        }}
                                                                        options={states}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="expect_education_area ul">
                                                                <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                    <Form.Label>Pin code</Form.Label>
                                                                    <Form.Control
                                                                        type="text"
                                                                        placeholder="Pincode"
                                                                        value={values.zipcode}
                                                                        onChange={(e) => {
                                                                            handleStates(
                                                                                "zipcode",
                                                                                e.target.value
                                                                            );
                                                                        }}
                                                                    />
                                                                </Form.Group>
                                                            </div>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="btn_area">
                                                                <div className="left_area">
                                                                    <button className="btn-primary-4"
                                                                        type="submit"
                                                                        onClick={() => history.push('/signup/step-2')}
                                                                    >Back</button>
                                                                </div>
                                                                <div className="right_area">
                                                                    {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                                    <button
                                                                        className="btn-primary-2"
                                                                        type="submit">
                                                                        {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                                        &nbsp; Submit
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default SignUpThree;