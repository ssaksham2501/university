import React, { useState, useEffect } from "react";
import Select from 'react-select';
import Form from 'react-bootstrap/Form';
import { Link, useHistory } from "react-router-dom";
import ActionController from "../../../apis/controllers/action.controller";
import AuthController from "../../../apis/controllers/auth.controller";
import Validation from "../../../helpers/vaildation";
import { getStoredItem, storeItem } from "../../../helpers/localstorage";
import { useIsRTL } from "react-bootstrap/esm/ThemeProvider";
import Selectable from "../../Layout/components/Selectable";

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];


function SignUpTwo() {
    const history = useHistory();
    const [selectedOption, setSelectedOption] = useState(null)
    const [educationList, setEducationList] = useState(null);
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [years, setYears] = useState([]);
    const handleChange = selectedOption => console.log(selectedOption);


    //validations  -- start
    const [isError, setError] = useState({
        education_id: {
            rules: ["required", "numeric"],
            isValid: true,
            message: "",
        },
        gained_percentage: {
            rules: ["required", "decimel", "range:0:100"],
            isValid: true,
            message: "",
        },
        year_of_graduation: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        ielts: {
            rules: [],
            isValid: true,
            message: "",
        },
        pte: {
            rules: [],
            isValid: true,
            message: "",
        },
        tofel: {
            rules: [],
            isValid: true,
            message: "",
        },
        ielts_score: {
            rules: ["decimel", "range:0:9"],
            isValid: true,
            message: "",
        },
        tofel_score: {
            rules: ["numeric", "range:0:120"],
            isValid: true,
            message: "",
        },
        pte_score: {
            rules: ["numeric", "range:0:90"],
            isValid: true,
            message: "",
        }
    });

    let validation = new Validation(isError);

    let defaultValues = {
        education_id: ``,
        gained_percentage: ``,
        year_of_graduation: ``,
        tofel: `0`,
        ielts: `0`,
        pte: `0`,
        ielts_score: ``,
        tofel_score: ``,
        pte_score: ``,
    };

    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        generateArrayOfYears();
        init();
    }, []);

    const init = async () => {
        let user = await getStoredItem("user")
        if (user && user.token) {
            setUser(user);
            getSingupStepTwo()
            setValues({
                education_id: user && user.education_id  ? user.education_id : ``,
                gained_percentage: user && user.gained_percentage ? user.gained_percentage : ``,
                year_of_graduation: user && user.year_of_graduation ? { label: user.year_of_graduation, value: user.year_of_graduation } : ``,
                tofel: user && user.tofel == `1` ? `1` : `0`,
                ielts: user && user.ielts == `1` ? `1` : `0`,
                pte: user && user.pte == `1` ? `1` : `0`,
                ielts_score: user && user.ielts_score && user.ielts_score > 0 ? user.ielts_score : ``,
                tofel_score: user && user.tofel_score && user.tofel_score > 0 ? user.tofel_score : ``,
                pte_score: user && user.pte_score && user.pte_score > 0 ? user.pte_score : ``,
            });
        }
        else {
            history.push('/');
        }
    }

    const generateArrayOfYears = () => {
        var max = new Date().getFullYear();
        var min = max - 60;
        var years = [];

        for (var i = min; i <= max; i++) {
            years.push({ ...i, value: i, label: i.toString() });
        }
        years.reverse();
        setYears(years);
    };

    const getSingupStepTwo = async () => {
        let response = await new ActionController().getSingupStepTwo();
        if (response.status) {
            setEducationList(response.listing)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    //signup  --start
    const signupSecondStep = async () => {
        if (isLoading === false) {
            setErrMsg(``);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new AuthController().studentSignupSecondStep(user.token, values);
                console.log(response);
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    storeItem(`user`, response.user);
                    history.push('/signup/step-3')

                } else {
                    setErrMsg(response.message);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //signup --end

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        console.log(node, 'node data')
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };

    return (
        <>
            <div className="Sign_up_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form2">
                                <div className="heading_area">
                                    <h1>
                                        Sign up
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>
                                <div className="bar_wrap">
                                    <div className="top_bar">
                                        <div className="line_bar">
                                            <div className="bar_inn" style={{ width: "50%" }}></div>
                                            <div className="bar_wrap">
                                                <div className="bar_circle circle_1"><p>1</p></div>
                                                <div className="bar_circle circle_2"><p>2</p></div>
                                                <div className="bar_circle circle_3"><p>3</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Form
                                    noValidate
                                    onSubmit={(e) => {
                                        e.preventDefault()
                                        signupSecondStep();
                                    }}
                                >
                                    <div className="input_fields_area">
                                        <div className="high_education_area">
                                            <h2>What’s your highest level of education?</h2>
                                            <div className='row'>
                                                {
                                                    educationList !== null
                                                    &&
                                                    educationList.map((item, index) => {
                                                        return <div key={`education` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <div className='radio_area'>
                                                                <Form.Check
                                                                    label={item.name}
                                                                    name="group1"
                                                                    type={`radio`}
                                                                    id={'education' + index}
                                                                    checked={item.id == values.education_id ? true : false}
                                                                    onChange={() => {
                                                                        handleStates(
                                                                            "education_id",
                                                                            item.id

                                                                        );
                                                                    }}
                                                                    value={item.id}
                                                                />
                                                            </div>
                                                        </div>
                                                    })
                                                }
                                            </div>
                                            {!isError.education_id.isValid && isError.education_id
                                                .message ? (
                                                <p className="valid-helper">
                                                    {
                                                        isError
                                                            .education_id
                                                            .message
                                                    }
                                                </p>
                                            ) : null}
                                        </div>
                                        <div className="expect_education_area">
                                            <h2>
                                                Expected or gained percentage
                                            </h2>
                                            <div className="form_search">
                                                <input type="numeric" placeholder="80%" className="form-control"
                                                    onChange={(
                                                        e
                                                    ) => {
                                                        handleStates(
                                                            "gained_percentage",
                                                            e.target
                                                                .value
                                                        );
                                                    }}
                                                    value={
                                                        values.gained_percentage
                                                            ? values.gained_percentage
                                                            : ``
                                                    } />
                                                {isError.gained_percentage
                                                    .message ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError
                                                                .gained_percentage
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}

                                            </div>
                                            {/* <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                <Form.Control type="numeric" placeholder="Grades" />
                                            </Form.Group> */}
                                        </div>
                                        <div className="select_area">
                                            <div className="graduation_area">
                                                <h2>Year of graduation of your highest education</h2>
                                                <Selectable
                                                    value={values.year_of_graduation}
                                                    onChange={(item) => handleStates(
                                                        "year_of_graduation",
                                                        item
                                                    )}
                                                    options={years} />
                                                {!isError.year_of_graduation.isValid && isError.year_of_graduation
                                                    .message ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError
                                                                .year_of_graduation
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}
                                            </div>
                                            <div className="graduation_area">
                                                <h2>Status of your IELTS/ TOEFL/PTE exam</h2>
                                                {/* <Select
                                                value={selectedOption}
                                                onChange={handleChange}
                                                options={options}
                                            /> */}

                                                <div className="high_education_area">
                                                    <div className='row'>
                                                        <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <Form.Check
                                                                label="IELTS"
                                                                name="status"
                                                                type={'checkbox'}
                                                                id={'education'}
                                                                checked={values.ielts === `1` ? true : false}
                                                                onChange={() => handleStates(
                                                                    "ielts",
                                                                    values.ielts == `1` ? `0` : `1`
                                                                )}
                                                            />
                                                        </div>
                                                        <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <Form.Check
                                                                label="TOFEL"
                                                                name="status"
                                                                type={'checkbox'}
                                                                id={'educationlorem'}
                                                                checked={values.tofel === `1` ? true : false}
                                                                onChange={() => handleStates(
                                                                    "tofel",
                                                                    values.tofel == `1` ? `0` : `1`
                                                                )}
                                                            />
                                                        </div>
                                                        <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <Form.Check
                                                                label="PTE"
                                                                name="status"
                                                                type={'checkbox'}
                                                                id={'educationPTE'}
                                                                checked={values.pte === `1` ? true : false}
                                                                onChange={() => handleStates(
                                                                    "pte",
                                                                    values.pte == `1` ? `0` : `1`
                                                                )}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {
                                                values.ielts === `1`
                                                &&
                                                <div className="expect_education_area">
                                                    <h2>Overall IELTS Bands</h2>
                                                    <div className="form_search">
                                                        <input type="numeric" placeholder="0 - 9" className="form-control"
                                                            // value={values.ielts_score}
                                                            onChange={(
                                                                e
                                                            ) => {
                                                                handleStates(
                                                                    "ielts_score",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={
                                                                values.ielts_score && values.ielts_score > 0
                                                                    ? values.ielts_score
                                                                    : ``
                                                            } />
                                                        {!isError.ielts_score.isValid && isError.ielts_score
                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .ielts_score
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}

                                                    </div>
                                                </div>
                                            }
                                            {
                                                values.tofel === `1`
                                                &&
                                                <div className="expect_education_area">
                                                    <h2>Overall TOFEL iBT Scores</h2>
                                                    <div className="form_search">
                                                        <input type="numeric" placeholder="0 - 120" className="form-control"
                                                            // value={values.tofel_score}
                                                            onChange={(
                                                                e
                                                            ) => {
                                                                handleStates(
                                                                    "tofel_score",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={
                                                                values.tofel_score && values.tofel_score > 0
                                                                    ? values.tofel_score
                                                                    : ``
                                                            } />
                                                        {!isError.tofel_score.isValid && isError.tofel_score
                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .tofel_score
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}

                                                    </div>
                                                </div>
                                            }
                                            {
                                                values.pte === `1`
                                                &&
                                                <div className="expect_education_area">
                                                    <h2>Overall PTE Scores</h2>
                                                    <div className="form_search">
                                                        <input type="numeric" placeholder="0 - 90" className="form-control"
                                                            // value={values.pte_score}
                                                            onChange={(
                                                                e
                                                            ) => {
                                                                handleStates(
                                                                    "pte_score",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={
                                                                values.pte_score && values.pte_score > 0
                                                                    ? values.pte_score
                                                                    : ``
                                                            } />
                                                        {!isError.pte_score.isValid && isError.pte_score
                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .pte_score
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}

                                                    </div>
                                                </div>
                                            }
                                        </div>
                                        <div className="btn_area">
                                            <div className="left_area">
                                                <button className="btn-primary-4"
                                                    type="submit"
                                                    onClick={() => history.push('/signup')}
                                                >Back</button>
                                            </div>
                                            <div className="right_area">
                                                {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                <button className="btn-primary-2"
                                                    type="submit"
                                                // onClick={() => history.push('/signup/step-3')}
                                                > {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                    &nbsp; Next</button>

                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

}
export default SignUpTwo;


