import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Form from 'react-bootstrap/Form';
import AuthController from "../../../apis/controllers/auth.controller";
import ActionController from "../../../apis/controllers/action.controller";
import Validation from "../../../helpers/vaildation";
import { getFlagIcon } from "../../../helpers/General";
import { getStoredItem, storeItem } from "../../../helpers/localstorage";

function SignUpOne() {
    const history = useHistory();
    const [countrylist, setCountryList] = useState(null);
    const [profession, setProfession] = useState(null);
    const [preferintake, setPreferIntake] = useState(null);
    const [mounting, setMounting] = useState(true);
    const [maxCountries, setMaxCountries] = useState(5);
    const [selectedCountries, setSelectedCountries] = useState([]);
    const [selectedStream, setSelectedStream] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(``);
    const [user, setUser] = useState(null);
    const [streamSearch, setStreamSearch] = useState(``);
    const [streamSearch2, setStreamSearch2] = useState(``);
    //validations  -- start
    const [isError, setError] = useState({
        country: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        resumption_semster: {
            rules: ["required", "numeric"],
            isValid: true,
            message: "",
        },
        stream_id: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });

    let validation = new Validation(isError);

    let defaultValues = {
        country: [],
        resumption_semster: ``,
        stream_id: [],
    };

    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        init();
    }, []);

    const init = async () => {
        let user = await getStoredItem("user")
        if (user && user.token) {
            setUser(user);
            getSingupStepOne()
            setValues({
                country: user && user.countries && user.countries.length > 0 ? user.countries : [],
                resumption_semster: user && user.resumption_semster ? user.resumption_semster : null,
                stream_id: user && user.streams && user.streams.length > 0 ? user.streams : [],
            });
        }
        else {
            history.push('/');
        }
    }

    const getSingupStepOne = async () => {
        setMounting(true);
        let response = await new ActionController().getSingupStepOne();
        if (response.status) {
            setCountryList(response.country)
            setProfession(response.streams)
            setPreferIntake(response.preferredintake)
            setMounting(false);
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    //signup  --start
    const signupFirstStep = async () => {
        if (isLoading === false) {
            setErrMsg(``);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new AuthController().studentSignup(user.token, values);
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    storeItem(`user`, response.user);
                    history.push('/signup/step-2')

                } else {
                    setErrMsg(response.message);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //signup --end

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        console.log(node, 'node data')
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    }

    const handleCountrySelect = (country) => {
        let s = values.country ? values.country : [];
        let index = s.indexOf(country.id);
        
        if (index > -1) {
            s.splice(index, 1);
        }
        else {
            s.push(country.id);
        }
        handleStates(
            "country",
            s
        );
    }
    

    return (
        <>

            <div className="Sign_up_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap">
                                <div className="heading_area">
                                    <h1>
                                        Sign up
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>

                                {mounting
                                    ?
                                    <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                    :
                                    <>
                                        <div className="bar_wrap">
                                            <div className="top_bar">
                                                <div className="line_bar">
                                                    <div className="bar_inn" style={{ width: "0%" }}></div>
                                                    <div className="bar_wrap">
                                                        <div className="bar_circle circle_1"><p>1</p></div>
                                                        <div className="bar_circle circle_2"><p>2</p></div>
                                                        <div className="bar_circle circle_3"><p>3</p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <Form
                                            noValidate
                                            onSubmit={(e) => {
                                                e.preventDefault()
                                                signupFirstStep();
                                            }}
                                        >
                                            <div className="main">
                                                <div className="input_fields_area">
                                                    <div className="high_education_area md ul">
                                                        <h2>Choose your dream country </h2>
                                                        {
                                                        countrylist && countrylist.length > 6 && maxCountries < 1
                                                        &&
                                                        <div className="serach_area">
                                                            <Form.Group className="form-group">
                                                                <input 
                                                                    className="form-control"
                                                                    type="text"
                                                                    autoFocus={true}
                                                                    id="county-search"
                                                                    placeholder="Search"
                                                                    value={streamSearch}
                                                                    onChange={(e) => {
                                                                        setStreamSearch(e.target.value);
                                                                    }}
                                                                />
                                                                <div className="seacrh">
                                                                    {
                                                                        streamSearch && streamSearch.trim() ?
                                                                            <a
                                                                                href=""
                                                                                onClick={(e) => {
                                                                                    e.preventDefault();
                                                                                    setStreamSearch(``);
                                                                                }}
                                                                            ><i className="fal fa-times"></i></a>
                                                                        :
                                                                            <a
                                                                                href=""
                                                                                onClick={(e) => {
                                                                                    e.preventDefault();
                                                                                    document.getElementById("county-search").focus();
                                                                                }}
                                                                            ><i className="fal fa-search"></i></a>
                                                                    }
                                                                </div>
                                                            </Form.Group>
                                                        </div>
                                                    }
                                                        <div className="country_shoed">

                                                            <div className='row'>
                                                                {countrylist !== null && countrylist.filter((item, i) => i < (countrylist.length > 6 && maxCountries > 0 ? maxCountries : countrylist.length) && ( item.name && item.name.toLowerCase().indexOf(streamSearch.toLowerCase()) > -1)).map((country, index) => {
                                                                    return <div key={`countrylist` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                        <div className='radio_area'>
                                                                            <Form.Check
                                                                                label={country.name}
                                                                                name="country"
                                                                                type={`checkbox`}
                                                                                id={'country' + index}
                                                                                checked={values.country.includes(country.id) ? true : false}
                                                                                onChange={() => handleCountrySelect(country)}
                                                                                value={country.id}
                                                                            />

                                                                             <label
                                                                                htmlFor={'country' + index}
                                                                                className="image_area"
                                                                            >
                                                                                <img src={getFlagIcon(country.short_name)} />
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                })
                                                                }
                                                                {
                                                                    countrylist.length > 6 && maxCountries > 0
                                                                    &&
                                                                    <div key={`countrylist-more`} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                        <div className='radio_area'>
                                                                            <Form.Check
                                                                                label={(countrylist.length - 5) + ' more'}
                                                                                name="country"
                                                                                type={`radio`}
                                                                                id={'country-more'}
                                                                                checked={false}
                                                                            />

                                                                            <div
                                                                                className="image_area"
                                                                                onClick={() => {
                                                                                    setMaxCountries(0);
                                                                                }}
                                                                            >
                                                                                <i style={{ fontSize: '50px' }} className="fas fa-chevron-square-down"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!isError.country.isValid && isError.country
                                                        .message ? (
                                                        <p className="valid-helper">
                                                            {
                                                                isError
                                                                    .country
                                                                    .message
                                                            }
                                                        </p>
                                                    ) : null}
                                                </div>
                                                <div className="high_education_area">
                                                    <h2>What is your preferred intake?</h2>
                                                    <div className='row'>
                                                        {
                                                            preferintake !== null
                                                            &&
                                                            preferintake.map((item, index) => {
                                                                return <div key={`preferintake` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                    <Form.Check
                                                                        label={`${item.title} / ${item.year}`}
                                                                        name="preferintake"
                                                                        type={`radio`}
                                                                        id={'preferintake' + index}
                                                                        checked={values.resumption_semster === item.id ? true : false}
                                                                        onChange={() => {
                                                                            handleStates(
                                                                                "resumption_semster",
                                                                                item.id

                                                                            );
                                                                        }}
                                                                        value={item.id}
                                                                    />
                                                                </div>
                                                            })
                                                        }
                                                    </div>
                                                    {!isError.resumption_semster.isValid && isError.resumption_semster

                                                        .message ? (
                                                        <p className="valid-helper">
                                                            {
                                                                isError
                                                                    .resumption_semster
                                                                    .message
                                                            }
                                                        </p>
                                                    ) : null}
                                                </div>
                                                <div className="high_education_area">
                                                    <h2>What do you wish to pursue</h2>
                                                    <div className="serach_area">
                                                        <Form.Group className="form-group">
                                                            <input className="form-control" type="text" id="stream-search" placeholder="Search" onChange={(e) => {
                                                                setStreamSearch2(e.target.value);
                                                            }} />
                                                            <div className="seacrh">
                                                            {
                                                                streamSearch2 && streamSearch2.trim() ?
                                                                    <a
                                                                        href=""
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                            setStreamSearch2(``);
                                                                        }}
                                                                    ><i className="fal fa-times"></i></a>
                                                                :
                                                                    <a
                                                                        href=""
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                            document.getElementById("stream-search").focus();
                                                                        }}
                                                                    ><i className="fal fa-search"></i></a>
                                                            }
                                                            </div>
                                                        </Form.Group>
                                                    </div>
                                                    <div className="country_shoed">
                                                        <div className='row'>
                                                            {
                                                                profession && profession.length > 0 && profession.filter((item) => (item.name.toLowerCase().indexOf(streamSearch2.toLowerCase()) > -1)).map((stream, index) => {
                                                                    return <div key={`profession` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                        <Form.Check
                                                                            label={stream.name}
                                                                            name="stream"
                                                                            type={`checkbox`}
                                                                            id={'profession' + index}
                                                                            checked={values.stream_id.includes(stream.id) ? true : false}
                                                                            onChange={(e) => {
                                                                                let s = values.stream_id;
                                                                                s.push(stream.id);
                                                                                handleStates(
                                                                                    "stream_id",
                                                                                    s
                                                                                );
                                                                            }}
                                                                            value={stream.id}
                                                                        />
                                                                    </div>
                                                                })
                                                            }
                                                        </div>
                                                        {!isError.stream_id.isValid && isError.stream_id

                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .stream_id
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="btn_area ul">
                                                    {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                    <div className="center_area">
                                                        <button className="btn-primary-2"
                                                            type="submit"
                                                        // onClick={() => history.push('/signup/step-2')}  
                                                        > {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                            &nbsp; Next</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </Form>
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

}
export default SignUpOne;