import React, { useState, useEffect } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import AuthController from "../../../apis/controllers/auth.controller";

function SignUpSuccess() {
    const location = useLocation();
    const history = useHistory();
    const [mounting, setMounting] = useState(true);

    useEffect(() => {
        init()
    }, []);

    const init = async () => {
        let response = await new AuthController().checkLoginState();
        if (!response.status) {
            new AuthController().setUpLogin({});
            history.push('/login');
        }
        else if (response.status && response.user_type === 'agent' && response.agent_approved) {
            history.push('/');
        }
        setMounting(false);
    }


    return (
        <>
            <div className="Sign_up_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form2">
                                {
                                    mounting
                                    ?
                                        <div className="heading_area text-center">
                                            <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                        </div>
                                    :
                                        <div className="heading_area text-center">
                                            {
                                                location.pathname.includes('awaiting-approval')
                                                    ?
                                                    <>
                                                        <p className="success-circle text-warning"><i className="fas fa-exclamation-circle"></i></p>
                                                        <h1>
                                                            Under Verification
                                                        </h1>
                                                        <p>Thank you for registration with us. We are verifiying your profile and accredations. We will get a respond you though email once you profile is approved and ready to use. This would take around 15 minutes to 1 hour.</p>
                                                        <p style={{ padding: '40px 110px' }}>
                                                            <Link to={'/knowledge-base'} className="btn btn-primary-2 float-start">Have a Question?</Link>
                                                            <Link to={'/my-profile'} className="btn btn-primary-2 float-end">Contact Support</Link>
                                                        </p>
                                                    </>
                                                    :
                                                    <>
                                                        <p className="success-circle"><i className="fa fas fa-check-circle"></i></p>
                                                        <h1>
                                                            Congratulation!
                                                        </h1>
                                                        <p>Thank you for registration with us. You are important to us. Please explore the other areas of website and find the suitable agent or university for you.</p>
                                                        <p style={{ padding: '40px 110px' }}>
                                                            <Link to={'/colleges'} className="btn btn-primary-2 float-start">Explore Colleges</Link>
                                                            <Link to={'/my-profile'} className="btn btn-primary-2 float-end">Go to Profile</Link>
                                                        </p>
                                                    </>
                                            }
                                        
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

}
export default SignUpSuccess;


