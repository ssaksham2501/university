import React from "react";
import Form from 'react-bootstrap/Form';
import { Link, useHistory } from "react-router-dom";
import StudentEditPreferenceTwoFun from "../../functions/Students/StudentEditPreferenceTwoFun";
import Selectable from "../Layout/components/Selectable";

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];


function SignEditPreferenceStepTwo(props) {
    const history = useHistory();

    const {
        StudentEditTwo,
        mounting,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        handleStates,
        educationList,
        setEducationList,
        years,
        setYears
    } = StudentEditPreferenceTwoFun(props);


    return (
        <>
            <div className="Sign_up_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form2">
                                <div className="heading_area">
                                    <h1>
                                        Sign up
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>
                                <div className="bar_wrap">
                                    <div className="top_bar">
                                        <div className="line_bar">
                                            <div className="bar_inn" style={{ width: "100%" }}></div>
                                            <div className="bar_wrap">
                                                <div className="bar_circle circle_1"><p>1</p></div>
                                                <div className="bar_circle circle_2"><p>2</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Form
                                    noValidate
                                    onSubmit={(e) => {
                                        e.preventDefault()
                                        if (isLoading === false) {
                                            StudentEditTwo.editPreferences();
                                        }
                                    }}
                                >
                                    <div className="input_fields_area">
                                        <div className="high_education_area">
                                            <h2>What’s your highest level of education?</h2>
                                            <div className='row'>
                                                {
                                                    educationList !== null
                                                    &&
                                                    educationList.map((item, index) => {
                                                        return <div key={`education` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <div className='radio_area'>
                                                                <Form.Check
                                                                    label={item.name}
                                                                    name="group1"
                                                                    type={`radio`}
                                                                    id={'education' + index}
                                                                    checked={item.id == values.education_id ? true : false}
                                                                    onChange={() => {
                                                                        handleStates(
                                                                            "education_id",
                                                                            item.id

                                                                        );
                                                                    }}
                                                                    value={item.id}
                                                                />
                                                            </div>
                                                        </div>
                                                    })
                                                }
                                            </div>
                                            {!isError.education_id.isValid && isError.education_id
                                                .message ? (
                                                <p className="valid-helper">
                                                    {
                                                        isError
                                                            .education_id
                                                            .message
                                                    }
                                                </p>
                                            ) : null}
                                        </div>
                                        <div className="expect_education_area">
                                            <h2>
                                                Expected or gained percentage
                                            </h2>
                                            <div className="form_search">
                                                <input type="numeric" placeholder="80%" className="form-control"
                                                    onChange={(
                                                        e
                                                    ) => {
                                                        handleStates(
                                                            "gained_percentage",
                                                            e.target
                                                                .value
                                                        );
                                                    }}
                                                    value={
                                                        values.gained_percentage
                                                            ? values.gained_percentage
                                                            : ``
                                                    } />
                                                {isError.gained_percentage
                                                    .message ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError
                                                                .gained_percentage
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}

                                            </div>
                                            {/* <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                <Form.Control type="numeric" placeholder="Grades" />
                                            </Form.Group> */}
                                        </div>
                                        <div className="select_area">
                                            <div className="graduation_area">
                                                <h2>Year of graduation of your highest education</h2>
                                                <Selectable
                                                    value={years.find((item) => item.value == values.year_of_graduation)}
                                                    onChange={(item) => handleStates(
                                                        "year_of_graduation",
                                                        item.value
                                                    )}
                                                    options={years} />
                                                {!isError.year_of_graduation.isValid && isError.year_of_graduation
                                                    .message ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError
                                                                .year_of_graduation
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}
                                            </div>
                                            <div className="graduation_area">
                                                <h2>Status of your IELTS/ TOEFL/PTE exam</h2>
                                                {/* <Select
                                                value={selectedOption}
                                                onChange={handleChange}
                                                options={options}
                                            /> */}

                                                <div className="high_education_area">
                                                    <div className='row'>
                                                        <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <Form.Check
                                                                label="IELTS"
                                                                name="status"
                                                                type={'checkbox'}
                                                                id={'education'}
                                                                checked={parseInt(values.ielts) === 1 ? true : false}
                                                                onChange={() => handleStates(
                                                                    "ielts",
                                                                    values.ielts == 1 ? 0 : 1
                                                                )}
                                                            />
                                                        </div>
                                                        <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <Form.Check
                                                                label="TOFEL"
                                                                name="status"
                                                                type={'checkbox'}
                                                                id={'educationlorem'}
                                                                checked={parseInt(values.tofel) === 1 ? true : false}
                                                                onChange={() => handleStates(
                                                                    "tofel",
                                                                    values.tofel == 1 ? 0 : 1
                                                                )}
                                                            />
                                                        </div>
                                                        <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                            <Form.Check
                                                                label="PTE"
                                                                name="status"
                                                                type={'checkbox'}
                                                                id={'educationPTE'}
                                                                checked={parseInt(values.pte) === 1 ? true : false}
                                                                onChange={() => handleStates(
                                                                    "pte",
                                                                    values.pte == 1 ? 0 : 1
                                                                )}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {
                                                parseInt(values.ielts) === 1
                                                &&
                                                <div className="expect_education_area">
                                                    <h2>Overall IELTS Bands</h2>
                                                    <div className="form_search">
                                                        <input type="numeric" placeholder="0 - 9" className="form-control"
                                                            // value={values.ielts_score}
                                                            onChange={(
                                                                e
                                                            ) => {
                                                                handleStates(
                                                                    "ielts_score",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={
                                                                values.ielts_score && values.ielts_score > 0
                                                                    ? values.ielts_score
                                                                    : ``
                                                            } />
                                                        {isError.ielts_score
                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .ielts_score
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}

                                                    </div>
                                                </div>
                                            }
                                            {
                                                parseInt(values.tofel) === 1
                                                &&
                                                <div className="expect_education_area">
                                                    <h2>Overall TOFEL iBT Scores</h2>
                                                    <div className="form_search">
                                                        <input type="numeric" placeholder="0 - 120" className="form-control"
                                                            // value={values.tofel_score}
                                                            onChange={(
                                                                e
                                                            ) => {
                                                                handleStates(
                                                                    "tofel_score",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={
                                                                values.tofel_score && values.tofel_score > 0
                                                                    ? values.tofel_score
                                                                    : ``
                                                            } />
                                                        {isError.tofel_score
                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .tofel_score
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}

                                                    </div>
                                                </div>
                                            }
                                            {
                                                parseInt(values.pte) === 1
                                                &&
                                                <div className="expect_education_area">
                                                    <h2>Overall PTE Scores</h2>
                                                    <div className="form_search">
                                                        <input type="numeric" placeholder="0 - 90" className="form-control"
                                                            // value={values.pte_score}
                                                            onChange={(
                                                                e
                                                            ) => {
                                                                handleStates(
                                                                    "pte_score",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={
                                                                values.pte_score && values.pte_score > 0
                                                                    ? values.pte_score
                                                                    : ``
                                                            } />
                                                        {isError.pte_score
                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .pte_score
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}

                                                    </div>
                                                </div>
                                            }
                                        </div>
                                        <div className="btn_area">
                                            <div className="left_area">
                                                <Link to='/student-change-preference-step-1'><button className="btn-primary-4" >Back</button></Link>
                                            </div>
                                            <div className="right_area">
                                                {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                <button className="btn-primary-2"
                                                    type="submit"
                                                // onClick={() => history.push('/signup/step-3')}
                                                > {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                    &nbsp; Save changes</button>

                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

}
export default SignEditPreferenceStepTwo;


