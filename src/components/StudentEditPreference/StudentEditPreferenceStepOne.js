import React, { useState } from "react";
import Form from 'react-bootstrap/Form';
import { Link, useHistory } from "react-router-dom";
import StudentEditPreferenceFun from "../../functions/Students/StudentEditPreferenceOneFun";
import { getFlagIcon } from "../../helpers/General";

function SignEditPreferenceStepOne(props) {
    const history = useHistory();
    const [maxCountries, setMaxCountries] = useState(5);
    const {
        StudentEdit,
        mounting,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        handleStates,
        states,
        countrylist,
        profession,
        setProfession,
        preferintake,
        setPreferIntake
    } = StudentEditPreferenceFun(props);

    const [streamSearch, setStreamSearch] = useState(``);

    const [streamSearch2, setStreamSearch2] = useState(``);

    return (
        <>

            <div className="Sign_up_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap">
                                <div className="heading_area">
                                    <h1>
                                        Edit Preferences
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>

                                {mounting
                                    ?
                                    <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                    :
                                    <>
                                        <div className="bar_wrap">
                                            <div className="top_bar">
                                                <div className="line_bar">
                                                    <div className="bar_inn" style={{ width: "0%" }}></div>
                                                    <div className="bar_wrap">
                                                        <div className="bar_circle circle_1"><p>1</p></div>
                                                        <div className="bar_circle circle_2"><p>2</p></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <Form
                                            noValidate
                                            onSubmit={(e) => {
                                                e.preventDefault()
                                                if (isLoading === false) {
                                                    StudentEdit.editPreferences();
                                                }
                                            }}
                                        >
                                            <div className="main">
                                                <div className="input_fields_area">
                                                    <div className="high_education_area md ul">
                                                        <h2>Choose your dream country </h2>
                                                        {
                                                            countrylist && countrylist.length > 6 && maxCountries < 1
                                                            &&
                                                            <div className="serach_area">
                                                                <Form.Group className="form-group">
                                                                    <input
                                                                        className="form-control"
                                                                        type="text"
                                                                        autoFocus={true}
                                                                        id="county-search"
                                                                        placeholder="Search"
                                                                        value={streamSearch}
                                                                        onChange={(e) => {
                                                                            setStreamSearch(e.target.value);
                                                                        }}
                                                                    />
                                                                    <div className="seacrh">
                                                                        {
                                                                            streamSearch && streamSearch.trim() ?
                                                                                <a
                                                                                    href=""
                                                                                    onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        setStreamSearch(``);
                                                                                    }}
                                                                                ><i className="fal fa-times"></i></a>
                                                                                :
                                                                                <a
                                                                                    href=""
                                                                                    onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        document.getElementById("county-search").focus();
                                                                                    }}
                                                                                ><i className="fal fa-search"></i></a>
                                                                        }
                                                                    </div>
                                                                </Form.Group>
                                                            </div>
                                                        }
                                                        <div className="country_shoed">

                                                            <div className='row'>
                                                                {
                                                                    countrylist !== null
                                                                        ?
                                                                        countrylist.filter((item, i) => i < (countrylist && countrylist.length > 6 && maxCountries > 0 ? maxCountries : countrylist && countrylist.length) && (item.name.toLowerCase().indexOf(streamSearch.toLowerCase()) > -1)).map((country, index) => {
                                                                            return <div key={`countrylist` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                                <div
                                                                                    className='radio_area'
                                                                                >
                                                                                    <Form.Check
                                                                                        label={country.name}
                                                                                        name="country"
                                                                                        type={`checkbox`}
                                                                                        id={'country' + index}
                                                                                        checked={values.country.includes(country.id) ? true : false}
                                                                                        onChange={() => {
                                                                                            StudentEdit.handleCountrySelect(country);
                                                                                        }}
                                                                                    />
                                                                                    <label
                                                                                        htmlFor={'country' + index}
                                                                                        className="image_area"
                                                                                    >
                                                                                        <img src={getFlagIcon(country.short_name)} />
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        })
                                                                        :
                                                                        <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                                                }
                                                                {
                                                                    countrylist.length > 6 && maxCountries > 0
                                                                    &&
                                                                    <div key={`countrylist-more`} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                        <div className='radio_area'>
                                                                            <Form.Check
                                                                                label={(countrylist.length - 5) + ' more'}
                                                                                name="country"
                                                                                type={`radio`}
                                                                                id={'country-more'}
                                                                                checked={false}
                                                                            />

                                                                            <div
                                                                                className="image_area"
                                                                                onClick={() => {
                                                                                    setMaxCountries(0);
                                                                                }}
                                                                            >
                                                                                <i style={{ fontSize: '50px' }} className="fas fa-chevron-square-down"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!isError.country.isValid && isError.country
                                                        .message ? (
                                                        <p className="valid-helper">
                                                            {
                                                                isError
                                                                    .country
                                                                    .message
                                                            }
                                                        </p>
                                                    ) : null}
                                                </div>
                                                <div className="high_education_area">
                                                    <h2>What is your preferred intake?</h2>
                                                    <div className='row'>
                                                        {
                                                            preferintake !== null
                                                            &&
                                                            preferintake.map((item, index) => {
                                                                return <div key={`preferintake` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                    <Form.Check
                                                                        label={`${item.title} / ${item.year}`}
                                                                        name="preferintake"
                                                                        type={`radio`}
                                                                        id={'preferintake' + index}
                                                                        checked={parseInt(values.resumption_semster) === item.id ? true : false}
                                                                        onChange={() => {
                                                                            handleStates(
                                                                                "resumption_semster",
                                                                                item.id

                                                                            );
                                                                        }}
                                                                        value={item.id}
                                                                    />
                                                                </div>
                                                            })
                                                        }
                                                    </div>
                                                    {!isError.resumption_semster.isValid && isError.resumption_semster

                                                        .message ? (
                                                        <p className="valid-helper">
                                                            {
                                                                isError
                                                                    .resumption_semster
                                                                    .message
                                                            }
                                                        </p>
                                                    ) : null}
                                                </div>
                                                <div className="high_education_area">
                                                    <h2>What do you wish to pursue</h2>
                                                    <div className="serach_area">
                                                        <Form.Group className="form-group">
                                                            <input className="form-control" type="text" id="stream-search" placeholder="Search" onChange={(e) => {
                                                                setStreamSearch2(e.target.value);
                                                            }} />
                                                            <div className="seacrh">
                                                                {
                                                                    streamSearch2 && streamSearch2.trim() ?
                                                                        <a
                                                                            href=""
                                                                            onClick={(e) => {
                                                                                e.preventDefault();
                                                                                setStreamSearch2(``);
                                                                            }}
                                                                        ><i className="fal fa-times"></i></a>
                                                                        :
                                                                        <a
                                                                            href=""
                                                                            onClick={(e) => {
                                                                                e.preventDefault();
                                                                                document.getElementById("stream-search").focus();
                                                                            }}
                                                                        ><i className="fal fa-search"></i></a>
                                                                }
                                                            </div>
                                                        </Form.Group>
                                                    </div>
                                                    <div className="country_shoed">
                                                        <div className='row'>
                                                            {
                                                                profession && profession.length > 0 && profession.filter((item) => (item.name.toLowerCase().indexOf(streamSearch2.toLowerCase()) > -1)).map((stream, index) => {
                                                                    return <div key={`profession` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                        <Form.Check
                                                                            label={stream.name}
                                                                            name="stream"
                                                                            type={`checkbox`}
                                                                            id={'profession' + index}
                                                                            checked={values.stream_id.includes(stream.id) ? true : false}
                                                                            onChange={(e) => {
                                                                                // let s = values.stream_id;
                                                                                // s.push(stream.id);
                                                                                // handleStates(
                                                                                //     "stream_id",
                                                                                //     s
                                                                                // );
                                                                                StudentEdit.handleStreamSelect(stream);
                                                                            }}
                                                                            value={stream.id}
                                                                        />
                                                                    </div>
                                                                })
                                                            }
                                                        </div>
                                                        {!isError.stream_id.isValid && isError.stream_id

                                                            .message ? (
                                                            <p className="valid-helper">
                                                                {
                                                                    isError
                                                                        .stream_id
                                                                        .message
                                                                }
                                                            </p>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className="btn_area">
                                                        <div className="left_area">
                                                            <Link to='/my-profile'><button className="btn-primary-4" >Cancel</button></Link>
                                                        </div>
                                                        <div className="right_area">
                                                            <button className="btn-primary-2" type="submit">
                                                                {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                                &nbsp;Next</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Form>
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );

}
export default SignEditPreferenceStepOne;