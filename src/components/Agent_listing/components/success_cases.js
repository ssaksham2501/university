import React from "react";
import success1 from "../../../assets/images/success1.png"
import success2 from "../../../assets/images/success2.png";
import success3 from "../../../assets/images/success3.png";
import success4 from "../../../assets/images/success4.png";
import success5 from "../../../assets/images/success5.png";
import success6 from "../../../assets/images/success6.png";
import photo from '../../../assets/images/top_uni3.png';

const Success_cases = () => {
    return (
        <div>
            <div className="success_area" >
                <div className="inner_area">
                    <div className="box_area">
                        <div className="heading_area">
                            <h5>Success cases</h5>
                        </div>
                        <div className="info">
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={photo} />
                                    </div>
                                </div>
                                <div className="right_area">
                                    <h4>Leslie Alexander</h4>
                                    <p>Penn State University</p>
                                    <p >2022 Batch</p>
                                </div>
                            </div>
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={success2} />
                                    </div>
                                </div>
                                <div className="right_area">
                                    <h4>Leslie Alexander</h4>
                                    <p>Penn State University</p>
                                    <p >2022 Batch</p>
                                </div>
                            </div>
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={success3} />
                                    </div>
                                </div>
                                <div className="right_area">
                                    <h4>Leslie Alexander</h4>
                                    <p>Penn State University</p>
                                    <p >2022 Batch</p>
                                </div>
                            </div>
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={success4} />
                                    </div>
                                </div>
                                <div className="right_area">
                                    <h4>Leslie Alexander</h4>
                                    <p>Penn State University</p>
                                    <p >2022 Batch</p>
                                </div>
                            </div>
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={success5} />
                                    </div>
                                </div>
                                <div className="right_area">
                                    <h4>Leslie Alexander</h4>
                                    <p>Penn State University</p>
                                    <p >2022 Batch</p>
                                </div>
                            </div>
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={success6} />
                                    </div>
                                </div>
                                <div className="right_area">
                                    <h4>Leslie Alexander</h4>
                                    <p>Penn State University</p>
                                    <p >2022 Batch</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Success_cases;


