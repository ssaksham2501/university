import React, { Component, useState, useEffect } from "react";
import review1 from "../../../assets/images/review1.png";
import review2 from "../../../assets/images/review2.png";
import review3 from "../../../assets/images/review3.png";
import { Rating } from "react-simple-star-rating";
import AddReviewModal from "../../Layout/add_review_modal";
import AgentProfileController from "../../../apis/controllers/agentController/agent.controller";
import { renderImage, renderNoAvatar, timeAgo } from "../../../helpers/General";
import top_agent2 from "../../../assets/upload/demo.png";
import emptyreveiwimg from "../../../assets/images/Feedback-pana.png";
const Agent_review = (props) => {
    const [ratingValue, setRatingValue] = useState(0); // initial rating v

    const [page, setPage] = useState(1);
    const [addreview, setadd] = useState(false);
    const [initLoader, setInitLoader] = useState(true);
    const [loader, setLoader] = useState(false);
    const [moreShow, setMoreShow] = useState(false);
    const [reviewAccepted, setReviewAccepted] = useState(false);
    const [reviewList, setReviewList] = useState([]);

    const getReviewListing = async () => {
        if (
            ((props.agent && props.agent.user_name) ||
                (props.university && props.university.slug)) &&
            !loader
        ) {
            setLoader(true);
            let response = await new AgentProfileController().reviewListing({
                agent: props.agent ? props.agent.user_name : null,
                university: props.university ? props.university.slug : null,
                page: page,
            });
            setLoader(false);
            setInitLoader(false);
            if (response && response.status) {
                setReviewAccepted(response.review_status);
                if (response.review_added && props.setReviewAccepted) {
                    props.setReviewAccepted(response.review_status);
                }

                let list =
                    reviewList && reviewList.length > 0
                        ? [...reviewList, ...response.listing]
                        : response.listing;
                setReviewList(list);
                setPage(response.page + 1);
                if (list.length >= response.total) {
                    setMoreShow(false);
                } else {
                    setMoreShow(true);
                }
            } else {
                setReviewList([]);
            }
        }
    };
    useEffect(() => {
        getReviewListing();
    }, []);
    return (
        <div>
            <div className="review_section">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-sm-4 p-0">
                            <div className="review_wrap">
                                <div className="heading_area">
                                    <div className="left_area">
                                        <h1>Reviews</h1>
                                    </div>
                                    <div className="right_area">
                                        {!reviewAccepted && (
                                            <button
                                                className="btn-primary-2"
                                                onClick={() => setadd(true)}
                                            >
                                                Write a review
                                            </button>
                                        )}
                                    </div>
                                </div>
                                <div className="review_area">
                                    {reviewList &&
                                        reviewList.length > 0 &&
                                        reviewList &&
                                        reviewList.map((item, index) => (
                                            <div className="review_detail">
                                                <div className="left_area">
                                                    <div className="image_area">
                                                        <img
                                                            src={
                                                                item.image
                                                                    ? renderImage(
                                                                          item.image,
                                                                          "medium"
                                                                      )
                                                                    : renderNoAvatar(
                                                                          item.users_first_name +
                                                                              ` ` +
                                                                              item.users_last_name
                                                                      )
                                                            }
                                                            alt=""
                                                        />
                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="text_area ul">
                                                        <div className="divide">
                                                            <div className="name">
                                                                <h4>
                                                                    {
                                                                        item.users_first_name
                                                                    }{" "}
                                                                    {
                                                                        item.users_last_name
                                                                    }
                                                                </h4>
                                                            </div>
                                                            <div className="created">
                                                                <p>
                                                                    {item.created
                                                                        ? timeAgo(
                                                                              item.created
                                                                          )
                                                                        : ""}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div className="rating_area">
                                                            <div className="rating_met">
                                                                <ul>
                                                                    <li>
                                                                        <i
                                                                            className={
                                                                                `fas fa-star` +
                                                                                (item.rating >=
                                                                                    0.1 ||
                                                                                item.rating >=
                                                                                    1
                                                                                    ? " active"
                                                                                    : "")
                                                                            }
                                                                        ></i>
                                                                    </li>
                                                                    <li>
                                                                        <i
                                                                            className={
                                                                                `fas fa-star` +
                                                                                (item.rating >=
                                                                                2
                                                                                    ? " active"
                                                                                    : "")
                                                                            }
                                                                        ></i>
                                                                    </li>
                                                                    <li>
                                                                        <i
                                                                            className={
                                                                                `fas fa-star` +
                                                                                (item.rating >=
                                                                                3
                                                                                    ? " active"
                                                                                    : "")
                                                                            }
                                                                        ></i>
                                                                    </li>
                                                                    <li>
                                                                        <i
                                                                            className={
                                                                                `fas fa-star` +
                                                                                (item.rating >=
                                                                                4
                                                                                    ? " active"
                                                                                    : "")
                                                                            }
                                                                        ></i>
                                                                    </li>
                                                                    <li>
                                                                        <i
                                                                            className={
                                                                                `fas fa-star` +
                                                                                (item.rating >=
                                                                                5
                                                                                    ? " active"
                                                                                    : "")
                                                                            }
                                                                        ></i>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <p>{item.review} </p>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                </div>
                                {!initLoader && reviewList.length < 1 && (
                                    <div className="empty_img_area">
                                        <div className="text_lable">
                                            <p>No Review Found</p>
                                        </div>
                                        <div className="img_area">
                                            <img src={emptyreveiwimg} alt="" />
                                        </div>
                                    </div>
                                )}

                                {initLoader && (
                                    <p className="text-center initloader">
                                        <i className="fa fa-spin fa-spinner"></i>
                                    </p>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {moreShow && reviewList && reviewList.length > 0 && (
                <div className="View-more my-2">
                    <button
                        type="button"
                        className="btn btn-primary-2 float-end"
                        onClick={() => getReviewListing()}
                    >
                        {loader && <i className="fa fa-spin fa-spinner"></i>}{" "}
                        View More
                    </button>
                </div>
            )}

            {props && props.agent && addreview && (
                <AddReviewModal
                    agent={props.agent}
                    show={addreview}
                    close={() => setadd(false)}
                />
            )}
        </div>
    );
};

export default Agent_review;
