import React from "react";
import agent_photo1 from "../../../assets/images/agent_photo1.png"
import agent_photo2 from "../../../assets/images/agent_photo2.png"
import agent_photo3 from "../../../assets/images/agent_photo3.png"
import agent_photo4 from "../../../assets/images/agent_photo4.png"
import agent_photo5 from "../../../assets/images/agent_photo5.png"

const Photos_deatil = () => {
    return (
        <div>
            <div className="photos_area">
                {/* <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"> */}
                <div className="inner_area">
                    <div className="box_area">
                        <div className="heading_area">
                            <h5>Photos</h5>
                        </div>
                        <div className="gallery_wrap">
                            <div className="gallery_inn ">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={agent_photo1} />
                                    </div>
                                </div>
                            </div>
                            <div className="gallery_inn">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={agent_photo2} />
                                    </div>
                                </div>
                            </div>
                            <div className="gallery_inn">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={agent_photo3} />
                                    </div>
                                </div>
                            </div>
                            <div className="gallery_inn">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={agent_photo4} />
                                    </div>
                                </div>
                            </div>
                            <div className="gallery_inn">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={agent_photo5} />
                                    </div>
                                </div>
                            </div>
                            <div className="gallery_inn">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={agent_photo1} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        //         </div>
        //     </div>
        // </div>
    );
}

export default Photos_deatil;
