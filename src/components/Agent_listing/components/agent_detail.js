import React, { Component, useState } from "react";
import { Rating } from "react-simple-star-rating";
import { Link, withRouter } from "react-router-dom";
import agent_info from "../../../assets/images/agent_info.png";
import {
    intToString,
    renderImage,
    renderNoAvatar,
    stringLimit,
} from "../../../helpers/General";

const Agent_info = (props) => {
    const [ratingValue, setRatingValue] = useState(0); // initial rating
    const handleRating = (rate) => {
        setRatingValue(rate);
    };
    const handleReset = () => {
        setRatingValue(4);
    };

    return (
        <div>
            <div className="agent_info_wrap">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_detail_wrap">
                                <div className="agent_detail_area">
                                    <div className="left_area">
                                        <div className="image_area">
                                            <img
                                                src={
                                                    props.agent &&
                                                    props.agent.image
                                                        ? renderImage(
                                                              props.agent.image,
                                                              "medium"
                                                          )
                                                        : renderNoAvatar(
                                                              (props.agent &&
                                                              props.agent
                                                                  .first_name
                                                                  ? props.agent
                                                                        .first_name
                                                                  : ``) +
                                                                  ` ` +
                                                                  (props.agent &&
                                                                  props.agent
                                                                      .last_name
                                                                      ? props
                                                                            .agent
                                                                            .last_name
                                                                      : ``)
                                                          )
                                                }
                                            />
                                        </div>
                                    </div>
                                    <div className="right_area">
                                        <div className="left_inn">
                                            {props.agent &&
                                                props.agent.rating > 0 &&
                                                props.agent.rating_user_count >
                                                    0 && (
                                                    <div className="rating_area">
                                                        <div className="rating_met">
                                                            <ul>
                                                                <li>
                                                                    <i
                                                                        className={
                                                                            `fas fa-star` +
                                                                            (props.agent &&
                                                                            (props
                                                                                .agent
                                                                                .rating >=
                                                                                0.1 ||
                                                                                props
                                                                                    .agent
                                                                                    .rating >=
                                                                                    1)
                                                                                ? " active"
                                                                                : "")
                                                                        }
                                                                    ></i>
                                                                </li>
                                                                <li>
                                                                    <i
                                                                        className={
                                                                            `fas fa-star` +
                                                                            (props.agent &&
                                                                            props
                                                                                .agent
                                                                                .rating >=
                                                                                2
                                                                                ? " active"
                                                                                : "")
                                                                        }
                                                                    ></i>
                                                                </li>
                                                                <li>
                                                                    <i
                                                                        className={
                                                                            `fas fa-star` +
                                                                            (props.agent &&
                                                                            props
                                                                                .agent
                                                                                .rating >=
                                                                                3
                                                                                ? " active"
                                                                                : "")
                                                                        }
                                                                    ></i>
                                                                </li>
                                                                <li>
                                                                    <i
                                                                        className={
                                                                            `fas fa-star` +
                                                                            (props.agent &&
                                                                            props
                                                                                .agent
                                                                                .rating >=
                                                                                4
                                                                                ? " active"
                                                                                : "")
                                                                        }
                                                                    ></i>
                                                                </li>
                                                                <li>
                                                                    <i
                                                                        className={
                                                                            `fas fa-star` +
                                                                            (props.agent &&
                                                                            props
                                                                                .agent
                                                                                .rating >=
                                                                                5
                                                                                ? " active"
                                                                                : "")
                                                                        }
                                                                    ></i>
                                                                </li>
                                                            </ul>
                                                            <span className="show_rating">
                                                                <i class="fas fa-user pe-1"></i>{" "}
                                                                {intToString(
                                                                    props.agent
                                                                        .rating_user_count
                                                                )}
                                                            </span>
                                                        </div>
                                                    </div>
                                                )}
                                            <div className="detail_area">
                                                <h3>
                                                    {props.agent &&
                                                    props.agent.is_company
                                                        ? props.agent
                                                              .company_name
                                                        : props.agent &&
                                                          props.agent
                                                              .first_name +
                                                              " " +
                                                              props.agent
                                                                  .last_name}
                                                </h3>
                                                <p>
                                                    {props.agent
                                                        ? stringLimit(
                                                              props.agent.bio,
                                                              300
                                                          )
                                                        : ``}
                                                </p>
                                            </div>
                                        </div>
                                        <div className="right_inn">
                                            <div className="btn_area">
                                                <button
                                                    type="button"
                                                    onClick={
                                                        props.handleApplyModal
                                                    }
                                                    className="btn-primary-4"
                                                >
                                                    Contact now
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Agent_info;
