import React, { useEffect } from "react";
import { useLocation, useParams } from "react-router-dom";
import AuthController from "../../apis/controllers/auth.controller";
import ModalController from "../../apis/controllers/modal.controller";
import AgentsDetail from "../../functions/agents/AgentsDetail";
import { parseYoutubeUrl } from "../../helpers/General";
import ListingGrids from "../listing/components/ListingGrids";
import Agent_info from "./components/agent_detail";
import Photos_deatil from "./components/agent_photos";
import Agent_review from "./components/review_listing";
import Success_cases from "./components/success_cases";

function Agent_listing(props) {
    const params = useParams();
    const { agent, agentDetailhandler, universities } = AgentsDetail(props);

    useEffect(() => {
        window.scrollTo(0, 0);
        agentDetailhandler.init(params.slug);
    }, [params.slug]);

    const handleApplyModal = (type) => {
        if(agent && new AuthController().getLoginUserId()){
            new ModalController().setApplyModal({
                type: 'agent',
                slug: agent.user_name,
                info: null,
                agent: agent,
                modalTitle: "Send message to " + agent.first_name + (agent.last_name ? ' ' + agent.last_name : '')
            });
        } else if(agent) {
            new ModalController().setApplyModal(false);
            new ModalController().setStartModal(true);
        }
    }

    return (
        <div className="breacam_bg top_space">
            <div className="agent_listing">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_detail_wrap">
                                <Agent_info
                                    agent={agent}
                                    handleApplyModal={handleApplyModal}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="agent_internal_info">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="detail_area">
                                <div className="row">
                                    <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                        <div className="box_detail_area">
                                            <div className="box_area">
                                                <div className="icon_area">
                                                    <i className="fas fa-map-marker-alt"></i>
                                                </div>
                                                <h5>{agent && agent.country_name ? agent.country_name : '-'}</h5>
                                                <p>{agent && (agent.state_name + (agent.pincode && agent.state_name ? ', ' + agent.pincode : agent.pincode)) }</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                        <div className="box_detail_area">
                                            <div className="box_area">
                                                <div className="icon_area">
                                                    <i className="far fa-check-circle"></i>
                                                </div>
                                                <h5>{agent && agent.range_of_success_rate ? agent.range_of_success_rate : `-`}</h5>
                                                <p>Sucess Rate</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                        <div className="box_detail_area">
                                            <div className="box_area">
                                                <div className="icon_area">
                                                    <i className="far fa-graduation-cap"></i>
                                                </div>
                                                <h5>{agent && agent.range_of_success_cases ? agent.range_of_success_cases : '-'}</h5>
                                                <p>Sucess Cases</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                        <div className="box_detail_area">
                                            <div className="box_area">
                                                <div className="icon_area">
                                                    <i class="fas fa-user-tie"></i>
                                                </div>
                                                <h5>{agent && universities && universities.length > 0 ? universities.length + '+' : '-'}</h5>
                                                <p>Universities</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="university_listing_wrap">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="university_wrap">
                                <div className="left_wrap">
                                    {
                                        (agent && agent.promotional_area_enable > 0 && agent.promotional_area_heading)
                                        &&
                                        <div className="heading_wrap">
                                            <div className="video_met">
                                                <h5>{agent.promotional_area_heading}</h5>
                                            </div>
                                        </div>
                                    }
                                    {
                                        (agent && agent.promotional_area_enable > 0 && agent.promotional_video)
                                        &&
                                        <div className="heading_wrap">
                                            <div className="video_met">
                                                <iframe width="100%" height="415" src={parseYoutubeUrl(agent.promotional_video)} frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    }
                                    <div className="heading_wrap">
                                        <h1>University</h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in sem at nisl
                                            luctus placerat nec in urna.  Mauris in sem.</p>
                                    </div>
                                    <div className="lsiting_show_wrap">
                                        <div className="list_grid">
                                            <div className="list_grid_wrap">
                                            {
                                                agent && universities && universities.length > 0 && universities.map((item, index) => {
                                                    return (<ListingGrids
                                                        key={`listin-grid` + index}
                                                        listView={false}
                                                        university={item}
                                                    />)
                                                })
                                            }
                                            </div>
                                            <div className="load_more_university">
                                                <a href="javascript:;" className="btn btn-primary-1">+ View more</a>
                                            </div>
                                        </div>
                                    </div>
                                    {
                                        agent
                                        &&
                                        <Agent_review
                                            agent={agent}
                                            university={null}
                                        />
                                    }
                                    
                                </div>
                                <div className="right_area">
                                    <div className="success_cases">
                                        <Success_cases />
                                        {/* <Photos_deatil  /> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            {/* { addreview&&
            <AddReviewModal
              show={addreview}
              close={()=>setAddreview(false)}
            />
            } */}
        </div>
    );
}

export default Agent_listing;
