import React, { useState, useEffect } from "react";
import contact_bg from '../../assets/images/contact_bg.png';
import contact_map from '../../assets/images/contact_map.png';
import Validation from "../../helpers/vaildation";
import { Link } from "react-router-dom";
import ContactController from "../../apis/controllers/contact.controller";

function Contactus() {
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(``);
    const [isSuccess, setIsSuccess] = useState(false);
    const [contactDetails, setContactDetails] = useState(null);

    //validations  -- start
    const [isError, setError] = useState({
        first_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        last_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        email: {
            rules: ["required", "email"],
            isValid: true,
            message: "",
        },
        message: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        first_name: null,
        last_name: null,
        email: null,
        message: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        getContact();
        setValues(defaultValues);
    }, []);

    const handleStates = (field, value) => {

        /** Validate each field on change */
        let node = validation.validateField(field, value);
        console.log(node, 'node data')
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };

    //validations  -- end

    //contact  --start
    const contactUs = async () => {
        if (isLoading === false) {
            setErrMsg(``);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new ContactController().contactUs(values);
                console.log(response, "APiresponse");
                setIsLoading(false);
                if (response && response.status) {
                    setIsSuccess(response.status);
                    setValues(defaultValues);
                } else {
                    setErrMsg(response.message);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //contact --end

    //getcontact --start()
    const getContact = async () => {
        let response = await new ContactController().getContactDetail();
        if (response && response.status) {
            setContactDetails(response.page);
        }
    };
    //getcontact --end

    return (
        <div>
            <div className="contactus_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="contact_wrap">
                                <div className="detail_area">
                                    <div className="left_area">
                                        <div className="image_area">
                                            <img src={contact_bg} />
                                        </div>
                                        <div className="inner_detail_area">
                                            <h2>{contactDetails !== null ? contactDetails.title : ``}</h2>
                                            <p>{contactDetails !== null ? contactDetails.description : ''}</p>

                                        </div>
                                    </div>
                                    <div className="right_area">
                                        {isSuccess == false ? <>
                                            <div className="inner_area">
                                                <div className="input_fields_area">
                                                    <div className="row">
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                                                            <form
                                                                noValidate
                                                                onSubmit={(e) => {
                                                                    e.preventDefault();
                                                                    contactUs();
                                                                }}
                                                            >
                                                                <div className="name_area">
                                                                    <div className="row">
                                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                                            <div className="expect_education_area">
                                                                                <div className="form_search">
                                                                                    <label className={
                                                                                        !isError
                                                                                            .first_name
                                                                                            .isValid
                                                                                            ? "valid-box form_controls form-label"
                                                                                            : "form_controls form-label"
                                                                                    }>First name</label>
                                                                                    <input type="text" placeholder="First name" className="form-control"
                                                                                        onChange={(
                                                                                            e
                                                                                        ) => {
                                                                                            handleStates(
                                                                                                "first_name",
                                                                                                e.target
                                                                                                    .value);
                                                                                        }}
                                                                                        value={
                                                                                            values.first_name
                                                                                                ? values.first_name
                                                                                                : ``
                                                                                        } />
                                                                                    {isError.first_name
                                                                                        .message ? (
                                                                                        <p className="valid-helper">
                                                                                            {
                                                                                                isError
                                                                                                    .first_name
                                                                                                    .message
                                                                                            }
                                                                                        </p>
                                                                                    ) : null}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                                            <div className="expect_education_area">
                                                                                <div className="form_search" >
                                                                                    <label
                                                                                        className={
                                                                                            !isError.last_name.isValid
                                                                                                ? "valid-box form_controls form-label"
                                                                                                : "form_controls form-label"
                                                                                        }>Last name</label>
                                                                                    <input type="text" placeholder="Last name" className="form-control"
                                                                                        onChange={(e) => {
                                                                                            handleStates(
                                                                                                "last_name",
                                                                                                e.target.value
                                                                                            );
                                                                                        }}
                                                                                        value={
                                                                                            values.last_name
                                                                                                ? values.last_name
                                                                                                : ``
                                                                                        } />
                                                                                    {isError.last_name.message ? (
                                                                                        <p className="valid-helper">
                                                                                            {isError.last_name.message}
                                                                                        </p>
                                                                                    ) : null}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                            <div className="form_search">
                                                                                <label className={
                                                                                    !isError.email
                                                                                        .isValid
                                                                                        ? "valid-box form_controls  form-label"
                                                                                        : "form_controls  form-label"
                                                                                }

                                                                                >Email address</label>
                                                                                <input type="email" placeholder="Email address" className="form-control"
                                                                                    onChange={(
                                                                                        e
                                                                                    ) => {
                                                                                        handleStates(
                                                                                            "email",
                                                                                            e.target
                                                                                                .value
                                                                                        );
                                                                                    }}
                                                                                    value={
                                                                                        values.email
                                                                                            ? values.email
                                                                                            : ``
                                                                                    } />
                                                                                {isError.email
                                                                                    .message ? (
                                                                                    <p className="valid-helper">
                                                                                        {
                                                                                            isError
                                                                                                .email
                                                                                                .message
                                                                                        }
                                                                                    </p>
                                                                                ) : null}

                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                            <div className="form_search">
                                                                                <label
                                                                                    className={
                                                                                        !isError.message
                                                                                            .isValid
                                                                                            ? "valid-box form_controls form_controls_tarea form-label"
                                                                                            : "form_controls form_controls_tarea form-label"
                                                                                    }
                                                                                >Message</label>
                                                                                <textarea as="textarea" rows={4} placeholder="Type your message...." className="form-control"
                                                                                    onChange={(
                                                                                        e
                                                                                    ) => {
                                                                                        handleStates(
                                                                                            "message",
                                                                                            e.target
                                                                                                .value
                                                                                        );
                                                                                    }}
                                                                                    value={
                                                                                        values.message
                                                                                            ? values.message
                                                                                            : ``
                                                                                    }
                                                                                />
                                                                                {isError.message
                                                                                    .message ? (
                                                                                    <p className="valid-helper">
                                                                                        {
                                                                                            isError
                                                                                                .message
                                                                                                .message
                                                                                        }
                                                                                    </p>
                                                                                ) : null}

                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                            {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                                            <div className="btn_area">
                                                                                <button
                                                                                    className="btn-primary-2 w-100"
                                                                                    type="submit"
                                                                                >
                                                                                    {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                                                    &nbsp; Submit
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div></>
                                            :
                                            <div className="message_area">
                                                <div className="box_area">
                                                    <div className="message_detail_area text-center">
                                                        <div className="icon_area">
                                                            <i className="fas fa-check"></i>
                                                        </div>
                                                        <h2>Your message has been sent!</h2>
                                                        <div className="btn_area">
                                                            <button
                                                                className="btn-primary-2"
                                                                onClick={() => {
                                                                    setIsSuccess(false);
                                                                }}
                                                            ><i className="far fa-arrow-left"></i> Go back</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                            {
                                contactDetails !== null && contactDetails.contact_info && contactDetails.contact_info.address && contactDetails.contact_info.number && contactDetails.contact_info.email
                                &&
                                <div className="map_area">
                                    <div className="detail_map_area">
                                        <div className="image_area d-sm-block d-none">
                                            <img src={contact_map} />
                                        </div>
                                        <div className="inner_detail_area">
                                            <h2>{contactDetails !== null ? contactDetails.contact_info.get_in_touch_title : ''}</h2>
                                            <p>{contactDetails !== null ? contactDetails.contact_info.get_in_touch_description : ''}</p>
                                            <div className="contact_info">
                                                <div className="contact_inner_info">
                                                    {
                                                        contactDetails !== null && contactDetails.contact_info && contactDetails.contact_info.address
                                                        &&
                                                        <div className="detail_info">
                                                            <div className="left_icon">
                                                                <Link to="/"><i className="fas fa-map-marker-alt"></i></Link>
                                                            </div>
                                                            <div className="right_text">
                                                                <Link to="/"><p>{contactDetails.contact_info.address}</p></Link>
                                                            </div>
                                                        </div>
                                                    }
                                                    {
                                                        contactDetails !== null && contactDetails.contact_info && contactDetails.contact_info.number
                                                        &&
                                                        <div className="detail_info">
                                                            <div className="left_icon">
                                                                <Link to="/"><i className="fas fa-phone-alt"></i></Link>
                                                            </div>
                                                            <div className="right_text">
                                                                <Link to="/"><p>{contactDetails.contact_info.number}</p></Link>
                                                            </div>
                                                        </div>
                                                    }
                                                    {
                                                        contactDetails !== null && contactDetails.contact_info && contactDetails.contact_info.email
                                                        &&
                                                        <div className="detail_info">
                                                            <div className="left_icon">
                                                                <Link to="/"><i className="fas fa-at"></i></Link>
                                                            </div>
                                                            <div className="right_text">
                                                                <Link to="/"><p>{contactDetails.contact_info.email}</p></Link>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Contactus;

