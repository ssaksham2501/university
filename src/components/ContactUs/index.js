import React, { useState } from "react";
import CmsBanner from "./cms_banner";
import Contactus from "./contact_us";
function ContactUs() {
    return (
        <div>
            <div className="contact_section">
                <CmsBanner />
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="contact_wrap">
                                <Contactus />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactUs;