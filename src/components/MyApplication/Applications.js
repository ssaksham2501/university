import React, { useState, useEffect } from "react";
import StudentdetailModal from "../Layout/applicant_student_detail_modal";
import ApplicationGrid from "./ApplicationGrid";
import FunctionMyApplication from "./function";
function MyApplications() {
    const { mounting, getMyApplicationDetail, myApplicationData } = FunctionMyApplication();
    const [ viewApplication, setViewApplication ] = useState(null);
    
    
    useEffect(() => {
        getMyApplicationDetail();
    }, []);

    return (
        <>
            <div>
                <div className="university_section">
                    <div className="banner_inner">
                        <div>
                            <div className="inner_heading_area">
                                <h4>My Applications</h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua.
                                </p>
                            </div>
                            <div className="center_heading_area"></div>
                            {
                                mounting
                                ?
                                    <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                :
                                <>
                                    {myApplicationData &&
                                        myApplicationData.map((item, index) => (
                                            <ApplicationGrid
                                                key={`myapps` + index}
                                                item={item}
                                                viewApplication={(item) => {
                                                    setViewApplication(item)
                                                }}
                                            />
                                        ))
                                    }
                                </>
                            }
                        </div>
                    </div>
                </div>
            </div>
            {
                viewApplication !== null
                &&
                <StudentdetailModal
                    info={viewApplication}
                    close={() => setViewApplication(null)}
                />
            }
        </>
    );
}

export default MyApplications;
