import React, { useState } from "react";
import StudentDashboardController from "../../apis/controllers/studentController/studentDashboard";
const FunctionMyApplication = () => {
    const [myApplicationData, setMyApplicationData] = useState([]);
    const [mounting, setMounting] = useState(true);

    const getMyApplicationDetail = async () => {
        if (mounting) {
            let response = await new StudentDashboardController().getMyApplication();
            setMounting(false);
            if (response && response.status) {
                setMyApplicationData(response.leads);
            }
        }
    };
    return {
        getMyApplicationDetail,
        myApplicationData,
        mounting
    };
};

export default FunctionMyApplication;
