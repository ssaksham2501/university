import React, { useState } from "react";
import { Link } from "react-router-dom";
import agent_applicant from "../../assets/images/agent_applicant.png";
import { renderImage, renderNoAvatar, _date, _time } from "../../helpers/General";
function ApplicationGrid(props) {
    const { myApplicationData } = props;
    return (
        <>
            <div>
                {
                        <div>
                            <div className="center_heading_area"></div>
                            <div className="box_area">
                                <div className="box_inner_area">
                                    <div className="top_area">
                                        <div className="applicant_detail_area">
                                            <div className="applicant_image_area">
                                                <div className="image_area">
                                                    {
                                                        props.item.lead_type == 'university'
                                                        ?
                                                            <img src={renderImage(props.item.logo, 'small')} />
                                                        :
                                                            <img src={props.item.image ? renderImage(props.item.image, 'small') : renderNoAvatar(props.item.agent_name)} />
                                                    }

                                                </div>
                                            </div>
                                            <div className="applicant_info_area">
                                                <div className="applicant_name">
                                                    <div className="left_area">
                                                        <h4>
                                                            {props.item.lead_type == 'agent' ? props.item.agent_name : props.item.university_name}
                                                        </h4>
                                                    </div>
                                                    <div className="right_area">
                                                        <p>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div className="applicant_location_area">
                                                    <a href="" onClick={(e) => e.preventDefault()}>
                                                        <div className="left_area">
                                                            <i class="fal fa-map-marker-alt"></i>
                                                        </div>
                                                        <div className="right_area_in">
                                                        <p>
                                                            {
                                                                props.item.lead_type == 'agent'
                                                                ?
                                                                    (props.item.agent_state ? (props.item.agent_state + ', ') : `` ) + props.item.agent_country
                                                                :
                                                                    (props.item.state ? (props.item.state + ', ') : `` ) + props.item.country
                                                            }
                                                            </p>
                                                        </div>
                                                    </a>
                                                </div>
                                                {
                                                    props.item.lead_type == 'agent' && props.item.university_id
                                                    &&
                                                    <div className="applicant_uni_name">
                                                        <p>{props.item.university_name}</p>
                                                    </div>
                                                }
                                                {
                                                    props.item.information && props.item.information.courses && props.item.information.courses.length > 0
                                                    &&
                                                    <div className="applicant_uni_name">
                                                        <p>
                                                            {
                                                                props.item.information.courses.map((item, key) => (
                                                                    <>{(key > 0 ? `, ` : '') + item.title}</>
                                                                ))
                                                            }
                                                            &nbsp;</p>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        <div className="save_icon_area">
                                            <div className="icon_area">
                                                <small>{ _date(props.item.created) + ` `  + _time(props.item.created) }</small>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="bottom_area">
                                        <div className="left_area">
                                            <div className="detail_area">
                                                <ul>
                                                    <li>
                                                        <a
                                                            href=""
                                                            className="listing_button"
                                                            onClick={(e) => e.preventDefault()}
                                                        >
                                                            {props.item.information && props.item.information.education && props.item.information.education.title}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a
                                                            href=""
                                                            className="listing_button"
                                                            onClick={(e) => e.preventDefault()}
                                                        >
                                                            <strong>Intake:</strong> { props.item.information && props.item.information.prefered_intake && (props.item.information.prefered_intake.title + ` ` + props.item.information.prefered_intake.year) }
                                                        </a>
                                                    </li>
                                                    {
                                                        props.item.information && props.item.information.ielts
                                                        &&
                                                        <li>
                                                            <a
                                                                href=""
                                                                className="listing_button"
                                                                onClick={(e) => e.preventDefault()}
                                                                >
                                                                    <strong>IELTS:</strong> {props.item.information.ielts_score && (props.item.information.ielts_score * 1).toFixed(1)}
                                                            </a>
                                                        </li>
                                                    }
                                                    {
                                                        props.item.information && props.item.information.tofel
                                                        &&
                                                        <li>
                                                            <a
                                                                href=""
                                                                className="listing_button"
                                                                onClick={(e) => e.preventDefault()}
                                                                >
                                                                    <strong>TOEFL:</strong> {props.item.information.tofel_score && (props.item.information.tofel_score * 1).toFixed(0)}
                                                            </a>
                                                        </li>
                                                    }
                                                    {
                                                        props.item.information && props.item.information.pte
                                                        &&
                                                        <li>
                                                            <a
                                                                href=""
                                                                className="listing_button"
                                                                onClick={(e) => e.preventDefault()}
                                                                >
                                                                    <strong>PTE:</strong> {props.item.information.pte_score && (props.item.information.pte_score * 1).toFixed(0)}
                                                            </a>
                                                        </li>
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="right_area">
                                            <div className="purchase_btn">
                                                {
                                                    props.item.compelted > 0
                                                    ?
                                                    <a
                                                        href=""
                                                        onClick={(e) => e.preventDefault()}
                                                        class="btn btn-primary-2"
                                                    >
                                                        Write a Review
                                                    </a>
                                                    :
                                                    <a
                                                        href=""
                                                        onClick={(e) => {
                                                            e.preventDefault();
                                                            props.viewApplication(props.item);
                                                        }}
                                                        class="btn btn-primary-2"
                                                    >
                                                        View Details
                                                    </a>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </div>
        </>
    );
}

export default ApplicationGrid;
