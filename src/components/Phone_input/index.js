import React, { useState, useEffect } from "react";
import PhoneInput, { formatPhoneNumber } from 'react-phone-number-input';
import 'react-phone-number-input/style.css';

function Phonecomponent(props) {
    return (
        <>
            <PhoneInput
                placeholder="Phone Number"
                defaultCountry="IN"
                rules={{ required: true }}
                value={props.phone
                    ? props.phone : ''}
                disabled={props.disabled ? props.disabled : false}
                onChange={(v) => {
                    let val = v && v.replace(/[^a-zA-Z0-9]/g, '');
                    if (val && val.length > 0) {
                        props.updateValue('+'+val);
                    }
                }}
            />


        </>
    )
}

export default (Phonecomponent);