import React, { useState, useEffect } from "react";
import PagesController from "../../apis/controllers/pages.controller";
function TermsConditions() {
    const [page, setPage] = useState(null);

    useEffect(() => {
        window.scrollTo(0, 0);
        gettermscondition()
     
    }, []);

    const gettermscondition = async () => {
        let response = await new PagesController().termsConditions();
        if(response.status) {
            setPage(response.page)
            console.log('show')   
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    // useEffect(() => {
    //     axios.defaults.headers.common = { 'Authorization': `Bearer cMhemM7dQaq4ppP3c88gacIL9Rrl8iuO2VDPlQuY1cDWH9CSgPmH2jHdJzyE5MoY` }
    //     axios.get(
    //         'https://globizcloudserver.com/university/public/api/pages/terms-condition'
    //     )
    //         .then(function (response) {
    //             let data = response.data;
    //             if (data && data.status === true) {
    //                 setPage(data.page);
    //             }
    //             else {
    //                 console.log('Error while fetching api data.');
    //             }
    //         })
    //         .catch(function (error) {
    //             // handle error
    //             console.log(error);
    //         });
    // }, []);

    return (
        <div>
            <div className="cms_banner_area top_space">
                <div className="bg_area">
                    <div className="heading_area">
                        <h1>
                            {page !== null ? page.title : ``}
                        </h1>
                    </div>
                </div>
            </div>
            <div className="terms_section top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xxl-12 col-xxl-12 col-xxl-12 col-xxl-12 col-xxl-12">
                            <div className="terms_wrap text-center">
                                <div className="detail_area">
                                    {page ? <div dangerouslySetInnerHTML={{ __html: page.description }}/> : null}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TermsConditions;