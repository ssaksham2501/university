import React, { useState } from "react";
import error_img from '../assets/images/error_img.png';


const Error = () => {
    return (
        <>
            <div>
                <div className="error_area top_space">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 co-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="detail_area">
                                    <div className="image_area">
                                        <img src={error_img} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Error;