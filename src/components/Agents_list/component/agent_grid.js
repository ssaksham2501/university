import React from "react";
import { isMobile, isTablet } from "react-device-detect";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating';
import ModalController from "../../../apis/controllers/modal.controller";
import { isEmptyObj, renderImage, renderNoAvatar, stringLimit } from "../../../helpers/General";

const AgentGrid = (props) => {
    const { item, index } = props;
    const [ratingValue, setRatingValue] = React.useState(0)
    const handleRating = (rate) => {
        setRatingValue(rate)
    }
    const handleReset = () => {
        setRatingValue(4)
    }

    
    return (
        <div className={props.grid && !isTablet && !isMobile ? `col-lg-${props.grid} col-md-${props.grid} col-sm-${props.grid} col-12` : `col-lg-6 col-md-6 col-sm-6 col-12`}>
            <div className="uni_detail_area">
                <div className="agent_box">
                    <div className="agent_inner_area">
                        <div className="left_area">
                            <div className="img_area">
                                <Link to={item.user_name ? '/agent/' + item.user_name : '/404'}>
                                    <img src={item.image ? renderImage(item.image, 'medium') : renderNoAvatar( (props.item && props.item.first_name ? props.item.first_name : ``) + ` ` + (props.item && props.item.last_name ? props.item.last_name : ``) )} alt="" />
                                </Link>
                            </div>
                        </div>
                        <div className="right_area">
                            <div className="top-area">
                                <div className="left_area">
                                    <h5>
                                        <Link to={item.user_name ? '/agent/' + item.user_name : '/404'}>
                                            {item.is_company
                                                ?
                                                    <>{item.company_name}</>
                                                :
                                                    <>{item.first_name ? item.first_name : null}{' '}{item.last_name ? item.last_name : null}</>
                                            }
                                        </Link>
                                    </h5>
                                    <div class="desc">{item.bio ? stringLimit(item.bio, 85) : null}</div>
                                </div>
                                <div className="right-area">
                                    {/* {
                                        item.rating &&
                                        <div className="rating_area">
                                            <Rating
                                                ratingValue={item.rating ? parseInt(item.rating) : 0}
                                                showTooltip={true}
                                                initialValue={item.rating ? parseInt(item.rating) : 0}
                                                tooltipDefaultText="ABCD"
                                                tooltipArray={["0.1", "0.3", "5.7", "7.8", "EXC"]}
                                            />
                                        </div>
                                    } */}
                                    {
                                        item.rating &&
                                        <div className="rating_area">
                                            <span className="custome-star-rate">
                                                <i class="fas fa-star"></i> {item.rating}
                                            </span>
                                        </div>
                                    }
                                </div>
                            </div>
                            {item.email ? <div className="desc">
                                {item.email ? item.email : null}
                            </div> : null}
                            {item.state_name ? <div className="location_area">
                                <div className="location_left_area">
                                    <i className="fas fa-map-marker-alt"></i>
                                </div>
                                <div className="location_right_area">
                                    <p>{item.state_name ? item.state_name + (item.country_name ? ', ' + item.country_name : '') : ``}</p>
                                </div>
                            </div> : null}
                            {item.range_of_success_rate ? <div className="success_area">
                                <div className="success_left_area">
                                    <i className="fal fa-check-circle"></i>
                                </div>
                                <div className="success_right_area">
                                    <p>{item.range_of_success_rate}% Success Rate</p>
                                </div>
                            </div> : null}
                        </div>
                    </div>
                    <div className="bottom_area">
                        <Link onClick={() => {
                            props.handleApplyModal(item)
                        }}>
                            Contact Now
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
    isApply: state.ApplyNowModalReducer.isApply,
});
export default connect(mapStateToProps)(AgentGrid);