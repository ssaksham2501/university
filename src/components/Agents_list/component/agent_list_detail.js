import React, { useState } from "react";
import { Dropdown, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import photo from '../../../assets/images/top_uni3.png';
import { Rating } from 'react-simple-star-rating'

const Agent_list_detail = () => {
    const [list, setList] = useState(false);
    const [ratingValue, setRatingValue] = useState(0) // initial rating v
    const handleRating = (rate) => {
        setRatingValue(rate)
    }
    // Catch Rating value
    const handleReset = () => {
        setRatingValue(4)
        // other logic
    }

    return (
        <div className="listing_side_view">
            <div className="listing_side_main_set_wrap">
                <div className="subscribe">
                    <div className="left">
                        <Form>
                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                <Form.Control type="email" placeholder="Course, University" />
                            </Form.Group>
                        </Form>
                    </div>
                    <div className="right">
                        <Form>
                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                <Form.Control type="email" placeholder="Location" />
                            </Form.Group>
                            <div className="subscribe_btn">
                                <Link to='/'>
                                    <i className="fal fa-search"></i>
                                </Link>
                            </div>
                        </Form>
                    </div>
                </div>
                <div className="listing_head_wrap">
                    <div className="left">
                        <p>46 <span>listing found</span></p>
                    </div>
                    <div className="right">
                        <div className="sort_by_warp">
                            <ul>
                                <li>
                                    <div className="sort_by_swap">
                                        <div className="left">
                                            <p>Sort by</p>
                                        </div>

                                        <div className="right">
                                            <Dropdown>
                                                <Dropdown.Toggle id="dropdown-basic">
                                                    Popular
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </div>
                                        <div className="right">
                                            <Dropdown>
                                                <Dropdown.Toggle id="dropdown-basic">
                                                    Filters
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu>
                                                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                                                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Agent_list_detail;
