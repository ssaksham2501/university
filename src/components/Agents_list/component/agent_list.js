import React, { useState, useEffect } from "react";
import AgentGrid from "./agent_grid";


const Agent_info = () => {
    const [agents, setAgents] = useState(null);

    useEffect(() => {
        let data = [
            {
                name: "Cody Fisher 1",
                bio: "Lorem ipsum sit dolor amet dor lorem sit. Lorem sit dolor.",
                location: "Lorem ipsum sit, canada",
                success_rate: "90% Sucess Rate",
                rating: "4.0"
            },
            {
                name: "Cody Fisher 2",
                bio: "Lorem ipsum sit dolor amet dor lorem sit. Lorem sit dolor.",
                location: "Lorem ipsum sit, canada",
                success_rate: "90% Sucess Rate",
                rating: "4.0"
            },
            {
                name: "Cody Fisher 3",
                bio: "Lorem ipsum sit dolor amet dor lorem sit. Lorem sit dolor.",
                location: "Lorem ipsum sit, canada",
                success_rate: "90% Sucess Rate",
                rating: "4.0"
            },
            {
                name: "Cody Fisher 4",
                bio: "Lorem ipsum sit dolor amet dor lorem sit. Lorem sit dolor.",
                location: "Lorem ipsum sit, canada",
                success_rate: "90% Sucess Rate",
                rating: "4.0"
            },
            {
                name: "Cody Fisher 4",
                bio: "Lorem ipsum sit dolor amet dor lorem sit. Lorem sit dolor.",
                location: "Lorem ipsum sit, canada",
                success_rate: "90% Sucess Rate",
                rating: "4.0"
            },
            {
                name: "Manpreet",
                bio: "Lorem ipsum sit dolor amet dor lorem sit. Lorem sit dolor.",
                location: "Lorem ipsum sit, canada",
                success_rate: "90% Sucess Rate",
                rating: "4.0"
            }
        ];
        setAgents(data);
    }, []);

    return (
        <div>
            <div className="agent_list">
                <div className="row">
                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="uni_detail_wrap">
                            <div className="row">
                                {/* {
                                    agents !== null
                                    &&
                                    agents.map((item, i) => {
                                        return <AgentGrid
                                            data={item} />
                                    }
                                    )
                                } */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Agent_info;


