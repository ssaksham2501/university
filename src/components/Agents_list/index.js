import React from "react";
import Agent_info from "./component/agent_list";
import Agent_list_detail from "./component/agent_list_detail";

function Agent_list() {
    return (
        <div className="agent_list top_space">
            <div className="container">
                <div className="row">
                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="agent_detail_wrap">
                            <Agent_list_detail />
                            <Agent_info />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Agent_list;


