import React from "react";
import { Link } from "react-router-dom";
import footer_signup from "../../../assets/images/footer_signup.png"
function FooterImg() {
    return (
        <>
            <div className="footer_img">
                <div className="sub_container">
                    <div className="sub_row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="footer_img_area">
                                <img src={footer_signup} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default FooterImg;
