import React, { useState } from "react";
import Autocomplete from "react-autocomplete";
import Form from 'react-bootstrap/Form';
import { Link } from "react-router-dom";
import Select from 'react-select';
import FilterController from "../../../apis/controllers/filter.controller";

import AgentAuthorization from '../../../functions/agentSignup/agentAuthFun';
import { getFlagIcon, renderImage } from '../../../helpers/General';
import AutoComplete from "../../Layout/components/AutoComplete";
import SortableComponent from "../../sortableset";

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

function AgentSignUpTwo(props) {

    const [maxCountries, setMaxCountries] = useState(5);
    const {
        values,
        setValues,
        isError,
        setError,
        states,
        errMsg,
        isLoading,
        showHidePassword,
        showPasswordBlock,
        StepAgentOne,
        setPassword,
        handleStates,
        countrylist,
        profession,
        preferintake,
        universityListing,
        selectedColleges,
        setSelectedColleges,
        searchUniversities,
        setSearchUniversities
    } = AgentAuthorization(props);

    const onRemoveItem2 = (item, index) => {
        const items = values.two.university.filter(sitem => sitem.slug !== item.slug);
        handleStates('university', items, 'two');
        setSelectedColleges(items);
    };

    const [streamSearch, setStreamSearch] = useState(``);


    return (
        <>
            <div className="Sign_up_area agent top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form2">
                                <div className="heading_area">
                                    <h1>
                                        Sign up
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>
                                <div className="bar_wrap">
                                    <div className="top_bar">
                                        <div className="line_bar">
                                            <div className="bar_inn" style={{ width: "50%" }}></div>
                                            <div className="bar_wrap">
                                                <div className="bar_circle circle_1"><p>1</p></div>
                                                <div className="bar_circle circle_2"><p>2</p></div>
                                                <div className="bar_circle circle_3"><p>3</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Form
                                    noValidate
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        if (isLoading === false) {
                                            StepAgentOne.stepTwo();
                                        }
                                    }}
                                >
                                    {['radio'].map((type) => (
                                        <div key={`inline-${type}`} className="main">
                                            <div className="input_fields_area">
                                                <div className="high_education_area ul">
                                                    <h2>Choose your dream country </h2>
                                                    {
                                                        countrylist && countrylist.length > 6 && maxCountries < 1
                                                        &&
                                                        <div className="serach_area">
                                                            <Form.Group className="form-group">
                                                                <input
                                                                    className="form-control"
                                                                    type="text"
                                                                    autoFocus={true}
                                                                    id="county-search"
                                                                    placeholder="Search"
                                                                    value={streamSearch}
                                                                    onChange={(e) => {
                                                                        setStreamSearch(e.target.value);
                                                                    }}
                                                                />
                                                                <div className="seacrh">
                                                                    {
                                                                        streamSearch && streamSearch.trim() ?
                                                                            <a
                                                                                href=""
                                                                                onClick={(e) => {
                                                                                    e.preventDefault();
                                                                                    setStreamSearch(``);
                                                                                }}
                                                                            ><i className="fal fa-times"></i></a>
                                                                            :
                                                                            <a
                                                                                href=""
                                                                                onClick={(e) => {
                                                                                    e.preventDefault();
                                                                                    document.getElementById("county-search").focus();
                                                                                }}
                                                                            ><i className="fal fa-search"></i></a>
                                                                    }
                                                                </div>
                                                            </Form.Group>
                                                        </div>
                                                    }
                                                    <div className="country_shoed">
                                                        <div className='row'>
                                                            {
                                                                countrylist !== null
                                                                    ?
                                                                    countrylist.filter((item, i) => i < (countrylist && countrylist.length > 6 && maxCountries > 0 ? maxCountries : countrylist && countrylist.length) && (item.name.toLowerCase().indexOf(streamSearch.toLowerCase()) > -1)).map((country, index) => {
                                                                        return <div key={`countrylist` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                            <div
                                                                                className='radio_area'
                                                                            >
                                                                                <Form.Check
                                                                                    label={country.name}
                                                                                    name="country"
                                                                                    type={`checkbox`}
                                                                                    id={'country' + index}
                                                                                    checked={values.two.country.includes(country.id) ? true : false}
                                                                                    onChange={() => {
                                                                                        StepAgentOne.handleCountrySelect(country);
                                                                                    }}
                                                                                />
                                                                                <label
                                                                                    htmlFor={'country' + index}
                                                                                    className="image_area"
                                                                                >
                                                                                    <img src={getFlagIcon(country.short_name)} />
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    })
                                                                    :
                                                                    <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                                            }
                                                            {isError.country.message && <p className="text-danger text-center mt-3">{isError.country.message}</p>}
                                                            {
                                                                countrylist && countrylist.length > 6 && maxCountries > 0
                                                                &&
                                                                <div key={`countrylist-more`} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                    <div className='radio_area'>
                                                                        <div class="form-check">
                                                                            <label
                                                                                title=""
                                                                                for="country-more"
                                                                                class="form-check-label"
                                                                                onClick={() => {
                                                                                    setMaxCountries(0);
                                                                                }}
                                                                            >{countrylist && (countrylist.length - 5) + ' more'}</label>
                                                                        </div>
                                                                        {isError.country.message && <p className="text-danger text-center mt-3">{isError.country.message}</p>}
                                                                        <div
                                                                            className="image_area"
                                                                            onClick={() => {
                                                                                setMaxCountries(0);
                                                                            }}
                                                                        >
                                                                            <i style={{ fontSize: '50px' }} className="fas fa-chevron-square-down"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className='choose_university_area'>
                                                        <div className="select_area">
                                                            <div className="graduation_area">
                                                                <h2>Choose universities </h2>
                                                                <AutoComplete
                                                                    id="text-search"
                                                                    className="form-control search-autocomplete"
                                                                    placeholder="Search universities or colleges..."
                                                                    value={searchUniversities}
                                                                    blurOptions={true}
                                                                    onChange={async (val) => {
                                                                        setSearchUniversities(val);
                                                                        if (values.two.country && values.two.country.length > 0) {
                                                                            let resp = await new FilterController().gridSearch({
                                                                                q: val,
                                                                                grids: 1,
                                                                                countries: values.two.country
                                                                            });
                                                                            if (resp && resp.status) {
                                                                                return resp.search;
                                                                            }
                                                                        }
                                                                        return [];
                                                                    }}
                                                                    onSelect={StepAgentOne.handleUniversitySelect}
                                                                />
                                                                {isError.university.message && <p className="text-danger text-center mt-3">{isError.university.message}</p>}
                                                            </div>
                                                        </div>
                                                        <div className='university_area'>
                                                            
                                                                
                                                                {selectedColleges && selectedColleges.length > 0
                                                                    ?
                                                                        <SortableComponent
                                                                            items={selectedColleges}
                                                                            onUpdateList={(list) => {
                                                                                setSelectedColleges(list);
                                                                                handleStates(
                                                                                    "university",
                                                                                    list, 'two'
                                                                                );
                                                                            }}
                                                                            onRemoveItem={(item, index) => {
                                                                                onRemoveItem2(item, index);
                                                                            }}
                                                                        />
                                                                    
                                                                    :
                                                                    <div className='row align-items-stretch'><div className="">Empty State</div></div>
                                                                }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div className="btn_area">
                                                    {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                    <div className="left_area">
                                                        <Link to='/agent-signup'><button className="btn-primary-4" >Back</button></Link>
                                                    </div>
                                                    <div className="right_area">
                                                        <button className="btn-primary-2" type="submit">
                                                            {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                            &nbsp;Next</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </Form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default AgentSignUpTwo;