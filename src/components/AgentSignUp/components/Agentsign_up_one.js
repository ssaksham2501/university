import React from "react";
import Form from 'react-bootstrap/Form';
import Select from 'react-select';
import AgentAuthorization from "../../../functions/agentSignup/agentAuthFun";
import Selectable from "../../Layout/components/Selectable";

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

function AgentSignUpOne(props) {
    const { values, setValues, isError, setError, states, errMsg, isLoading, showHidePassword, showPasswordBlock, StepAgentOne, setPassword, handleStates } = AgentAuthorization(props);

    return (
        <>
            <div>
                <div className="Sign_up_area agent top_space">
                    <div className="container">
                        <div className="row">
                            <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                                <div className="sign_up_first_wrap form1">
                                    <div className="heading_area">
                                        <h1>
                                            Sign up
                                        </h1>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                            aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                    </div>
                                    <div className="bar_wrap">
                                        <div className="top_bar">
                                            <div className="line_bar">
                                                <div className="bar_inn" style={{ width: "0%" }}></div>
                                                <div className="bar_wrap">
                                                    <div className="bar_circle circle_1"><p>1</p></div>
                                                    <div className="bar_circle circle_2"><p>2</p></div>
                                                    <div className="bar_circle circle_3"><p>3</p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="heading_area ul">
                                        <h2>
                                            Please enter your details
                                        </h2>
                                    </div>
                                    <Form
                                        noValidate
                                        onSubmit={async (e) => {
                                            e.preventDefault();
                                            if (isLoading === false) {
                                                await StepAgentOne.stepOne();
                                            }
                                        }}
                                    >
                                        <div className="input_fields_area">
                                            <div className="row">
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                                                    <div className="input_fields_area">
                                                        <div className="row">
                                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="expect_education_area  ul">
                                                                    {/* <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Is Company</Form.Label>
                                                                        <div className="form-check">
                                                                            <input className="form-check-input" type="radio" id="flexRadioDefault1" checked={values && values.one && parseInt(values.one.isCompany) === 1 ? true : false} onChange={(e) => {
                                                                                handleStates(
                                                                                    "isCompany",
                                                                                    '1', 'one'
                                                                                );
                                                                            }} />
                                                                            <label className="form-check-label" for="flexRadioDefault1">
                                                                                Yes
                                                                            </label>
                                                                        </div>
                                                                        <div className="form-check">
                                                                            <input
                                                                                className="form-check-input" type="radio" id="flexRadioDefault2" checked={values && values.one && parseInt(values.one.isCompany) === 0 ? true : false} onChange={(e) => {
                                                                                    handleStates(
                                                                                        "isCompany",
                                                                                        '0', 'one'
                                                                                    );
                                                                                }} />
                                                                            <label className="form-check-label" for="flexRadioDefault2">
                                                                                No
                                                                            </label>
                                                                        </div>
                                                                    </Form.Group> */}
                                                                    <div className="campany_check mb-3">
                                                                        <div className="row">
                                                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
                                                                                <Form.Check
                                                                                    label='Individual'
                                                                                    name="stream"
                                                                                    className="ps-5"
                                                                                    type={`checkbox`}
                                                                                    id="flexRadioDefault1"
                                                                                    checked={values && values.one && parseInt(values.one.isCompany) === 0 ? true : false} onChange={(e) => {
                                                                                        handleStates(
                                                                                            "isCompany",
                                                                                            '0', 'one'
                                                                                        );
                                                                                    }}
                                                                                />
                                                                            </div>
                                                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6">
                                                                                <Form.Check
                                                                                    label='Campany'
                                                                                    name="stream"
                                                                                    className="ps-5"
                                                                                    type={`checkbox`}
                                                                                    id="flexRadioDefault2"
                                                                                    checked={values && values.one && parseInt(values.one.isCompany) === 1 ? true : false} onChange={(e) => {
                                                                                        handleStates(
                                                                                            "isCompany",
                                                                                            '1', 'one'
                                                                                        );
                                                                                    }}
                                                                                />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {values && values.one && parseInt(values.one.isCompany) === 1 ? <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="expect_education_area  ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Company name</Form.Label>
                                                                        <Form.Control type="text" placeholder="Enter your company name"
                                                                            onChange={(e) => {
                                                                                handleStates(
                                                                                    "companyName",
                                                                                    e.target.value, 'one'
                                                                                );
                                                                            }}
                                                                            value={values.one.companyName
                                                                                ? values.one.companyName
                                                                                : ``} />
                                                                        {!isError.companyName.isValid && isError.companyName.message && <p className="text-danger mt-2">{isError.companyName.message}</p>}
                                                                    </Form.Group>
                                                                </div>
                                                            </div> : null}
                                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                                <div className="expect_education_area  ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>First name</Form.Label>
                                                                        <Form.Control type="text" placeholder="Enter your first name"
                                                                            onChange={(e) => {
                                                                                handleStates(
                                                                                    "firstName",
                                                                                    e.target.value, 'one'
                                                                                );
                                                                            }}
                                                                            value={values.one.firstName
                                                                                ? values.one.firstName
                                                                                : ``} />
                                                                        {!isError.firstName.isValid && isError.firstName.message && <p className="text-danger mt-2">{isError.firstName.message}</p>}
                                                                    </Form.Group>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                                <div className="expect_education_area  ul">
                                                                    <Form>
                                                                        <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                            <Form.Label>Last name</Form.Label>
                                                                            <Form.Control type="text" placeholder="Enter your last name"
                                                                                onChange={(e) => {
                                                                                    handleStates(
                                                                                        "lastName",
                                                                                        e.target.value, 'one'
                                                                                    );
                                                                                }}
                                                                                value={values.one.lastName
                                                                                    ? values.one.lastName
                                                                                    : ``} />
                                                                            {!isError.lastName.isValid && isError.lastName.message && <p className="text-danger  mt-2">{isError.lastName.message}</p>}
                                                                        </Form.Group>
                                                                    </Form>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="expect_education_area  ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Email address</Form.Label>
                                                                        <Form.Control type="email" placeholder="person@example.com"
                                                                            onChange={StepAgentOne.handleEmail}
                                                                            value={values.one.email
                                                                                ? values.one.email
                                                                                : ``} />
                                                                        {!isError.email.isValid && isError.email.message && <p className="text-danger  mt-2">{isError.email.message}</p>}
                                                                    </Form.Group>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="select_area">
                                                                    <div className="graduation_area ul ">
                                                                        <Form.Label>State</Form.Label>
                                                                        <Selectable
                                                                            value={values.one.stateId}
                                                                            placeholder="Enter your location"
                                                                            onChange={(item) => {
                                                                                handleStates(
                                                                                    "stateId",
                                                                                    item, 'one'
                                                                                );
                                                                            }}
                                                                            options={states}
                                                                        />
                                                                        {!isError.stateId.isValid && isError.stateId.message && <p className="text-danger  mt-2">{isError.stateId.message}</p>}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="expect_education_area ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Pin code</Form.Label>
                                                                        <Form.Control type="text" placeholder="111001"
                                                                            onChange={(e) => {
                                                                                handleStates(
                                                                                    "pinCode",
                                                                                    e.target.value, 'one'
                                                                                );
                                                                            }}
                                                                            value={values.one.pinCode
                                                                                ? values.one.pinCode
                                                                                : ``} />
                                                                        {!isError.pinCode.isValid && isError.pinCode.message && <p className="text-danger  mt-2">{isError.pinCode.message}</p>}
                                                                    </Form.Group>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                                <div className="expect_education_area  ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Password</Form.Label>
                                                                        <Form.Control type="password" placeholder="******"
                                                                            onChange={(e) => {
                                                                                handleStates(
                                                                                    "password",
                                                                                    e.target.value, 'one'
                                                                                );
                                                                            }}
                                                                            value={values.one.password
                                                                                ? values.one.password
                                                                                : ``} />
                                                                        {!isError.password.isValid && isError.password.message && <p className="text-danger  mt-2">{isError.password.message}</p>}
                                                                    </Form.Group>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                                <div className="expect_education_area  ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Confirm password</Form.Label>
                                                                        <Form.Control type="password" placeholder="******"
                                                                            onChange={StepAgentOne.handleConfirmPassword}
                                                                            value={values.one.confirmPassword
                                                                                ? values.one.confirmPassword
                                                                                : ``} />
                                                                        {!isError.confirmPassword.isValid && isError.confirmPassword.message && <p className="text-danger  mt-2">{isError.confirmPassword.message}</p>}
                                                                    </Form.Group>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="expect_education_area  ul">
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Bio or Tag Line</Form.Label>
                                                                        <Form.Control type="text" placeholder="What types of services you provide?"
                                                                            maxLength={300}
                                                                            onChange={(e) => {
                                                                                handleStates(
                                                                                    "bio",
                                                                                    e.target.value, 'one'
                                                                                );
                                                                            }}
                                                                            value={values.one.bio
                                                                                ? values.one.bio
                                                                                : ``} />
                                                                        {!isError.bio.isValid && isError.bio.message && <p className="text-danger mt-2">{isError.bio.message}</p>}
                                                                        {/* <small className="text-muted float-right">{(300 - (values.one.bio && values.one.bio.trim() ? values.one.bio.trim().length : 0) )` characters remaining`}</small> */}
                                                                        <small className="text-muted float-right">{(300 - (values.one.bio && values.one.bio.trim() ? values.one.bio.trim().length : 0) ) + ` characters remaining`}</small>
                                                                    </Form.Group>
                                                                </div>
                                                            </div>
                                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div className="btn_area">
                                                                    {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                                    <div className="center_area">
                                                                        <button className="btn-primary-2" type="submit">
                                                                            {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                                            &nbsp;Next</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
}
export default AgentSignUpOne;