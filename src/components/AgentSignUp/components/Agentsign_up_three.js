import React, { useState, useRef } from "react";
import Form from 'react-bootstrap/Form';
import { Link } from "react-router-dom";
import Constant from "../../../apis/constants";
import ImageUploadController from "../../../apis/controllers/imageController";
import AgentAuthorization from '../../../functions/agentSignup/agentAuthFun';
import Selectable from "../../Layout/components/Selectable";
import UploaderBox from "../../Layout/components/UploaderBox";

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

function AgentSignUpThree(props) {

    const {
        values,
        setValues,
        isError,
        setError,
        states,
        errMsg,
        isLoading,
        showHidePassword,
        showPasswordBlock,
        StepAgentOne,
        setPassword,
        handleStates,
        countrylist,
        profession,
        preferintake,
        universityListing,
        range,
        range1,
        range2
    } = AgentAuthorization(props);




    return (
        <>
            <div className="Sign_up_area agent top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form3">
                                <div className="heading_area">
                                    <h1>
                                        Sign up
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>
                                <div className="bar_wrap">
                                    <div className="top_bar">
                                        <div className="line_bar">
                                            <div className="bar_inn" style={{ width: "100%" }}></div>
                                            <div className="bar_wrap">
                                                <div className="bar_circle circle_1"><p>1</p></div>
                                                <div className="bar_circle circle_2"><p>2</p></div>
                                                <div className="bar_circle circle_3"><p>3</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {
                                    range
                                        ?
                                        <Form
                                            noValidate
                                            onSubmit={(e) => {
                                                e.preventDefault();
                                                if (isLoading === false) {
                                                    StepAgentOne.stepThree();
                                                }
                                            }}
                                        >
                                            <div className="select_area">
                                                <div className="graduation_area">
                                                    <h2>{range && range.question1_title ? range.question1_title : null}</h2>
                                                    <Selectable
                                                        id={`successCases`}
                                                        value={values.three.successCases}
                                                    
                                                        onChange={(item) => {
                                                            handleStates(
                                                                "successCases",
                                                                item, 'three'
                                                            );
                                                        }}
                                                        options={range1}
                                                        
                                                    />
                                                    {!isError.successCases.isValid && isError.successCases.message && <p className="text-danger  mt-2">{isError.successCases.message}</p>}
                                                </div>
                                                <div className="graduation_area">
                                                    <h2>{range && range.question2_title ? range.question2_title : null}</h2>
                                                    <Selectable
                                                        id={`successRate`}
                                                        value={values.three.successRate}
                                                        onChange={(item) => {
                                                            handleStates(
                                                                "successRate",
                                                                item, 'three'
                                                            );
                                                        }}
                                                        options={range2}
                                                    />
                                                    {!isError.successRate.isValid && isError.successRate.message && <p className="text-danger  mt-2">{isError.successRate.message}</p>}
                                                </div>
                                                <div className="graduation_area">
                                                    <h2>Accreditation</h2>
                                                    <UploaderBox
                                                        onResponse={(response) => StepAgentOne.handleProof(response, 'accreditation')}
                                                    />
                                                    {!isError.accreditation.isValid && isError.accreditation.message && <p className="text-danger  mt-2">{isError.accreditation.message}</p>}
                                                    <div className="file-preview">
                                                        {
                                                            values.three.accreditation.map((a, index) => {
                                                                return <span className="badge bg-info">
                                                                    <i className="fas fa-paperclip"></i>
                                                                    {a.split('/').slice(-1).pop()}
                                                                    <a
                                                                        href=""
                                                                        className="clear"
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                            StepAgentOne.removeProof(index, a, 'accreditation');
                                                                        }}
                                                                    ><i className="fa fa-times"></i></a>
                                                                </span>
                                                            })
                                                        }
                                                    </div>
                                                </div>


                                                <div className="graduation_area">
                                                    <h2>Credentials</h2>
                                                    <UploaderBox
                                                        onResponse={(response) => StepAgentOne.handleProof(response, 'credentials')}
                                                    />
                                                    {!isError.credentials.isValid && isError.credentials.message && <p className="text-danger  mt-2">{isError.credentials.message}</p>}
                                                    <div className="file-preview">
                                                        {
                                                            values.three.credentials.map((a, index) => {
                                                                return <span className="badge bg-info">
                                                                    <i className="fas fa-paperclip"></i>
                                                                    {a.split('/').slice(-1).pop()}
                                                                    <a
                                                                        href=""
                                                                        className="clear"
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                            StepAgentOne.removeProof(index, a, 'credentials');
                                                                        }}
                                                                    ><i className="fa fa-times"></i></a>
                                                                </span>
                                                            })
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="btn_area">
                                                {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                                <div className="left_area">
                                                    <Link to='/agent-signup/step-2'><button className="btn-primary-4" >Back</button></Link>
                                                </div>
                                                <div className="right_area">
                                                    <button type="submit" className="btn-primary-2" >{isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                        &nbsp;Next</button>
                                                </div>
                                            </div>
                                        </Form>
                                        :
                                        <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
}
export default AgentSignUpThree