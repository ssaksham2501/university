import React, { useState } from "react";
import Form from 'react-bootstrap/Form';
import { Link } from "react-router-dom";
import FilterController from "../../apis/controllers/filter.controller";
import AgentEditPreferenceFun from "../../functions/agents/AgentEditPreferenceFun";
import AgentAuthorization from "../../functions/agentSignup/agentAuthFun";
import { getFlagIcon, renderImage } from "../../helpers/General";
import AutoComplete from "../Layout/components/AutoComplete";

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

function AgentEditPreference(props) {
    
    const [maxCountries, setMaxCountries] = useState(5);
    const {
        AgentEdit,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        handleStates,
        states,
        countrylist,
        universityListing,
        selectedColleges,
        setSelectedColleges,
        searchUniversities,
        setSearchUniversities
    } = AgentEditPreferenceFun(props);

    const onRemoveItem2 = (item, index) => {
        const items = values.university.filter(sitem => sitem.slug !== item.slug);
        handleStates('university', items);
        const itemm = selectedColleges.filter(sitems => sitems.slug !== item.slug);
        setSelectedColleges(itemm)
    };

    const [streamSearch, setStreamSearch] = useState(``);

    return (
        <>
            <div className="Sign_up_area agent top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
                            <div className="sign_up_first_wrap form2">
                                <div className="heading_area">
                                    <h1>
                                        Edit Preferences
                                    </h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus turpis tortor,
                                        aliquam a semper sed, pharetra id mi. Mauris cursus,</p>
                                </div>
                                <Form
                                    noValidate
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        if (isLoading === false) {
                                            AgentEdit.editPreferences();
                                        }
                                    }}
                                >
                                    {['radio'].map((type) => (
                                        <div key={`inline-${type}`} className="main">
                                            <div className="input_fields_area">
                                                <div className="high_education_area ul">
                                                    <h2>Choose your dream country </h2>
                                                    {
                                                        countrylist && countrylist.length > 6 && maxCountries < 1
                                                        &&
                                                        <div className="serach_area">
                                                                <Form.Group className="form-group">
                                                                    <input 
                                                                        className="form-control"
                                                                        type="text"
                                                                        autoFocus={true}
                                                                        id="county-search"
                                                                        placeholder="Search"
                                                                        value={streamSearch}
                                                                        onChange={(e) => {
                                                                            setStreamSearch(e.target.value);
                                                                        }}
                                                                    />
                                                                    <div className="seacrh">
                                                                        {
                                                                            streamSearch && streamSearch.trim() ?
                                                                                <a
                                                                                    href=""
                                                                                    onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        setStreamSearch(``);
                                                                                    }}
                                                                                ><i className="fal fa-times"></i></a>
                                                                            :
                                                                                <a
                                                                                    href=""
                                                                                    onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        document.getElementById("county-search").focus();
                                                                                    }}
                                                                                ><i className="fal fa-search"></i></a>
                                                                        }
                                                                    </div>
                                                                </Form.Group>
                                                        </div>
                                                    }
                                                    <div className="country_shoed">
                                                        <div className='row'>
                                                            {
                                                                countrylist !== null
                                                                ?
                                                                countrylist.filter((item, i) => i < (countrylist && countrylist.length > 6 && maxCountries > 0 ? maxCountries : countrylist && countrylist.length) && (item.name.toLowerCase().indexOf(streamSearch.toLowerCase()) > -1)).map((country, index) => {
                                                                    return <div key={`countrylist` + index} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                        <div
                                                                            className='radio_area'
                                                                        >
                                                                            <Form.Check
                                                                                label={country.name}
                                                                                name="country"
                                                                                type={`checkbox`}
                                                                                id={'country' + index}
                                                                                checked={values.country.includes(country.id) ? true : false}
                                                                                onChange={() => {
                                                                                    AgentEdit.handleCountrySelect(country);
                                                                                }}
                                                                            />
                                                                            <label
                                                                                htmlFor={'country' + index}
                                                                                className="image_area"
                                                                            >
                                                                                <img src={getFlagIcon(country.short_name)} />
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                })
                                                                :
                                                                    <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                                            }
                                                                {isError.country.message && <p className="text-danger text-center mt-3">{isError.country.message}</p>}
                                                            {
                                                                countrylist && countrylist.length > 6 && maxCountries > 0
                                                                &&
                                                                <div key={`countrylist-more`} className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6'>
                                                                    <div className='radio_area'>
                                                                        <div class="form-check">
                                                                            <label
                                                                                title=""
                                                                                for="country-more"
                                                                                class="form-check-label"
                                                                                onClick={() => {
                                                                                    setMaxCountries(0);
                                                                                }}
                                                                            >{countrylist && (countrylist.length - 5) + ' more'}</label>
                                                                        </div>
                                                                        {isError.country.message && <p className="text-danger text-center mt-3">{isError.country.message}</p>}
                                                                        <div
                                                                            className="image_area"
                                                                            onClick={() => {
                                                                                setMaxCountries(0);
                                                                            }}
                                                                        >
                                                                            <i style={{ fontSize: '50px' }} className="fas fa-chevron-square-down"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className='choose_university_area'>
                                                        <div className="select_area">
                                                            <div className="graduation_area">
                                                                <h2>Choose universities </h2>
                                                                <AutoComplete
                                                                    id="text-search"
                                                                    className="form-control search-autocomplete"
                                                                    placeholder="Search universities or colleges..."
                                                                    blurOptions={true}
                                                                    value={searchUniversities}
                                                                    onChange={async (val) => {
                                                                        setSearchUniversities(val);
                                                                        if (values.country && values.country.length > 0) {
                                                                            let resp = await new FilterController().gridSearch({
                                                                                q: val,
                                                                                grids: 1,
                                                                                countries: values.country
                                                                            });
                                                                            if (resp && resp.status) {
                                                                                return resp.search;
                                                                            }
                                                                        }
                                                                        return [];
                                                                    }}
                                                                    onSelect={AgentEdit.handleUniversitySelect}
                                                                />
                                                                {isError.university.message && <p className="text-danger text-center mt-3">{isError.university.message}</p>}
                                                            </div>
                                                        </div>
                                                        <div className='university_area'>
                                                            <div className='row'>
                                                                {selectedColleges && selectedColleges.length > 0
                                                                    ?
                                                                        selectedColleges.map((item, index) => (
                                                                        <div key={index} className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                                            <div className='choose_university_area'>
                                                                                <div className='inner_box_area'>
                                                                                    <div className='innerr_image_area'>
                                                                                        <div className='image_area'>
                                                                                            <img src={item.logo ? renderImage(item.logo) : null} />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className='info_area'>
                                                                                        <div className="unisersity_name">
                                                                                            <p>{item.label}</p>
                                                                                        </div>
                                                                                        <div className="uni_location_area">
                                                                                            <a href="" onClick={(e) => e.preventDefault()}>
                                                                                                <div className="left_area">
                                                                                                    <i class="fal fa-map-marker-alt"></i>
                                                                                                </div>
                                                                                                <div className="right_area_in">
                                                                                                    {item.country_name}
                                                                                                </div>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div className='cross_area'>
                                                                                            <Link to="" onClick={(e) => {
                                                                                                e.preventDefault();
                                                                                                onRemoveItem2(item, index);
                                                                                            }}>
                                                                                                <i class="fal fa-times"></i>
                                                                                            </Link>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            </div>))
                                                                    :
                                                                        <div className="">Empty State</div>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div className="btn_area">
                                                    <div className="left_area">
                                                        <Link to='/my-profile'><button className="btn-primary-4" >Back</button></Link>
                                                    </div>
                                                    <div className="right_area">
                                                        <button className="btn-primary-2" type="submit">
                                                            {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                            &nbsp;Save changes</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </Form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default AgentEditPreference;