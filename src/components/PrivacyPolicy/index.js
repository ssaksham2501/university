import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, withRouter } from "react-router-dom";
import PagesController from "../../apis/controllers/pages.controller";


function PrivacyPolicy() {
    const [page, setPage] = useState(null);
    const [content, setContent] = useState(null);

    useEffect(() => {
        getprivacypolicy()
     
    }, []);

    const  getprivacypolicy = async () => {
        let response = await new PagesController().privacyPolicy();
        if(response.status) {
            setPage(response.page.sections)
            setContent(response.page.table_of_content) 
        }
        else {
            console.log('Error while fetching api data.')
        }
    }
    
    return (
        <div>
            <div className="cms_banner_area top_space">
                <div className="bg_area">
                    <div className="heading_area">
                        <h1>
                            {page !== null ? page.title : ``}
                        </h1>
                    </div>
                </div>
            </div>
            <div className="privacy_section top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="privacy_wrap">
                                <div className="row">
                                    <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                        <div className="detail_area">
                                            {
                                                page !== null
                                                &&
                                                page.map((sections, i) => {
                                                    return <><h3>{sections.title}</h3><div dangerouslySetInnerHTML=
                                                    {{ __html: sections.description }} /> </>
                                                })
                                            }

                                            {/* <ul>
                                                <li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li>
                                                <li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li>
                                            </ul>
                                            <ol>
                                                <li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li>
                                                <li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li><li>ffhfgh</li>
                                                <li>
                                                    dfgdfg
                                                </li>
                                            </ol> */}

                                        </div>
                                    </div>
                                    <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                        <div className="right_wrap">
                                            <div className="black_box_area">
                                                <h4>Table of content</h4>
                                                {
                                                    content !== null
                                                    &&
                                                    content.map((item, i) => {
                                                        return <Link>
                                                            <p>{item.title}</p>
                                                        </Link>
                                                    })
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PrivacyPolicy;