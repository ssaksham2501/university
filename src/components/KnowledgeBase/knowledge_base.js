import React, { useState, useEffect } from "react";
import Form from 'react-bootstrap/Form'
import { Link } from "react-router-dom";
import CategoriesList from "./knowledge_base_CategoryList";
import KnowledgebaseData from "../../apis/controllers/knowledgebase.controller";
import { stringLimit, stripTags } from "../../helpers/General";

function Knowledgebase() {
    const [listData, setListData] = useState([]);
    const [searchData, setSearchData] = useState('')
    const [isLoading, setLoading] = useState(false);
    const [categorySlugs, setCategorySlugs] = useState(false);


    useEffect(() => {
        window.scrollTo(0, 0);
        collegListdata();
    }, [])

    const collegListdata = async (searchData, categorySlug) => {
        if (!isLoading) {
            setLoading(true);
            let response = await KnowledgebaseData.CollegesList({
                search: searchData,
                categories: categorySlug,
            })
            setLoading(false);
            if (response && response.status) {
                setListData(response.articals);
            }
        }
    }

    const getCategorySlug = async (slugs) => {
        setSearchData(``);
        setCategorySlugs(slugs);
        await collegListdata(``, slugs);
    }

    const getInputData = async (e) => {
        e.preventDefault();
        await collegListdata(searchData)
    }


    // console.log(inputData, 'seach data')
    return (
        <div>
            <div className="cms_banner_area top_space">
                <div className="bg_area ul">
                    <div className="heading_area">
                        <h1>
                            How can we help?
                        </h1>
                        <h2>Ask a question or describe a problem you're having</h2>
                    </div>
                </div>
                <div className="searching_area">
                    <div className="Search_bar">
                        <Form>
                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                <Form.Control type="search" placeholder="Search for knowlegde base " style={{ 'outline': 'none' }} onChange={(event) => setSearchData(event.target.value)} />
                            </Form.Group>
                            <div className="subscribe_btn">
                                <button className="btn-primary-2" type="submit" onClick={getInputData}>
                                    Search
                                </button>
                            </div>
                        </Form>
                    </div>
                </div>
                <div className="slider_area">
                    <div className="light_bg_area">
                        <CategoriesList
                            getCategorySlug={getCategorySlug}
                            categorySlugs={categorySlugs}
                        />
                    </div>
                </div>
            </div>

            <div className="knowledge_base_section top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="knowledge_base_wrap">
                                <div className="detail_area">
                                    {isLoading
                                        ?
                                        <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                        :
                                        <>
                                            {
                                                listData && listData.length > 0
                                                    ?
                                                    listData.map((curData, index) => {
                                                        // console.log(curData.categories, 'data')
                                                        return (
                                                            <div className="border_box_area shadow" key={index}>
                                                                <div className="tags_area">
                                                                    <ul>
                                                                        {
                                                                            curData.categories.map((data, index) => {
                                                                                return (
                                                                                    <li key={index}>
                                                                                        <p>{data.title}</p>
                                                                                    </li>
                                                                                )
                                                                            })
                                                                        }

                                                                    </ul>
                                                                </div>
                                                                <h6>{curData.title}</h6>
                                                                <p>{stringLimit(stripTags(curData.description), 200)} </p>
                                                                <div className="read_more">
                                                                    <Link to={{
                                                                        pathname: `/knowledge-base/${curData.slug}`,
                                                                    }}>
                                                                        <p>Read More <i className="far fa-arrow-right"></i></p>
                                                                    </Link>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                    :
                                                    <div className="border_box_area shadow">
                                                        <div className="tags_area">
                                                            <p>No Records Found!</p>
                                                        </div>
                                                    </div>
                                            }
                                        </>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Knowledgebase;