import React, { useState, useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import KnowledgebaseData from "../../apis/controllers/knowledgebase.controller";
import { stringLimit, stripTags, renderImage, toHtml } from '../../helpers/General'


function Knowledgebasedetail() {
    const [data, setData] = useState([])
    const [isLoading, setLoading] = useState(false)
    const [categories, setCategories] = useState([])
    const [relatedArticals, setRelatedArticals] = useState([])
    const location = useLocation()
    const history = useHistory()


    useEffect(() => {
        window.scrollTo(0, 0);
        knowledgebaseDetail()
        getCategories()
    }, [location.pathname])

    const knowledgebaseDetail = async () => {
        let slug = location.pathname.split('/');
        slug = slug.filter((item) => {
            return item ? true : false;
        });
        // console.log(slug, `slug from detail page`);
        slug = slug[slug.length - 1];
        if (!isLoading) {
            setLoading(true)
            let response = await KnowledgebaseData.KnowledgeBaseDetail(slug)

            setLoading(false)
            if (response && response.status) {
                setData(response.articals)
                setRelatedArticals(response.relatedArticals)
            }
        }

    }

    const getKnowledgeBaseDetail = (slug) => {
        return history.push(`/knowledge-base/${slug}`)
    }


    const getCategories = async () => {
        let response = await KnowledgebaseData.CategoriesList()
        setCategories(response.articals)
    }

    console.log(data, "find img")
    return (
        <div>
            <div className="knowledge_base_detail_section top_space">
                <div className="container">
                    <div className="row">

                        {
                            isLoading ?
                                <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                :
                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div className="knowledge_wrap">
                                        <div className="row">
                                            <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                                                <div className="left_wrap">
                                                    <div className="top_detail_area">
                                                        <div className="tags_area">
                                                            <ul>
                                                                {
                                                                    categories.map((previousdata, index) => {
                                                                        return (
                                                                            <li key={index}>
                                                                                <p>{previousdata.title}</p>
                                                                            </li>

                                                                        )
                                                                    })
                                                                }

                                                            </ul>
                                                        </div>

                                                        <h1>{data && data.title && data.title}</h1>
                                                      
                                                        <p>{data && data.description && toHtml(data.description)}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                                                <div className="right_wrap">
                                                    <div className="detail_area">
                                                        <h1>Related topics</h1>
                                                        {
                                                            relatedArticals.map((curData, index) => {
                                                                return (
                                                                    <div className="border_box_area" key={index}>
                                                                        <h6>{curData.title}</h6>
                                                                        <div>
                                                                        <p>{stringLimit(stripTags(curData.description), 100)} </p></div>
                                                                        <div className="read_more">
                                                                            <p style={{ 'cursor': "pointer" }} onClick={() => getKnowledgeBaseDetail(curData.slug)}  >Read More <i className="far fa-arrow-right" ></i></p>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })
                                                        }

                                                    </div>
                                                    <div className="shape"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Knowledgebasedetail;