
import React, { useState, useEffect } from 'react';
import { ScrollMenu, VisibilityContext } from 'react-horizontal-scrolling-menu';
import KnowledgebaseData from '../../apis/controllers/knowledgebase.controller';



function CategoriesList(props) {
    const [categorySlug, setCategorySlug] = useState(props.categorySlugs ? props.categorySlugs : [])
    const [data, setData] = useState([])


    useEffect(() => {
        getCategories()
    }, [])

   
    const getCategories = async () => {
        let response = await KnowledgebaseData.CategoriesList()
        setData(response.articals)
    } 

    const handleClick = (slug, id) => {
        
        let indexofSlug = categorySlug.indexOf(slug)
        if(indexofSlug >= 0){
            categorySlug.splice(indexofSlug, 1)
            setCategorySlug(categorySlug)
            props.getCategorySlug(categorySlug)
        }
        else {
            setCategorySlug([...categorySlug, slug])
            props.getCategorySlug([...categorySlug, slug])
        }
        
    };

      

    return (
        <div>
            <div className="scroll_slider_area">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="scroll_wrap" style={{ 'alignItems': 'inline' }}>
                                <ScrollMenu LeftArrow={LeftArrow} RightArrow={RightArrow}>
                                    {
                                        data.map((curdata, index) => {
                                            // console.log(curdata.slug, 'categry data')
                                            return (
                                                <div className="card" key={index}>
                                                    <div className="detail_area">
                                                            <button
                                                            onClick={() => {handleClick(curdata.slug, curdata.id)} }
                                                            className={`btn-primary-5` + (categorySlug && categorySlug.length > 0 && categorySlug.includes(curdata.slug)  ? ' active' : '') }
                                                            >
                                                            {curdata.title}
                                                           </button> 
                                                        </div>
                                                </div>
                                            )
                                        })
                                    }
                                </ScrollMenu>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
    function LeftArrow() {
        const { scrollPrev } = React.useContext(VisibilityContext);

        return (
            <div className="left_arrow">
                <div className='arrow_bg' onClick={() => scrollPrev()} >

                </div>

            </div>
        );
    }

    function RightArrow() {
        const { scrollNext } = React.useContext(VisibilityContext);

        return (
            <div className="right_arrow">
                <div className='arrow_bg' onClick={() => scrollNext()} style={{ color: "red" }} >

                </div>

            </div>

        );
    }
}



export default CategoriesList;