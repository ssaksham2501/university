import React from "react";
import SignUpTwo from "../SignUp/components/sign_up_two";

function SignUp2() {
  return (
    <div>
      <div className="Sign_up_area top_space">
        <div className="container">
          <div className="row">
            <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
               <SignUpTwo />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUp2;

