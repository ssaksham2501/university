import React, { useState } from "react";
import { Link, withRouter } from "react-router-dom";
import bgimg from "../../assets/images/agent_process_bg.png"
import agentperson from "../../assets/images/agent_process_banner.png"
import processleftbg from "../../assets/images/process_left_bg.png"
import processrightbg from "../../assets/images/process_right_bg.png"
import agentpro1 from "../../assets/images/agent_pro1.png"
import agentpro2 from "../../assets/images/agent_pro2.png"
import agentpro3 from "../../assets/images/agent_pro3.png"
import agentpro4 from "../../assets/images/agent_pro4.png"
import agentpro5 from "../../assets/images/agent_pro5.png"
import AboutHeading from "../Home/components/aboutheading";
import getstartedbg from "../../assets/images/get_started_bg.png";
import getbg from "../../assets/images/get_bg.png";
import joinus from "../../assets/images/join_us.png";

function Studentprocess() {
    return (
        <div>
            <div className="agent_process_section top_space">
                <div className="banner_img">
                    <div className="bg_image_area">
                        <img src={bgimg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 order-lg-0 order-2">
                                        <div className="agent_left_detail">
                                            <div className="agent_detail">
                                                <h1>Lorem ipsum dolor amet sit dor</h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
                                                    purus sit amet luctus venenatis, lectus magna</p>
                                                <div className="btn_area">
                                                    <div className="left_area">
                                                        <Link to='/'><button className="btn-primary-2" >Get started</button></Link>
                                                    </div>
                                                    <div className="right_area">
                                                        <Link to='/'> <button className="btn-primary-4" ><i className="fas fa-play"></i>Watch</button></Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div className="agent_right_detail">
                                            <div className="image_area">
                                                <img src={agentperson} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="agent_process_left_section">
                <div className="banner_img">
                    <div className="bg_image_area">
                        <img src={processleftbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_left_img_detail">
                                            <div className="image_area">
                                                <img src={agentpro1} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_right_text_detail">
                                            <div className="heading_area  el">
                                                <AboutHeading title="Lorem ipsum" description="Lorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                                                  lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur 
                                                  adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                                                  fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                                                  elit ut aliquam, purus sit amet luctus venenatis.Lectus magna fringilla urna, 
                                                  porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                  purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="agent_process_right_section">
                <div className="banner_img">
                    <div className="bg_image_area">
                        <img src={processrightbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 order-lg-0 order-2">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 order-lg-0 order-2">
                                        <div className="agent_left_text_detail ">
                                            <div className="heading_area el ">
                                                <AboutHeading title="Lorem ipsum" description="Lorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                                                  lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur 
                                                  adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                                                  fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                                                  elit ut aliquam, purus sit amet luctus venenatis.Lectus magna fringilla urna, 
                                                  porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                  purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_right_img_detail">
                                            <div className="image_area">
                                                <img src={agentpro2} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="agent_process_left_section">
                <div className="banner_img">
                    <div className="bg_image_area">
                        <img src={processleftbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_left_img_detail_four">
                                            <div className="image_area">
                                                <img src={agentpro3} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_right_text_detail">
                                            <div className="heading_area el text-start">
                                                <AboutHeading title="Lorem ipsum" description="Lorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                                                  lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur 
                                                  adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                                                  fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                                                  elit ut aliquam, purus sit amet luctus venenatis.Lectus magna fringilla urna, 
                                                  porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                  purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="agent_process_right_section">
                <div className="banner_img">
                    <div className="bg_image_area">
                        <img src={processrightbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 order-lg-0 order-2">
                                        <div className="agent_left_text_detail">
                                            <div className="heading_area el ">
                                                <AboutHeading title="Lorem ipsum" description="Lorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                                                  lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur 
                                                  adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                                                  fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                                                  elit ut aliquam, purus sit amet luctus venenatis.Lectus magna fringilla urna, 
                                                  porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                  purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                        <div className="agent_right_img_detail_five">
                                            <div className="image_area">
                                                <img src={agentpro4} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="agent_process_left_section">
                <div className="banner_img">
                    <div className="bg_image_area">
                        <img src={processleftbg} />
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="agent_process_wrap">
                                <div className="row">
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_left_img_detail_four">
                                            <div className="image_area">
                                                <img src={agentpro5} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                        <div className="agent_right_text_detail">
                                            <div className="heading_area el text-start">
                                                <AboutHeading title="Lorem ipsum" description="Lorem ipsum dolor sit amet,
                                                 consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis,
                                                  lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur 
                                                  adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                                                  fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                                                  elit ut aliquam, purus sit amet luctus venenatis.Lectus magna fringilla urna, 
                                                  porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                  purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="join_us_section_agent top-space">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="get_started_area">
                                <div className="background_area">
                                    <div className="bg_img_area">
                                        <img src={getstartedbg} />
                                    </div>
                                    <div className="inner_bg_area">
                                        <div className="bg_img">
                                            <img src={getbg} />
                                            <div className="inner_detail">
                                                <div className="info_area">
                                                    <div className="left_area">
                                                        <h2>Join us</h2>
                                                        <p>Lectus magna fringilla urna, porttitor Lectus magna
                                                            fringilla urna, porttitorLorem ipsum dolor sit amet.</p>
                                                        <div className="btn_area">
                                                            <Link to='/#'>
                                                                <button className="btn-primary-4">Register now</button>
                                                            </Link>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="person_img">
                                                        <img src={joinus} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Studentprocess;