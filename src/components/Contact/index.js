import React from "react";
import Banner from "./components/banner";
import Form from "./components/form";
import Services from "./components/services";

function Contact() {
    return (
      <div>
          <Banner />
          <Services />
          <Form />
      </div>
    );
}

export default Contact;
