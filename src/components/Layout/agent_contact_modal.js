import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import agentinfo from "../../assets/images/agent_info.png"
import Form from 'react-bootstrap/Form';
import Select from 'react-select';
import { getAge, intToString, renderImage, renderNoAvatar, stringLimit } from '../../helpers/General';
import AuthController from '../../apis/controllers/auth.controller';
import LeadsController from '../../apis/controllers/leads.controller';
import Selectable from './components/Selectable';
import Validation from '../../helpers/vaildation';
import ModalController from '../../apis/controllers/modal.controller';

function AgentContactModal(props) {
    const [user, setUser] = useState(null);
    const [errMsg, setErrMsg] = useState(``);
    const [loading, setLoading] = useState(false);
    const [mounting, setMounting] = useState(true);
    const [courses, setCourses] = useState([]);
    const [selectedCourses, setSelectedCourses] = useState([]);
    const [message, setMessage] = useState(``);
    const [completed, setCompleted] = useState(null);
    
    
    const handleClose = () => props.close();

    useEffect(() => {
        console.log(props);
        init();
    }, [])

    const init = async () => {
        let response = await new LeadsController().getApplicationData({
            type: props.data.type,
            university: props.data && props.data.info ? props.data.info.slug : null,
            agent: props.data && props.data.agent ? props.data.agent.user_name : null
        });
        if (response && response.status && response.me) {
            setMounting(false);
            setUser(response.me);
            setCourses(response.courses);
        }
    }

    const submitApplication = async () => {
        if (message.trim())
        {
            if (!loading) {
                setErrMsg(``);
                setLoading(true);
                let data = {
                    message: message,
                    type: props.data && props.data.type,
                    university: props.data && props.data.info ? props.data.info.slug : null,
                    agent: props.data && props.data.agent ? props.data.agent.user_name : null,
                    courses: selectedCourses
                }
                let response = await new LeadsController().submitApplication(data);
                if (response && response.status) {
                    setCompleted(response.message);
                }
                else {
                    setCompleted(null);
                    setErrMsg(response.message);
                }
                setLoading(false);
            }
        }
        else
        {
            setErrMsg('Please write a message explaining your requirement.')
        }
    }

    return (
        <>
            <Modal show={props.show} className="application_student apply  ps-0" size="lg" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>{props.data && props.data.modalTitle}</Modal.Title>
                </Modal.Header>
                {
                    !completed
                    &&
                    <Modal.Body>
                        {
                            props.data
                                ?
                                <div className="agent_contact_area">
                                    <div className="bg_area agent">
                                        <div className="top_area">
                                            <div className="agent_info_area">
                                                <div className={`uni_image_area ` + (props.data && props.data.agent && props.data.type == 'agent' && ' t-avatar') }>
                                                    <div className="image_area">
                                                        {
                                                            props.data && props.data.info && props.data.info.logo && props.data.type == 'university'
                                                            &&
                                                            <img src={renderImage(props.data.info.logo.original, 'medium')} />
                                                        }
                                                        
                                                        {
                                                            props.data && props.data.agent && props.data.type == 'agent'
                                                            &&
                                                            <img src={props.data.agent && props.data.agent.image ? renderImage(props.data.agent.image, 'medium') : renderNoAvatar( (props.data.agent && props.data.agent.first_name ? props.data.agent.first_name : ``) + ` ` + (props.data.agent && props.data.agent.last_name ? props.data.agent.last_name : ``))} />
                                                        }
                                                    </div>
                                                </div>
                                                {
                                                    props.data.type == 'university'
                                                    ?
                                                        <div className="uni_info_area">
                                                            {
                                                                props.data && props.data.info && props.data.info.rating_user_count > 0
                                                                &&
                                                                <div className='rating_area mb-2'>
                                                                    <div className="rating_met">
                                                                        <ul>
                                                                            <li>
                                                                                <i className={`fas fa-star` + ((props.data && props.data.info && props.data.info.rating >= 0.1) || (props.data && props.data.info && props.data.info.rating >= 1) ? ' active' : '')}></i>
                                                                            </li>
                                                                            <li>
                                                                                <i className={`fas fa-star` + (props.data && props.data.info && props.data.info.rating >= 2 ? ' active' : '')}></i>
                                                                            </li>
                                                                            <li>
                                                                                <i className={`fas fa-star` + (props.data && props.data.info && props.data.info.rating >= 3 ? ' active' : '')}></i>
                                                                            </li>
                                                                            <li>
                                                                                <i className={`fas fa-star` + (props.data && props.data.info && props.data.info.rating >= 4 ? ' active' : '')}></i>
                                                                            </li>
                                                                            <li>
                                                                                <i className={`fas fa-star` + (props.data && props.data.info && props.data.info.rating >= 5 ? ' active' : '')}></i>
                                                                            </li>
                                                                        </ul>
                                                                        {
                                                                            props.data && props.data.info && props.data.info.rating_user_count > 0
                                                                                ?
                                                                                <span className="show_rating">
                                                                                    <i class="fas fa-user pe-1"></i> {intToString(props.data && props.data.info && props.data.info.rating_user_count)}
                                                                                </span>
                                                                                :
                                                                                <span className="show_rating">Be first to write a review</span>
                                                                        }
                                                                    </div>
                                                                </div>
                                                            }
                                                            <div className="uni_name">
                                                                <h4>{props.data && props.data.info && props.data.info.title}</h4>
                                                            </div>
                                                            <div className="des_area">
                                                                <p>{stringLimit(props.data && props.data.info && props.data.info.description, 110)}</p>
                                                            </div>
                                                            <div className="uni_location_area">
                                                                <div className="left_area">
                                                                    <i class="fal fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="right_area_in">
                                                                    {props.data && props.data.info && props.data.info.address}
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    :   
                                                        <div className="uni_info_area">
                                                        {
                                                            props.data && props.data.agent && props.data.agent.rating_user_count > 0
                                                            &&
                                                            <div className='rating_area mb-2'>
                                                                <div className="rating_met">
                                                                    <ul>
                                                                        <li>
                                                                            <i className={`fas fa-star` + ((props.data && props.data.agent && props.data.agent.rating >= 0.1) || (props.data && props.data.agent && props.data.agent.rating >= 1) ? ' active' : '')}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star` + (props.data && props.data.agent && props.data.agent.rating >= 2 ? ' active' : '')}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star` + (props.data && props.data.agent && props.data.agent.rating >= 3 ? ' active' : '')}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star` + (props.data && props.data.agent && props.data.agent.rating >= 4 ? ' active' : '')}></i>
                                                                        </li>
                                                                        <li>
                                                                            <i className={`fas fa-star` + (props.data && props.data.agent && props.data.agent.rating >= 5 ? ' active' : '')}></i>
                                                                        </li>
                                                                    </ul>
                                                                    {
                                                                        props.data && props.data.agent && props.data.agent.rating_user_count > 0
                                                                            ?
                                                                            <span className="show_rating">
                                                                                <i class="fas fa-user pe-1"></i> {intToString(props.data && props.data.agent && props.data.agent.rating_user_count)}
                                                                            </span>
                                                                            :
                                                                            <span className="show_rating">Be first to write a review</span>
                                                                    }
                                                                </div>
                                                            </div>
                                                        }
                                                        <div className="uni_name">
                                                            {
                                                                props.data && props.data.agent && props.data.agent.is_company
                                                                ? <h4>{props.data && props.data.agent ? props.data.agent.company_name : ``}</h4>
                                                                : <h4>{props.data && props.data.agent ? props.data.agent.first_name + (props.data.agent.last_name ? ` ` + props.data.agent.last_name : `` ) : ``}</h4>
                                                            }
                                                            
                                                        </div>
                                                        <div className="des_area">
                                                            <p>{stringLimit(props.data && props.data.agent && props.data.agent.bio, 110)}</p>
                                                        </div>
                                                        <div className="uni_location_area">
                                                            <div className="left_area">
                                                                <i class="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area_in">
                                                                {props.data && props.data.agent && props.data.agent.state_name ? props.data.agent.state_name + (props.data.agent.country_name ? ', ' + props.data.agent.country_name : '') : ``}
                                                            </div>
                                                        </div>
                                                            {props.data && props.data.agent.range_of_success_rate ? <div className="uni_location_area">
                                                                <div className="left_area">
                                                                    <i className="fal fa-check-circle"></i>
                                                                </div>
                                                                <div className="right_area_in">
                                                                    <p>{props.data.agent.range_of_success_rate}% Success Rate</p>
                                                                </div>
                                                            </div> : ``}
                                                    </div>           
                                                }
                                            </div>
                                        </div>
                                        {
                                            mounting
                                                ?
                                                <div className={`bottom_area`}><p className="text-center"><i className="fa fa-spin fa-spinner"></i></p></div>
                                                :
                                                <div className={`bottom_area` + (props.data && props.data.type === 'agent' && props.data.info ? ` univeristy` : ``)}>
                                                    {
                                                        props.data && props.data.type === 'agent' && props.data.info
                                                            ?
                                                            <div className="agent_info">
                                                                <div className='meta_set'>
                                                                    <div className='left_it'>
                                                                        <h3>{user && user.first_name} {user && user.last_name}</h3>
                                                                        <p><small><i class="fal fa-map-marker-alt"></i> {user && user.state_name && user.country ? user.state_name + ', ' : (user && user.state_name)}{user && user.country && user.country.short_name}</small></p>
                                                                    </div>
                                                                    <div className='right_it'>
                                                                        <div className="edit_area">
                                                                            <Link to='/student-change-preference-step-1'>
                                                                                <i class="far fa-pen"></i>Edit
                                                                            </Link>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="inner_detail">
                                                                    <p className="text-muted">Applicant Details</p>
                                                                    <p>Education: <strong>{user && user.education && user.education.title} ({user && user.year_of_graduation})</strong></p>
                                                                    <p>Expected or Gained Percentage: <strong>{user && user.gained_percentage && user.gained_percentage}%</strong></p>
                                                                    <p>Prefered Intake: <strong>{user && user.prefered_intake && (user.prefered_intake.title + ' ' + user.prefered_intake.year)}</strong></p>
                                                                    {
                                                                        user && user.ielts > 0 && user.ielts_score > 0
                                                                        &&
                                                                        <p>IELTS Score: <strong>{(user.ielts_score * 1).toFixed(1)} Bands</strong></p>
                                                                    }
                                                                    {
                                                                        user && user.tofel > 0 && user.tofel_score > 0
                                                                        &&
                                                                        <p>TOEFL Score: <strong>{(user.tofel_score * 1).toFixed(1)}</strong></p>
                                                                    }
                                                                    {
                                                                        user && user.pte > 0 && user.pte_score > 0
                                                                        &&
                                                                        <p>PTE Score: <strong>{(user.pte_score * 1).toFixed(1)}</strong></p>
                                                                    }
                                                                </div>
                                                            </div>
                                                            :
                                                            <div className="agent_info">
                                                                <h3>{user && user.first_name} {user && user.last_name}<br /></h3>
                                                                <p><small><i class="fal fa-map-marker-alt"></i> {user && user.state_name && user.country ? user.state_name + ', ' : (user && user.state_name)}{user && user.country && user.country.short_name}</small></p>
                                                                <div className="inner_detail">
                                                                    <p className="text-muted">Applicant Information</p>
                                                                    <p>Education: <strong>{user && user.education && user.education.title} ({user && user.year_of_graduation})</strong></p>
                                                                    <p>Expected or Gained Percentage: <strong>{user && user.gained_percentage && user.gained_percentage}%</strong></p>
                                                                    <p>Prefered Intake: <strong>{user && user.prefered_intake && (user.prefered_intake.title + ' ' + user.prefered_intake.year)}</strong></p>
                                                                    {
                                                                        user && user.ielts > 0 && user.ielts_score > 0
                                                                        &&
                                                                        <p>IELTS Score: <strong>{(user.ielts_score * 1).toFixed(1)} Bands</strong></p>
                                                                    }
                                                                    {
                                                                        user && user.tofel > 0 && user.tofel_score > 0
                                                                        &&
                                                                        <p>TOEFL Score: <strong>{(user.tofel_score * 1).toFixed(1)}</strong></p>
                                                                    }
                                                                    {
                                                                        user && user.pte > 0 && user.pte_score > 0
                                                                        &&
                                                                        <p>PTE Score: <strong>{(user.pte_score * 1).toFixed(1)}</strong></p>
                                                                    }
                                                                </div>
                                                            </div>
                                                    }
                                            
                                                    <div className="edit_area">
                                                        {
                                                            props.data && props.data.type === 'agent' && props.data.info
                                                                ?
                                                                <div className='agent_view'>
                                                                    <div className='left_it'>
                                                                        <div className="image_area">
                                                                            {props.data && props.data.info && props.data.info.logo ? <img src={props.data.info && props.data.info.logo ? renderImage(props.data.info.logo, 'large') : ``} alt="" /> : null}
                                                                        </div>
                                                                    </div>
                                                                    <div className='right_it'>
                                                                        <div className='name'>
                                                                            <p>
                                                                                {props.data && props.data.info && props.data.info.title}
                                                                            </p>
                                                                            <p>
                                                                                {props.data && props.data.info && props.data.info.address}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                :
                                                                <Link to='/student-change-preference-step-1'>
                                                                    <i class="far fa-pen"></i>Edit
                                                                </Link>
                                                        }
                                                    </div>
                                                </div>
                                        }
                                    </div>
                                    {
                                        !mounting && !completed
                                        &&
                                        <>
                                            <div className="inputs_area">
                                                {/* <div className="select_area">
                                                <Form.Label>University </Form.Label>
                                                <Select options={options} />
                                            </div> */}
                                                {
                                                    courses && courses.length > 0
                                                    &&
                                                    <div className="select_area">
                                                        <Form.Label>Courses you want to pursue</Form.Label>
                                                        <Selectable
                                                            isMulti={true}
                                                            value={selectedCourses}
                                                            placeholder="Select Courses"
                                                            onChange={(item) => {
                                                                setSelectedCourses(item);
                                                            }}
                                                            options={courses}
                                                        />
                                                    </div>
                                                }
                                                <div className="graduation_area">
                                                    <Form.Label>Message </Form.Label>
                                                    <Form.Control
                                                        autoFocus={true}
                                                        maxLength={300}
                                                        as="textarea"
                                                        placeholder="Type your message...."
                                                        style={{ height: '120px' }}
                                                        value={message}
                                                        onChange={(e) => {
                                                            setMessage(e.target.value);
                                                        }}
                                                    />
                                                    <small className="text-muted float-right">{( (message && message.trim() ? (300 - message.trim().length) : '300') + ` characters remaining` )}</small>
                                                </div>
                                            </div>
                                            <div className="submit_btn_area">
                                                {errMsg && <p className="text-danger text-center mt-3">{errMsg}</p>}
                                                <div className="add_area">
                                                    <button
                                                        className="btn-primary-2 w-100"
                                                        onClick={submitApplication}
                                                    >{loading && <i className="fa fa-spin fa-spinner"></i>} Submit</button>
                                                </div>
                                            </div>
                                        </>
                                    }
                                </div>
                                :
                                <div className="agent_contact_area"><i className='fa fa-spin fa-spinner'></i></div>

                        }

                    </Modal.Body>
                }
                {
                    completed
                    &&
                        <>
                            <div className="inputs_area">
                                <div className="box_area">
                                    <div className='success_case'>
                                        <>
                                            <p className="success-circle"><i className="fa fas fa-check-circle"></i></p>
                                            <div className='inner_set'>
                                                <h1>Congratulation!</h1>
                                                <p>{completed}</p>
                                            </div>
                                        </>
                                        <p className='butn'>
                                            <Link to={'/knowledge-base'} className="btn btn-primary-2 float-start">Have a Question?</Link>
                                            <Link to={'/my-profile'} className="btn btn-primary-2 float-end">Contact Support</Link>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </>
                }
            </Modal>
        </>
    );
}


export default (AgentContactModal);