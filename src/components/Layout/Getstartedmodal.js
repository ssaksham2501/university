import React, { useState, useEffect } from "react";
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { parsePhoneNumber } from 'react-phone-number-input';
import ModalController from "../../apis/controllers/modal.controller";
import { storeItem } from "../../helpers/localstorage";
import Phonecomponent from "../Phone_input";
import Login from "../../functions/auth/login";
import { Form } from "react-bootstrap";

function Getstartedmodal(props) {
    const { show, close } = props;
    const { values, setValues, errMsg, isLoading, showHidePassword, showPasswordBlock, loginHandler, setPassword } = Login(props);
    
    useEffect(() => {
        window.scrollTo(0, 0);
        document.querySelector('.PhoneInput input').focus();
    }, []);

    return (
        <>
            <div className="user_type_modal">
                <Modal
                    show={show}
                    onHide={() => close()}
                    dialogClassName="modal-90w"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title" className='head-set'>
                            Get Started
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <form
                            onSubmit={(e) => {
                                e.preventDefault();
                                if (!showPasswordBlock) {
                                    loginHandler.getStarted();
                                }
                                else{
                                    loginHandler.passwordHandler();
                                }
                            }}
                        >
                        <div className='get_start'>
                            <div className='content'>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisNullam maximus orci  suscipit rutrum.
                                </p>
                            </div>
                            <div className="get_started_area">
                                <div className="expect_education_area">
                                    <div className="phone_input_area top_space">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0">

                                                    <div className="phone_wrap">
                                                        {
                                                            !showPasswordBlock
                                                            ?
                                                                <div className="detail_area">
                                                                    <label>Enter your phone number</label>
                                                                    <Phonecomponent
                                                                        value={values.country_code + values.phone_number}
                                                                        updateValue={(v) => {
                                                                            let phone = parsePhoneNumber(v);
                                                                            if (phone && v && phone.countryCallingCode && phone.nationalNumber) {
                                                                                setValues({
                                                                                    phone_number: phone.nationalNumber,
                                                                                    country_code: phone.countryCallingCode
                                                                                })
                                                                            }
                                                                            else
                                                                            {
                                                                                setValues({
                                                                                    phone_number: ``,
                                                                                    country_code: ``
                                                                                })
                                                                            }
                                                                        }}
                                                                    />
                                                                </div>
                                                            :
                                                                <div className="detail_area">
                                                                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                                <Form.Label>Enter password for <strong>+{values.country_code + '-' + values.phone_number}</strong></Form.Label>
                                                                                <div className="sub_control">
                                                                                <Form.Control
                                                                                    type={showHidePassword ? `text` : `password`}
                                                                                    placeholder="**********"
                                                                                    onChange={(e) => {
                                                                                        setPassword(e.target.value)
                                                                                    }}
                                                                                    autoFocus={true}
                                                                                />
                                                                                {errMsg && <p className="text-danger text-center mt-3">{errMsg}</p>}
                                                                                <Link
                                                                                    to=''
                                                                                    onClick={(e) => {
                                                                                        e.preventDefault();
                                                                                        loginHandler.togglePassword()
                                                                                    }}
                                                                                    className='eyename'
                                                                                > <i className={showHidePassword ? `far fa-eye` : `far fa-eye-slash`}></i></Link>
                                                                                </div>
                                                                            </Form.Group>
                                                                    <div className='forget'>
                                                                        <Link to=""
                                                                            onClick={(e) => {
                                                                                e.preventDefault();
                                                                                props.openForgotPasswordModal();
                                                                            }}
                                                                        >Forgot password?</Link>
                                                                    </div>
                                                                    
                                                                </div>
                                                        }
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="btn_area">
                                    
                                    <div className="center_area">
                                        <button className="btn-primary-2 w-100"
                                            type="submit"
                                        >
                                            {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                            &nbsp;
                                            Continue</button>
                                    </div>

                                    <div className="terms_area">
                                        <p>By continuing , you agree to our <Link to='/#'>Terms of service </Link> and <Link to='/#'>Privacy
                                            policy </Link></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (Getstartedmodal);