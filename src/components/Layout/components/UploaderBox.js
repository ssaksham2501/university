import React, { useRef, useState } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Constant from "../../../apis/constants";
import ActionController from "../../../apis/controllers/action.controller";

function UploaderBox(props) {
    const fileInput = useRef(null);
    const [isLoading, setLoader] = useState(false);
    const [progress, setProgress] = useState(null);
    const [isError, setIsError] = useState(null);
    

    const handleImage = async (value) => {
        if (['image/jpeg', 'image/png', 'image/gif', 'image/webp', 'application/pdf'].includes(value.type)) {
            if (value.size < Constant.maxFileSize) {
                setIsError(null);
                if (isLoading === false) {
                    setLoader(true);
                    let callback = (p) => {
                        setProgress(p > 5 ? (p * 1 - 5) : p);
                    };

                    let response = await new ActionController().uploadFile(value, callback);
                    setProgress('100%');
                    setLoader(false);
                    if (response && response.status) {
                        props.onResponse(response);
                    } 
                    else {
                        setIsError('File could not be uploaded.');        
                    }
                }
            }
            else {
                setIsError('File size exceeds the maximum limit of 5MB.');
            }
        }
        else {
            setIsError('Please upload file having extension jpg, jpeg, png, gif, webp or pdf.');
        }

    }

    return (
        <>
            <Form.Label>Upload Accreditation</Form.Label>
            <div className='uplaod_set'>
                <div className='inner_uplaod'>
                    <Link
                        onClick={() =>
                            fileInput.current.click()
                        }
                    >
                        <i className="far fa-cloud-upload"></i>
                        <p>
                            Upload or drop your file
                        </p>
                        <div>
                            <div class="progress">
                                <div class="progress-bar" style={{width: progress ? progress  + '%' : '0'}}></div>
                            </div>
                        </div>
                    </Link>
                    <input
                        ref={fileInput}
                        id="photo2"
                        type="file"
                        onChange={(event) => {
                            handleImage(event.target.files[0])
                            event.target.value = '';
                        }}
                        accept="image/*,.pdf"
                    />
                </div>
                <small className="text-danger">{ isError ? isError : `` }</small>
            </div>
        </>
    );       
}
export default UploaderBox;