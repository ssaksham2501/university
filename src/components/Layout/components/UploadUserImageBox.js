import React, { useRef, useState } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Constant from "../../../apis/constants";
import ActionController from "../../../apis/controllers/action.controller";
import { renderImage, renderNoAvatar } from "../../../helpers/General";

function UploadUserImageBox(props) {
    const fileInput = useRef(null);
    const [isLoading, setLoader] = useState(false);
    const [progress, setProgress] = useState(0);
    const [isError, setIsError] = useState(null);


    const handleImage = async (value) => {
        if (['image/jpeg', 'image/png', 'image/gif', 'image/webp', 'application/pdf'].includes(value.type)) {
            if (value.size < Constant.maxFileSize) {
                setIsError(null);
                if (isLoading === false) {
                    setLoader(true);
                    setProgress(0.5);
                    let callback = (p) => {
                        setProgress(p > 100 ? (p * 1 - 5) : p);
                    };

                    let response = await new ActionController().uploadUserImage(value, callback);
                    setProgress(100);
                    setLoader(false);
                    if (response && response.status) {
                        setProgress(0);
                        props.onResponse(response);
                    }
                    else {
                        setIsError(response.message);
                    }
                }
            }
            else {
                setIsError('File size exceeds the maximum limit of 5MB.');
            }
        }
        else {
            setIsError('Please upload file having extension jpg, jpeg, png, gif, webp or pdf.');
        }
    }

    return (
        <>
            <div className="img_area_profile">
                <img src={props.image && props.image.small ? renderImage(props.image.small) : renderNoAvatar(props.name)} />
                {
                    !isLoading
                    &&
                    <div className="edits_btn">
                        <ul>
                            <li>
                                <a href="" onClick={(e) => {
                                    e.preventDefault();
                                    fileInput.current.click()
                                }}><i class="fal fa-pen"></i></a>
                            </li>
                            {
                                props.image
                                &&
                                <li>
                                    <a href="" onClick={async (e) => {
                                            e.preventDefault();
                                                setLoader(true);
                                                await props.removeImage()
                                                setLoader(false);
                                        }}><i class="fal fa-times"></i></a>
                                </li>
                            }
                        </ul>
                    </div>
                }
                <input
                    ref={fileInput}
                    id="photo2"
                    type="file"
                    onChange={(event) => {
                        handleImage(event.target.files[0])
                        event.target.value = '';
                    }}
                    accept="image/*"
                />
                 {progress !== 0 ? 
                <div class="progress">
                    <div class="progress-bar" style={{ width: progress ? progress + '%' : '0' }}></div>
                </div> : null}
            </div>
            {isLoading && progress !== 0 ? <div class="progress">
                <div class="progress-bar" style={{ width: progress ? progress + '%' : '0' }}></div>
            </div> : null}
            <small className="text-danger">{isError ? isError : ``}</small>
        </>
    );
}
export default UploadUserImageBox;