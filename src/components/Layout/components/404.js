import React, { useState, useEffect } from "react";
import pagenotfound from "../../../assets/images/404.png"

function Pagenotfound(props) {
    return (
        <>
           <div className="page_not_foun top_space">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12">
                            <div className="img_area_set">
                                <div className="img_area">
                                    <img src={pagenotfound} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </>
    )
}

export default (Pagenotfound);