import ReadMoreAndLess from 'react-read-more-less';
 
const ReadMoreAndLessText = (props) =>  {    
    return (
        <ReadMoreAndLess
            className="read-more-content"
            charLimit={props.limit}
            readMoreText="more"
            readLessText="less"
        >
            {props.text}
        </ReadMoreAndLess>
    );
}

export default ReadMoreAndLessText;