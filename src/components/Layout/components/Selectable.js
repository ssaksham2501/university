import Select from 'react-select';
const Selectable = (props) => {

    return (
        <Select
            id={`select-` + props.id ? props.id : `1`}
            isMulti={props.isMulti ? true : false}
            maxLength={props.maxLength > 0 ? props.maxLength : null}
            styles={{
                option: (provided, state) => {
                    return {
                        ...provided,
                        ":active": {
                            backgroundColor: '#b4ecb969',
                        },
                        backgroundColor: state.isSelected ? '#B4ECB9 !important' : (state.isFocused ? '#b4ecb969' : ''),
                    }
                }
            }}
            placeholder={props.placeholder ? props.placeholder : ``}
            options={props.options && props.options.length > 0 ? props.options : []}
            className={(props.className ? `selectable ` + props.className : `selectable` )}
            value={props.value ? props.value : ``}
            onChange={(val) => {
                if(props.onChange)
                props.onChange(val)
            }}
        />);
}
export default Selectable;