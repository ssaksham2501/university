import React, { useState, useEffect } from "react";
import Autocomplete from 'react-autocomplete';
const AutoComplete = (props) => {
    const [timeoutFilters, setTimeoutFilters] = useState(null);
    const [options, setOptions] = useState(props.items ? props.items : []);
    const [value, setValue] = useState(props.value ? props.value : ``);
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        setValue(props.value);
    }, [props.value]);

    
    return (
        <Autocomplete
            id={`autocomplete-` + props.id}
            wrapperStyle={{
                width: '100%',
                position: 'relative'
            }}
            renderInput={(iprops) => {
                return <input
                    {...iprops}
                    id={`autocomplete-input-` + props.id}
                    placeholder={props.placeholder ? props.placeholder : ``}
                    className={props.className}
                />
            }}
            onMenuVisibilityChange={(isOpen) => {
                if (!isOpen && props.blurOptions) {
                    setOptions([]);
                }
            }}
            getItemValue={(item) => item.label}
            items={options}
            renderMenu={(items, value) => {
                if(items.length < 1) return <></>
                return (<div className="menu">
                    {
                        loading
                        ?
                            (<div className="item"><div style={{
                                padding: `8px 12px`,
                                userSelect: `none`,
                                boxSizing: `border-box`,
                                fontSize: `1rem`,
                                fontWeight: `400`,
                                lineHeight: `1.5`,
                                color: `#212529`,
                                background: 'white'
                            }}><i className="fa fa-spin fa-spinner"></i> Loading...</div></div>)
                        :
                                items
                    }
                </div>)
            }}
            renderItem={(item, isHighlighted) =>
                <div
                    key={`autocompleted-` + item.value}
                    style={{
                        padding: `8px 12px`,
                        userSelect: `none`,
                        boxSizing: `border-box`,
                        fontSize: `1rem`,
                        fontWeight: `400`,
                        lineHeight: `1.5`,
                        color: `#212529`,
                        background: isHighlighted ? '#b4ecb969' : 'white'
                    }}
                >
                    {item.label} {item.badge ? <small className="badge">{item.badge}</small> : ``}
                </div>
            }
            value={value}
            onChange={async (e, val) => {
                setValue(val);
                setOptions([]);
                setLoading(true);
                if (timeoutFilters) {
                    clearTimeout(timeoutFilters);
                }

                let timeout = setTimeout(async () => {
                    let response = await props.onChange(val);
                    setLoading(false);
                    setOptions(response);
                    setTimeoutFilters(null);
                }, 300);
                
                setTimeoutFilters(timeout);
            }}
            onSelect={(val, item) => {
                if(props.onSelect)
                props.onSelect(val, item);
                setValue(val);
            }}
        />)
}
export default AutoComplete;