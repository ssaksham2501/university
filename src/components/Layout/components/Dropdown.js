import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Dropdown = (props) => {
    const [show, setShow] = useState(false);
    const [selected, setSelected] = useState(null);

    useEffect(() => {
        document.onclick = (e) => {
            e.stopPropagation();
            let dd = document.querySelector('#' + props.id + ' .dropdown-menu.show');
            if (dd && show) {
                setShow(false);
            }
        }
    }, [show]);

    return (
        <div
            className="dropdown"
            id={props.id}
            onClick={(e) => {
                e.stopPropagation();
            }}
        >
            <button
                type="button"
                className="dropdown-toggle btn btn-primary"
                onClick={() => {
                    setShow(!show)
                }}
            >{selected && selected.value? selected.title : props.title}</button>
            <div className={`dropdown-menu` + (show ? ` show` : ``)}>
                {
                    props.items.map((item, index) => {
                        return (<Link
                            key={props.id + index}
                            className={`dropdown-item` + (selected && selected.value === item.value ? ` active` : '') + (item.class ? ` ` + item.class : ``)}
                            to={item.herf ? item.herf : '#'}
                            onClick={(e) => {
                                e.preventDefault();
                                setSelected(item);
                                if (props.onSelect) {
                                    props.onSelect(item);
                                }
                                setShow(false);
                            }}
                        >{item.title}</Link>);
                    })
                }
            </div>
        </div>);
}

export default Dropdown;