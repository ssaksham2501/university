import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import footerlogo from '../../assets/images/footer-logo.png'
import News from "../Home/components/news";

function Header() {
  return (
    <>
      <News />
      <div className="footer_section">
        <div className="container">
          <div className="row">
            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
              <div className="logo_side_wrap">
                <div className="logo_side">
                  <Link to='/'>
                    <img src={footerlogo} alt="" />
                  </Link>
                </div>
                <div className="content">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam.
                  </p>
                </div>
                <div className="social_link">
                  <ul>
                    <li>
                      <Link to='/'>
                        <i className="fab fa-twitter"></i>
                      </Link>
                    </li>
                    <li>
                      <Link to='/'>
                        <i className="fab fa-instagram"></i>
                      </Link>
                    </li>
                    <li>
                      <Link to='/'>
                        <i className="fab fa-facebook"></i>
                      </Link>
                    </li>
                    <li>
                      <Link to='/'>
                        <i className="fab fa-youtube"></i>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-6 col-12">
              <div className="row">
                <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6">
                  <div className="support_links">
                    <div className="head">
                      <h6>Unigo</h6>
                    </div>
                    <ul>
                      <li>
                        <Link to='/colleges'>Universities</Link>
                      </li>
                      <li>
                        <Link to='/agents'>Agents</Link>
                      </li>
                      <li>
                        <Link to='/for-students'>For Students</Link>
                      </li>
                      <li>
                        <Link to='/for-agents'>For Agents</Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6">
                  <div className="support_links">
                    <div className="head">
                      <h6>Company</h6>
                    </div>
                    <ul>
                      <li>
                        <Link to='/'>My account</Link>
                      </li>
                      <li>
                        <Link to='/student-about'>About us</Link>
                      </li>
                      <li>
                        <Link to='/terms-and-conditions'>Terms and Conditions</Link>
                      </li>
                      <li>
                        <Link to='/privacy-policy-conditions'>Privacy policy</Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                  <div className="support_links">
                    <div className="head">
                      <h6>Support</h6>
                    </div>
                    <ul>
                      <li>
                        <Link to='/contact-us'>Contact us</Link>
                      </li>
                      <li>
                        <Link to="/knowledge-base">Knowledge base</Link>
                      </li>
                      <li>
                        <Link to='/faq'>FAQs</Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="copy_right">
        <p>© &nbsp;2022 &nbsp;|&nbsp; All Rights Reserved &nbsp;|&nbsp; Powered By <a href="https://globiztechnology.com/" target="_blank">Globiz Technology Inc.</a></p>
      </div>
    </>
  );
}

export default withRouter(Header);
