import React, { useEffect } from "react";
import { Form } from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';
import { parsePhoneNumber } from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import { Link, useHistory } from "react-router-dom";
import RecoverPassword from "../../functions/auth/recoverPassword";
import Phonecomponent from "../Phone_input";
import OTPInput, { ResendOTP } from "otp-input-react";

function RecoverPasswordModal(props) {
    const history = useHistory();
    const { show, close } = props;
    const {errMsg, successMessage, isLoading, values, recoverPasswordHandler, isError, showHidePassword, showHideConfirmPassword} = RecoverPassword(props);
    
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    

    return (
        <>
            <div className="user_type_modal">
                <Modal
                    show={show}
                    onHide={() => close()}
                    dialogClassName="modal-90w"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title" className='head-set'>
                            {!successMessage ? `Reset Password` : `Congratulations!`}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='get_start'>
                            {
                                !successMessage
                                &&
                                <div className='content'>
                                    <p>
                                        You can reset your password for your account.
                                    </p>
                                </div>
                            }
                            <div className="get_started_area">
                                <div className="expect_education_area">
                                    <div className="phone_input_area">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                                                    <div className="phone_wrap">
                                                        {
                                                            successMessage
                                                            ?
                                                            <div class="detail_area message_area">
                                                                <p class="text-center"><i class="fa fa-check-circle"></i></p>
                                                                <p>{successMessage}</p>
                                                            </div>
                                                            :
                                                            <div className="detail_area">
                                                                <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                    <Form.Label>New Password</Form.Label>
                                                                    <div className="relative_met">
                                                                    <Form.Control
                                                                        type={showHidePassword ? `text` : `password`}
                                                                        placeholder="**********"
                                                                        value={values.new_password}
                                                                        onChange={(e) => {
                                                                            recoverPasswordHandler.handleChange(`new_password`, e.target.value);
                                                                        }}
                                                                    />
                                                                    <Link
                                                                        to=''
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                            recoverPasswordHandler.togglePassword()
                                                                        }}
                                                                        className='eye_met'
                                                                    > <i className={showHidePassword ? `far fa-eye` : `far fa-eye-slash`}></i></Link>
                                                                    {!isError.new_password.isValid ? (
                                                                            <p className="valid-helper">
                                                                                {
                                                                                    isError
                                                                                        .new_password
                                                                                        .message
                                                                                }
                                                                            </p>
                                                                        ) : null}
                                                                    </div>
                                                                </Form.Group>
                                                                <Form.Group className="form_search mt-3" controlId="exampleForm.ControlInput1">
                                                                    <Form.Label>Confirm Password</Form.Label>
                                                                    <div className="relative_met">
                                                                    <Form.Control
                                                                        type={showHideConfirmPassword ? `text` : `password`}
                                                                        placeholder="**********"
                                                                        value={values.confirm_password}
                                                                        onChange={(e) => {
                                                                            recoverPasswordHandler.handleChange(`confirm_password`, e.target.value);
                                                                        }}
                                                                    />
                                                                    <Link
                                                                        to=''
                                                                        onClick={(e) => {
                                                                            e.preventDefault();
                                                                            recoverPasswordHandler.toggleConfirmPassword()
                                                                        }}
                                                                        className='eye_met'
                                                                    > <i className={showHideConfirmPassword ? `far fa-eye` : `far fa-eye-slash`}></i></Link>
                                                                    {!isError.confirm_password.isValid ? (
                                                                        <p className="valid-helper">
                                                                            {
                                                                                isError
                                                                                    .confirm_password
                                                                                    .message
                                                                            }
                                                                        </p>
                                                                    ) : null}
                                                                    </div>
                                                                </Form.Group>
                                                            </div>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="btn_area">
                                    {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                    <div className="center_area">
                                        {
                                            !successMessage
                                            &&
                                                <button className="btn-primary-2 w-100"
                                                    type="submit"
                                                    onClick={() => {
                                                        recoverPasswordHandler.submitRequest();
                                                    }}
                                                >
                                                    {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                    &nbsp;
                                                    Continue</button>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (RecoverPasswordModal);