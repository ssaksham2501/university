import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { Link, withRouter } from "react-router-dom";
import membership_icon from '../../assets/images/membership_icon.png'
import visa_dicon from "../../assets/images/visa_dicon.png"

function AddCreditModal(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => props.close();


    return (
        <>
            <div className="add_card_modal">
                <Modal show={props.show} className="application_student" size="md" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Credits</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="box_area ul">
                            <div className="box_inner_area ul">
                                <div className='row'>
                                    <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6'>
                                        <div className="membership_area">
                                            <div className="box_area">
                                                <div className="inner_detail_area">
                                                    <div className="icon_area">
                                                        <img src={membership_icon} />
                                                    </div>
                                                    <div className="info_area">
                                                        <h2>100</h2>
                                                        <p>Credit</p>
                                                        <div className="manage_btn_area">
                                                            <Link to='/'><button className="btn-primary-4" >$12</button></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6'>
                                        <div className="membership_area">
                                            <div className="box_area">
                                                <div className="inner_detail_area">
                                                    <div className="icon_area">
                                                        <img src={membership_icon} />
                                                    </div>
                                                    <div className="info_area">
                                                        <h2>200</h2>
                                                        <p>Credit</p>
                                                        <div className="manage_btn_area">
                                                            <Link to='/'><button className="btn-primary-4" >$24</button></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6'>
                                        <div className="membership_area">
                                            <div className="box_area">
                                                <div className="inner_detail_area">
                                                    <div className="icon_area">
                                                        <img src={membership_icon} />
                                                    </div>
                                                    <div className="info_area">
                                                        <h2>300</h2>
                                                        <p>Credit</p>
                                                        <div className="manage_btn_area">
                                                            <Link to='/'><button className="btn-primary-4" >$36</button></Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="add_card_input_area">
                                    <Form>
                                        <div className='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                            <div className="expect_education_area  adcredit">
                                                <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                    <Form.Label>Credit</Form.Label>
                                                    <Form.Control type="email" placeholder="Enter credit" />
                                                </Form.Group>
                                            </div>
                                        </div>
                                    </Form>
                                    <div className="inner_detail_area addcredit">
                                        <div className="card_box_area">
                                            <div className="card_info_area">
                                                <div className="left_info_area">
                                                    <div className="icon_area">
                                                        <img src={visa_dicon} />
                                                    </div>
                                                    <div className="info_area">
                                                        <h4>4438
                                                            6721
                                                            7392
                                                            8721</h4>
                                                        <p>Expiration 10/30</p>
                                                    </div>
                                                </div>
                                                <div className="right_info_area">
                                                    <div className="default_card_info">
                                                        <p>Default</p>
                                                    </div>
                                                    <div className="icon_area">
                                                        <Link to='/#'>
                                                            <i class="fas fa-ellipsis-h"></i>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className="add_btn_area paybtn">
                                <div className="add_area">
                                    <Link to='/'><button className="btn-primary-2 w-100" >Pay $50</button></Link>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (AddCreditModal);