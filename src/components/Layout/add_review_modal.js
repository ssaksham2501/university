import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import Form from 'react-bootstrap/Form';
import Validation from '../../helpers/vaildation';
import AgentProfileController from '../../apis/controllers/agentController/agent.controller';

function AddReviewModal(props) {
    const [errMsg, setErrMsg] = useState(``);
    const [mounting, setMounting] = useState(true);
    const [reviewStatus, setReviewStatus] = useState(null);
    
    const [loading, setLoading] = useState(``);
    const [completed, setCompleted] = useState(false);
    const [values, setValues] = useState({
        rating: ``,
        review: ``
    });
    const [isError, setError] = useState({
        rating: {
            rules: ["required", "numeric"],
            isValid: true,
            message: "",
        },
        review: {
            rules: ["max:300"],
            isValid: true,
            message: "",
        }
    });

    let validation = new Validation(isError);
    
    useEffect(() => {
        setMounting(true);
        init();
    }, []);

    const init = async () => {
        let response = await new AgentProfileController().isAdded({
            type: props.agent ? 'agent' : 'university',
            slug: props.agent ? props.agent.user_name : (props.university ? props.university.slug : null)
        });
        if (response && response.status) {
            setCompleted(response.added);
            setReviewStatus(!response.review_status);
            setMounting(false);
        }
        console.log(response);
    }

    const handleClose = () => props.close();

    const handleStates = (field, value) => {
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        setValues({ ...values, [field]: value });
    };

    const submitReview = async () => {
        if (!loading) {
            let validation = new Validation(isError);
            let isValid = validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setErrMsg(``);
                setLoading(true);
                let response = await new AgentProfileController().addReview({
                    university: props && props.university && props.university.slug ? props.university.slug : null,
                    agent: props && props.agent && props.agent.user_name ? props.agent.user_name : null,
                    review: values.review,
                    rating: values.rating
                });
                setLoading(false);
                if (response && response.status) {
                    setCompleted(true);
                }
                else {

                    setErrMsg(response.message);
                }
            } else {
                console.log(isValid.errors);
                setError({ ...isValid.errors });
            }
        }
    }

    return (
        <>
            <Modal
                show={props.show}
                className="application_student"
                size="lg"
                onHide={handleClose}
                animation={true}
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>Write a Review</Modal.Title>
                </Modal.Header>
                {
                    mounting
                    ?
                        <Modal.Body>
                            <div className="box_area">
                                <div className="box_inner_area">
                                    <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                </div>
                            </div>
                        </Modal.Body>
                    :
                        <Modal.Body>
                            {
                                !completed
                                    ?
                                    <div className="box_area">
                                        <div className="box_inner_area">
                                            <div className="top_area">
                                                <div className="add_review_area">
                                                    <div className="rating_area">
                                                        <Rating
                                                            ratingValue={values.rating > 0 ? values.rating : null}
                                                            showTooltip={false}
                                                            initialValue={0}
                                                            tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                                                            onClick={(e) => {
                                                                handleStates(
                                                                    "rating",
                                                                    e
                                                                );
                                                            }}
                                                        />
                                                        {!isError.rating.isValid && isError.rating.message && <p><small className="text-danger  mt-2">{isError.rating.message}</small></p>}
                                                    </div>
                                                    <div className="review_field_area">
                                                        <Form.Label>Review</Form.Label>
                                                        <Form.Control
                                                            as="textarea"
                                                            placeholder="Enter your message..."
                                                            maxLength="300"
                                                            style={{ height: '120px' }}
                                                            onChange={(e) => {
                                                                handleStates(
                                                                    "review",
                                                                    e.target.value
                                                                );
                                                            }}
                                                            value={values.review ? values.review : ``}
                                                        />
                                                        {!isError.review.isValid && isError.review.message && <p><small className="text-danger  mt-2">{isError.review.message}</small></p>}
                                                        <small className="text-muted float-right">{(300 - (values.review && values.review.trim() ? values.review.trim().length : 0) )` characters remaining`}</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="submit_btn_area">
                                            {
                                                errMsg
                                                &&
                                                <p className="text-danger text-center">{errMsg}</p>
                                            }
                                            <div className="add_area">
                                                <button
                                                    type='button'
                                                    className="btn-primary-2 w-100"
                                                    onClick={async (e) => {
                                                        e.preventDefault();
                                                        await submitReview();
                                                    }}
                                                >{loading && <i className="fa fa-spin fa-spinner"></i>}Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <div className="box_area">
                                        <div className='success_case'>
                                            
                                                {
                                                    reviewStatus
                                                    ?
                                                        <>
                                                            <p className="success-circle text-warning"><i className="fas fa-exclamation-circle"></i></p>
                                                            <div className='inner_set'>
                                                                <h1>Awaiting!</h1>
                                                                <p>We are reviewing your submission. We will drop you an email once your review published.</p>
                                                            </div>
                                                        </>
                                                    :
                                                        <>
                                                            <p className="success-circle"><i className="fa fas fa-check-circle"></i></p>
                                                            <div className='inner_set'>
                                                                <h1>Congratulation!</h1>
                                                                <p>Thank you for submitting a review. We will revert you back once your review get published</p>
                                                            </div>
                                                        </>
                                                }
                                            <p className='butn'>
                                                <Link to={'/knowledge-base'} className="btn btn-primary-2 float-start">Have a Question?</Link>
                                                <Link to={'/my-profile'} className="btn btn-primary-2 float-end">Contact Support</Link>
                                            </p>
                                        </div>
                                    </div>
                            }

                        </Modal.Body>
                }
            </Modal>
        </>
    );
}


export default (AddReviewModal);