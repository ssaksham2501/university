import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import applicant_stu1 from "../../assets/images/applicant_stu1.png"
import Select from 'react-select'
import doloricon from "../../assets/images/dolor_popup_icon.png"
import dots2 from '../../assets/images/dots2.png';

function PurchaseModal() {
    const [show, setShow] = useState(true);
    const handleClose = () => setShow(false);
    const options = [
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' }
    ]

    return (
        <>
            <Modal show={show} className="application_student" size="md" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>Purchase</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="box_area ul">
                        <div className="box_inner_area ul">
                            <div className="top_area">
                                <div className="purchase_detail_area">
                                    <div className="cards_detail_area">
                                        <div className="box_area">
                                            <div className="top_area">
                                                <div className="left_area">
                                                    <div className="inner_info">
                                                        <h6>Availabe Credits</h6>
                                                    </div>
                                                    <div className="balance_detail">
                                                        <div className="inner_info">
                                                            <h3>1000 <sub>Credits</sub></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="add_credit_btn_area">
                                                        <Link to='/'><button className="btn-primary-4" >Add Credit</button></Link>
                                                    </div>
                                                </div>
                                                <div className="doller_img">
                                                    <img src={doloricon} />
                                                </div>
                                                <div className="dots2">
                                                <img src={dots2} />
                                            </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="applicant_detail">
                                        <div className="left_area">
                                            <div className="applicant_image_area">
                                                <div className="image_area">
                                                    <img src={applicant_stu1} />
                                                </div>
                                            </div>
                                            <div className="applicant_info_area">
                                                <div className="applicant_name">
                                                    <div className="left_area">
                                                        <h4>Harvy gray</h4>
                                                    </div>
                                                    <div className="right_area">
                                                        <p>(Age 23)</p>
                                                    </div>
                                                </div>
                                                <div className="applicant_location_area">
                                                    <Link to='/#'>
                                                        <div className="left_area">
                                                            <i class="fal fa-map-marker-alt"></i>
                                                        </div>
                                                        <div className="right_area_in">
                                                            PA 16801, United States
                                                        </div>
                                                    </Link>
                                                </div>
                                                <div className="applicant_uni_name">
                                                    <p>Penn State University</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="right_viewer_detail">
                                            <div className="icon_area">
                                                <Link to='/'>
                                                    <i class="fal fa-eye"></i>80+ View
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="credits_detail_area text-center">
                                        <div className="credits_box_area">
                                            <div className="credits_count">
                                                <div className="star_bg">
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <h3>20 <sub>Credits</sub></h3>
                                            </div>
                                            <div className="des_card">
                                                <p>Lorem ipsum sit doe Lorem
                                                    sit doe amet amet.</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="submit_btn_area">
                            <div className="add_area">
                                <Link to='/'><button className="btn-primary-2 w-100" >Pay Now</button></Link>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
}


export default (PurchaseModal);