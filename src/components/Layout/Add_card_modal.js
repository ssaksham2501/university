import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { Link, withRouter } from "react-router-dom";

function Addcardmodal(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => props.close();


    return (
        <>
            <div className="add_card_modal">
                <Modal show={props.show} className="application_student manage-card" size="md" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Add card</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="box_area ul">
                            <div className="box_inner_area ul">
                                <div className="add_card_input_area">
                                    <Form>
                                        <div className='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                            <div className="expect_education_area">
                                                <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                    <Form.Label>Name on card</Form.Label>
                                                    <Form.Control type="email" placeholder="Name on card" />
                                                </Form.Group>
                                            </div>
                                        </div>
                                        <div className='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                                            <div className="expect_education_area">
                                                <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                    <Form.Label>Card number</Form.Label>
                                                    <Form.Control type="email" placeholder="Card number" />
                                                </Form.Group>
                                            </div>
                                        </div>
                                        <div className='row'>
                                            <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6'>
                                                <div className="expect_education_area">
                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                        <Form.Label>Expiry date</Form.Label>
                                                        <Form.Control type="email" placeholder="Expiry date" />
                                                    </Form.Group>
                                                </div>
                                            </div>
                                            <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6'>
                                                <div className="expect_education_area">
                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                        <Form.Label>Security code</Form.Label>
                                                        <Form.Control type="email" placeholder="Security code" />
                                                    </Form.Group>
                                                </div>
                                            </div>
                                        </div>
                                    </Form>
                                </div>
                            </div>
                        </div>
                        <div className="add_btn_area">
                            <div className="add_area">
                                <Link to='/'><button className="btn-primary-2 w-100" >Save</button></Link>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (Addcardmodal);