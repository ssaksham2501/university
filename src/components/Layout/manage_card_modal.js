import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import visa_dicon from "../../assets/images/visa_dicon.png"
import master_icon from "../../assets/images/master_icon.png"
import { Form } from 'react-bootstrap';

function Managecardmodal(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => props.close();
    
    return (
        <>
            <Modal show={props.show} className="application_student manage-card" size="md" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>Add card</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="box_area ul">
                        <div className="box_inner_area ul">
                            <div className="card_heading_area">
                                <h4>My cards</h4>
                            </div>
                            <div className="inner_detail_area">
                                <form>
                                <div className='card_show_areas'>
                                    <input type="radio" id="l-1" name="district[]" value="1"  />
                                    <label for="l-1" className="card_box_area"> 
                                        <div className="card_info_area">
                                            <div className="left_info_area">
                                                <div className="icon_area">
                                                    <img src={visa_dicon} />
                                                </div>
                                                <div className="info_area">
                                                    <h4>4438
                                                        6721
                                                        7392
                                                        8721</h4>
                                                    <p>Expiration 10/30</p>
                                                </div>
                                            </div>
                                            <div className="right_info_area">
                                                <div className="default_card_info">
                                                    <p>Default</p>
                                                </div>
                                                <div className="icon_area">
                                                    <Link to='/#'>
                                                        <i class="fas fa-ellipsis-h"></i>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <div className='card_show_areas' >
                                    <input type="radio" id="l-2" name="district[]" value="1"  />
                                    <label for="l-2" className="card_box_area"> 
                                        <div className="card_info_area">
                                            <div className="left_info_area">
                                                <div className="icon_area">
                                                    <img src={visa_dicon} />
                                                </div>
                                                <div className="info_area">
                                                    <h4>4438
                                                        6721
                                                        7392
                                                        8721</h4>
                                                    <p>Expiration 10/30</p>
                                                </div>
                                            </div>
                                            <div className="right_info_area">
                                                <div className="icon_area">
                                                    <Link to='/#'>
                                                        <i class="fas fa-ellipsis-h"></i>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <div className='card_show_areas'>
                                    <input type="radio" id="l-3" name="district[]" value="1"  />
                                    <label for="l-3" className="card_box_area"> 
                                        <div className="card_info_area">
                                            <div className="left_info_area">
                                                <div className="icon_area">
                                                    <img src={visa_dicon} />
                                                </div>
                                                <div className="info_area">
                                                    <h4>4438
                                                        6721
                                                        7392
                                                        8721</h4>
                                                    <p>Expiration 10/30</p>
                                                </div>
                                            </div>
                                            <div className="right_info_area">
                                                <div className="icon_area">
                                                    <Link to='/#'>
                                                        <i class="fas fa-ellipsis-h"></i>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="add_btn_area">
                        <div className="add_area">
                            <Link to='/'><button className="btn-primary-2 w-100" >+ Add more card</button></Link>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
}


export default (Managecardmodal);