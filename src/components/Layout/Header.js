import { React, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { compose } from "redux";
import AuthController from "../../apis/controllers/auth.controller";
import ModalController from "../../apis/controllers/modal.controller";
import user from "../../assets/images/agent_photo1.png";
import logo from "../../assets/images/logo.png";
import { isEmpty, isEmptyObj, renderNoAvatar } from "../../helpers/General";
import { renderImage } from "../../helpers/General";
import { getStoredItem } from "../../helpers/localstorage";
import AgentContactModal from "./agent_contact_modal";
import ForgotPasswordModal from "./ForgotPasswordModal";
import Getstartedmodal from "./Getstartedmodal";
import OTPmodal from "./Otpmodal";
import RecoverPasswordModal from "./RecoverPasswordModal";
import Usertypemodal from "./Usermodaltype";

function Header(props) {
    const [showRegistrationModal, setShowRegistrationModal] = useState(false);
    const [showForgotPasswordModal, setShowForgotPasswordModal] = useState(false);
    const [showRecoverPasswordModal, setShowRecoverPasswordModal] = useState(false);
    const [recoverData, setRecoverData] = useState(null);

    const [showotpmodel, setShowotpModel] = useState(false);
    const [showusertypemodal, setShowUserTypeModal] = useState(false);
    const [otp, setotp] = useState();

    const init = async () => {
        window.scrollTo(0, 0);
        let item = localStorage.getItem('SET_USER_DATA');
        item = JSON.parse(item);
        let user = item;
        if (user) {
            if (user && user.access && user.access.token) {
                new AuthController().setUpLogin(user);
            }
        }
    }
    useEffect(() => {
        init();
        checkRoute();
    }, []);


    const handleStartModal = (value) => {
        new ModalController().setStartModal(value);
    }

    const checkRoute = () => {
        let url = window.location.href;
        if(url.includes('/login')){
            new ModalController().setStartModal(true);
        } else {
            new ModalController().setStartModal(false);
        }
    }

    return (
        <div>
            <div className="header_section">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="head_area">
                                <div className="left_area">
                                    <div className="logo_area">
                                        <Link to='/'>
                                            <img src={logo} />
                                        </Link>
                                    </div>
                                </div>
                                <div className="right_area">
                                    <ul className="d-lg-flex d-none align-items-center">
                                        <li className="menus">
                                            <Link to='/student-about'>About us</Link>
                                        </li>
                                        <li className="menus">
                                            <Link to='/for-students'>How it's work</Link>
                                        </li>
                                        <li className="menus">
                                            <Link to='/for-agents'>Find an agent</Link>
                                        </li>
                                        {/* <li className="menus">
                                            <Link to='/colleges'>Universities</Link>
                                        </li>
                                        <li className="menus">
                                            <Link to='/agents'>Agents</Link>
                                        </li> */}

                                        <li className="menus">
                                            <Link to='/contact-us'>Contact us</Link>
                                        </li>
                                        {/* <li className="dot">
                                            <Link to='/#'><i className="fal fa-bell"></i></Link>
                                        </li>
                                        <li>
                                            <Link to='/#'> <i className="fal fa-bookmark"></i></Link>
                                        </li> */}
                                        <li className="search_icon">
                                            <Link to='/#'>  <i className="fal fa-search"></i></Link>
                                        </li>
                                        {props && isEmptyObj(props.user) ? <li className="btn">
                                            <button className="btn-primary-4" onClick={() => handleStartModal(true)}>Registration</button>
                                        </li>
                                            :
                                            <li className="user">
                                                <Link to='/my-profile'>
                                                    <div className="img_area">
                                                        <img src={props.user && props.user.image ? renderImage(props.user.image, 'medium') : renderNoAvatar( (props.user && props.user.first_name ? props.user.first_name : ``) + ` ` + (props.user && props.user.last_name ? props.user.last_name : ``) )} />
                                                    </div>
                                                    <span>{props.user && props.user.first_name ? props.user.first_name : null} {props.user && props.user.last_name ? props.user.last_name : null}</span>

                                                </Link>
                                            </li>}
                                    </ul>
                                    <ul className="d-lg-none d-block phone">
                                        <li>
                                            <Link to='/phonemenu'>
                                                <i className="far fa-bars"></i>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <Managecardmodal /> */}
                {/* <Addcardmodal /> */}
                {/* <StudentdetailModal /> */}
                {/* <Loginmodal /> */}
                {
                    props.isStart
                    &&
                    <Getstartedmodal
                        show={props.isStart}
                        close={() => handleStartModal(false)}
                        openOtpModal={() => {
                            handleStartModal(false);
                            setShowotpModel(true);
                        }}
                        openForgotPasswordModal={() => {
                            handleStartModal(false);
                            setShowotpModel(false);
                            setShowForgotPasswordModal(true);
                        }}
                    />
                }
                {
                    showForgotPasswordModal
                    &&
                    <ForgotPasswordModal
                        show={showForgotPasswordModal}
                        close={() => setShowForgotPasswordModal(false)}
                        openGetStartedModal={() => {
                            setShowForgotPasswordModal(false);
                            setShowRegistrationModal(true);
                        }}
                        restRecoverPassword={(token, otp) => {
                            setShowForgotPasswordModal(false);
                            setShowRecoverPasswordModal(true);
                            setRecoverData({ token, otp })
                        }}
                    />
                }
                {
                    showRecoverPasswordModal && recoverData
                    &&
                    <RecoverPasswordModal
                        data={recoverData}
                        show={showRecoverPasswordModal}
                        close={() => setShowRecoverPasswordModal(false)}
                    />
                }
                {
                    showotpmodel
                        ?
                        <OTPmodal
                            show={showotpmodel}
                            close={() => setShowotpModel(false)}
                            openUserTypeModal={() => {
                                setShowotpModel(false);
                                setShowUserTypeModal(true);

                            }}
                            openGetStartedModal={() => {
                                setShowotpModel(false);
                                setShowRegistrationModal(true);
                            }}

                        /> : null
                }

                {
                    showusertypemodal
                    &&
                    <Usertypemodal
                        show={showusertypemodal}
                        close={() => setShowUserTypeModal(false)}

                    />
                }
                {props.isApply &&
                    <AgentContactModal
                        data={props.isApply}
                        show={props.isApply ? true : false}
                        close={() => {
                            new ModalController().setApplyModal(null);
                        }}
                    />
                }
                {/* <AddCreditModal /> */}
                {/* <AgentContactModal /> */}
                {/* <UniversityContactModal /> */}
                {/* <PurchaseModal /> */}
                {/* <AddReviewModal /> */}
                {/* <ReviewListingModal /> */}
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.UserReducer.user,
        isStart: state.StartedModalReducer.isStart,
        isApply: state.ApplyNowModalReducer.isApply,
    };
};

// export default withRouter(Header);
export default connect(mapStateToProps)(compose(
    withRouter(Header)

));





