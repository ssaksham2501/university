import moment from 'moment';
import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import applicant_stu1 from "../../assets/images/applicant_stu1.png"
import { renderImage, renderNoAvatar } from '../../helpers/General';

function StudentdetailModal(props) {
    const [info, setInfo] = useState(null);
    const [show, setShow] = useState(true);
    const handleClose = () => {
        setShow(false);
        setTimeout(() => {
            props.close();
        }, 500);
    };
    
    useEffect(() => {
        init();
    }, [])

    const init = () => {
        if (props.info) {
            console.log(props.info);
            setInfo(props.info);
        }
    }

    return (
        <>
            <Modal show={show} className="application_student" size="xl" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>Application Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="box_area ul">
                        <div className="box_inner_area ul">
                            <div className="top_area">
                                <div className="applicant_detail_area">
                                    <div className="applicant_image_area">
                                        <div className="image_area">
                                            <img src={info && info.user_image ? renderImage(info.user_image, 'medium') : renderNoAvatar(info ? info.information.first_name : ``)} />
                                        </div>
                                    </div>
                                    <div className="applicant_info_area">
                                        <div className="applicant_name">
                                            <div className="left_area">
                                                <h4>{info && info.information ? info.information.first_name + ` ` + info.information.last_name : ``}</h4>
                                            </div>
                                            <div className="right_area">
                                                {
                                                    info && info.information && info.information.dob
                                                    &&
                                                    <p>(Age {moment().diff(info.information.dob, 'years', false)})</p>
                                                }
                                            </div>
                                        </div>
                                        <div className="des_area">
                                            <p>{info && info.message ? info.message : '-'}</p>
                                        </div>
                                        <div className="applicant_location_area">
                                            <Link to='/#'>
                                                <div className="left_area">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                </div>
                                                <div className="right_area_in">
                                                    {info && info.information && info.information.state_name} {info && info.information && info.information.country && info.information.country_name}
                                                </div>
                                            </Link>
                                        </div>
                                        
                                        <div className="applicant_location_area">
                                            <Link to='/#'>
                                                <div className="left_area">
                                                    <i class="far fa-at"></i>
                                                </div>
                                                <div className="right_area_in">
                                                    {info && info.information && info.information.email}
                                                </div>
                                            </Link>
                                        </div>
                                        <div className="applicant_location_area">
                                            <Link to='/#'>
                                                <div className="left_area">
                                                    <i class="far fa-phone"></i>
                                                </div>
                                                <div className="right_area_in">
                                                    +{info && info.information && info.information.country_code}-{info && info.information && info.information.phonenumber}
                                                </div>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="save_icon_area">
                                    <div className="icon_area">
                                        <Link to='/'>
                                            <i class="far fa-bookmark"></i>
                                        </Link>
                                    </div>
                                    
                                    {/* <div className="view_area">
                                        <Link to='/'>
                                            <i class="fal fa-eye"></i> 80+ View
                                        </Link>
                                          </div>
                                    <div className="rating_area">
                                     <Rating
                                            // onClick={}
                                            ratingValue={ratingValue}
                                            showTooltip={true}
                                            initialValue={3.8}
                                            // readonly
                                            tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                                        />
                                    </div> */}
                                </div>
                            </div>
                            <div className="center_area">
                            </div>
                            <div className="bottom_area">
                                <div className="study_detail">
                                    <h3>Applicant Information</h3>
                                    <div className="study_inner_detail">
                                        <p>Education: <strong>{info && info.information && info.information.education && info.information.education.title} ({info && info.information && info.information.year_of_graduation})</strong></p>
                                        <p>Expecte or Gained Percentage: <strong>{info && info.information && info.information.gained_percentage && info.information.gained_percentage}%</strong></p>
                                        <p>Wish to pursue: <strong>{info && info.university_name}, {info && info.country}</strong></p>
                                        {
                                            info && info.information && info.information.countries && info.information.countries.length > 0
                                            &&
                                            <p>Other Interseted Countries: <strong>
                                                {
                                                    info.information.countries.map((item, key) => (
                                                        <>{(key > 0 ? `, ` : '') + item.short_name}</>
                                                    ))
                                                }
                                            </strong></p>
                                        }
                                        {
                                            info && info.information && info.information.streams && info.information.streams.length > 0
                                            &&
                                            <p>Interseted Stearms: <strong>
                                                {
                                                    info.information.streams.map((item, key) => (
                                                        <>{(key > 0 ? `, ` : '') + item.title}</>
                                                    ))
                                                }
                                            </strong></p>
                                        }
                                        {
                                            info && info.information && info.information.courses && info.information.courses.length > 0
                                            &&
                                            <p>Interseted Courses: <strong>
                                                {
                                                    info.information.courses.map((item, key) => (
                                                        <>{(key > 0 ? `, ` : '') + item.title}</>
                                                    ))
                                                }
                                            </strong></p>
                                        }
                                        <p>Prefered Intake: <strong>{info && info.information && info.information.prefered_intake && (info.information.prefered_intake.title + ' ' + info.information.prefered_intake.year)}</strong></p>
                                        {
                                            info && info.information && info.information.ielts > 0 && info.information.ielts_score > 0
                                            &&
                                            <p>IELTS Score: <strong>{(info.information.ielts_score * 1).toFixed(1)} Bands</strong></p>
                                        }
                                        {
                                            info && info.information && info.information.tofel > 0 && info.information.tofel_score > 0
                                            &&
                                            <p>TOEFL Score: <strong>{(info.information.tofel_score * 1).toFixed(1)}</strong></p>
                                        }
                                        {
                                            info && info.information && info.information.pte > 0 && info.information.pte_score > 0
                                            &&
                                            <p>PTE Score: <strong>{(info.information.pte_score * 1).toFixed(1)}</strong></p>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </Modal.Body>
            </Modal>
        </>
    );
}


export default (StudentdetailModal);