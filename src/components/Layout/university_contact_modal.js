import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import agentcon from "../../assets/images/agent_con.png"
import agentinfo from "../../assets/images/agent_info.png"
import Form from 'react-bootstrap/Form';
import Select from 'react-select';

function UniversityContactModal() {
    const [show, setShow] = useState(true);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const options = [
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' }
    ]
    return (
        <>
            <Modal show={show} className="application_student" size="lg" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>Apply</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="agent_contact_area">
                        <div className="bg_area">
                            <div className="top_area">
                                <div className="agent_info_area">
                                    <div className="uni_image_area">
                                        <div className="image_area">
                                            <img src={agentcon} />
                                        </div>
                                    </div>
                                    <div className="uni_info_area">
                                        <div className="uni_name">
                                            <h4>Penn State University</h4>
                                        </div>
                                        <div className="des_area">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in sem at nisl
                                                luctus in urna.  Mauris in sem.</p>
                                        </div>
                                        <div className="uni_location_area">
                                            <Link to='/#'>
                                                <div className="left_area">
                                                    <i class="fal fa-map-marker-alt"></i>
                                                </div>
                                                <div className="right_area_in">
                                                    PA 16801, United States
                                                </div>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="bottom_area">
                                <div className="agent_info">
                                    <h3>Alex Molet</h3>
                                    <div className="inner_detail">
                                        <p>Prefered Intake : May/ july 2022</p>
                                        <p>Exam : ILETS</p>
                                        <p>Overall Score : 6.5 Bands</p>
                                    </div>
                                </div>
                                <div className="edit_area">
                                    <Link to='/'>
                                        <i class="far fa-pen"></i>Edit
                                    </Link>
                                </div>
                            </div>
                            <div className="edit_bottom_area">
                                <div className="left_area">
                                    <div className="agent_info">
                                        <div className="divide_area">
                                            <div className="agent_name">
                                                <h3>Alex Molet</h3>
                                            </div>
                                            <div className="edit_area">
                                                <Link to='/'>
                                                    <i class="far fa-pen"></i>Edit
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="inner_detail">
                                            <p>Prefered Intake : May/ july 2022</p>
                                            <p>Exam : ILETS</p>
                                            <p>Overall Score : 6.5 Bands</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="right_area">
                                    <div className="image_area">
                                        <img src={agentinfo} />
                                    </div>
                                    <div className="name_area">
                                        <p>Cody Fisher</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="inputs_area">
                            <div className="select_area">
                                <Form.Label>Course</Form.Label>
                                <Select options={options} />
                            </div>
                            <div className="graduation_area">
                                <Form.Label>Message </Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="Type your message...."
                                    style={{ height: '120px' }}
                                />
                            </div>
                        </div>
                        <div className="submit_btn_area">
                            <div className="add_area">
                                <Link to='/'><button className="btn-primary-2 w-100" >Submit</button></Link>
                            </div>
                        </div>
                    </div>

                </Modal.Body>
            </Modal>
        </>




    );
}


export default (UniversityContactModal);