import React, {useEffect,useState} from "react";
import "./../../assets/sass/pages/home.scss";
import Header from "./Header";
import Footer from "./Footer";
import DashboardSidebar from "../Dashboard/Component/DashboardSidebar";
import FooterImg from "../SignUp/components/Footerimg";
import { useHistory, useLocation } from "react-router-dom";
import AuthController from "../../apis/controllers/auth.controller";


const LayoutOne = ({ children }) => {
    return (
      <div>
          <Header />
          <div className="main_add_met">
          {children}
          </div>
          <Footer />
      </div>
    );
}

const DashboardLayout = ({ children }) => {
  const [mounted, setMounted] = useState(false);
  const loc = useLocation();
  const history = useHistory();
  const init = async () => {
      window.scrollTo(0, 0);
      let response = await new AuthController().checkLoginState();
      if (!response.status) {
        new AuthController().setUpLogin({});
        history.push('/login');
      }
      else if(response.status && response.user_type === 'agent' && !response.agent_approved) {
        history.push('/awaiting-approval');
      }
      setMounted(true);
  }
  useEffect(() => {
      init()
  }, [loc]);

  return (
    <div>
        <Header />
        <div className="main_add_met">
          <div className="dashboard_wrap top_space">
                {
                  mounted
                  ?
                  <div className="dashboard_wrap_inner">
                    <div className="side_bar">
                         <DashboardSidebar />
                    </div>
                    <div className="user_content_side">
                        {children }
                    </div>
                  </div>
                  :
                  <p style={{padding: `250px`}} className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                }
          </div>
        </div>
    </div>
  );
}

const LayoutTwo = ({ children }) => {
    return (
      <div>
          <Header />
          <div className="main_add_met">
          {children }
          </div>
      </div>
    );
}

const LayoutThree = ({ children }) => {
  return (
    <div>
       <div className="main_add_met">
        {children }
        </div>
    </div>
  );
}

const Layoutfour = ({children }) => {
  return (
    <div>
      <Header />
        <div className="main_add_met">
         {children }
        </div>
      <FooterImg />
    </div>
  );
}

export { LayoutOne, LayoutTwo, LayoutThree, DashboardLayout, Layoutfour};
