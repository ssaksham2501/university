import { React, useState } from 'react'
import { Link, useHistory } from 'react-router-dom';
import profile from "../../../assets/images/profile.png";
import GetStarted from '../../Home/components/get_started';
import ForgotPasswordModal from '../ForgotPasswordModal';
import Getstartedmodal from '../Getstartedmodal';
import OTPmodal from '../Otpmodal';
import RecoverPasswordModal from '../RecoverPasswordModal';
import Usertypemodal from '../Usermodaltype';

const Phonemenuss = (props) => {
    const [showRegistrationModal, setShowRegistrationModal] = useState(false);
    const [showForgotPasswordModal, setShowForgotPasswordModal] = useState(false);
    const [showRecoverPasswordModal, setShowRecoverPasswordModal] = useState(false);
    const [recoverData, setRecoverData] = useState(null);
    
    const [showotpmodel, setShowotpModel] = useState(false);
    const [showusertypemodal, setShowUserTypeModal] = useState(false);
    let history = useHistory();
    
    return (
        <>
            <div className="phoneside d-lg-none d-block">
                <div className='container'>
                    <div className='row'>
                        <div className='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-0'>
                            <div className='head text-center'>
                                <h6>University</h6>
                                <div className='icon-Back'>
                                    <a
                                        onClick={() => {
                                            history.goBack();
                                        }}><i className="fas fa-long-arrow-left"></i></a>
                                </div>
                            </div>
                            <div className='profile'>
                                <ul>
                                    <li>
                                        <Link to="/profile">
                                            <div className='useree'>
                                                <div className='left'>
                                                    <div className='user-img'>
                                                        <img src={profile} />
                                                    </div>
                                                </div>
                                                <div className='right'>
                                                    <div className='name'>
                                                        <p>Alex Molet</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    </li>

                                    <li>
                                        <Link to="/about-us">Admission</Link>
                                    </li>
                                    <li>
                                        <Link to="/privacy-policy">Services</Link>
                                    </li>
                                    <li>
                                        <Link to="/contact-us">About us</Link>
                                    </li>
                                    <li>
                                        <Link to="/terms-conditions">Contact us</Link>
                                    </li>
                                    <li>
                                        <button className="btn-primary-4" onClick={() => setShowRegistrationModal(true)}>Registration</button>
                                    </li>
                                    <li>
                                        <a>Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                    showRegistrationModal
                    &&
                    <Getstartedmodal
                        show={showRegistrationModal}
                        close={() => setShowRegistrationModal(false)}
                        openOtpModal={() => {
                            setShowRegistrationModal(false);
                            setShowotpModel(true);
                        }}
                        openForgotPasswordModal={() => {
                            setShowRegistrationModal(false);
                            setShowotpModel(false);
                            setShowForgotPasswordModal(true);
                        }}
                    />
                }
                {
                    showForgotPasswordModal
                    &&
                    <ForgotPasswordModal
                        show={showForgotPasswordModal}
                        close={() => setShowForgotPasswordModal(false)}
                        openGetStartedModal={() => {
                            setShowForgotPasswordModal(false);
                            setShowRegistrationModal(true);
                        }}
                        restRecoverPassword={(token, otp) => {
                            setShowForgotPasswordModal(false);
                            setShowRecoverPasswordModal(true);
                            setRecoverData({token, otp})
                        }}
                    />
                }
                {
                    showRecoverPasswordModal && recoverData
                    &&
                    <RecoverPasswordModal
                        data={recoverData}
                        show={showRecoverPasswordModal}
                        close={() => setShowRecoverPasswordModal(false)}
                    />
                }
                {
                    showotpmodel
                        ?
                        <OTPmodal
                            show={showotpmodel}
                            close={() => setShowotpModel(false)}
                            openUserTypeModal={() => {
                                setShowotpModel(false);
                                setShowUserTypeModal(true);

                            }}
                            openGetStartedModal={() => {
                                setShowotpModel(false);
                                setShowRegistrationModal(true);
                            }}

                        /> : null
                }
        </>
    )
}



export default (Phonemenuss);
