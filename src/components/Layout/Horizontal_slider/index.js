import React from 'react';
import { ScrollMenu, VisibilityContext } from 'react-horizontal-scrolling-menu';


const getItems = () =>
    Array(20)
        .fill(0)
        .map((_, ind) => ({ id: `element-${ind}` }));

function HorizontalSlider({title}) {
    const [items, setItems] = React.useState(getItems);
    const [selected, setSelected] = React.useState([]);

    const isItemSelected = (id) => !!selected.find((el) => el === id);

    const handleClick =
        (id) =>
            ({ getItemById, scrollToItem }) => {
                const itemSelected = isItemSelected(id);

                setSelected((currentSelected) =>
                    itemSelected
                        ? currentSelected.filter((el) => el !== id)
                        : currentSelected.concat(id)
                );
            };

    return (
        <div>
            <div className="scroll_slider_area top_space">
                <div className="container">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="scroll_wrap">
                                <ScrollMenu LeftArrow={LeftArrow} RightArrow={RightArrow}>
                                    {items.map(({ id }) => (
                                        <Card
                                            itemId={id} // NOTE: itemId is required for track items
                                            title={id}
                                            key={id}
                                            onClick={handleClick(id)}
                                            selected={isItemSelected(id)}
                                            autoplay={true}
                                        />
                                    ))}
                                </ScrollMenu>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
    function LeftArrow() {
        const { scrollPrev } = React.useContext(VisibilityContext);

        return (
            <div className="left_arrow">
                <div className='arrow_bg' onClick={() => scrollPrev()} >
           
                </div>
              
            </div>
        );
    }

    function RightArrow() {
        const { scrollNext } = React.useContext(VisibilityContext);

        return (
            <div className="right_arrow">
                <div className='arrow_bg' onClick={() => scrollNext()} style={{ color: "red" }} >
                  
                </div>
               
            </div>

        );
    }

    function Card() {
        return (
            <div className="card">
                <div className="detail_area">
                    <button className="btn-primary-5">{title}</button>
                </div>
            </div>

        );
    }
}



export default HorizontalSlider;