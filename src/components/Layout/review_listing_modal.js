import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import { Dropdown } from "react-bootstrap";
// import ListingGrids from "./listi_view";
import Form from 'react-bootstrap/Form';
import Agent_review from '../Agent_listing/components/review_listing';
import AddReviewModal from './add_review_modal';


function ReviewListingModal(props) {
    
    const handleClose = () => props.close();
    const [reviewAccepted, setReviewAccepted] = useState(true);


    return (
        <>
            <Modal show={props.show} className="application_student review_list_met" size="lg" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>Reviews
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="box_area">
                        <div className="review_listing_area">
                            <div className="review_slider">
                                {
                                    props && props.university
                                    &&
                                    <Agent_review 
                                        agent={null}
                                        university={props.university}
                                        setReviewAccepted={(status) => {
                                            setReviewAccepted(status)
                                        }}
                                    />
                                }
                            </div>
                        </div>
                        {
                            !reviewAccepted
                            &&
                            <div className="submit_btn_area">
                                <div className="add_area">
                                    <button className="btn-primary-2 w-100" onClick={() => props.openAddReview()}>Write a review</button>
                                </div>
                            </div>
                        }
                    </div>

                </Modal.Body>
            </Modal>
            
        </>
    );
}


export default (ReviewListingModal);






