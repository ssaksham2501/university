import React, { useState, useEffect } from "react";
import Modal from 'react-bootstrap/Modal';
import { Link } from "react-router-dom";
import OTPInput, { ResendOTP } from "otp-input-react";
import ModalController from "../../apis/controllers/modal.controller";
import { getStoredItem, storeItem } from "../../helpers/localstorage";

function OTPmodal(props) {
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [otp, setOtp] = useState('');

    const handleChange = otp => setOtp(otp);

    useEffect(() => {
        window.scrollTo(0, 0);
        init();
    }, []);

    const init = async () => {
        let user = await getStoredItem("user")
        setUser(user);
    }


    //otp  --start
    const otpmodal = async () => {
        if (isLoading === false) {
            setErrMsg(``);

            /** Check full form validation and submit **/
            if (otp) {
                setIsLoading(true);
                let response = await new ModalController().otpVerification(otp);
                setIsLoading(false);
                if (response && response.status && response.user) {
                    setOtp('');
                    storeItem(`user`, response.user);
                    props.openUserTypeModal();
                } else {
                    setErrMsg(response.message);
                }
            } else {
                setErrMsg('Please enter a valid otp.');
            }
        }
    };

    const resendOtpRequest = () => {
        new ModalController().resendOtpRequest();
    }
    //otp --end

    return (
        <>
            <div className="user_type_modal">
                <Modal
                    className="otp_modal"
                    show={props.show}
                    onHide={() => props.close()}
                    dialogClassName="modal-90w"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title">
                            Verify your Phone Number
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='otp_set'>
                            <div className="change_area">
                                <div className='left_set'>
                                    <p>
                                        OTP sent to <strong>{user && user.phonenumber}</strong>
                                    </p>
                                </div>
                                <div className='right_set'>
                                    <Link to='/#'><p onClick={() => {
                                        props.openGetStartedModal();
                                    }}>Change </p></Link>  </div>
                            </div>
                            <div className="otp_area">
                                <div className="otp_detail">
                                    <p>Enter OTP</p>
                                    <OTPInput autoFocus OTPLength={6} otpType="number" value={otp} onChange={handleChange} />
                                    <ResendOTP className='otpnum_cout' onResendClick={resendOtpRequest} />
                                    {errMsg && <p className="text-danger text-center mt-3">{errMsg}</p>}
                                </div>
                                <div className="btn_area">
                                   
                                    <div className="center_area">
                                        <button className="btn-primary-2 w-100"
                                            type="submit"
                                            onClick={() => otpmodal()}>
                                            {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                            &nbsp; Proceed
                                        </button>
                                    </div>
                                    <div className="terms_area">
                                        <p>By continuing , you agree to our <Link to='/#'>Terms of service </Link> and <Link to='/#'>Privacy
                                            policy </Link></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (OTPmodal);