
import React, { useEffect } from "react";
import Modal from 'react-bootstrap/Modal';
import AgentsListing from '../../functions/agents/AgentsListing';
import AgentGrid from '../Agents_list/component/agent_grid';
import Agent_info from '../Agents_list/component/agent_list';
import Dropdown from "./components/Dropdown";

function AgentListModal(props) {
    const { agents, agentListingHandler, isLoading, totalAgents, sorting, setSorting, page, setPage } = AgentsListing(props);
    const handleClose = () => props.close();

    useEffect(() => {
        setSorting(sorting);
        agentListingHandler.sendRequest(true);
        return () => {

        }
    }, [sorting])


    return (
        <>
            <Modal show={props.open} className="application_student AgentListModal agent_new_popup" size="xl" onHide={handleClose} animation={true} aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title>{totalAgents} Agents of <strong>{props.universityDetail && props.universityDetail.slug ? props.universityDetail.title : ``}</strong></Modal.Title>
                    <div className="sort_by_warp d-sm-block d-none">
                        <ul>
                            <li>
                                <div className="sort_by_swap">
                                    <div className="left">
                                        <p>Sort by</p>
                                    </div>

                                    <div className="right">
                                        <Dropdown
                                            id="agent-sort-web"
                                            title={sorting ? sorting.title : `Rating`}
                                            selected={sorting}
                                            items={[
                                                {
                                                    title: "Rating",
                                                    value: "rating"
                                                },
                                                {
                                                    title: "Popular",
                                                    value: "popular"
                                                }
                                            ]}
                                            onSelect={async (item) => {
                                                setPage(1);
                                                setSorting(item);
                                            }}
                                        />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </Modal.Header>
                <Modal.Body>
                    <div className="agent_list p-0 mb-0 ">
                        <div className="container">
                            <div className="row">
                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                     <div className="sort_by_warp d-sm-none d-block">
                                            <ul>
                                                <li>
                                                    <div className="sort_by_swap">
                                                        <div className="left">
                                                            <p>Sort by</p>
                                                        </div>

                                                        <div className="right">
                                                            <Dropdown
                                                                id="agent-sort-mobile"
                                                                title={sorting ? sorting.title : `Rating`}
                                                                selected={sorting}
                                                                items={[
                                                                    {
                                                                        title: "Rating",
                                                                        value: "rating"
                                                                    },
                                                                    {
                                                                        title: "Popular",
                                                                        value: "popular"
                                                                    }
                                                                ]}
                                                                onSelect={async (item) => {
                                                                    setPage(1);
                                                                    setSorting(item);
                                                                }}
                                                            />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    <div className="agent_detail_wrap">
                                        <div className="listing_side_view">
                                            <div className="listing_side_main_set_wrap ">
                                                <div className="listing_head_wrap mt-0 ">
                                                    <div className="left">
                                                        {/* <p>{totalAgents} <span>agents found</span></p> */}
                                                    </div>
                                                    <div className="right">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="agent_list">
                                            <div className="row">
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    {
                                                        agents !== null
                                                        &&
                                                        <div className="uni_detail_wrap"
                                                            onScroll={(e) => agentListingHandler.handleScroll(e)}
                                                        >
                                                            <div className="row">
                                                                {
                                                                    agents.map((item, i) => {
                                                                        return <AgentGrid
                                                                            key={`modal-grid-` + i}
                                                                            grid="4"
                                                                            index={i}
                                                                            item={item}
                                                                            handleApplyModal={props.handleApplyModal}
                                                                        />
                                                                    }
                                                                    )
                                                                }
                                                            </div>
                                                            {
                                                                isLoading
                                                                &&
                                                                <p className="text-center"><i class="fa fa-spin fa-spinner"></i></p>
                                                            }    
                                                        </div>
                                                    }
                                                    {
                                                        isLoading && !agents
                                                        &&
                                                        <p className="text-center"><i class="fa fa-spin fa-spinner"></i></p>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </Modal.Body>
            </Modal>
        </>
    );
}


export default (AgentListModal);