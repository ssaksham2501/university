import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { Link, withRouter } from "react-router-dom";

function Loginmodal() {
    const [show, setShow] = useState(false);

    return (
        <>
            <div className="login_modal">
                <Modal
                    show={show}
                    onHide={() => setShow(false)}
                    dialogClassName="modal-90w"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title" className='head-set el'>
                            Login
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='login_pop'>
                            <div className='content'>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisNullam maximus orci  suscipit rutrum.
                                </p>
                            </div>
                            <div className="login_input_area">
                                <div className="expect_education_area">
                                    <Form>
                                        <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Username </Form.Label>
                                            <Form.Control type="email" placeholder="Enter username or email" />
                                        </Form.Group>
                                    </Form>
                                </div>
                                <div className="expect_education_area">
                                    <Form>
                                        <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control type="email" placeholder="Enter password" />
                                            <Link to='/'> <i class="far fa-eye-slash"></i></Link>

                                        </Form.Group>
                                    </Form>
                                    <div className='forget'>
                                        <Link to="">Forgot password?</Link>
                                    </div>
                                </div>
                                <div className="btn_area">
                                    <div className="center_area">
                                        <Link to='/'><button className="btn-primary-6 w-100" >Login</button></Link>
                                    </div>
                                    <p>
                                        Don’t have an account? <Link to="">Sign Up</Link>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (Loginmodal);