import React, { useState, useEffect } from "react";
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { useHistory } from "react-router-dom";
import ModalController from "../../apis/controllers/modal.controller";
import { getStoredItem, storeItem } from "../../helpers/localstorage";

function Usertypemodal(props) {
    const history = useHistory();
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [usertype, setUserType] = useState('');

    useEffect(() => {
        window.scrollTo(0, 0);
        init();
    }, []);

    const init = async () => {
        let user = await getStoredItem("user")
        setUser(user);
    }


    //usersignup  --start
    const usermodal = async (usertype) => {
        if (isLoading === false) {
            setErrMsg(``);
           
            /** Check full form validation and submit **/
            if (usertype) {
                setIsLoading(true);
                let response = await new ModalController().userTypeSignup(usertype);
                setIsLoading(false);
                if (response && response.status && response.user) {
                    props.close();
                    storeItem(`user`, response.user);
                    if (response.user.user_type === 'student') {
                        history.push('/signup');
                    }
                    else {
                        history.push('/agent-signup');
                    }

                } else {
                    setErrMsg(response.message);
                }
            } else {
                setErrMsg('Please select your signup type.');
            }
        }
    };
    //usersignup  --end
    return (
        <>
            <div className="User_type_modal">
                <Modal
                    show={props.show}
                    onHide={() => props.close()}
                    dialogClassName="modal-90w"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title" className='head-set'>
                            User type
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="user_type_input_area">
                            <div className="high_education_area">
                                <Form>
                                    {['radio'].map((type) => (
                                        <div key={`inline-${type}`} className="mb-3">
                                            <Form.Check
                                                label="Student"
                                                name="group1"
                                                type={type}
                                                id={`inline-${type}-13`}
                                                onClick={async () => {
                                                    await usermodal('student');
                                                }}
                                            />
                                            <Form.Check
                                                label="Agent"
                                                name="group1"
                                                type={type}
                                                id={`inline-${type}-14`}
                                                onClick={async () => {
                                                    await usermodal('agent');
                                                }}
                                            />
                                        </div>
                                    ))}
                                </Form>
                            </div>
                            {/* <div className="btn_area">
                                {errMsg && <p className="text-danger text-center">{errMsg}</p>}
                                <div className="center_area">
                                    <button className="btn-primary-2 w-100"
                                        type="submit"
                                        onClick={() => usermodal()}>
                                        {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                        &nbsp; Proceed
                                    </button>
                                </div>
                            </div> */}
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (Usertypemodal);