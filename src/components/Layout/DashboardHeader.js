import React from "react";
import { Link, withRouter } from "react-router-dom";
import logo from "../../assets/images/logo.png";


function DashboardHeader() {
    return (
        <div>
            <div className="header_section dashboard">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="head_area">
                                <div className="left_area">
                                    <div className="logo_area">
                                        <Link to='/'>
                                            <img src={logo} />
                                        </Link>
                                    </div>
                                </div>
                                <div className="right_area">
                                    <ul className="d-lg-block d-none">
                                        <li className="menus">
                                            <Link to='/#'>Admission</Link>
                                        </li>
                                        <li className="menus">
                                            <Link to='/#'>Services</Link>
                                        </li>
                                        <li className="menus">
                                            <Link to='/#'>About us</Link>
                                        </li>
                                        <li className="menus">
                                            <Link to='/#'>Contact us</Link>
                                        </li>
                                        <li className="dot">
                                            <Link to='/#'><i className="fal fa-bell"></i></Link>
                                        </li>
                                        <li>
                                            <Link to='/#'> <i className="fal fa-bookmark"></i></Link>
                                        </li>
                                        <li className="search_icon">
                                            <Link to='/#'>  <i className="fal fa-search"></i></Link>
                                        </li>
                                        <li className="btn">
                                            <button className="btn-primary-4" >Registration</button>
                                        </li>
                                    </ul>
                                    <ul className="d-lg-none d-block phone">
                                        <li>
                                            <Link to='/phonemenu'>
                                                <i className="far fa-bars"></i>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default withRouter(DashboardHeader);
