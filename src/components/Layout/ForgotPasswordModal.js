import React, { useEffect } from "react";
import { Form } from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';
import { parsePhoneNumber } from 'react-phone-number-input';
import 'react-phone-number-input/style.css';
import { Link, useHistory } from "react-router-dom";
import ModalController from "../../apis/controllers/modal.controller";
import ForgotPassword from "../../functions/auth/forgotPassword";
import Phonecomponent from "../Phone_input";
import OTPInput, { ResendOTP } from "otp-input-react";

function ForgotPasswordModal(props) {
    const history = useHistory();
    const { show, close } = props;
    const { forgotPasswordHandler, email, errMsg, isLoading, otpToken, otp} = ForgotPassword(props);
    
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    
    return (
        <>
            <div className="user_type_modal">
                <Modal
                    show={show}
                    onHide={() => close()}
                    dialogClassName="modal-90w"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="example-custom-modal-styling-title" className='head-set'>
                            {otpToken ? 'Recover password' : 'Forgot Password ?'}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className='get_start'>
                            <div className='content'>
                                <p>
                                    { otpToken ? `An email having OTP sent to your inbox. Please provide the OTP to recover the password.` : `Recover your password by providing your registered email address.` }
                                </p>
                            </div>
                            <div className="get_started_area">
                                <div className="expect_education_area">
                                    <div className="phone_input_area top_space">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 px-0">

                                                    <div className="phone_wrap">
                                                        <div className="detail_area">
                                                            {
                                                                otpToken
                                                                ?
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Enter One Time Password</Form.Label>
                                                                        <OTPInput className='numberinput' autoFocus OTPLength={6} otpType="number" value={otp} onChange={forgotPasswordHandler.changeOTP} />
                                                                        <ResendOTP className='otpnum_cout' onResendClick={forgotPasswordHandler.resendOtpRequest} />

                                                                    </Form.Group>
                                                                :
                                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                        <Form.Label>Enter your email address </Form.Label>
                                                                        <Form.Control
                                                                            type="email"
                                                                            placeholder="john.doe@example.com"
                                                                            value={email}
                                                                            onChange={forgotPasswordHandler.handleChange}
                                                                        />
                                                                        {errMsg && <p className="text-danger text-center mt-3">{errMsg}</p>}
                                                                    </Form.Group>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="btn_area">
                                                        
                                                        <div className="center_area">
                                                            <button className="btn-primary-2 w-100"
                                                                type="submit"
                                                                onClick={() => {
                                                                    if (otpToken) {
                                                                        forgotPasswordHandler.validateOtp();
                                                                    }
                                                                    else {
                                                                        forgotPasswordHandler.sendRequest()
                                                                    }
                                                                }}
                                                            >
                                                                {isLoading && <i className="fa fa-spin fa-spinner"></i>}
                                                                &nbsp;
                                                                Continue</button>
                                                        </div>

                                                        <div className="terms_area">
                                                            <p>Do not have any accont?<Link to='#' onClick={(e) => {
                                                                e.preventDefault();
                                                                props.openGetStartedModal();
                                                            }}>Sign Up</Link></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                               
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    );
}

export default (ForgotPasswordModal);