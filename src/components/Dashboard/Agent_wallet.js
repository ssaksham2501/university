import React , {useState} from "react";
import DashboardHeadings from "../Dashboard/Component/Dashboardheading";
import wallet_icon from "../../assets/images/wallet_icon.png";
import dolor_icon from "../../assets/images/dolor_icon.png"
import card_img from "../../assets/images/card_img.png"
import { Dropdown, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Managecardmodal from "../Layout/manage_card_modal";
import Addcardmodal from "../Layout/Add_card_modal";
import StudentdetailModal from "../Layout/applicant_student_detail_modal";
function AgentWallet() {
    const [managecard, manageCard] = useState(false);
    const [addcard, addCard] = useState(false);
    return (
        <>
            <div>
                <div className="heading_area">
                    <DashboardHeadings title="My Wallet" />
                </div>

                <div className="inner_info_mine">
                    <div className="my_applications_area">
                        <div className="cards_area">
                            <div className="row">
                                <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12'>
                                    <div className="cards_detail_area top">
                                        <div className="box_area">
                                            <div className="default_card">
                                                <img src={card_img} />
                                            </div>
                                            <div className="top_area">
                                                <div className="left_area">
                                                    <div className="image_area">
                                                        <img src={wallet_icon} />
                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="manage_btn_area">
                                                       <button className="btn-primary-4" onClick={() => manageCard(true)} >Manage Card</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="bottom_area">
                                                <p>4438   6721   7392   8721</p>
                                                <div className="inner_info">
                                                    <div className="name_info">
                                                        <p>Sweety Paul</p>
                                                    </div>
                                                    <div className="expiry_info">
                                                        <p>10/30</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12'>
                                    <div className="cards_detail_area">
                                        <div className="box_area bg">
                                            <div className="top_area">
                                                <div className="left_area">
                                                    <div className="inner_info">
                                                        <h4>My Wallet</h4>
                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="add_credit_btn_area">
                                                        <button className="btn-primary-4" onClick={() => addCard(true)} >Add Credit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="bottom_area">
                                                <div className="inner_info">
                                                    <h3>1000 <sub>Credits</sub></h3>
                                                </div>
                                            </div>
                                            <div className="doller_img">
                                                <img src={dolor_icon} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="inner_heading_area">
                            <h2>Wallet History</h2>
                        </div>
                        <div className="my_orders my_order">
                            <div className="box_area">
                                <div className="table_row_area">
                                    <div className="table-responsive">
                                        <table className="table">
                                            <thead>
                                                <tr>
                                                    <th width="">
                                                        Transactions id
                                                    </th>
                                                    <th width="">
                                                        Order id
                                                    </th>
                                                    <th width="">
                                                        Amount
                                                    </th>
                                                    <th width="">
                                                        Date & Time
                                                    </th>
                                                    <th width="">
                                                        Status
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr className="applicant_area ul">
                                                    <td valign="middle">
                                                        <p> Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $10.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="view_area">
                                                            <div className="transaction_status">
                                                                <p>Paid</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area">
                                                    <td valign="middle">
                                                        <p>Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $15.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="transaction_status">
                                                            <p>Paid</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area ul">
                                                    <td valign="middle">
                                                        <p> Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $10.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="view_area">
                                                            <div className="transaction_status">
                                                                <p>Paid</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area">
                                                    <td valign="middle">
                                                        <p>Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $15.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="transaction_status">
                                                            <p>Paid</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area ul">
                                                    <td valign="middle">
                                                        <p> Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $10.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="view_area">
                                                            <div className="transaction_status">
                                                                <p>Paid</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area">
                                                    <td valign="middle">
                                                        <p>Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $15.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="transaction_status ul">
                                                            <p>Failed</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area ul">
                                                    <td valign="middle">
                                                        <p> Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $10.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="view_area">
                                                            <div className="transaction_status">
                                                                <p>Paid</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr className="applicant_area">
                                                    <td valign="middle">
                                                        <p>Mavidh256478987 </p>
                                                    </td>
                                                    <td valign="middle">
                                                        #5687
                                                    </td>
                                                    <td valign="middle">
                                                        $15.00
                                                    </td>
                                                    <td valign="middle">
                                                        19mar,2022 4:30 PM
                                                    </td>
                                                    <td valign="middle">
                                                        <div className="transaction_status ul">
                                                            <p>Failed</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            { managecard&&
            <Managecardmodal
              show={managecard}
              close={()=>manageCard(false)}
            />
            }
             { addcard&&
            <Addcardmodal
              show={addcard}
              close={()=>addCard(false)}
            />
            }
                {/* <Addcardmodal 
                 managecard = {false}
                /> */}
                {/* <StudentdetailModal 
                 managecard = {false}
                /> */}
        </>
    );
}


export default AgentWallet;