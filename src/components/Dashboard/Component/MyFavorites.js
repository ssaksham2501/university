import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import FavoriteUniversities from "../../../functions/Students/favoritesUniversity";
import  ListingGrids from "../../listing/components/ListingGrids";
import DashboardHeadings from "./Dashboardheading";
import Preference from "./preference";
function MyFavorites(props) {

    const { favUniverties, setFavUniverties, loader, fetch, setFetching, page, done, total, getFavList, applyUniversity, favUniversity, } = FavoriteUniversities();


    const [list, setList] = useState(false);
    useEffect(() => {
        window.scrollTo(0, 0);
        getFavList(1);
    }, []);


    const handlePage = () => {
        if (!done && !fetch) {
            setFetching(true);
            getFavList(page);
        }
    }



    return (
        <>
            <div>
                <div className="heading_area">
                    <DashboardHeadings title="My favorites" />
                </div>
                <div className="listing_section dashboard">
                    <div className="inner_info_mine listing">
                        <div className="listing_side_main_set_wrap two d-xl-block d-none ">
                            <div className="right_area">
                                <div className="listing_head_wrap">
                                    <div className="left d-sm-block d-none">
                                        <p>{total > 0 ? <>{total}<span> listing found</span></> : `` }</p>
                                    </div>
                                    <div className="right">
                                        <div className="sort_by_warp">
                                            <ul>
                                                <li>
                                                <div className="listing_view">
                                                    <Link to="" className={!list ? 'inactive' : "active"} onClick={(e) => {
                                                    e.preventDefault();
                                                    setList(true)
                                                    }}>
                                                    <i className="fal fa-bars"></i>
                                                    </Link>
                                                    <Link to="" className={list ? 'inactive' : "active"} onClick={(e) => {
                                                    e.preventDefault();
                                                    setList(false);
                                                    }}>
                                                    <i className="fal fa-border-all"></i>
                                                    </Link>
                                                </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="d-flex">
                            <div className="my_favorites_area">
                                <div className="lsiting_show_wrap">
                                    <div className="list_grid">
                                        <div className={list ? "list_grid_wrap list_single" : "list_grid_wrap"}>
                                            {
                                                favUniverties && favUniverties.length > 0 && favUniverties.map((item, index) => {
                                                    return (<ListingGrids
                                                        key={`listin-grid` + index}
                                                        listView={false}
                                                        university={item}
                                                        setFavourite={async (slug, index) => {
                                                            await favUniversity(slug, index);
                                                        }}
                                                        applyNow={(e) => applyUniversity(e)}
                                                    />)
                                                })
                                            }
                                            {
                                                loader
                                                &&
                                                <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p>
                                            }
                                        </div>
                                    </div>
                                </div>
                                <a onClick={() => handlePage()}>View More</a>
                            </div>
                            <div className="profile_area d-xl-block d-none">
                                <div className="profile-flexi">
                                    <Preference />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}


export default MyFavorites;