import React, { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ActionController from "../../../apis/controllers/action.controller";
import AgentProfileController from "../../../apis/controllers/agentController/agent.controller";
import AuthController from "../../../apis/controllers/auth.controller";
import StudentProfileController from "../../../apis/controllers/studentController/student.controller";
import coins_img from '../../../assets/images/coins_img.png';
import dolor_icon from "../../../assets/images/dolor_icon.png";
import dots from '../../../assets/images/dots.png';
import dots2 from '../../../assets/images/dots2.png';
import facebook_icon from '../../../assets/images/facebook_icon.png';
import whatsapp_icon from '../../../assets/images/whatsapp_icon.png';
import { getFlagIcon, isEmpty } from "../../../helpers/General";
import Validation from "../../../helpers/vaildation";
import AddCreditModal from "../../Layout/Profile_add_credit_modal";

function Preferances(props) {
    const [maxCountries, setMaxCountries] = useState(7);
    const [states, setStates] = useState([]);
    const [userdata, setUser] = useState(null);
    const [imageBox, setImageBox] = useState(false);
    const [mount, setMount] = useState(true);
    const [profileLoader, setProfileLoader] = useState(false);
    const [passwordLoader, setPasswordLoader] = useState(false);
    const [cerdits, setCredits] = useState(false);
    let defaultValues = {
      firstName: null,
      lastName: null,
      email: null,
      phoneNumber: null,
      stateId: null,
      pincode: null,
    };
    const [values, setValues] = useState(defaultValues);
  
    let defaultOtherValues = {
      oldPassword: null,
      newPsssword: null,
      confirmPassword: null
    };
    const [otherValues, setOtherValues] = useState(defaultOtherValues);
  
    const [isError, setError] = useState({
      image: {
        rules: [],
        isValid: true,
        message: "",
      },
      firstName: {
        rules: ["required"],
        isValid: true,
        message: "",
      },
      lastName: {
        rules: ["required"],
        isValid: true,
        message: "",
      },
      email: {
        rules: ["required", "email"],
        isValid: true,
        message: "",
      },
      stateId: {
        rules: ["required"],
        isValid: true,
        message: "",
      },
      pincode: {
        rules: ["required"],
        isValid: true,
        message: "",
      },
      oldPassword: {
        rules: ["required", 'password'],
        isValid: true,
        message: "",
      },
      newPsssword: {
        rules: ["required", 'password'],
        isValid: true,
        message: "",
      },
      confirmPassword: {
        rules: ["required", 'password'],
        isValid: true,
        message: "",
      },
      bio: {
        rules: ["required"],
        isValid: true,
        message: "",
      },
    });
  
    let validation = new Validation(isError);
  
    const handleStates = (field, value) => {
      let node = validation.validateField(field, value);
      setError({ ...isError, [field]: node });
      setValues({ ...values, [field]: value });
    };
  
    const handleOtherStates = (field, value) => {
      let node = validation.validateField(field, value);
      setError({ ...isError, [field]: node });
      setOtherValues({ ...otherValues, [field]: value });
    };
  
    useEffect(() => {
      window.scrollTo(0, 0);
      setValues(defaultValues);
      setImageBox(false);
      init()
    }, []);
  
    const init = async () => {
      await getStatesData();
      await getProfile();
    }
  
    const submitEditProfile = async () => {
      if (!profileLoader) {
          let validation = new Validation(isError);
          let isValid = await validation.isFormValid(values);
          if (isValid && !isValid.haveError) {
              setProfileLoader(true);
              let response = await new StudentProfileController().updateProfile(values);
              setProfileLoader(false);
              if (response && response.status) {
                  new AuthController().setUpLogin(response.user);
              }
          } else {
              setError({ ...isValid.errors });
              setProfileLoader(false);
          }
      }
    };
  
    const getStatesData = async () => {
      setMount(true);
      let response = await new ActionController().getStates();
      if (response.status) {
        setStates(response.listing)
        setMount(false);
      }
      else {
        console.log('Error while fetching api data.')
        setMount(false);
      }
      setMount(false);
    }
  
    const getProfile = async () => {
      setMount(true);
      let res = await new StudentProfileController().profileDetails();
      if (res && res.status) {
        let data = res && res.user;
        setValues({
          ...values,
          firstName: data.first_name,
          lastName: data.last_name,
          email: data.email,
          phoneNumber: data.phonenumber,
          countryCode: data.country_code,
          stateId: data.state_id,
          pincode: data.pincode,
          bio: data.bio,
        });
        setUser(data);
        setMount(false);
      }
      setMount(false);
    };
  
  
    const submitChangePassword = async () => {
      let validation = new Validation(isError);
      let isValid = await validation.isFormValid(otherValues);
      let node = validation.matchValues(`confirmPassword`, otherValues.newPsssword, otherValues.confirmPassword, "Confirm password does not match.");
      setError({ ...isValid.errors, confirmPassword: node, });
      if (isValid && !isValid.haveError) {
        setPasswordLoader(true);
        let response = await new AgentProfileController().changePasswords(otherValues);
        if (response && response.status) {
          setOtherValues(defaultOtherValues)
          setPasswordLoader(false);
        }
      } else {
        setError({ ...isValid.errors });
        setPasswordLoader(false);
      }
    };
  
    const removeImage = async () => {
      if (props.user && props.user.image && props.user.image.small) {
        let response = await new ActionController().removeUserFile();
        if (response && response.status) {
          console.log(`response`, response.user);
          new AuthController().setUpLogin(response.user); 
        }
      }
    }
  return (
    <>

        {props && props.user && props.user.user_type === 'agent' ?
        <div className="right">
            <div className="wallet_detail_area">
            <div className="box_area bg">
                <div className="top_area">
                <div className="left_area">
                    <div className="inner_info">
                    <h4>My Wallet</h4>
                    </div>
                </div>
                <div className="right_area">
                    <div className="manage_btn_area">
                    <button className="btn-primary-4" onClick={() => setCredits(true)} >Add Credit</button>
                    </div>
                </div>
                </div>
                <div className="bottom_area">
                <div className="inner_info">
                    <p>Availabe Credits</p>
                    <h3>1000 <sub>Credits</sub></h3>
                </div>
                </div>
                <div className="doller_img">
                <img src={dolor_icon} />
                </div>
                <div className="dots2">
                <img src={dots2} />
                </div>
            </div>
            </div>
            <div className="head_set">
            <h2>Preferances</h2>
            </div>
            <div className="preferances">
            <div className="flag_icon_area">
                {/* {userdata && userdata.countries && userdata.countries.map((item, index) => (
                <div key={index} className="image_area">
                    <img src={getFlagIcon(item.short_name)} />
                </div>
                ))} */}
                {userdata && userdata.countries.filter((item, i) => i < (userdata && userdata.countries.length > 7 && maxCountries > 0 ? maxCountries : userdata && userdata.countries.length)).map((item, index) => (
                <div key={index} className="image_area">
                    <img src={getFlagIcon(item.short_name)} />
                </div>
                ))}
                <div className="image_area add">
                <Link to='/agent-change-preference'>
                    <i class="far fa-plus"></i>
                </Link>
                </div>
            </div>
            {userdata && userdata.universities && userdata.universities.length > 0 &&
                <div className="uni_area">
                <div className="left_area">
                    <i class="fal fa-graduation-cap"></i>
                </div>
                <div className="right_area">
                    <h2>{userdata && userdata.universities && userdata.universities.length}+</h2>
                    <p>Universities </p>
                </div>
                </div>}
            <div className="dots">
                <img src={dots} />
            </div>
            </div>
            <div className="save_changes">
            <Link to='/agent-change-preference'>
                <button className="btn-primary-2 w-100">Change</button>
            </Link>
            </div>
            <div className="invite_friend_area">
            <div className="top_box">
                <div className="image_area">
                <img src={coins_img} />
                </div>
                <div className="info_area">
                <p>Invite a friend and get <strong>4 credit</strong>
                    on his first purchase.</p>
                </div>
                <div className="dots2">
                <img src={dots2} />
                </div>
            </div>
            <div className="bottom_box">
                <div className="expect_education_area">
                <Form>
                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                    <Form.Control plaintext readOnly defaultValue="Loremipsum/v8ak47" />
                    <Link to='/'> <i class="fal fa-clone"></i></Link>
                    </Form.Group>
                </Form>
                </div>
                <div className="btn_copy_area">
                <div className="facebook_btn">
                    <Link to='/'>
                    <img src={facebook_icon} />
                    </Link>

                </div>
                <div className="whatsapp_btn">
                    <Link to='/'>
                    <img src={whatsapp_icon} />
                    </Link>
                </div>
                </div>
            </div>
            </div>
        </div>
        : <div className="right">
            <div className="head_set">
            <h2>Preferances</h2>
            </div>
            <div className="preferances">
            {/* <div className="flag">
                <div className="leftim">
                <img src={flag} />
                </div>
                <div className="rightin">
                <p>
                    Canada
                </p>
                </div>
            </div> */}
            <div className="flag_icon_area">
                {userdata && userdata.countries.filter((item, i) => i < (userdata && userdata.countries.length > 7 && maxCountries > 0 ? maxCountries : userdata && userdata.countries.length)).map((item, index) => (
                <div key={index} className="image_area">
                    <img src={getFlagIcon(item.short_name)} />
                </div>
                ))}
                <div className="image_area add">
                <Link to='/student-change-preference-step-1'>
                    <i class="far fa-plus"></i>
                </Link>
                </div>
            </div>
            <div className="intake">
                <p>Preferred intake : {userdata && userdata.prefered_intake ? userdata.prefered_intake.title + ' ' + userdata.prefered_intake.year : null}</p>
                {userdata && userdata.streams && userdata.streams.length > 0 ? <p>Wish to pursue : {userdata && userdata.streams && userdata.streams.reduce(function (a, b) {
                return (a.title || a) + ", " + b.title
                })} </p> : null}
            </div>
            <div className="dots">
                <img src={dots} />
            </div>
            </div>
            <div className="save_changes">
            <Link to='/student-change-preference-step-1'>
                <button className="btn-primary-2 w-100">Change</button>
            </Link>
            </div>
        </div>}
            
         
        { cerdits&&
            <AddCreditModal
              show={cerdits}
              close={()=>setCredits(false)}
            />
          }
      
    </>
  );

}
const mapStateToProps = state => ({
  user: state.UserReducer.user,
});
export default connect(mapStateToProps)(Preferances);
