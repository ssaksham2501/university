
import React from "react";


function DashboardHeadings(props) {
    return (
      <>
          <div className="heading_wrap_set_dashboard">
                <h2>{props.title}</h2>
          </div>
      </>
    );
  }


  export default  DashboardHeadings;