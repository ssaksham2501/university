import React, { useState } from "react";
import DashboardHeadings from "./Dashboardheading";
import { Dropdown, Form } from "react-bootstrap";
import agent_photo5 from "../../../assets/images/agent_photo5.png"
import { Link } from "react-router-dom";
import agent_banner from "../../../assets/images/agent_banner.png"
import agent_applicant from "../../../assets/images/agent_applicant.png"
import inprofile from "../../../assets/images/inprofile.png"
import uni_logo from "../../../assets/images/uni_logo.png"
import dots2 from '../../../assets/images/dots2.png';
import dots_bottom from '../../../assets/images/dots_bottom.png';
function MyApplications() {

    const [ratingValue, setRatingValue] = useState(0) // initial rating v

    const handleRating = (rate) => {
        setRatingValue(rate)
    }
    
    // Catch Rating value
    const handleReset = () => {
        setRatingValue(4)
    }

    return (
        <>
            <div>
                <div className="university_section">
                    <div className="banner_inner">
                        
                        <div>
                            <div className="inner_heading_area">
                                <h4>My Applications</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div className="center_heading_area">
                            </div>
                            <div className="box_area">
                                <div className="box_inner_area">
                                    <div className="top_area">
                                        <div className="applicant_detail_area">
                                            <div className="applicant_image_area">
                                                <div className="image_area">
                                                    <img src={agent_applicant} />
                                                </div>
                                            </div>
                                            <div className="applicant_info_area">
                                                <div className="applicant_name">
                                                    <div className="left_area">
                                                        <h4>Dinkle Kapoor</h4>
                                                    </div>
                                                    <div className="right_area">
                                                        <p>(Age 23)</p>
                                                    </div>
                                                </div>
                                                <div className="applicant_location_area">
                                                    <Link to='/#'>
                                                        <div className="left_area">
                                                            <i class="fal fa-map-marker-alt"></i>
                                                        </div>
                                                        <div className="right_area_in">
                                                            <p> PA 16801, United States</p>

                                                        </div>
                                                    </Link>
                                                </div>
                                                <div className="applicant_uni_name">
                                                    <p>Penn State University </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="save_icon_area">
                                            <div className="icon_area">
                                                <Link to='/'>
                                                    <i class="far fa-bookmark"></i>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="bottom_area">
                                        <div className="left_area">
                                            <div className="detail_area">
                                                <ul>
                                                    <li>
                                                        <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                    </li>
                                                    <li>
                                                        <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                    </li>
                                                    <li className="other">
                                                        <Link to='/' className="listing_button">Canada</Link>
                                                    </li>
                                                    <li className="other">
                                                        <Link to='/' className="listing_button">May/july 2020 </Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="right_area">
                                            <div className="purchase_btn">
                                                <Link to='/' class="btn btn-primary-2">
                                                    Purchase
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="box_inner_area">
                                    <div className="top_area">
                                        <div className="applicant_detail_area">
                                            <div className="applicant_image_area">
                                                <div className="image_area">
                                                    <img src={agent_applicant} />
                                                </div>
                                            </div>
                                            <div className="applicant_info_area">
                                                <div className="applicant_name">
                                                    <div className="left_area">
                                                        <h4>Dinkle Kapoor</h4>
                                                    </div>
                                                    <div className="right_area">
                                                        <p>(Age 23)</p>
                                                    </div>
                                                </div>
                                                <div className="applicant_location_area">
                                                    <Link to='/#'>
                                                        <div className="left_area">
                                                            <i class="fal fa-map-marker-alt"></i>
                                                        </div>
                                                        <div className="right_area_in">
                                                            <p> PA 16801, United States</p>
                                                        </div>
                                                    </Link>
                                                </div>
                                                <div className="applicant_uni_name">
                                                    <p>Penn State University </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="save_icon_area">
                                            <div className="icon_area">
                                                <Link to='/'>
                                                    <i class="far fa-bookmark"></i>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="bottom_area">
                                        <div className="left_area">
                                            <div className="detail_area">
                                                <ul>
                                                    <li>
                                                        <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                    </li>
                                                    <li>
                                                        <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                    </li>
                                                    <li className="other">
                                                        <Link to='/' className="listing_button">Canada</Link>
                                                    </li>
                                                    <li className="other">
                                                        <Link to='/' className="listing_button">May/july 2020 </Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="right_area">
                                            <div className="purchase_btn">
                                                <Link to='/' class="btn btn-primary-2">
                                                    Purchase
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="box_inner_area">
                                    <div className="top_area">
                                        <div className="applicant_detail_area">
                                            <div className="applicant_image_area">
                                                <div className="image_area">
                                                    <img src={agent_applicant} />
                                                </div>
                                            </div>
                                            <div className="applicant_info_area">
                                                <div className="applicant_name">
                                                    <div className="left_area">
                                                        <h4>Dinkle Kapoor</h4>
                                                    </div>
                                                    <div className="right_area">
                                                        <p>(Age 23)</p>
                                                    </div>
                                                </div>
                                                <div className="applicant_location_area">
                                                    <Link to='/#'>
                                                        <div className="left_area">
                                                            <i class="fal fa-map-marker-alt"></i>
                                                        </div>
                                                        <div className="right_area_in">
                                                            <p> PA 16801, United States</p>
                                                        </div>
                                                    </Link>
                                                </div>
                                                <div className="applicant_uni_name">
                                                    <p>Penn State University </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="save_icon_area">
                                            <div className="icon_area">
                                                <Link to='/'>
                                                    <i class="far fa-bookmark"></i>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="bottom_area">
                                        <div className="left_area">
                                            <div className="detail_area">
                                                <ul>
                                                    <li>
                                                        <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                    </li>
                                                    <li>
                                                        <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                    </li>
                                                    <li className="other">
                                                        <Link to='/' className="listing_button">Canada</Link>
                                                    </li>
                                                    <li className="other">
                                                        <Link to='/' className="listing_button">May/july 2020 </Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="right_area">
                                            <div className="purchase_btn">
                                                <Link to='/' class="btn btn-primary-2">
                                                    Purchase
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="box_area ul">
                                    <div className="box_inner_area ul">
                                        <div className="top_area">
                                            <div className="applicant_detail_area">
                                                <div className="applicant_image_area">
                                                    <div className="image_area">
                                                        <img src={agent_applicant} />
                                                    </div>
                                                </div>
                                                <div className="applicant_info_area">
                                                    <div className="applicant_name">
                                                        <div className="left_area">
                                                            <h4>Dinkle Kapoor</h4>
                                                        </div>
                                                        <div className="right_area">
                                                            <p>(Age 23)</p>
                                                        </div>
                                                    </div>
                                                    <div className="applicant_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i class="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area_in">
                                                                <p> PA 16801, United States</p>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="applicant_uni_name">
                                                        <p>Penn State University </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="save_icon_area">
                                                <div className="icon_area">
                                                    <Link to='/'>
                                                        <i class="far fa-bookmark"></i>
                                                    </Link>
                                                </div>
                                                {/* <div className="view_area">
                                                    <Link to='/'>
                                                        <i class="fal fa-eye"></i> 80+ View
                                                    </Link>

                                                </div> */}
                                                {/* <div className="rating_area">

                                                    <Rating
                                                        // onClick={}
                                                        ratingValue={ratingValue}
                                                        showTooltip={true}
                                                        initialValue={3.8}
                                                        // readonly
                                                        tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                                                    />
                                                </div> */}
                                            </div>
                                        </div>

                                        <div className="bottom_area">
                                            <div className="left_area">
                                                <div className="detail_area">
                                                    <ul>
                                                        <li>
                                                            <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                        </li>
                                                        <li>
                                                            <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">Canada</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">May/july 2020 </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="purchase_btn">
                                                    <Link to='/' class="btn btn-primary-2">
                                                        Purchase
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="box_inner_area ul">
                                        <div className="top_area">
                                            <div className="applicant_detail_area">
                                                <div className="applicant_image_area">
                                                    <div className="image_area">
                                                        <img src={agent_applicant} />
                                                    </div>
                                                </div>
                                                <div className="applicant_info_area">
                                                    <div className="applicant_name">
                                                        <div className="left_area">
                                                            <h4>Dinkle Kapoor</h4>
                                                        </div>
                                                        <div className="right_area">
                                                            <p>(Age 23)</p>
                                                        </div>
                                                    </div>
                                                    <div className="applicant_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i class="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area_in">
                                                                <p> PA 16801, United States</p>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="applicant_uni_name">
                                                        <p>Penn State University </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="save_icon_area">
                                                <div className="icon_area">
                                                    <Link to='/'>
                                                        <i class="far fa-bookmark"></i>
                                                    </Link>
                                                </div>
                                                {/* <div className="view_area">
                                                    <Link to='/'>
                                                        <i class="fal fa-eye"></i> 80+ View
                                                    </Link>
                                                      </div> */}
                                                {/* <div className="rating_area">
                                                    <Rating
                                                        // onClick={}
                                                        ratingValue={ratingValue}
                                                        showTooltip={true}
                                                        initialValue={3.8}
                                                        // readonly
                                                        tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                                                    />
                                                </div> */}
                                            </div>
                                        </div>

                                        <div className="bottom_area">
                                            <div className="left_area">
                                                <div className="detail_area">
                                                    <ul>
                                                        <li>
                                                            <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                        </li>
                                                        <li>
                                                            <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">Canada</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">May/july 2020 </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="purchase_btn">
                                                    <Link to='/' class="btn btn-primary-2">
                                                        Purchase
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="box_inner_area ul">
                                        <div className="top_area">
                                            <div className="applicant_detail_area">
                                                <div className="applicant_image_area">
                                                    <div className="image_area">
                                                        <img src={agent_applicant} />
                                                    </div>
                                                </div>
                                                <div className="applicant_info_area">
                                                    <div className="applicant_name">
                                                        <div className="left_area">
                                                            <h4>Dinkle Kapoor</h4>
                                                        </div>
                                                        <div className="right_area">
                                                            <p>(Age 23)</p>
                                                        </div>
                                                    </div>
                                                    <div className="applicant_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i class="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area_in">
                                                                <p> PA 16801, United States</p>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="applicant_uni_name">
                                                        <p>Penn State University </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="save_icon_area">
                                                <div className="icon_area">
                                                    <Link to='/'>
                                                        <i class="far fa-bookmark"></i>
                                                    </Link>
                                                </div>
                                                {/* <div className="view_area">
                                                    <Link to='/'>
                                                        <i class="fal fa-eye"></i> 80+ View
                                                    </Link>
                                                     </div> */}
                                                {/* <div className="rating_area">
                                                    <Rating
                                                        // onClick={}
                                                        ratingValue={ratingValue}
                                                        showTooltip={true}
                                                        initialValue={3.8}
                                                        // readonly
                                                        tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                                                    />
                                                </div> */}
                                            </div>
                                        </div>

                                        <div className="bottom_area">
                                            <div className="left_area">
                                                <div className="detail_area">
                                                    <ul>
                                                        <li>
                                                            <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                        </li>
                                                        <li>
                                                            <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">Canada</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">May/july 2020 </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="purchase_btn">
                                                    <Link to='/' class="btn btn-primary-2">
                                                        Purchase
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="box_inner_area ul">
                                        <div className="top_area">
                                            <div className="applicant_detail_area">
                                                <div className="applicant_image_area">
                                                    <div className="image_area">
                                                        <img src={agent_applicant} />
                                                    </div>
                                                </div>
                                                <div className="applicant_info_area">
                                                    <div className="applicant_name">
                                                        <div className="left_area">
                                                            <h4>Dinkle Kapoor</h4>
                                                        </div>
                                                        <div className="right_area">
                                                            <p>(Age 23)</p>
                                                        </div>
                                                    </div>
                                                    <div className="applicant_location_area">
                                                        <Link to='/#'>
                                                            <div className="left_area">
                                                                <i class="fal fa-map-marker-alt"></i>
                                                            </div>
                                                            <div className="right_area_in">
                                                                <p> PA 16801, United States</p>
                                                            </div>
                                                        </Link>
                                                    </div>
                                                    <div className="applicant_uni_name">
                                                        <p>Penn State University </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="save_icon_area">
                                                <div className="icon_area">
                                                    <Link to='/'>
                                                        <i class="far fa-bookmark"></i>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="bottom_area">
                                            <div className="left_area">
                                                <div className="detail_area">
                                                    <ul>
                                                        <li>
                                                            <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                        </li>
                                                        <li>
                                                            <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">Canada</Link>
                                                        </li>
                                                        <li className="other">
                                                            <Link to='/' className="listing_button">May/july 2020 </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="purchase_btn">
                                                    <Link to='/' class="btn btn-primary-2">
                                                        Purchase
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}


export default MyApplications;