import React from "react";
import DashboardHeadings from "./Dashboardheading";
import user from '../../../assets/images/inprofile.png';
import flag from '../../../assets/images/flag.png';
import usa_icon from '../../../assets/images/usa_icon.png';
import ireland_icon from '../../../assets/images/ireland_icon.png';
import aus_icon from '../../../assets/images/aus_icon.png';
import dummy_icon from '../../../assets/images/dummy_icon.png';
import dummy3_icon from '../../../assets/images/dummy3_icon.png';
import dots from '../../../assets/images/dots.png';
import dots2 from '../../../assets/images/dots2.png';
import coins_img from '../../../assets/images/coins_img.png';
import facebook_icon from '../../../assets/images/facebook_icon.png';
import whatsapp_icon from '../../../assets/images/whatsapp_icon.png';
import Form from 'react-bootstrap/Form'
import Select from 'react-select';
import { Link } from "react-router-dom";
import dolor_icon from "../../../assets/images/dolor_icon.png"

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

export default class AgentProfile extends React.Component {
    state = {
        selectedOption: null,
    };
    handleChange = (selectedOption) => {
        this.setState({ selectedOption }, () =>
            console.log(`Option selected:`, this.state.selectedOption)
        );
    };
    render() {
        const { selectedOption } = this.state;

        return (
            <>
                <div>
                    <div className="heading_area">
                        <DashboardHeadings title="Hi Alex" />
                    </div>
                    <div className="inner_info_mine">
                        <div className="profile_area">
                            <div className="profile-flexi">
                                <div className="left">
                                    <div className="head_set">
                                        <h2>Personal info</h2>
                                    </div>
                                    <div className="my_detail_box">
                                        <div className="my_profile">
                                            <p>Profile Picture</p>
                                            <div className="img_area_profile">
                                                <img src={user} />
                                            </div>
                                        </div>
                                        <div className="edit_detail">
                                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <Form>
                                                    <div className="row">
                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                            <Form.Group className="form_body" controlId="name">
                                                                <Form.Label>First name</Form.Label>
                                                                <Form.Control type="text" placeholder="Enter First name" />
                                                            </Form.Group>
                                                        </div>
                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                            <Form.Group className="form_body" controlId="lastname">
                                                                <Form.Label>Last name</Form.Label>
                                                                <Form.Control type="text" placeholder="Enter Last name" />
                                                            </Form.Group>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <Form.Group className="form_body" controlId="email">
                                                                <Form.Label>Email address</Form.Label>
                                                                <Form.Control type="email" placeholder="Enter your email" />
                                                            </Form.Group>
                                                        </div>
                                                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                            <Form.Group className="form_body" controlId="phonenumber">
                                                                <Form.Label>Phone number</Form.Label>
                                                                <Form.Control type="number" placeholder="Enter your email" />
                                                            </Form.Group>
                                                        </div>
                                                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <div className="row">
                                                                <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                                    <Form.Group className="form_body" controlId="state">
                                                                        <Form.Label>State</Form.Label>
                                                                        <Select
                                                                            value={selectedOption}
                                                                            onChange={this.handleChange}
                                                                            options={options}
                                                                        />
                                                                    </Form.Group>
                                                                </div>
                                                                <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                                    <Form.Group className="form_body" controlId="pincode">
                                                                        <Form.Label>Pincode</Form.Label>
                                                                        <Form.Control type="number" placeholder="Enter your Pincode" />
                                                                    </Form.Group>
                                                                </div>
                                                                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                    <div className="save_changes">
                                                                        <Link to='/'>
                                                                            <button className="btn-primary-2">Save changes</button>
                                                                        </Link>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Form>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="change_password_area">
                                        <div className="row">
                                            <div className="col-xxl-10 col-xl-5 col-lg-8 col-md-10 col-sm-10 col-12">
                                                <div className="input_fields_area">
                                                    <div className="change_heading_area">
                                                        <h2>Change Password</h2>
                                                    </div>
                                                    <Form>
                                                        <div className="expect_education_area">
                                                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                <Form.Label>Old password</Form.Label>
                                                                <Form.Control type="email" placeholder="Enter your password" />
                                                            </Form.Group>
                                                        </div>
                                                        <div className="expect_education_area">
                                                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                <Form.Label>New password</Form.Label>
                                                                <Form.Control type="email" placeholder="Enter new password" />
                                                            </Form.Group>
                                                        </div>
                                                        <div className="expect_education_area">
                                                            <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                                <Form.Label>Confirm password</Form.Label>
                                                                <Form.Control type="email" placeholder="Password" />
                                                            </Form.Group>
                                                        </div>
                                                    </Form>
                                                </div>
                                                <div className="btn_area">
                                                    <div className="left_area">
                                                        <Link to='/'> <button className="btn-primary-2" >Change password</button></Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="right">
                                    <div className="wallet_detail_area">
                                        <div className="box_area bg">
                                            <div className="top_area">
                                                <div className="left_area">
                                                    <div className="inner_info">
                                                        <h4>My Wallet</h4>
                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="manage_btn_area">
                                                        <Link to='/'><button className="btn-primary-4" >Add Credit</button></Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="bottom_area">
                                                <div className="inner_info">
                                                    <p>Availabe Credits</p>
                                                    <h3>1000 <sub>Credits</sub></h3>
                                                </div>
                                            </div>
                                            <div className="doller_img">
                                                <img src={dolor_icon} />
                                            </div>
                                            <div className="dots2">
                                                <img src={dots2} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="head_set">
                                        <h2>Preferances</h2>
                                    </div>
                                    <div className="preferances">
                                        <div className="flag_icon_area">
                                            <div className="image_area">
                                                <img src={flag} />
                                            </div>
                                            <div className="image_area">
                                                <img src={usa_icon} />
                                            </div>
                                            <div className="image_area">
                                                <img src={aus_icon} />
                                            </div>
                                            <div className="image_area">
                                                <img src={ireland_icon} />
                                            </div>
                                            <div className="image_area">
                                                <img src={dummy_icon} />
                                            </div>
                                            <div className="image_area">
                                                <img src={dummy3_icon} />
                                            </div>
                                            <div className="image_area">
                                                <img src={ireland_icon} />
                                            </div>
                                            <div className="image_area add">
                                                <Link to='/'>
                                                    <i class="far fa-plus"></i>
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="uni_area">
                                            <div className="left_area">
                                                <i class="fal fa-graduation-cap"></i>
                                            </div>
                                            <div className="right_area">
                                                <h2>30+</h2>
                                                <p>Universities </p>
                                            </div>
                                        </div>
                                        <div className="dots">
                                            <img src={dots} />
                                        </div>
                                    </div>
                                    <div className="save_changes">
                                        <Link to='/'>
                                            <button className="btn-primary-2 w-100">Change</button>
                                        </Link>
                                    </div>
                                    <div className="invite_friend_area">
                                        <div className="top_box">
                                            <div className="image_area">
                                                <img src={coins_img} />
                                            </div>
                                            <div className="info_area">
                                                <p>Invite a friend and get <strong>4 credit</strong>
                                                    on his first purchase.</p>
                                            </div>
                                            <div className="dots2">
                                                <img src={dots2} />
                                            </div>
                                        </div>
                                        <div className="bottom_box">
                                            <div className="expect_education_area">
                                                <Form>
                                                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                                                        <Form.Control plaintext readOnly defaultValue="Loremipsum/v8ak47" />
                                                        <Link to='/'> <i class="fal fa-clone"></i></Link>
                                                    </Form.Group>
                                                </Form>
                                            </div>
                                            <div className="btn_copy_area">
                                                <div className="facebook_btn">
                                                    <Link to='/'>
                                                        <img src={facebook_icon} />
                                                    </Link>

                                                </div>
                                                <div className="whatsapp_btn">
                                                    <Link to='/'>
                                                        <img src={whatsapp_icon} />
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );

    }
}
