
import React from "react";
import DashboardHeadings from "./Dashboardheading";
import Form from 'react-bootstrap/Form';
import { Link, withRouter } from "react-router-dom";

function ChangePassword() {
  return (
    <>
      <div>
        <div className="heading_area">
          <DashboardHeadings title="Change Pasword" />
        </div>
        <div className="inner_info_mine">
          <div className="row">
            <div className="col-xxl-5 col-xl-5 col-lg-8 col-md-8 col-sm-10 col-12">
              <div className="input_fields_area">
                <div className="expect_education_area">
                  <Form>
                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                      <Form.Label>Old password</Form.Label>
                      <Form.Control type="email" placeholder="Enter your password" />
                    </Form.Group>
                  </Form>
                </div>
                <div className="expect_education_area">
                  <Form>
                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                      <Form.Label>New password</Form.Label>
                      <Form.Control type="email" placeholder="Enter new password" />
                    </Form.Group>
                  </Form>
                </div>
                <div className="expect_education_area">
                  <Form>
                    <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                      <Form.Label>Confirm password</Form.Label>
                      <Form.Control type="email" placeholder="Password" />
                    </Form.Group>
                  </Form>
                </div>
              </div>
              <div className="btn_area">
                <div className="right_area">
                  <Link to='/'> <button className="btn-primary-8" >Change password</button></Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}


export default ChangePassword;