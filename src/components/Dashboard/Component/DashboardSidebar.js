import React from "react";
import bg from '../../../assets/images/Rectangle-464.png';
import Shape1 from '../../../assets/images/Vector1.png';
import Shape2 from '../../../assets/images/Vector2.png';

import { Link, useHistory } from "react-router-dom";
import AuthController from "../../../apis/controllers/auth.controller";
import user from '../../../assets/images/profile.png';
import { connect } from "react-redux";
import { renderImage, renderNoAvatar } from "../../../helpers/General";

function DashboardSidebar(props) {
    let history = useHistory();
    return (
        <>
            <div className="Side_bar_wrap_dashboard">
                <div className="Side_bar_wrap_in">
                    <div className="user_img">
                        <div className="">
                            <div className="bg_img">
                                <img src={bg} />
                            </div>
                            <div className="user_img_inn">
                                <div className="user_set">
                                    <img src={props.user && props.user.image ? renderImage(props.user.image, 'medium') : renderNoAvatar(props.user ? props.user.first_name + (props.user.last_name ? ` ` + props.user.last_name : `` ) : ``)}  />
                                </div>
                                <div className="name">
                                    <p>{props.user && props.user.first_name ? props.user.first_name : null} {props.user && props.user.last_name ? props.user.last_name : null}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="menu_list">
                        <div className="Shape1">
                            <img src={Shape1} />
                        </div>
                        <div className="menus">
                            <ul>
                                <li>
                                    <Link to="/home-page"><i class="fas fa-home-lg-alt"></i>
                                      <div className="name-lable">
                                           <p>Home</p>
                                      </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/my-profile"><i className="fas fa-user"></i>
                                        <div className="name-lable">
                                           <p>Profile</p>
                                      </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/my-favorites"><i className="fal fa-bookmark"></i>
                                        <div className="name-lable">
                                           <p>Favorites</p>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/applications"><i className="fal fa-file"></i>
                                        <div className="name-lable">
                                           <p>Applications</p>
                                        </div>
                                    </Link>
                                </li>
                                {/* <li>
                                    <Link to="/"><i className="fal fa-comment-alt"></i>
                                        <div className="name-lable">
                                           <p>Message</p>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="my-wallet"><i class="fas fa-wallet"></i>
                                        <div className="name-lable">
                                           <p>Wallet</p>
                                        </div>
                                    </Link>
                                </li> */}
                                <li className="logout">
                                    <Link to="/"
                                        onClick={() => {
                                            new AuthController().logout();
                                        }}>
                                        <i className="fal fa-sign-out"
                                        >
                                        </i>
                                        <div className="name-lable">
                                            <p>Logout</p>
                                        </div>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                        <div className="Shape2">
                            <img src={Shape2} />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
  });
  export default connect(mapStateToProps)(DashboardSidebar);
