import React from "react";
import AgentWallet from "../Agent_wallet";
import AgentDashboardHome from "./Agent_home";
import AgentProfile from "./Agent_profile";
import MyApplications from "../../MyApplication/Applications";
import ChangePassword from "./ChangePassword";
import MyFavorites from "./MyFavorites";
import My_Profile from "./My_profile";

function Dashboardmain() {
    return (
        <>
            <My_Profile />
            {/* <ChangePassword /> */}
            {/* <MyFavorites /> */}
            {/* <MyApplications /> */}
            <AgentDashboardHome />
            {/* <AgentWallet /> */}
            {/* <AgentProfile /> */}
        </>
    );
}

export default Dashboardmain;
