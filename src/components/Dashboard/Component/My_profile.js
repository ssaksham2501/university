import React, { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form';

import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Select from 'react-select';
import ActionController from "../../../apis/controllers/action.controller";
import AgentProfileController from "../../../apis/controllers/agentController/agent.controller";
import AuthController from "../../../apis/controllers/auth.controller";
import StudentProfileController from "../../../apis/controllers/studentController/student.controller";
import coins_img from '../../../assets/images/coins_img.png';
import dolor_icon from "../../../assets/images/dolor_icon.png";
import dots from '../../../assets/images/dots.png';
import dots2 from '../../../assets/images/dots2.png';
import facebook_icon from '../../../assets/images/facebook_icon.png';
import flag from '../../../assets/images/flag.png';
import whatsapp_icon from '../../../assets/images/whatsapp_icon.png';
import { getFlagIcon, isEmpty } from "../../../helpers/General";
import { renderImage } from "../../../helpers/General";
import Validation from "../../../helpers/vaildation";
import Selectable from "../../Layout/components/Selectable";
import UploadUserImageBox from "../../Layout/components/UploadUserImageBox";
import AddCreditModal from "../../Layout/Profile_add_credit_modal";
import Phonecomponent from "../../Phone_input";
import DashboardHeadings from "./Dashboardheading";
import Preference from "./preference";


function My_Profile(props) {
  const [maxCountries, setMaxCountries] = useState(7);
  const [states, setStates] = useState([]);
  const [userdata, setUser] = useState(null);
  const [imageBox, setImageBox] = useState(false);
  const [mount, setMount] = useState(true);
  const [profileLoader, setProfileLoader] = useState(false);
  const [passwordLoader, setPasswordLoader] = useState(false);
  const [cerdits, setCredits] = useState(false);
  let defaultValues = {
    firstName: null,
    lastName: null,
    email: null,
    phoneNumber: null,
    stateId: null,
    pincode: null,
  };
  const [values, setValues] = useState(defaultValues);

  let defaultOtherValues = {
    oldPassword: null,
    newPsssword: null,
    confirmPassword: null
  };
  const [otherValues, setOtherValues] = useState(defaultOtherValues);

  const [isError, setError] = useState({
    image: {
      rules: [],
      isValid: true,
      message: "",
    },
    firstName: {
      rules: ["required"],
      isValid: true,
      message: "",
    },
    lastName: {
      rules: ["required"],
      isValid: true,
      message: "",
    },
    email: {
      rules: ["required", "email"],
      isValid: true,
      message: "",
    },
    stateId: {
      rules: ["required"],
      isValid: true,
      message: "",
    },
    pincode: {
      rules: ["required"],
      isValid: true,
      message: "",
    },
    oldPassword: {
      rules: ["required", 'password'],
      isValid: true,
      message: "",
    },
    newPsssword: {
      rules: ["required", 'password'],
      isValid: true,
      message: "",
    },
    confirmPassword: {
      rules: ["required", 'password'],
      isValid: true,
      message: "",
	},
	bio: {
      rules: ["required"],
      isValid: true,
      message: "",
    },
  });

  let validation = new Validation(isError);

  const handleStates = (field, value) => {
    let node = validation.validateField(field, value);
    setError({ ...isError, [field]: node });
    setValues({ ...values, [field]: value });
  };

  const handleOtherStates = (field, value) => {
    let node = validation.validateField(field, value);
    setError({ ...isError, [field]: node });
    setOtherValues({ ...otherValues, [field]: value });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    setValues(defaultValues);
    setImageBox(false);
    init()
  }, []);

  const init = async () => {
    await getStatesData();
    await getProfile();
  }

  const submitEditProfile = async () => {
	if (!profileLoader) {
		let validation = new Validation(isError);
		let isValid = await validation.isFormValid(values);
		if (isValid && !isValid.haveError) {
			setProfileLoader(true);
			let response = await new StudentProfileController().updateProfile(values);
			setProfileLoader(false);
			if (response && response.status) {
				new AuthController().setUpLogin(response.user);
			}
		} else {
			setError({ ...isValid.errors });
			setProfileLoader(false);
		}
	}
  };

  const getStatesData = async () => {
    setMount(true);
    let response = await new ActionController().getStates();
    if (response.status) {
      setStates(response.listing)
      setMount(false);
    }
    else {
      console.log('Error while fetching api data.')
      setMount(false);
    }
    setMount(false);
  }

  const getProfile = async () => {
    setMount(true);
    let res = await new StudentProfileController().profileDetails();
    if (res && res.status) {
      let data = res && res.user;
      setValues({
        ...values,
        firstName: data.first_name,
        lastName: data.last_name,
        email: data.email,
        phoneNumber: data.phonenumber,
        countryCode: data.country_code,
        stateId: data.state_id,
        pincode: data.pincode,
        bio: data.bio,
      });
      setUser(data);
      setMount(false);
    }
    setMount(false);
  };


  const submitChangePassword = async () => {
    let validation = new Validation(isError);
    let isValid = await validation.isFormValid(otherValues);
    let node = validation.matchValues(`confirmPassword`, otherValues.newPsssword, otherValues.confirmPassword, "Confirm password does not match.");
    setError({ ...isValid.errors, confirmPassword: node, });
    if (isValid && !isValid.haveError) {
      setPasswordLoader(true);
      let response = await new AgentProfileController().changePasswords(otherValues);
      if (response && response.status) {
        setOtherValues(defaultOtherValues)
        setPasswordLoader(false);
      }
    } else {
      setError({ ...isValid.errors });
      setPasswordLoader(false);
    }
  };

  const removeImage = async () => {
    if (props.user && props.user.image && props.user.image.small) {
      let response = await new ActionController().removeUserFile();
      if (response && response.status) {
        console.log(`response`, response.user);
        new AuthController().setUpLogin(response.user); 
      }
    }
  }

  return (
    <>
      {mount ? <p className="text-center"><i className="fa fa-spin fa-spinner"></i></p> : <div>
        <div className="heading_area">
          {props.user ? <DashboardHeadings title={`"Hi ${props.user && props.user.first_name ? props.user.first_name : null}"`} /> : null}
        </div>
        <div className="inner_info_mine">
          <div className="profile_area">
            <div className="profile-flexi">
              <div className="left">
                <div className="head_set">
                  <h2>Personal info</h2>
                </div>
                <div className="my_detail_box">
                  <div className="my_profile">
                    <p>Profile Picture</p>

                    <UploadUserImageBox
                      name={userdata ? userdata.first_name + (userdata.last_name ? ` ` + userdata.last_name : `` ) : ``}
                      image={props.user && props.user.image && props.user.image.small ? props.user.image : null}
                      onResponse={(response) => {
                        new AuthController().setUpLogin(response.user);
                      }}
                      removeImage={async () => {
                        await removeImage();
                      }}
                    />
                  </div>
                  <div className="edit_detail">
                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                      <Form
                        noValidate
                        onSubmit={async (e) => {
                          e.preventDefault();
                          await submitEditProfile();
                        }}

                      >
                        <div className="row">
                          <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <Form.Group className="form_body" controlId="name">
                              <Form.Label>First name</Form.Label>
                              <Form.Control type="text" placeholder="Enter First name"
                                onChange={(e) => {
                                  handleStates(
                                    "firstName",
                                    e.target.value
                                  );
                                }}
                                value={values.firstName
                                  ? values.firstName
                                  : ``} />
                              {!isError.firstName.isValid && isError.firstName.message && <p className="text-danger  mt-2">{isError.firstName.message}</p>}
                            </Form.Group>

                          </div>
                          <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <Form.Group className="form_body" controlId="lastname">
                              <Form.Label>Last name</Form.Label>
                              <Form.Control type="text" placeholder="Enter Last name"
                                onChange={(e) => {
                                  handleStates(
                                    "lastName",
                                    e.target.value
                                  );
                                }}
                                value={values.lastName
                                  ? values.lastName
                                  : ``} />
                              {!isError.lastName.isValid && isError.lastName.message && <p className="text-danger  mt-2">{isError.lastName.message}</p>}
                            </Form.Group>
                          </div>
                          <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <Form.Group className="form_body" controlId="email">
                              <Form.Label>Email address</Form.Label>
                              <Form.Control type="email" placeholder="Enter your email"
                                onChange={(e) => {
                                  handleStates(
                                    "email",
                                    e.target.value
                                  );
                                }}
                                value={values.email
                                  ? values.email
                                  : ``} />
                              {!isError.email.isValid && isError.email.message && <p className="text-danger  mt-2">{isError.email.message}</p>}
                            </Form.Group>
                          </div>
                          <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            {/* <Form.Group className="form_body" controlId="phonenumber"> */}
                            <Form.Label>Phone number</Form.Label>
                            {/* <Form.Control type="number" placeholder="Enter your email"
                                value={values.phoneNumber
                                  ? values.phoneNumber
                                  : ``} /> */}
                            <Phonecomponent
                              phone={values.phoneNumber && values.countryCode
                                ? '+' + values.countryCode + values.phoneNumber : ''}
                              disabled={true}
                            />

                            {/* </Form.Group> */}
                          </div>
                          <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="row">
                              <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <Form.Group className="form_body" controlId="state">
                                  <Form.Label>State</Form.Label>
                                  <Selectable
                                    value={states.find((item) => item.id == values.stateId)}
                                    onChange={(item) => {
                                      handleStates(
                                        "stateId",
                                        item.id
                                      );
                                    }}
                                    options={states}
                                  />
                                  {!isError.stateId.isValid && isError.stateId.message && <p className="text-danger  mt-2">{isError.stateId.message}</p>}
                                </Form.Group>
                              </div>
                              <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <Form.Group className="form_body" controlId="pincode">
                                  <Form.Label>Pincode</Form.Label>
                                  <Form.Control type="number" placeholder="Enter your Pincode"
                                    onChange={(e) => {
                                      handleStates(
                                        "pincode",
                                        e.target.value
                                      );
                                    }}
                                    value={values.pincode
                                      ? values.pincode
                                      : ``} />
                                  {!isError.pincode.isValid && isError.pincode.message && <p className="text-danger  mt-2">{isError.pincode.message}</p>}
                                </Form.Group>
                              </div>
                              {
                                props && props.user 
								&&
								<div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
									<Form.Group className="form_search" controlId="exampleForm.ControlInput1">
										<Form.Label>Bio or Tag Line</Form.Label>
										<Form.Control type="text" placeholder="What types of services you provide?"
											maxLength={300}
											onChange={(e) => {
												handleStates(
													"bio",
													e.target.value, 'one'
												);
											}}
											value={values.bio
												? values.bio
												: ``} />
										{!isError.bio.isValid && isError.bio.message && <p className="text-danger mt-2">{isError.bio.message}</p>}
										<small className="text-muted float-right">{(300 - (values.bio && values.bio.trim() ? values.bio.trim().length : 0) ) + ` characters remaining`}</small>
									</Form.Group>
								</div>
                              }
                              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="save_changes">
                                  <button className="btn-primary-2" type="submit">
                                    {profileLoader && <i className="fa fa-spin fa-spinner"></i>}
                                    &nbsp; Save changes</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Form>
                    </div>
                  </div>
                </div>
                <div className="change_password_area">
                  <div className="row">
                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                      <div className="input_fields_area">
                        <div className="change_heading_area">
                          <h2>Change Password</h2>
                        </div>
                        <Form
                          noValidate

                        >
                          <div className="expect_education_area">
                            <Form.Group className="form_body" controlId="oldPassword">
                              <Form.Label>Old password</Form.Label>
                              <Form.Control type="password" placeholder="Enter your password"
                                onChange={(e) => {
                                  handleOtherStates(
                                    "oldPassword",
                                    e.target.value
                                  );
                                }}
                                value={otherValues.oldPassword
                                  ? otherValues.oldPassword
                                  : ``} />
                              {!isError.oldPassword.isValid && isError.oldPassword.message && <p className="text-danger  mt-2">{isError.oldPassword.message}</p>}
                            </Form.Group>
                          </div>
                          <div className="expect_education_area">
                            <Form.Group className="form_body" controlId="newpassword">
                              <Form.Label>New password</Form.Label>
                              <Form.Control type="password" placeholder="Enter new password"
                                onChange={(e) => {
                                  handleOtherStates(
                                    "newPsssword",
                                    e.target.value
                                  );
                                }}
                                value={otherValues.newPsssword
                                  ? otherValues.newPsssword
                                  : ``} />
                              {!isError.newPsssword.isValid && isError.newPsssword.message && <p className="text-danger  mt-2">{isError.newPsssword.message}</p>}
                            </Form.Group>
                          </div>
                          <div className="expect_education_area">
                            <Form.Group className="form_body" controlId="confirmpassword">
                              <Form.Label>Confirm password</Form.Label>
                              <Form.Control type="password" placeholder="Password"
                                onChange={(e) => {
                                  handleOtherStates(
                                    "confirmPassword",
                                    e.target.value
                                  );
                                }}
                                value={otherValues.confirmPassword
                                  ? otherValues.confirmPassword
                                  : ``} />
                              {!isError.confirmPassword.isValid && isError.confirmPassword.message && <p className="text-danger  mt-2">{isError.confirmPassword.message}</p>}
                            </Form.Group>
                          </div>
                        </Form>
                      </div>
                      <div className="btn_area">
                        <div className="left_area">
                          <button className="btn-primary-2" onClick={async (e) => {
                            e.preventDefault();
                            await submitChangePassword();
                          }}>
                            {passwordLoader && <i className="fa fa-spin fa-spinner"></i>}
                            &nbsp; Change password</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* {props && props.user && props.user.user_type === 'agent' ?
                <div className="right">
                  <div className="wallet_detail_area">
                    <div className="box_area bg">
                      <div className="top_area">
                        <div className="left_area">
                          <div className="inner_info">
                            <h4>My Wallet</h4>
                          </div>
                        </div>
                        <div className="right_area">
                          <div className="manage_btn_area">
                            <button className="btn-primary-4" onClick={() => setCredits(true)} >Add Credit</button>
                          </div>
                        </div>
                      </div>
                      <div className="bottom_area">
                        <div className="inner_info">
                          <p>Availabe Credits</p>
                          <h3>1000 <sub>Credits</sub></h3>
                        </div>
                      </div>
                      <div className="doller_img">
                        <img src={dolor_icon} />
                      </div>
                      <div className="dots2">
                        <img src={dots2} />
                      </div>
                    </div>
                  </div>
                  <div className="head_set">
                    <h2>Preferances</h2>
                  </div>
                  <div className="preferances">
                    <div className="flag_icon_area">
                      {userdata && userdata.countries.filter((item, i) => i < (userdata && userdata.countries.length > 7 && maxCountries > 0 ? maxCountries : userdata && userdata.countries.length)).map((item, index) => (
                        <div key={index} className="image_area">
                          <img src={getFlagIcon(item.short_name)} />
                        </div>
                      ))}
                      <div className="image_area add">
                        <Link to='/agent-change-preference'>
                          <i class="far fa-plus"></i>
                        </Link>
                      </div>
                    </div>
                    {userdata && userdata.universities && userdata.universities.length > 0 &&
                      <div className="uni_area">
                        <div className="left_area">
                          <i class="fal fa-graduation-cap"></i>
                        </div>
                        <div className="right_area">
                          <h2>{userdata && userdata.universities && userdata.universities.length}+</h2>
                          <p>Universities </p>
                        </div>
                      </div>}
                    <div className="dots">
                      <img src={dots} />
                    </div>
                  </div>
                  <div className="save_changes">
                    <Link to='/agent-change-preference'>
                      <button className="btn-primary-2 w-100">Change</button>
                    </Link>
                  </div>
                  <div className="invite_friend_area">
                    <div className="top_box">
                      <div className="image_area">
                        <img src={coins_img} />
                      </div>
                      <div className="info_area">
                        <p>Invite a friend and get <strong>4 credit</strong>
                          on his first purchase.</p>
                      </div>
                      <div className="dots2">
                        <img src={dots2} />
                      </div>
                    </div>
                    <div className="bottom_box">
                      <div className="expect_education_area">
                        <Form>
                          <Form.Group className="form_search" controlId="exampleForm.ControlInput1">
                            <Form.Control plaintext readOnly defaultValue="Loremipsum/v8ak47" />
                            <Link to='/'> <i class="fal fa-clone"></i></Link>
                          </Form.Group>
                        </Form>
                      </div>
                      <div className="btn_copy_area">
                        <div className="facebook_btn">
                          <Link to='/'>
                            <img src={facebook_icon} />
                          </Link>

                        </div>
                        <div className="whatsapp_btn">
                          <Link to='/'>
                            <img src={whatsapp_icon} />
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                : <div className="right">
                  <div className="head_set">
                    <h2>Preferances</h2>
                  </div>
                  <div className="preferances">
                   
                    <div className="flag_icon_area">
                      {userdata && userdata.countries.filter((item, i) => i < (userdata && userdata.countries.length > 7 && maxCountries > 0 ? maxCountries : userdata && userdata.countries.length)).map((item, index) => (
                        <div key={index} className="image_area">
                          <img src={getFlagIcon(item.short_name)} />
                        </div>
                      ))}
                      <div className="image_area add">
                        <Link to='/student-change-preference-step-1'>
                          <i class="far fa-plus"></i>
                        </Link>
                      </div>
                    </div>
                    <div className="intake">
                      <p>Preferred intake : {userdata && userdata.prefered_intake ? userdata.prefered_intake.title + ' ' + userdata.prefered_intake.year : null}</p>
                      {userdata && userdata.streams && userdata.streams.length > 0 ? <p>Wish to pursue : {userdata && userdata.streams && userdata.streams.reduce(function (a, b) {
                        return (a.title || a) + ", " + b.title
                      })} </p> : null}
                    </div>
                    <div className="dots">
                      <img src={dots} />
                    </div>
                  </div>
                  <div className="save_changes">
                    <Link to='/student-change-preference-step-1'>
                      <button className="btn-primary-2 w-100">Change</button>
                    </Link>
                  </div>
                </div>} */}
                <Preference />
            </div>
          </div>  
        </div>
        
      </div>}
        { cerdits&&
            <AddCreditModal
              show={cerdits}
              close={()=>setCredits(false)}
            />
          }
      
    </>
  );

}
const mapStateToProps = state => ({
  user: state.UserReducer.user,
});
export default connect(mapStateToProps)(My_Profile);
