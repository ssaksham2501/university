import React, { Component, useState, useEffect } from "react";
import agent_banner from "../../../assets/images/agent_banner.png"
import agent_applicant from "../../../assets/images/agent_applicant.png"
import inprofile from "../../../assets/images/inprofile.png"
import uni_logo from "../../../assets/images/uni_logo.png"
import dots2 from '../../../assets/images/dots2.png';
import dots_bottom from '../../../assets/images/dots_bottom.png';
import { Link } from "react-router-dom";
import { Rating } from 'react-simple-star-rating'
import Leads from "../../../functions/agents/Leads";
function AgentDashboardHome() {
    const { mounting, leads, leadsHandler } = Leads();

    useEffect(() => {
        leadsHandler.init();
    }, []);


    return (
        <>
            <div>
                <div className="university_section">
                    <div className="banner_inner">
                        {/* <div className="banner_area">
                            <div className="image_area">
                                <img src={agent_banner} />
                            </div>
                            <div className="detail_area">
                                <div className="left_area">
                                    <div className="image_area">
                                        <img src={uni_logo} />
                                    </div>
                                    <div className="uni_name_area">
                                        <h4>Penn State University</h4>
                                    </div>
                                    <div className="university_location_area">
                                        <Link to='/#'>
                                            <div className="left_area">
                                                <i class="fal fa-map-marker-alt"></i>
                                            </div>
                                            <div className="right_area_in">
                                                <p> PA 16801, United States</p>

                                            </div>
                                        </Link>
                                    </div>
                                    <div className="des_area">
                                        <p>Lorem ipsum dolor sit amet ipsum dolor. </p>
                                    </div>
                                </div>
                                <div className="right_area">
                                    <div className="ad_area">
                                        <Link to='/'>
                                            Ad
                                        </Link>
                                    </div>
                                    <div className="apply_btn">
                                        <Link to='/' class="btn btn-primary-2">
                                            Apply Now
                                        </Link>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="agent_banner_area">
                            <div className="detail_area">
                                <div className="agent_left_area">
                                    <div className="agent_img">
                                        <div className="image_area">
                                            <img src={inprofile} />
                                        </div>
                                    </div>
                                    <div className="agent_info">
                                        <div className="agent_name">
                                            <h5>Rogger Holand</h5>
                                        </div>
                                        <div className="agent_location_area">
                                            <Link to='/#'>
                                                <div className="left_area">
                                                    <i class="fal fa-map-marker-alt"></i>
                                                </div>
                                                <div className="right_area_in">
                                                    <p>PA 16801, United States</p>

                                                </div>
                                            </Link>
                                        </div>
                                        <div className="des_area">
                                            <p>Lorem ipsum dolor sit amet ipsum dolor.</p>
                                        </div>
                                    </div>

                                </div>
                                <div className="right_area">
                                    <div className="ad_area">
                                        <Link to='/'>
                                            Ad
                                        </Link>
                                    </div>
                                    <div className="contact_btn">
                                        <Link to='/' class=" btn-primary-4">
                                            Contact Now
                                        </Link>
                                    </div>
                                </div>
                                <div className="dots2">
                                    <img src={dots2} />
                                </div>
                                <div className="dot_bottom">
                                    <img src={dots_bottom} />
                                </div>
                            </div>
                        </div> */}
                        <div>
                            <div className="inner_heading_area">
                                <h4>Applicants</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div className="center_heading_area">
                                <p>Work room</p>
                            </div>
                            {
                                mounting
                                ?
                                    <div className="box_area"><p className="text-center"><i className="fa fa-spinner fa-spin"></i></p></div>
                                :
                                    <div className="box_area">
                                        <div className="box_inner_area">
                                            <div className="top_area">
                                                <div className="applicant_detail_area">
                                                    <div className="applicant_image_area">
                                                        <div className="image_area">
                                                            <img src={agent_applicant} />
                                                        </div>
                                                    </div>
                                                    <div className="applicant_info_area">
                                                        <div className="applicant_name">
                                                            <div className="left_area">
                                                                <h4>Dinkle Kapoor <span>(Age 23)</span></h4>
                                                            </div>
                                                            <div className="right_area">
                                                            
                                                            </div>
                                                        </div>
                                                        <div className="applicant_location_area">
                                                            <Link to='/#'>
                                                                <div className="left_area">
                                                                    <i class="fal fa-map-marker-alt"></i>
                                                                </div>
                                                                <div className="right_area_in">
                                                                    <p> PA 16801, United States</p>

                                                                </div>
                                                            </Link>
                                                        </div>
                                                        <div className="applicant_uni_name">
                                                            <p>Penn State University </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="save_icon_area">
                                                    <div className="ineer">
                                                        <div className="icon_area">
                                                            <Link to='/'>
                                                                <i class="far fa-bookmark"></i>
                                                            </Link>
                                                        </div>
                                                        <div className='connect d-lg-block d-none'>
                                                            <p>2 People connected:</p>
                                                            <span className="active"></span>
                                                            <span className="active"></span>
                                                            <span></span>
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="bottom_area">
                                                <div className='connect d-lg-none d-block'>
                                                    <p>2 People connected:</p>
                                                    <span className="active"></span>
                                                    <span className="active"></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                                <div className="left_area">
                                                    <div className="detail_area">
                                                        <ul>
                                                            <li>
                                                                <Link to='/' className="listing_button">Ielts: 6.5</Link>
                                                            </li>
                                                            <li>
                                                                <Link to='/' className="listing_button">Bachelor of Science</Link>
                                                            </li>
                                                            <li className="other">
                                                                <Link to='/' className="listing_button">Canada</Link>
                                                            </li>
                                                            <li className="other">
                                                                <Link to='/' className="listing_button">May/july 2020 </Link>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div className="right_area">
                                                    <div className="purchase_btn">
                                                        <Link to='/' class="btn btn-primary-2">
                                                            Purchase
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            }
                            
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}


export default AgentDashboardHome;