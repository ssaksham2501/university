import React from "react";

const Map_detail = (props) => {
    return (
        <div>
            <div className="map_section">
                <div className="inner_area">
                    <div className="map_area">
                        <iframe src={props.universityDetail && props.universityDetail.map_link ? props.universityDetail.map_link : ``} width="100%" height="500"
                            style={{ marginRight: 10 }} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Map_detail;