import React, { useState, useEffect } from "react";
import AuthController from "../../../apis/controllers/auth.controller";
import ModalController from "../../../apis/controllers/modal.controller";
import AgentsListing from "../../../functions/agents/AgentsListing";
import { isEmptyObj } from "../../../helpers/General";
import AgentGrid from "../../Agents_list/component/agent_grid";
import AgentListModal from "../../Layout/AgentListPopUpModal";
import AgentContactModal from "../../Layout/agent_contact_modal";


const Agent_detail = (props) => {
    const [showList, setShowList] = useState(false)
    const { agents, agentListingHandler, isLoading, totalAgents } = AgentsListing(props);
    const init = async () => {
        await agentListingHandler.sendRequest();
    }

    useEffect(() => {
        init()
        return () => {

        }
    }, []);

    const handleApplyModal = (item) => {
        if(new AuthController().getLoginUserId()) {
            new ModalController().setApplyModal({
                type: 'agent',
                slug: item.user_name,
                info: props.universityDetail ? props.universityDetail : null,
                agent: item,
                modalTitle: "Send message to " + item.first_name + (item.last_name ? ' ' + item.last_name : '')
            });
        } else {
            new ModalController().setApplyModal(false);
            new ModalController().setStartModal(true);
        }
    }

    return (
        <div>
            {props.universityDetail && agents && agents.length > 0 &&
                <>
                    <div className="row">
                        {props.universityDetail && agents.map((item, index) => (
                            <AgentGrid
                                key={`agentgrid` + index}
                                index={index}
                                item={item}
                                handleApplyModal={handleApplyModal}
                            />
                        ))}
                    </div>
                    <div className="btn_area">
                        { agents && agents.length >= 3 && <button className="btn-primary-1"
                            onClick={async (e) => {
                                e.preventDefault();
                                setShowList(true)
                            }}>+ View More</button> }
                    </div>
                </>
            }
            {showList ? <AgentListModal
                open={showList}
                close={() => setShowList(false)}
                universityDetail={props.universityDetail}
                handleApplyModal={handleApplyModal}
            /> : null}

            
        </div>
    );
}

export default Agent_detail;


