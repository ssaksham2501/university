import React from "react";


const Courses_detail = (props) => {
    const { universityDetail } = props;
    const [loader, setLoader] = React.useState(false);
    // const [universityDetail, setUniversityDetail] = React.useState(props.universityDetail.courses)
    // console.log(props.universityDetail.courses, 'detail course page')


    return (
        <div>
            {universityDetail && universityDetail.courses && universityDetail.courses.length > 0 && <div className="courses_area">
                <div className="inner_area">
                    <div className="box_area">
                        <div className="heading_area">
                            <h5>Courses</h5>
                        </div>
                        <div className="scrll_list_met">
                            {
                                universityDetail && universityDetail.courses.length > 0 && universityDetail.courses.map((data, index) => {
                                    // console.log(data, 'map data')
                                    return (
                                        
                                            <div className="courses_info_area" key={index}>
                                                <div className="sub_heading_area">
                                                    <h6>{data.title}</h6>
                                                </div>
                                                <div className="course_slide">
                                                    {
                                                        data.courses && data.courses.length > 0 && data.courses.map((categorydata, index) => {
                                                            return (

                                                                <div className="course_detail" key={index}>
                                                                    <h4>{categorydata.title}</h4>
                                                                    <p>{categorydata.description}</p>
                                                                    <div className="row">
                                                                        {categorydata.duration ?<div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pe-0">
                                                                            <div className="inner_detail">
                                                                                <div className="icon_area">
                                                                                    <i className="fal fa-hourglass-start"></i>
                                                                                </div>
                                                                                <div className="text_area">
                                                                                    <p>{categorydata.duration}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>:null}
                                                                        {categorydata.location ?<div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                                                                            <div className="inner_detail">
                                                                                <div className="icon_area">
                                                                                    <i className="far fa-book-open"></i>
                                                                                </div>
                                                                                <div className="text_area">
                                                                                    <p>{categorydata.location}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>:null}
                                                                        {categorydata.language ?<div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pe-0">
                                                                            <div className="inner_detail">
                                                                                <div className="icon_area">
                                                                                    <i className="fal fa-globe"></i>
                                                                                </div>
                                                                                <div className="text_area">
                                                                                    <p>{categorydata.language}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>:null}
                                                                        {categorydata.time ?<div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                                                                            <div className="inner_detail">
                                                                                <div className="icon_area">
                                                                                    <i className="far fa-clock"></i>
                                                                                </div>
                                                                                <div className="text_area">
                                                                                    <p>{categorydata.time}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>:null}
                                                                    </div>
                                                                </div>

                                                            )
                                                        })
                                                    }

                                                </div>
                                            </div>
                                    

                                    )
                                })
                            }
                         </div>

                    </div>
                </div>
            </div>}
        </div>
    );
}

export default Courses_detail;


