import React, { useEffect, useState } from 'react';
import Slider from "react-slick";

import { connect } from 'react-redux';
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import { Player } from 'video-react';
import 'video-react/dist/video-react.css';
import ModalController from '../../../apis/controllers/modal.controller';
import { baseUrl, isEmptyObj } from '../../../helpers/General';



function Slider_listing(props) {
    const { universityDetail } = props;
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const [currentSlide, setCurrentSlide] = useState(0);
    const [slider1, setSlider1] = useState(null);
    const [slider2, setSlider2] = useState(null);

    useEffect(() => {
        setNav1(slider1);
        setNav2(slider2);

    });
    const settingsMain = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
    };

    const settingsThumbs = {
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        centerMode: true,
        swipeToSlide: true,
        focusOnSelect: true,
        centerPadding: '0px',
        margin: 70,
        vertical: true,
        verticalSwiping: true,
        responsive: [
            {
                breakpoint: 320,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 1,
                  vertical:false,
                  verticalSwiping:false,
                }
              },
              {
                breakpoint: 690,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 1,
                  vertical:false,
                  verticalSwiping:false,
                }
              },
            {
              breakpoint: 765,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              }
            },
            {
                breakpoint: 1120,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                }
              },
              {
                breakpoint: 1300,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 1,
                }
              },
        ]
    };

    const slidesData = props.universityDetail && props.universityDetail.photos ? props.universityDetail.photos : [];


    const handleWishlist = (e) => {
        if (isEmptyObj(props.user)) {
            e.preventDefault();
            new ModalController().setStartModal(true);
        } else {
            new ModalController().setStartModal(false);
            e.preventDefault();
            props.favourite(universityDetail.id)
        }
    }


    return (
        <div className="slider_detail">
            {slidesData && <div className="slider-wrapper">
                <Slider
                    {...settingsMain}
                    asNavFor={nav2}
                    ref={slider => (setSlider1(slider))}
                    afterChange={(e) => {
                        setCurrentSlide(e);
                    }}
                >
                    {slidesData.map((slide, index) =>
                        <div className="slick-slide" key={`bigslide` + index}>
                            
                            {
                                slide.media_type === 'video'
                                ?
                                    // <video width="100%" height="100%" autoplay="true" muted="true" controls controlsList='nodownload'>
                                    //     <source src={slide.media} type="video/mp4"></source>
                                    // </video>
                                    <Player
                                        muted={true}
                                        autoPlay={true}
                                        volume={1}
                                        controls={true}
                                    >
                                        <source src={slide.media.includes('http://') || slide.media.includes('https://') ? slide.media : baseUrl(slide.media)} />
                                    </Player>
                                :
                                    <>
                                        <h2 className="slick-slide-title">{slide.title}</h2>
                                        <img className="slick-slide-image" src={slide.media.includes('http://') || slide.media.includes('https://') ? slide.media : baseUrl(slide.media)} />
                                        <label className="slick-slide-label">{slide.sub_title}</label>
                                    </>
                            }
                            
                        </div>
                    )}

                </Slider>
                <div className="thumbnail-slider-wrap">
                    <Slider
                        {...settingsThumbs}
                        asNavFor={nav1}
                        ref={slider => (setSlider2(slider))}>

                        {slidesData.map((slide, index) =>
                            <div className="img_area" key={`slide` + index}>
                                <img className="slick-slide-image" src={slide.thumb.includes('http://') || slide.thumb.includes('https://') ? slide.thumb : baseUrl(slide.thumb)} />
                            </div>
                        )}
                    </Slider>
                </div>
            </div>}
        </div>
    );
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
});
export default connect(mapStateToProps)(Slider_listing);
