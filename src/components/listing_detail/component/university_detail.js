import React, { Component, useState } from "react";
import { Rating } from 'react-simple-star-rating'
import { Link, withRouter } from "react-router-dom";
import { stringLimit } from "../../../helpers/General";
import ReadMoreAndLessText from "../../Layout/components/ReadMoreAndLessText";

const University_detail = (props) => {
    const { universityDetail } = props;

    return (
        <div>
            {universityDetail ? <div className="university_area">
                <div className="inner_area">
                    {/* <div className="rating_area">
                        <Rating
                            ratingValue={universityDetail.rating ? universityDetail.rating : 0}
                            showTooltip={true}
                            initialValue={universityDetail.rating ? universityDetail.rating : 0}
                            readonly
                            tooltipArray={["0.1", "0.2", "0.3", "0.4", "0.5"]}
                        />
                    </div>
                    <div className="university_detail_area">
                        <div className="left_area">
                            <h1>
                                {universityDetail.title ? universityDetail.title : null}
                            </h1>
                        </div>
                        <div className="right_area">
                            <button className="btn-primary-2" >Apply Now</button>
                        </div>
                        <div className="des_area">
                            <p>
                                <ReadMoreAndLessText
                                    text={universityDetail.description ? universityDetail.description : ``}
                                    limit={250}
                                />
                            </p>
                        </div>
                    </div> */}
                    <div className="table_area">
                        <ul>
                            <li>
                                <Link to=''>
                                    <div className="left_icon">
                                        <i class="far fa-map-marker-alt"></i>
                                    </div>
                                    <div className="right_detail">
                                        {universityDetail.address ? universityDetail.address : '-'}
                                    </div>
                                </Link>
                            </li>
                            <li className="web_link">
                                <Link to=''>
                                    <div className="left_icon">
                                        <i class="far fa-globe"></i>
                                    </div>
                                    <div className="right_detail">
                                        {universityDetail.link ? universityDetail.link : '-'}
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to=''>
                                    <div className="left_icon">
                                        <i class="far fa-at"></i>
                                    </div>
                                    <div className="right_detail">
                                        {universityDetail.contact_email ? universityDetail.contact_email : '-'}
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to=''>
                                    <div className="left_icon">
                                        <i class="far fa-phone-alt"></i>
                                    </div>
                                    <div className="right_detail">
                                        {universityDetail.contact_phonenumber ? universityDetail.contact_phonenumber : '-'}
                                    </div>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="row align-items-stretch">
                        {
                            universityDetail.rank
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="fas fa-badge-check"></i>
                                        </div>
                                        <h5>#{universityDetail.rank ? universityDetail.rank : ``}</h5>
                                        <p>{universityDetail.rank_title ? universityDetail.rank_title : ``}</p>
                                    </div>
                                </div>
                            </div>
                        }
                        {
                            universityDetail.acceptance_rate
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="far fa-user-check"></i>
                                        </div>
                                        <h5>
                                            {universityDetail.acceptance_rate}
                                        </h5>
                                        <p>Acceptance rate</p>
                                    </div>
                                </div>
                            </div>
                        }

                        {
                            universityDetail.total_applicants
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="far fa-user-friends"></i>
                                        </div>
                                        <h5>{universityDetail.total_applicants}</h5>
                                        <p>Total Applicants</p>
                                    </div>
                                </div>
                            </div>
                        }
                        {
                            universityDetail.job_placement_value
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="fas fa-briefcase"></i>
                                        </div>
                                        <h5>{universityDetail && universityDetail.job_placement_value}%</h5>
                                        <p>Job Placement</p>
                                    </div>
                                </div>
                            </div>
                        }

                        {
                            universityDetail.tuition_cost && universityDetail.tuition_cost.trim()
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="far fa-usd-circle"></i>
                                        </div>
                                        <h5>${universityDetail.tuition_cost ? universityDetail.tuition_cost : ``}</h5>
                                        <p>Tution Fees</p>
                                    </div>
                                </div>
                            </div>
                        }
                        {
                            universityDetail.cost_of_living && universityDetail.cost_of_living.trim()
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="far fa-usd-circle"></i>
                                        </div>
                                        <h5>${universityDetail.cost_of_living ? universityDetail.cost_of_living : ``}</h5>
                                        <p>Living Cost</p>
                                    </div>
                                </div>
                            </div>
                        }
                        {
                            universityDetail.exam_type && universityDetail.gmat_score
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="far fa-graduation-cap"></i>
                                        </div>
                                        <h5>
                                            {universityDetail.gmat_score}
                                        </h5>
                                        <p>{universityDetail.exam_type} Score</p>
                                    </div>
                                </div>
                            </div>
                        }
                        {
                            universityDetail.exam_type && universityDetail.gre_score
                            &&
                            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
                                <div className="box_detail_area">
                                    <div className="box_area">
                                        <div className="icon_area">
                                            <i class="far fa-graduation-cap"></i>
                                        </div>
                                        <h5>
                                            {universityDetail.gre_score}
                                        </h5>
                                        <p>GRE Score</p>
                                    </div>
                                </div>
                            </div>
                        }


                    </div>
                </div>
            </div> : null}
        </div>
    );
}

export default University_detail;







































