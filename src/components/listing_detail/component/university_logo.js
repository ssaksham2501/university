import React from "react";
import logo_top from "../../../assets/images/logo_top.png"
import { renderImage } from "../../../helpers/General";

function University_logo(props) {
    const { universityDetail } = props;
    return (
        <div>
            <div className="background_area">
                <div className="img_area">
                   {universityDetail && universityDetail.logo ? <img src={universityDetail && universityDetail.logo ? renderImage(universityDetail.logo) : logo_top} alt="" />:null}
                   <img src={footer_signup} />
                </div>
            </div>
        </div>
    );
}

export default University_logo;
