import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { Rating } from "react-simple-star-rating";
import ModalController from "../../apis/controllers/modal.controller";
import UniversityController from "../../apis/controllers/university.controllers";
import { intToString, isEmptyObj, renderImage } from "../../helpers/General";
import AddReviewModal from "../Layout/add_review_modal";
import AgentListModal from "../Layout/AgentListPopUpModal";
import AgentContactModal from "../Layout/agent_contact_modal";
import ReadMoreAndLessText from "../Layout/components/ReadMoreAndLessText";
import ReviewListingModal from "../Layout/review_listing_modal";
import Agent_detail from "./component/agents_detail";
import Breadcrumbs from "./component/breadcrumbs";
import Courses_detail from "./component/courses_detail";
import Slider_listing from "./component/lising_detail_slider";
import Map_detail from "./component/listing_map";
import University_detail from "./component/university_detail";

function Listing_detail(props) {
    const history = useHistory();
    const [universityDetail, setUniversityDetail] = useState([]);
    const [loader, setLoader] = useState(false);
    const [favouriting, setFavouriting] = useState(false);
    const[addReview , setAddReview] = useState(false);
    const location = useLocation()
    const params = useParams();
    const [apply, setshow] = useState(false);
    const [reviewlist, setReviewlist] = useState(false);
    const [reviewAccepted, setReviewAccepted] = useState(false);
    
    let universityId = params.slug;
    useEffect(() => {
        getUniversityDetails();
    }, []);

    const getUniversityDetails = async () => {
        setLoader(true);
        let res = await new UniversityController().getUniversityDetails(universityId);
        if (res && res.status) {
            setUniversityDetail(res.university);
        }
        else {
            history.push('/404');
        }
        setLoader(false);
    };

    const favourite = async (slug) => {
        if (!favouriting) {
            setFavouriting(true);
            let response = await new UniversityController().favUniversity(slug)
            if (response && response.status) {
                let u = universityDetail;
                u.favourite = response.fav ? 1 : 0;
                setUniversityDetail(u);
            }
            setFavouriting(false);
        }
    }

    const handleApplyModal = (type) => {
        if (!isEmptyObj(props.user)) {
            new ModalController().setApplyModal({
                type: 'university',
                slug: universityDetail.slug,
                info: universityDetail,
                modalTitle: "Apply to " + universityDetail.title
            });
        } else {
            new ModalController().setApplyModal(null);
            new ModalController().setStartModal(true);
        }
    }


    const handleWishlist = async (e) => {
        if (isEmptyObj(props.user)) {
            e.preventDefault();
            new ModalController().setStartModal(true);
        } else {
            new ModalController().setStartModal(false);
            e.preventDefault();
            await favourite(universityDetail.slug)
        }
    }

    return (
        <div className="detail_met top-space">
            {
                loader
                    ?
                    <div className="listing_detail"><p className="text-center"><i class="fa fa-spin fa-spinner"></i></p></div>
                    :
                    <>

                        <div className="background top_space">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div className="university_courses_wrap up">
                                            <div className="penn_state_university_wrap">
                                                <div className="left_wrap up w-100">
                                                    <div className="background_area">
                                                        <div className="img_area">
                                                            {universityDetail && universityDetail.logo ? <img src={universityDetail && universityDetail.logo ? renderImage(universityDetail.logo, 'large') : ``} alt="" /> : null}
                                                        </div>
                                                    </div>
                                                    <div className="university_area up">
                                                        <div className="inner_area">
                                                            <div className="left_met">
                                                                {
                                                                    universityDetail
                                                                    &&
                                                                    <div
                                                                        className="rating_area"
                                                                        onClick={() => {
                                                                            if(universityDetail.rating_user_count > 0)
                                                                                setReviewlist(true)
                                                                            else
                                                                                setAddReview(true);
                                                                        }}
                                                                    >
                                                                        <div className="rating_met" >
                                                                            <ul>
                                                                                <li>
                                                                                    <i className={`fas fa-star` + (universityDetail.rating >= 0.1 || universityDetail.rating >= 1 ? ' active' : '')}></i>
                                                                                </li>
                                                                                <li>
                                                                                    <i className={`fas fa-star` + (universityDetail.rating >= 2 ? ' active' : '')}></i>
                                                                                </li>
                                                                                <li>
                                                                                    <i className={`fas fa-star` + (universityDetail.rating >= 3 ? ' active' : '')}></i>
                                                                                </li>
                                                                                <li>
                                                                                    <i className={`fas fa-star` + (universityDetail.rating >= 4 ? ' active' : '')}></i>
                                                                                </li>
                                                                                <li>
                                                                                    <i className={`fas fa-star` + (universityDetail.rating >= 5 ? ' active' : '')}></i>
                                                                                </li>
                                                                            </ul>
                                                                            {
                                                                                universityDetail.rating_user_count > 0
                                                                                ?
                                                                                    <span className="show_rating">
                                                                                        <i class="fas fa-user pe-1"></i> {intToString(universityDetail.rating_user_count)}
                                                                                    </span>
                                                                                :
                                                                                    <span className="show_rating">Be first to write a review</span>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                }
                                                                <div className="university_detail_area">
                                                                    <div className="left_area">
                                                                        <h1>
                                                                            {universityDetail.title ? universityDetail.title : null}
                                                                        </h1>
                                                                    </div>
                                                                    <div className="des_area">
                                                                        <p>
                                                                            <ReadMoreAndLessText
                                                                                text={universityDetail.description ? universityDetail.description : ``}
                                                                                limit={250}
                                                                            />
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="right_met">
                                                                <div
                                                                    className="saved"
                                                                    onClick={(e) => {
                                                                        handleWishlist(e);
                                                                    }}
                                                                    style={favouriting ? { opacity: `0.7` } : {}}
                                                                >
                                                                    <i class={universityDetail && parseInt(universityDetail.favourite) === 1 ? `fas fa-bookmark` : `fal fa-bookmark`}></i>
                                                                </div>
                                                                <div className="right_area">
                                                                    <button className="btn-primary-2" onClick={handleApplyModal} >Apply Now</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="listing_detail">
                            {/* <Breadcrumbs /> */}
                            <div className="container">
                                <div className="row">
                                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <Slider_listing
                                            universityDetail={universityDetail}
                                            favourite={(e) => favourite(e)}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div className="background">
                            <div className="container">
                                <div className="row">
                                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div>
                                            <div className="background_area">
                                                <div className="img_area">
                                                    {universityDetail && universityDetail.logo ? <img src={universityDetail && universityDetail.logo ? renderImage(universityDetail.logo, 'large') : ``} alt="" /> : null}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}
                        <div className="university_courses_wrap">
                            <div className="container">
                                <div className="row">
                                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div className="penn_state_university_wrap">
                                            <div className="left_wrap">
                                                <University_detail universityDetail={universityDetail} />
                                                <div className="agent_area">
                                                    <div className="inner_area">
                                                        <div className="heading_area">
                                                            <h4>Agents</h4>
                                                            <p>Lorem ipsum dolor sit amet, con sectetur adipiscing elit. Maecenas con sectetur
                                                                maximus est, vel tincidunt odio auctor nec. Nam sed sollicitudin sapien.</p>
                                                        </div>
                                                        {
                                                            universityDetail && universityDetail.slug
                                                            ?
                                                                <Agent_detail universityDetail={universityDetail} />
                                                            :
                                                                <p className="text-center"><i className="a fa-spin fa-spinner"></i></p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="right_area">
                                                <div className="d-md-block d-none">
                                                    <Map_detail universityDetail={universityDetail} />
                                                    </div>
                                                <Courses_detail
                                                    universityDetail={universityDetail}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
            }
            {/* <AgentListModal universityDetail={universityDetail}/> */}
            
            { universityDetail && universityDetail.slug && reviewlist &&
            <ReviewListingModal
              show={reviewlist}
              university={universityDetail}
              close={()=>setReviewlist(false)}
              openAddReview={()=>{
                setAddReview(true);
                setReviewlist(false);
              }}
            />
            }
            { addReview &&
                <AddReviewModal
                    university={universityDetail}
                    show={addReview}
                    close={()=>setAddReview(false)}
                />
            }
        </div>

    );
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
    isApply: state.ApplyNowModalReducer.isApply,
});
export default connect(mapStateToProps)(Listing_detail);
