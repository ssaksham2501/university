import React from "react";
import Banner from "./components/banner";
import Contact from "./components/contact";
import Services from "./components/services";

function About() {
    return (
      <div>
          <Banner />
          <Services />
          <Contact />
      </div>
    );
}

export default About;
