

import React, {Component} from 'react';
import {render} from 'react-dom';
import { Link } from 'react-router-dom';
import {arrayMove, SortableContainer, SortableElement} from 'react-sortable-hoc';
import { renderImage } from '../helpers/General';
import choose_uni1 from "../assets/images/choose_uni1.png";

const SortableItem = SortableElement((props) => (
    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 college-hoc">
        <div className='choose_university_area'>
            <div className='inner_box_area'>
                <div className='innerr_image_area'>
                    <div className='image_area'>
                        <img src={props.item.logo ? renderImage(props.item.logo, 'small') : choose_uni1} />
                    </div>
                </div>
                <div className='info_area'>
                    <div className="unisersity_name">
                        <p>{props.item.label}</p>
                    </div>
                    <div className="uni_location_area">
                        <a href="" onClick={(e) => e.preventDefault()}>
                            <div className="left_area">
                                <i class="fal fa-map-marker-alt"></i>
                            </div>
                            <div className="right_area_in">
                                {props.item.country_name}
                            </div>
                        </a>
                    </div>
                    <div className='cross_area'>
                        <Link to="" onClick={(e) => {
                            e.preventDefault();
                            props.onRemoveItem(props.item, props.index);
                        }}>
                            <i class="fal fa-times"></i>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    </div>
));

const SortableList = SortableContainer((props) => {
  return (
    <div className='row align-items-stretch'>
      {props.items.map((item, index) => (
		  <SortableItem key={`item-${index}`} index={index} item={item} onRemoveItem={props.onRemoveItem} />
      ))}
    </div>
  );
});

const SortableComponent = (props) => {
	const onSortEnd = ({ oldIndex, newIndex }) => {
	  	let i = arrayMove(props.items, oldIndex, newIndex)
	  	props.onUpdateList(i);
  	};
    
	return <SortableList axis='xy' items={props.items} onSortEnd={onSortEnd} onRemoveItem={props.onRemoveItem} />;
}

// render(<SortableComponent />, document.getElementById('root'));


export default SortableComponent