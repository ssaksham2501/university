import React from "react";
import SignUpThree from "../SignUp/components/sign_up_three";

function SignUp3() {
  return (
    <div>
      <div className="Sign_up_area top_space">
        <div className="container">
          <div className="row">
            <div className="col-xxl-7 col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 m-auto">
               <SignUpThree />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUp3;

