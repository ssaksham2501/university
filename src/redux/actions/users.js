import ActionType from "../constants/index";

export const setUserData = (param) => {
    let stored = localStorage.setItem("SET_USER_DATA", JSON.stringify(param));
    return {
        type: ActionType.SET_USER_DATA,
        payload: param,
    };
};

export const setAPIToken = (param) => {
    let stored = localStorage.setItem("SET_API_TOKEN", JSON.stringify(param));
    return {
        type: ActionType.SET_API_TOKEN,
        payload: param,
    };
};

export const setScroller = (param) => {
    let stored = localStorage.setItem("SET_SCROLLER", param);
    return {
        type: ActionType.SET_SCROLLER,
        payload: param,
    };
};

export const setGetStartedModal = (param) => {
    let stored = localStorage.setItem("SET_STARTED_MODAL", param);
    return {
        type: ActionType.SET_STARTED_MODAL,
        payload: param,
    };
};

export const setApplyNowModal = (param) => {
    let stored = localStorage.setItem("SET_APPLY_NOW_MODAL", param);
    return {
        type: ActionType.SET_APPLY_NOW_MODAL,
        payload: param,
    };
};