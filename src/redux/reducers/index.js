import { combineReducers } from "redux";
import UserReducer from "./user";
import APITokenReducer from "./apiTokens";
import ScrollerReducer from "./scroller";
import StartedModalReducer from "./startedModal";
import ApplyNowModalReducer from "./applyNowModal";
const appReducer = combineReducers({
    UserReducer,
    APITokenReducer,
    ScrollerReducer,
    StartedModalReducer,
    ApplyNowModalReducer
});
const rootReducer = (state, action) => {
    return appReducer(state, action);
};
export default rootReducer;
