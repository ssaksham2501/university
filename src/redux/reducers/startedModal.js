import ActionTypes from "../constants";

const initialState = {
  isStart: false,
};

const StartedModalReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.SET_STARTED_MODAL:
      return Object.assign(new Boolean(), state, {
        isStart: action.payload,
      });
    default:
      return state;
  }
};
export default StartedModalReducer;
