import ActionTypes from "../constants";

const initialState = {
    isApply: false,
};

const ApplyNowModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.SET_APPLY_NOW_MODAL:
            return Object.assign(new Boolean(), state, {
                isApply: action.payload,
            });
        default:
            return state;
    }
};
export default ApplyNowModalReducer;
