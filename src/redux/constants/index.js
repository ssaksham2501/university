import keyMirror from "fbjs/lib/keyMirror";

const ActionTypes = keyMirror({
    SET_USER_DATA: "SET_USER_DATA",
    SET_API_TOKEN: "SET_API_TOKEN",
    SET_SCROLLER: "SET_SCROLLER",
    SET_STARTED_MODAL: "SET_STARTED_MODAL",
    SET_APPLY_NOW_MODAL:'SET_APPLY_NOW_MODAL'
});

export default ActionTypes;
