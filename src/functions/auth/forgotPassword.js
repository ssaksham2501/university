import { useState } from "react";
import AuthController from "../../apis/controllers/auth.controller";
import ModalController from "../../apis/controllers/modal.controller";
import Validation from "../../helpers/vaildation";
function ForgotPassword(props) {

    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [email, setEmail] = useState(``);
    const [otp, setOTP] = useState(``);
    const [otpToken, setOTPToken] = useState(null);
    
    
    const forgotPasswordHandler = {
        handleChange: (e) => {
            setEmail(e.target.value);
            let validEmail = new Validation().validateRule('email', e.target.value);
            if (!validEmail.isValid) {
                setErrMsg('Please enter a valid email address.');
            }
            else {
                setErrMsg('');
            }
        },
        sendRequest: async () => {
            if (isLoading === false) {
                setErrMsg(``);
                let validEmail = new Validation().validateRule('email', email);
                if (email && validEmail.isValid) {
                    setIsLoading(true);
                    let response = await new AuthController().forgetPassword({
                        email: email,
                    });
                    setIsLoading(false);
                    if (response && response.status) {
                        setOTPToken(response.token);
                    }
                    else {
                        setErrMsg(response.message);
                    }
                } else {
                    setErrMsg('Please enter a valid email address.');
                }
            }
        },
        resendOtpRequest: () => {
            new ModalController().resendOtpRequest(otpToken);
        },
        changeOTP: (val) => {
            setOTP(val);
        },
        validateOtp: async () => {
            if (otp) {
                setIsLoading(true);
                let response = await new AuthController().verifyOtp(otpToken, otp);
                setIsLoading(false);
                if (response && response.status) {
                    props.restRecoverPassword(otpToken, otp);
                }
                else {
                    setErrMsg(response.message);
                }
            }
            else {
                setErrMsg('Please enter one time password to proceed.');
            }
        }
    }

    return {
        forgotPasswordHandler,
        email,
        setEmail,
        errMsg,
        isLoading,
        otpToken,
        otp
    }
}

export default ForgotPassword;