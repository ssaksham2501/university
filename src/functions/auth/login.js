import { useState } from "react";
import { useHistory } from "react-router-dom";
import ModalController from "../../../src/apis/controllers/modal.controller";
import { storeItem } from "../../../src/helpers/localstorage";
import AuthController from "../../apis/controllers/auth.controller";
function Login(props) {
    const history = useHistory();
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState(null);
    let defaultValues = {
        phone_number: ``,
        country_code: ``,
    };
    const [values, setValues] = useState(defaultValues);
    const [showHidePassword, setShowHidePassword] = useState(false);
    const [showPasswordBlock, setShowPasswordBlock] = useState(false);
    const [password, setPassword] = useState(``);
    
    //contact  --start

    const loginHandler = {
        togglePassword: () => {
            setShowHidePassword(!showHidePassword);
        },
        getStarted: async () => {
            if (isLoading === false) {
                setErrMsg(``);
                setPhoneNumber(null);
                /** Check full form validation and submit **/
                if (values.phone_number && values.country_code) {
                    setIsLoading(true);
                    let response = await new ModalController().login(values);
                    setIsLoading(false);
                    if (response && response.status && response.user) {
                        setValues(defaultValues);
                        storeItem(`user`, response.user);
                        props.openOtpModal();
                    }
                    else if (response && response.status && response.completed) {
                        setPhoneNumber(values.phone_number);
                        setShowPasswordBlock(true);
                    }
                    else {
                        setErrMsg(response.message);
                    }
                } else {
                    setErrMsg('Please enter a valid phonenumber.');
                }
            }
        },
        passwordHandler: async () => {
            if (isLoading === false) {
                setErrMsg(``);
                if (values.phone_number && values.country_code && password) {
                    setIsLoading(true);
                    let response = await new ModalController().authLogin({
                        phone_number: values.phone_number ? values.phone_number : null,
                        country_code: values.country_code ? values.country_code : null,
                        password: password ? password : null,
                    });
                    setIsLoading(false);
                    if (response && response.status && response.user) {
                        if (response.user.user_type === 'agent' && response.user.agent_approved < 1) {
                            history.push('/awaiting-approval');
                            props.close();
                        }
                        else {
                            new AuthController().setUpLogin(response.user);
                            props.close();
                        }
                    }
                    else {
                        setErrMsg(response.message);
                    }
                } else {
                    setErrMsg('Please enter a valid phonenumber.');
                }
            }
        }
    }

    return {
        loginHandler,
        values,
        setValues,
        errMsg,
        phoneNumber,
        setPhoneNumber,
        isLoading,
        showPasswordBlock,
        setPassword,
        showHidePassword
    }
}

export default Login;