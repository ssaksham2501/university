import { useState } from "react";
import AuthController from "../../apis/controllers/auth.controller";
import ModalController from "../../apis/controllers/modal.controller";
import Validation from "../../helpers/vaildation";
function RecoverPassword(props) {
    let defaultValues = {
        new_password: ``,
        confirm_password: ``,
        otp: ``,
    };
    const [errMsg, setErrMsg] = useState(``);
    const [successMessage, setSuccessMessage] = useState(``);
    
    const [isLoading, setIsLoading] = useState(false);
    const [showHidePassword, setShowHidePassword] = useState(false);
    const [showHideConfirmPassword, setShowHideConfirmPassword] = useState(false);
    
    const [isError, setError] = useState({
        new_password: {
            rules: ["required", 'password'],
            isValid: true,
            message: "",
        },
        confirm_password: {
            rules: ["required"],
            isValid: true,
            message: "",
        }
    });
    let validation = new Validation(isError);
    const [values, setValues] = useState(defaultValues);
    
    const recoverPasswordHandler = {
        handleChange: (field, value) => {
             /** Validate each field on change */
            if (field == 'confirm_password' && value) {
                let node2 = validation.matchValues(`confirm_password`, value, values.new_password, "Confirm password does not match.");
                console.log(node2);
                setError({ ...isError, ["confirm_password"]: node2 });
            }
            else {
                let node = validation.validateField(field, value);
                setError({ ...isError, [field]: node });
            }
            /** Validate each field on change */
            setValues({ ...values, [field]: value });
        },
        togglePassword: () => {
            setShowHidePassword(!showHidePassword);
        },
        toggleConfirmPassword: () => {
            setShowHideConfirmPassword(!showHideConfirmPassword);
        },
        submitRequest: async () => {
            if (isLoading === false) {
                setErrMsg(false);
                /** Check full form validation and submit **/
                let validation = new Validation(isError);
                let isValid = await validation.isFormValid(values);
                let node = validation.matchValues(`confirm_password`, values.new_password, values.confirm_password, "Confirm password does not match.");
                setError({ ...isError, ["confirm_password"]: node });
                if (isValid && node.isValid && !isValid.haveError) {
                    setIsLoading(true);
                    let response = await new AuthController().recoverdPassword(props.data.token, { ...values, otp: props.data.otp });
                    setIsLoading(false);
                    if (response && response.status) {
                        setValues(defaultValues);
                        setSuccessMessage(response.message);
                    } else {
                        setErrMsg(response.message);
                    }
                } else {
                    setError({ ...isValid.errors });
                }
            }
        }
    }

    return {
        recoverPasswordHandler,
        isError,
        values,
        errMsg,
        successMessage,
        isLoading,
        showHidePassword,
        showHideConfirmPassword
    }
}

export default RecoverPassword;