import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ActionController from "../../apis/controllers/action.controller";
import AuthController from "../../apis/controllers/auth.controller";
import { getStoredItem, removeItem, storeItem } from "../../helpers/localstorage";
import Validation from "../../helpers/vaildation";
function AgentAuthorization() {
    const history = useHistory();
    const [states, setStates] = useState([]);
    const [searchUniversities, setSearchUniversities] = useState(``);
    const [universityListing, setUniversityListing] = useState([]);
    const [selectedColleges, setSelectedColleges] = useState([]);
    const [range, setRange] = useState(null);
    const [range1, setRange1] = useState([]);
    const [range2, setRange2] = useState([]);
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [countrylist, setCountryList] = useState(null);
    const [mounting, setMounting] = useState(true);
    let defaultValues = {
        one: {
            image: null,
            isCompany: 0,
            companyName: null,
            firstName: null,
            lastName: null,
            email: null,
            stateId: null,
            pinCode: null,
            password: null,
            confirmPassword: null,
            bio: null,
        },
        two: {
            country: [],
            university: [],
        },
        three: {
            successCases: ``,
            successRate: ``,
            accreditation: [],
            credentials: [],
        },
    };
    const [values, setValues] = useState(defaultValues);
    const [showHidePassword, setShowHidePassword] = useState(false);
    const [showHideConfirmPassword, setShowHideConfirmPassword] = useState(false);


    const [isError, setError] = useState({
        image: {
            rules: [],
            isValid: true,
            message: '',
        },
        isCompany: {
            rules: [],
            isValid: true,
            message: '',
        },
        companyName: {
            rules: [],
            isValid: true,
            message: '',
        },
        firstName: {
            rules: ['required', 'alphabetic'],
            isValid: true,
            message: '',
        },
        lastName: {
            rules: ['required', 'alphabetic'],
            isValid: true,
            message: '',
        },
        email: {
            rules: ['required', 'email'],
            isValid: true,
            message: '',
        },
        stateId: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        pinCode: {
            rules: ['required', 'numeric'],
            isValid: true,
            message: '',
        },
        password: {
            rules: ['required', 'password'],
            isValid: true,
            message: '',
        },
        confirmPassword: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        bio: {
            rules: ['required', 'max:300'],
            isValid: true,
            message: '',
        },
        country: {
            rules: ['array'],
            isValid: true,
            message: '',
        },
        university: {
            rules: [],
            isValid: true,
            message: '',
        },
        successCases: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        successRate: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        accreditation: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        credentials: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
    });
    //contact  --start
    let validation = new Validation(isError);

    const handleStates = (field, value, step) => {

        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        setValues({
            ...values,
            [step]: {
                ...values[step],
                [field]: value,
            }
        });
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        init();
        getStatesData();
        getSingupStepOne();
        getRange();

    }, []);

    const init = async () => {
        let user = await getStoredItem(`user`)
        if (user && user.token) {
            setUser(user);
            setSelectedColleges(user && user.universities ? user.universities : []);
            setValues({
                ...values,
                one: {
                    ...values.one,
                    image: user && user.image ? user.image : null,
                    isCompany: user && user.is_company ? user.is_company : 0,
                    companyName: user && user.company_name ? user.company_name : null,
                    firstName: user && user.first_name
                        ? user.first_name
                        : null,
                    lastName: user && user.last_name ? user.last_name : null,
                    email: user && user.email ? user.email : null,
                    stateId: user && user.state_id ? {
                        id: user.state_id,
                        value: user.state_name,
                        label: user.state_name,
                    } : null,
                    pinCode: user && user.pincode
                        ? user.pincode
                        : null,
                    password: null,
                    confirmPassword: null,
                    bio: user && user.bio ? user.bio : null,
                },
                two: {
                    ...values.two,
                    country: user && user.countries ? user.countries : [],
                    university: user && user.universities ? user.universities : [],
                },
                three: {
                    ...values.three,
                    successCases: ``,
                    successRate: ``,
                    accreditation: user && user.accreditation ? user.accreditation : [],
                    credentials: user && user.credentials ? user.credentials : [],
                },
            });
        }
        else {
            history.push('/');
        }
    }

    const getStatesData = async () => {
        let response = await new ActionController().getStates();
        if (response.status) {
            setStates(response.listing)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    const getSingupStepOne = async () => {
        setMounting(true);
        let response = await new ActionController().getSingupStepOne();
        if (response.status) {
            let user = await getStoredItem(`user`)
            if (user && user.token && user.countries && user.countries.length > 0) {
                let a = response.country.filter((i) => user.countries.includes(i.id));
                let b = response.country.filter((i) => !user.countries.includes(i.id));
                setCountryList([...a, ...b]);
            }
            else {
                setCountryList(response.country);
            }
            setMounting(false);
        }
        else {
            console.log('Error while fetching api data.')
        }
    }


    const getRange = async () => {
        let response = await new ActionController().getSetting();
        if (response.status) {
            setRange(response.data)
            let data1 = response.data && response.data.question1_value;
            setRange1(data1)
            console.log(data1);
            let data2 = response.data && response.data.question2_value;
            setRange2(data2)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    const StepAgentOne = {
        handleProof: (response, field) => {
            let c = values.three[field];
            c.push(response.path);
            handleStates(
                field,
                c, 'three'
            );
            let u = getStoredItem('user')
            u[field] = c;
            storeItem(`user`, u);
        },
        removeProof: (index, path, field) => {
            let c = values.three[field];
            c.splice(index, 1);
            handleStates(
                field,
                c, 'three'
            );
            let u = getStoredItem('user')
            u[field] = c;
            storeItem(`user`, u);
            new ActionController().removeFile(path);
        },
        handleCountrySelect: (country) => {
            let s = values.two.country ? values.two.country : [];
            let index = s.indexOf(country.id);

            if (index > -1) {
                s.splice(index, 1);
                StepAgentOne.removeUniversityByCountry(country.id)
            }
            else {
                s.push(country.id);
            }
            handleStates(
                "country",
                s
            );
        },
        removeUniversityByCountry: (countryid) => {
            const items = [];
            for (let i in selectedColleges) {
                if (selectedColleges[i].country_id != countryid) {
                    items.push(selectedColleges[i]);
                }
            }
            setSelectedColleges(items);
            handleStates(
                "university",
                items, 'two'
            );
        },
        handleUniversitySelect: (val, item) => {
            const items = selectedColleges ? selectedColleges : [];
            let exist = selectedColleges && selectedColleges.some(sitem => sitem.slug === item.slug && sitem.country_id === item.country_id);
            if (!exist) {
                items.push(item);
            }
            setSelectedColleges(items);
            handleStates(
                "university",
                items, 'two'
            );
            setTimeout(() => {
                setSearchUniversities(``);
            }, 300);
        },
        handleEmail: async (e) => {
            handleStates(
                "email",
                e.target.value,
                'one'
            );
            let node = await validation.emailExist('email', e.target.value);
            setError({ ...isError, email: node });
        },
        handleConfirmPassword: async (e) => {
            handleStates(
                "confirmPassword",
                e.target.value,
                'one'
            );
            let node = validation.matchValues(`confirmPassword`, values.one.password, e.target.value, "Confirm password does not match.");
            setError({ ...isError, confirmPassword: node });
        },

        stepOne: async () => {
            if (isLoading === false) {
                let error = null;
                if (
                    values.one.isCompany === 1 ||
                    values.one.isCompany === '1'
                ) {
                    error = {
                        ...isError,
                        companyName: {
                            rules: ['required'],
                            isValid: values.one.companyName !== `` ? true : false,
                            message: '',
                        },
                    };
                } else {
                    error = {
                        ...isError,
                        companyName: {
                            rules: [],
                            isValid: true,
                            message: '',
                        },
                    };
                }
                setIsLoading(true);
                let validation = new Validation(error);
                let isValid = await validation.isFormValid(values.one);
                let node = validation.matchValues(`confirmPassword`, values.one.password, values.one.confirmPassword, "Confirm password does not match.");
                let node2 = await validation.emailExist('email', values.one.email);
                setError({ ...isValid.errors, confirmPassword: node, email: node2 });
                if (isValid && node.isValid && node2.isValid && !isValid.haveError) {
                    let response = await new AuthController().agentSignupStepOne(user.token, values.one);
                    if (response && response.status) {
                        storeItem(`user`, response.user);
                        setIsLoading(false);
                        history.push('/agent-signup/step-2')
                    } else {
                        setErrMsg(response.message);
                        setIsLoading(false);
                    }
                } else {
                    setIsLoading(false);
                }
            }
        },
        stepTwo: async () => {
            if (isLoading === false) {
                let validation = new Validation(isError);
                let isValid = await validation.isFormValid(values.two);
                if (isValid && !isValid.haveError) {
                    setIsLoading(true);
                    let response = await new AuthController().agentSignupStepTwo(user.token, values.two);
                    if (response && response.status) {
                        storeItem(`user`, response.user);
                        setIsLoading(false);
                        history.push('/agent-signup/step-3')
                    } else {
                        setErrMsg(response.message);
                        setIsLoading(false);
                    }
                } else {
                    setIsLoading(false);
                    setError({ ...isValid.errors });
                }
            }
        },
        stepThree: async () => {
            if (isLoading === false) {
                let validation = new Validation(isError);
                let isValid = await validation.isFormValid(values.three);
                if (isValid && !isValid.haveError) {
                    setIsLoading(true);
                    let response = await new AuthController().agentSignupStepThree(user.token, values.three);
                    if (response && response.status) {
                        removeItem(`user`);
                        setIsLoading(false);
                        history.push('/awaiting-approval');
                    } else {
                        setIsLoading(false);
                    }
                } else {
                    setIsLoading(false);
                    setError({ ...isValid.errors });
                }
            }
        },
    }

    return {
        StepAgentOne,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        showHidePassword,
        setShowHidePassword,
        showHideConfirmPassword,
        setShowHideConfirmPassword,
        handleStates,
        states,
        countrylist,
        universityListing,
        range,
        range1,
        range2,
        selectedColleges,
        setSelectedColleges,
        searchUniversities,
        setSearchUniversities
    }
}

export default AgentAuthorization;