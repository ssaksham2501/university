import React, { useState } from "react";
import LeadsController from "../../apis/controllers/leads.controller";
const Leads = () => {
    const [leads, setLeads] = useState([]);
    const [mounting, setMounting] = useState(true);
    
    const leadsHandler = {
        init: async () => {
            await leadsHandler.getLeads()
        },
        getLeads: async () => {
            if (mounting) {
                let response = await new LeadsController().getLeads();
                setMounting(false);
                if (response && response.status) {
                    setLeads(response.leads);
                }
            }
        }
    };
    return {
        leadsHandler,
        leads,
        mounting
    };
};

export default Leads;
