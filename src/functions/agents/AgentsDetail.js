import { useState } from "react";
import { useHistory } from "react-router-dom";
import AgentProfileController from "../../apis/controllers/agentController/agent.controller";
function AgentsDetail(props) {

    const history = useHistory();
    const [agent, setAgent] = useState(null);
    const [universities, setUniversities] = useState([]);
    

    const agentDetailhandler = {
        init: async (slug) => {
            let response = await new AgentProfileController().agentProfileDetails(slug);
            if (response && response.status) {
                setAgent(response.profile);
                setUniversities(response.universities);
            }
            else {
                history.push('/404');
            }
        }
    }
    
    return {
        agent,
        universities,
        agentDetailhandler
    }
}

export default AgentsDetail;