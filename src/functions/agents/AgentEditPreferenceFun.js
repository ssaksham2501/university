import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ActionController from "../../apis/controllers/action.controller";
import AgentProfileController from "../../apis/controllers/agentController/agent.controller";
import AuthController from "../../apis/controllers/auth.controller";
import StudentProfileController from "../../apis/controllers/studentController/student.controller";
import { getStoredItem, removeItem, storeItem } from "../../helpers/localstorage";
import Validation from "../../helpers/vaildation";
function AgentEditPreferenceFun() {
    const history = useHistory();
    const [states, setStates] = useState([]);
    const [searchUniversities, setSearchUniversities] = useState(``);
    const [universityListing, setUniversityListing] = useState([]);
    const [selectedColleges, setSelectedColleges] = useState([]);
    const [range, setRange] = useState(null);
    const [range1, setRange1] = useState([]);
    const [range2, setRange2] = useState([]);
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [countrylist, setCountryList] = useState(null);
    const [mounting, setMounting] = useState(true);
    let defaultValues = {
        country: [],
        university: [],
    };
    const [values, setValues] = useState(defaultValues);


    const [isError, setError] = useState({
        country: {
            rules: ['array'],
            isValid: true,
            message: '',
        },
        university: {
            rules: ['array'],
            isValid: true,
            message: '',
        },
    });
    //contact  --start
    let validation = new Validation(isError);

    const handleStates = (field, value) => {
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        setValues({ ...values, [field]: value });
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        getStatesData();
        getCountry();
        init();

    }, []);

    const init = async () => {
        let res = await new StudentProfileController().profileDetails();
        if (res && res.status) {
            let data = res && res.user;
            let countryArr = [];
            if (data.countries.length > 0) {
                countryArr = data.countries.map(item => item.id)
            }
            setValues({
                ...values,
                country: countryArr,
                university: data.universities,
            });
            setSelectedColleges(data.universities)
        }
    }

    const getStatesData = async () => {
        let response = await new ActionController().getStates();
        if (response.status) {
            setStates(response.listing)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    const getCountry = async () => {
        setMounting(true);
        let response = await new ActionController().getSingupStepOne();
        if (response.status) {
            let user = await getStoredItem(`user`)
            if (user && user.token && user.countries && user.countries.length > 0) {
                let a = response.country.filter((i) => user.countries.includes(i.id));
                let b = response.country.filter((i) => !user.countries.includes(i.id));
                setCountryList([...a, ...b]);
            }
            else {
                setCountryList(response.country);
            }
            setMounting(false);
        }
        else {
            console.log('Error while fetching api data.')
        }
    }




    const AgentEdit = {
        handleCountrySelect: (country) => {
            let s = values.country ? values.country : [];
            let index = s.indexOf(country.id);

            if (index > -1) {
                s.splice(index, 1);
                AgentEdit.removeUniversityByCountry(country.id)
            }
            else {
                s.push(country.id);
            }
            handleStates(
                "country",
                s
            );
        },
        removeUniversityByCountry: (countryid) => {
            const items = [];
            for (let i in selectedColleges) {
                if (selectedColleges[i].country_id != countryid) {
                    items.push(selectedColleges[i]);
                }
            }
            setSelectedColleges(items);
            handleStates(
                "university",
                items
            );
        },
        handleUniversitySelect: (val, item) => {
            const items = selectedColleges ? selectedColleges : [];
            let exist = selectedColleges && selectedColleges.some(sitem => sitem.slug === item.slug && sitem.country_id === item.country_id);
            if (!exist) {
                items.push(item);
            }
            setSelectedColleges(items);
            handleStates(
                "university",
                items
            );
            setTimeout(() => {
                setSearchUniversities(``);
            }, 300);
        },
        editPreferences: async () => {
            if (isLoading === false) {
                let validation = new Validation(isError);
                let isValid = await validation.isFormValid(values);
                if (isValid && !isValid.haveError) {
                    setIsLoading(true);
                    let response = await new AgentProfileController().changePrefernce(values);
                    if (response && response.status) {
                        setIsLoading(false);
                        history.push('/my-profile')
                    } else {
                        setErrMsg(response.message);
                        setIsLoading(false);
                    }
                } else {
                    setIsLoading(false);
                    setError({ ...isValid.errors });
                }
            }
        },
    }

    return {
        AgentEdit,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        handleStates,
        states,
        countrylist,
        universityListing,
        selectedColleges,
        setSelectedColleges,
        searchUniversities,
        setSearchUniversities
    }
}

export default AgentEditPreferenceFun;