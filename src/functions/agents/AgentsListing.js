import { useState } from "react";
import AgentProfileController from "../../apis/controllers/agentController/agent.controller";
import AuthController from "../../apis/controllers/auth.controller";
import Validation from "../../helpers/vaildation";
function AgentsListing(props) {
    let [lastPage, setLastPage] = useState(1);
    const [page, setPage] = useState(1);
    const [agents, setAgents] = useState([]);
    const [totalAgents, setTotalAgents] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    const [done, setDone] = useState(false);
    const [sorting, setSorting] = useState({title: "Rating", value: "rating"});

    const agentListingHandler = {
        handleScroll: async (e) => {
            if (e.target.scrollTop >= e.target.scrollHeight - e.target.clientHeight - 10 && !isLoading && !done && page != lastPage) {
                await agentListingHandler.sendRequest();
            }
        },
        sendRequest: async (bigModal) => {
            if (!isLoading && !done) {
                setIsLoading(true);
                let response = await new AgentProfileController().getListing({
                    page: page,
                    slug: props.universityDetail.slug,
                    modal: bigModal ? true : false,
                    sorting: sorting
                });
                
                if (response && response.status) {
                    setAgents(page === 1 ? response.listing : [...agents, ...response.listing]);
                    setTotalAgents(response.total);
                }

                if (response.listing.length < 1 && page > 1) {
                    setDone(true);
                }
                setLastPage(response.page);
                setPage(response.page + 1);
                setIsLoading(false);
            }
        }
    }

    return {
        agents,
        totalAgents,
        isLoading,
        agentListingHandler,
        sorting,
        setSorting,
        page,
        setPage
    }
}

export default AgentsListing;