import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ActionController from "../../apis/controllers/action.controller";
import AgentProfileController from "../../apis/controllers/agentController/agent.controller";
import StudentProfileController from "../../apis/controllers/studentController/student.controller";
import { getStoredItem } from "../../helpers/localstorage";
import Validation from "../../helpers/vaildation";
function StudentEditPreferenceFun() {
    const history = useHistory();
    const [states, setStates] = useState([]);
    const [profession, setProfession] = useState(null);
    const [preferintake, setPreferIntake] = useState(null);
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [countrylist, setCountryList] = useState(null);
    const [mounting, setMounting] = useState(true);
    let defaultValues = {
        country: [],
        resumption_semster: ``,
        stream_id: [],
    };
    const [values, setValues] = useState(defaultValues);


    const [isError, setError] = useState({
        country: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        resumption_semster: {
            rules: ["required", "numeric"],
            isValid: true,
            message: "",
        },
        stream_id: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    //contact  --start
    let validation = new Validation(isError);

    const handleStates = (field, value) => {
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        setValues({ ...values, [field]: value });
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        // getStatesData();
        getCountry();
        init();
        getSingupStepOne();
    }, []);

    const init = async () => {
        let res = await new StudentProfileController().profileDetails();
        if (res && res.status) {
            let data = res && res.user;
            let countryArr = [];
            if (data.countries.length > 0) {
                countryArr = data.countries.map(item => item.id)
            }
            let streamArr = [];
            if (data.streams.length > 0) {
                streamArr = data.streams.map(item => item.id)
            }
            setValues({
                ...values,
                country: countryArr ? countryArr : [],
                resumption_semster: data.resumption_semster,
                stream_id: streamArr ? streamArr : [],
            });
        }
    }

    const getSingupStepOne = async () => {
        setMounting(true);
        let response = await new ActionController().getSingupStepOne();
        if (response.status) {
            setProfession(response.streams)
            setPreferIntake(response.preferredintake)
            setMounting(false);
        }
        else {
            console.log('Error while fetching api data.')
        }
    }

    const getCountry = async () => {
        setMounting(true);
        let response = await new ActionController().getSingupStepOne();
        if (response.status) {
            let user = await getStoredItem(`user`)
            if (user && user.token && user.countries && user.countries.length > 0) {
                let a = response.country.filter((i) => user.countries.includes(i.id));
                let b = response.country.filter((i) => !user.countries.includes(i.id));
                setCountryList([...a, ...b]);
            }
            else {
                setCountryList(response.country);
            }
            setMounting(false);
        }
        else {
            console.log('Error while fetching api data.')
        }
    }




    const StudentEdit = {
        handleCountrySelect: (country) => {
            let s = values.country ? values.country : [];
            let index = s.indexOf(country.id);

            if (index > -1) {
                s.splice(index, 1);
            }
            else {
                s.push(country.id);
            }
            handleStates(
                "country",
                s
            );
        },
        handleStreamSelect: (stream) => {
            let s = values.stream_id ? values.stream_id : [];
            let index = s.indexOf(stream.id);

            if (index > -1) {
                s.splice(index, 1);
            }
            else {
                s.push(stream.id);
            }
            handleStates(
                "stream_id",
                s
            );
        },
        editPreferences: async () => {
            if (isLoading === false) {
                let validation = new Validation(isError);
                let isValid = await validation.isFormValid(values);
                if (isValid && !isValid.haveError) {
                    setIsLoading(true);
                    let response = await new StudentProfileController().StudentchangePrefernceOne(values);
                    if (response && response.status) {
                        setIsLoading(false);
                        history.push('/student-change-preference-step-2')
                    } else {
                        setErrMsg(response.message);
                        setIsLoading(false);
                    }
                } else {
                    setIsLoading(false);
                    setError({ ...isValid.errors });
                }
            }
        },
    }

    return {
        StudentEdit,
        mounting,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        handleStates,
        states,
        countrylist,
        profession,
        setProfession,
        preferintake,
        setPreferIntake
    }
}

export default StudentEditPreferenceFun;