import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import StudentDashboardController from "../../apis/controllers/studentController/studentDashboard";
import UniversityController from "../../apis/controllers/university.controllers";
function FavoriteUniversities(props) {
    let listing = [];

    const history = useHistory();
    const [favUniverties, setFavUniverties] = useState([]);
    const [loader, setLoader] = useState(false);
    const [page, setPage] = useState(1);
    const [done, setDone] = useState(false);
    const [total, setTotal] = useState(0)
    const [fetch, setFetching] = useState(false);
    const getFavList = async (page) => {
        if (!loader && !done) {
            let post = {
                page: page
            };
            let response = await new StudentDashboardController().favUnivertiesListing(post);
            if (response && response.status) {
                let list = response.university && response.university.data;
                listing = list;
                if (listing.length > 0) {
                    setTotal(listing.length)
                    if (listing.length < 10) {
                        setLoader(false);
                    } else {
                        setLoader(true);
                    }
                    setPage(post.page + 1);
                    if (post.page === 1) {
                        setFavUniverties(listing);
                    } else {
                        listing = [...favUniverties, ...list];
                        setFavUniverties(listing);
                    }
                } else {
                    if (post.page === 1) {
                        setFavUniverties([]);
                    }
                    setLoader(false);
                    setDone(true);
                }
            } else {
                setFavUniverties([]);
                history.push('/404');
            }
            setLoader(false);
            setFetching(false);
        }
    };

    const applyUniversity = async (id) => {
        let response = await new UniversityController().appliedUniversity(id)
        if (response && response.status) {
            alert(response.statusMessage)
        }
        else {
            alert('erroe message')
        }
    };


    const favUniversity = async (slug, index) => {
        let response = await new UniversityController().favUniversity(slug);
        if (response && response.status && listing[index]) {
            listing[index].favourite = response.fav ? 1 : 0;
            setFavUniverties(listing);
        }
    };


    return {
        favUniverties,
        setFavUniverties,
        fetch,
        setFetching,
        loader,
        page,
        done,
        total,
        getFavList,
        setPage,
        setDone,
        setLoader,
        applyUniversity,
        favUniversity,
    }
}

export default FavoriteUniversities;