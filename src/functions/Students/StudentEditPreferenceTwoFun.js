import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ActionController from "../../apis/controllers/action.controller";
import AgentProfileController from "../../apis/controllers/agentController/agent.controller";
import StudentProfileController from "../../apis/controllers/studentController/student.controller";
import { getStoredItem } from "../../helpers/localstorage";
import Validation from "../../helpers/vaildation";
function StudentEditPreferenceTwoFun() {
    const history = useHistory();
    const [errMsg, setErrMsg] = useState(``);
    const [isLoading, setIsLoading] = useState(false);
    const [educationList, setEducationList] = useState(null);
    const [user, setUser] = useState(null);
    const [years, setYears] = useState([]);
    const [mounting, setMounting] = useState(true);
    let defaultValues = {
        education_id: ``,
        gained_percentage: ``,
        year_of_graduation: ``,
        tofel: `0`,
        ielts: `0`,
        pte: `0`,
        ielts_score: ``,
        tofel_score: ``,
        pte_score: ``,
    };
    const [values, setValues] = useState(defaultValues);


    const [isError, setError] = useState({
        education_id: {
            rules: ["required", "numeric"],
            isValid: true,
            message: "",
        },
        gained_percentage: {
            rules: ["required", "decimel", "range:0:100"],
            isValid: true,
            message: "",
        },
        year_of_graduation: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        ielts: {
            rules: [],
            isValid: true,
            message: "",
        },
        pte: {
            rules: [],
            isValid: true,
            message: "",
        },
        tofel: {
            rules: [],
            isValid: true,
            message: "",
        },
        ielts_score: {
            rules: ["decimel", "range:0:9"],
            isValid: true,
            message: "",
        },
        tofel_score: {
            rules: ["numeric", "range:0:120"],
            isValid: true,
            message: "",
        },
        pte_score: {
            rules: ["numeric", "range:0:90"],
            isValid: true,
            message: "",
        }
    });
    //contact  --start
    let validation = new Validation(isError);

    const handleStates = (field, value) => {
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        setValues({ ...values, [field]: value });
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        initfun();
        init();
    }, []);

    const initfun = async () => {
        await generateArrayOfYears();
        await getSingupStepTwo();
    }

    const init = async () => {
        let res = await new StudentProfileController().profileDetails();
        if (res && res.status) {
            let data = res && res.user;
            setValues({
                ...values,
                education_id: data.education_id,
                gained_percentage: data.gained_percentage,
                year_of_graduation: parseInt(data.year_of_graduation),
                tofel: data.tofel,
                ielts: data.ielts,
                pte: data.pte,
                ielts_score: data.ielts_score && data.ielts_score > 0 ? data.ielts_score : ``,
                tofel_score: data.tofel_score && data.tofel_score > 0 ? data.tofel_score : ``,
                pte_score: data.pte_score && data.pte_score > 0 ? data.pte_score : ``,
            });
        }
    }

    const generateArrayOfYears = () => {
        var max = new Date().getFullYear();
        var min = max - 60;
        var years = [];

        for (var i = min; i <= max; i++) {
            years.push({ ...i, value: i, label: i.toString() });
        }
        years.reverse();
        setYears(years);
    };

    const getSingupStepTwo = async () => {
        let response = await new ActionController().getSingupStepTwo();
        if (response.status) {
            setEducationList(response.listing)
        }
        else {
            console.log('Error while fetching api data.')
        }
    }




    const StudentEditTwo = {
        editPreferences: async () => {
            if (isLoading === false) {
                let validation = new Validation(isError);
                let isValid = await validation.isFormValid(values);
                if (isValid && !isValid.haveError) {
                    setIsLoading(true);
                    let response = await new StudentProfileController().StudentchangePrefernceTwo(values);
                    if (response && response.status) {
                        setIsLoading(false);
                        history.push('/my-profile')
                    } else {
                        setErrMsg(response.message);
                        setIsLoading(false);
                    }
                } else {
                    setIsLoading(false);
                    setError({ ...isValid.errors });
                }
            }
        },
    }

    return {
        StudentEditTwo,
        mounting,
        values,
        setValues,
        errMsg,
        isError,
        setError,
        isLoading,
        handleStates,
        educationList,
        setEducationList,
        years,
        setYears
    }
}

export default StudentEditPreferenceTwoFun;