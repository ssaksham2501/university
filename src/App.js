import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AuthController from "./apis/controllers/auth.controller";
import "./App.scss";
import About from "./components/About";
import AgentEditPreference from "./components/AgentEditPreference/AgentEditPreference";
import AgentSignUp from "./components/AgentSignUp";
import AgentSignUpOne from "./components/AgentSignUp/components/Agentsign_up_one";
import AgentSignUpThree from "./components/AgentSignUp/components/Agentsign_up_three";
import AgentSignUpTwo from "./components/AgentSignUp/components/Agentsign_up_two";
import Agent_list from "./components/Agents_list";
import Agent_info from "./components/Agents_list/component/agent_list";
import Agent_listing from "./components/Agent_listing";
import AgentProcess from "./components/Agent_Process";
import ContactUs from "./components/ContactUs";
import AgentWallet from "./components/Dashboard/Agent_wallet";
import Dashboardmain from "./components/Dashboard/Component";
import AgentDashboardHome from "./components/Dashboard/Component/Agent_home";
import MyApplications from "./components/MyApplication/Applications";
import MyFavorites from "./components/Dashboard/Component/MyFavorites";
import My_Profile from "./components/Dashboard/Component/My_profile";
import Error from "./components/error";
import Faq from "./components/Faq/faq";
import Home from "./components/Home";
import Knowledgebase from "./components/KnowledgeBase/knowledge_base";
import Knowledgebasedetail from "./components/KnowledgeBase/knowledge_base_detail";
import Pagenotfound from "./components/Layout/components/404";
import HorizontalSlider from "./components/Layout/Horizontal_slider";
import {
    DashboardLayout,
    Layoutfour,
    LayoutOne,
    LayoutThree,
} from "./components/Layout/Layouts";
import Phonemenuss from "./components/Layout/Phonemenuss";
import Listing from "./components/listing";
import Listing_detail from "./components/listing_detail";
import Phonecomponent from "./components/Phone_input";
import PrivacyPolicy from "./components/PrivacyPolicy";
import SignUpOne from "./components/SignUp/components/sign_up_one";
import SignUpSuccess from "./components/SignUp/components/sign_up_success";
import SignUpThree from "./components/SignUp/components/sign_up_three";
import SignUpTwo from "./components/SignUp/components/sign_up_two";
import SortableComponent from "./components/sortableset";
import SignEditPreferenceStepOne from "./components/StudentEditPreference/StudentEditPreferenceStepOne";
import SignEditPreferenceStepTwo from "./components/StudentEditPreference/StudentEditPreferenceStepTwo";
import StudentAbout from "./components/Student_About";
import Studentprocess from "./components/Student_Process";
import TermsConditions from "./components/Terms&Conditions";
import { getStoredItem } from "./helpers/localstorage";
import store from "./redux/store";

function App() {
    return (
        <div>
            <Router>
                <Switch>
                    <RouteWrapper
                        path="/"
                        exact
                        component={() => <Home />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/login"
                        exact
                        component={() => <Home />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/about"
                        exact
                        component={() => <About />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/plugin"
                        exact
                        component={() => <SortableComponent />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/colleges"
                        exact
                        component={() => <Listing />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/college-detail/:slug"
                        exact
                        component={() => <Listing_detail />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/phonemenu"
                        exact
                        component={() => <Phonemenuss />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/agent/:slug"
                        exact
                        component={() => <Agent_listing />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/signup"
                        exact
                        component={() => <SignUpOne />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/signup/step-2"
                        exact
                        component={() => <SignUpTwo />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/signup/step-3"
                        exact
                        component={() => <SignUpThree />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/awaiting-approval"
                        exact
                        component={() => <SignUpSuccess />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/signup/success"
                        exact
                        component={() => <SignUpSuccess />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/error"
                        exact
                        component={() => <Error />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/contact-us"
                        exact
                        component={() => <ContactUs />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/faq"
                        exact
                        component={() => <Faq />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/terms-and-conditions"
                        exact
                        component={() => <TermsConditions />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/privacy-policy-conditions"
                        exact
                        component={() => <PrivacyPolicy />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/knowledge-base"
                        exact
                        component={() => <Knowledgebase />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/knowledge-base/:slug"
                        exact
                        component={() => <Knowledgebasedetail />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/for-agents"
                        exact
                        component={() => <AgentProcess />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/for-students"
                        exact
                        component={() => <Studentprocess />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/student-about"
                        exact
                        component={() => <StudentAbout />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/horizontal-slider"
                        exact
                        component={() => <HorizontalSlider />}
                        layout={LayoutOne}
                    />
                    <RouteWrapper
                        path="/my-profile"
                        exact
                        component={() => <My_Profile />}
                        layout={DashboardLayout}
                    />
                    <RouteWrapper
                        path="/agent-signup"
                        exact
                        component={() => <AgentSignUpOne />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/agent-signup/step-2"
                        exact
                        component={() => <AgentSignUpTwo />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/agent-signup/step-3"
                        exact
                        component={() => <AgentSignUpThree />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/agent-change-preference"
                        exact
                        component={() => <AgentEditPreference />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/student-change-preference-step-1"
                        exact
                        component={() => <SignEditPreferenceStepOne />}
                        layout={Layoutfour}
                    />
                    <RouteWrapper
                        path="/student-change-preference-step-2"
                        exact
                        component={() => <SignEditPreferenceStepTwo />}
                        layout={Layoutfour}
                    />

                    <RouteWrapper
                        path="/my-favorites"
                        component={() => <MyFavorites />}
                        layout={DashboardLayout}
                    />
                    <RouteWrapper
                        path="/applications"
                        component={() => <MyApplications />}
                        layout={DashboardLayout}
                    />
                    <RouteWrapper
                        path="/dashboard"
                        component={() => <AgentDashboardHome />}
                        layout={DashboardLayout}
                    />
                    <RouteWrapper
                        path="/my-wallet"
                        component={() => <AgentWallet />}
                        layout={DashboardLayout}
                    />
                    <RouteWrapper
                        path="/404"
                        component={() => <Pagenotfound />}
                        layout={LayoutOne}
                    />
                </Switch>
            </Router>
        </div>
    );
}

function RouteWrapper({ component: Component, layout: Layout, ...rest }) {
    return (
        <Provider store={store}>
            <Route
                {...rest}
                render={(props) => (
                    <Layout {...props}>
                        <Component {...props} />
                    </Layout>
                )}
            />
        </Provider>
    );
}

export default App;
