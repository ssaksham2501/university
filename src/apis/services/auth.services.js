import Constant from "../constants";
import { mainWrapper } from "./main";

const AuthService = {
    studentSignup,
    agentSignup,
    verfication,
    login,
    forgetPassword,
    recoverdPassword,
    verifyOTP,
    studentSignupSecondStep,
    studentSignupFinalStep,
    logOut,
    agentSignupStepOne,
    agentSignupStepTwo,
    agentSignupStepThree,
    checkEmailExist,
    checkLoginState
};

function studentSignup(token, params) {
    return mainWrapper.put(Constant.host + "/auth/student-step-one/" + token, params);
}

function studentSignupSecondStep(token, params) {
    return mainWrapper.put(Constant.host + "/auth/student-step-second/" + token, params);
}

function studentSignupFinalStep(token, params) {
    return mainWrapper.put(Constant.host + "/auth/student-step-final/" + token, params);
}

function agentSignup(params) {
    return mainWrapper.post(Constant.host + "/auth/agent-signup", params);
}

function verfication(params) {
    return mainWrapper.get(Constant.host + "/auth/email-verification", params);
}

function login(params) {
    return mainWrapper.post(Constant.host + "/auth/login", params);
}

function forgetPassword(params) {
    return mainWrapper.post(Constant.host + "/auth/forgot-password", params);
}

function verifyOTP(token, otp) {
    return mainWrapper.post(Constant.host + "/auth/verify-otp/" + token, { otp });
}

function recoverdPassword(token, params) {
    return mainWrapper.post(Constant.host + "/auth/recover-password/" + token, params);
}

function logOut() {
    return mainWrapper.post(Constant.host + '/auth/logout');
}

function agentSignupStepOne(token, params) {
    return mainWrapper.put(Constant.host + "/auth/agent-step-one/" + token, params);
}

function agentSignupStepTwo(token, params) {
    return mainWrapper.put(Constant.host + "/auth/agent-step-second/" + token, params);
}

function agentSignupStepThree(token, params) {
    return mainWrapper.put(Constant.host + "/auth/agent-step-final/" + token, params);
}

async function checkEmailExist(email) {
    return await mainWrapper.getAxios(Constant.host + '/auth/check-email-exist', {
        email: email
    });
}

function checkLoginState() {
    return mainWrapper.post(Constant.host + "/auth/auth-state");
}

export default AuthService;
