import Constant from "../constants";
import { mainWrapper } from "./main";

const LeadsService = {
    getApplicationData,
    submitApplication,
    getLeads
};

function getApplicationData(params) {
    return mainWrapper.post(Constant.host + "/leads/application-data",params);
}

function submitApplication(params) {
    return mainWrapper.post(Constant.host + "/leads/submit-application",params);
}

function getLeads() {
    return mainWrapper.get(Constant.host + "/leads");
}

export default LeadsService;