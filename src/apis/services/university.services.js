import Constant from "../constants";
import { mainWrapper } from "./main";

const UniversityService = {
    getUniversity,
    getUniversityDetails,
    favUniversity,
    appliedUniversity,
};

function getUniversity(params) {
    return mainWrapper.get(Constant.host + "/universities", params);
}

function getUniversityDetails(id) {
    return mainWrapper.get(Constant.host + `/universities/${id}/detail`);
}

function favUniversity(params) {
    return mainWrapper.post(Constant.host + "/universities/favourite", params);
}

function appliedUniversity(params) {
    return mainWrapper.post(Constant.host + "/applied", params);
}

export default UniversityService;
