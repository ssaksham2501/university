import Constant from "../constants";
import { mainWrapper } from "./main";

const ContactService = {
    saveContactUs,
    getContact,
};
function saveContactUs(params) {
    return mainWrapper.post(Constant.host + "/help-support",params);
}

function getContact(params) {
    return mainWrapper.get(Constant.host + "/pages/contact-us",params);
}
export default ContactService;






