import Constant from "../../constants";
import { mainWrapper } from "../main";


const AgentDashboardService = {
    allLeads,
    myLeads,
};

function allLeads(params) {
    return mainWrapper.get(Constant.host + "/leads/all-listing",params);
}

function myLeads(params) {
    return mainWrapper.get(Constant.host + "/leads/my-application",params);
}

export default AgentDashboardService;
