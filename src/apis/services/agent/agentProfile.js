import Constant from "../../constants";
import { mainWrapper } from "../main";


const AgentProfileService = {
    profileDetails,
    updateProfile,
    changePasswords,
    getListing,
    changePrefernce,
    reviewListing,
    addReview,
    alreadySubmittedReview
};

function profileDetails(slug) {
    return mainWrapper.get(Constant.host + "/agent/"+ slug );
}

function updateProfile(params) {
    return mainWrapper.put(Constant.host + "/update-profile", params);
}

function changePasswords(params) {
    return mainWrapper.put(Constant.host + "/change-password" , params);
}


function getListing(params) {
    return mainWrapper.get(Constant.host + "/agents" , params);
}

function changePrefernce(params) {
    return mainWrapper.put(Constant.host + "/agent/update-preferences" , params);
}

function reviewListing(params) {
    return mainWrapper.get(Constant.host + "/reviews", params);
}
function addReview(params) {
    return mainWrapper.post(Constant.host + "/reviews/add", params);
}
function alreadySubmittedReview(params) {
    return mainWrapper.post(Constant.host + "/reviews/is-added", params);
}

export default AgentProfileService;
