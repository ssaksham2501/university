import Constant from "../../constants";
import { mainWrapper } from "../main";


const StudentProfileService = {
    profileDetails,
    updateProfile,
    changePasswords,
    StudentchangePrefernceOne,
    StudentchangePrefernceTwo,
};

function profileDetails() {
    return mainWrapper.get(Constant.host + "/profile");
}

function updateProfile(params) {
    return mainWrapper.put(Constant.host + "/update-profile", params);
}

function changePasswords(params) {
    return mainWrapper.put(Constant.host + "/change-password" , params);
}

function StudentchangePrefernceOne(params) {
    return mainWrapper.put(Constant.host + "/update-preferences/step-1" , params);
}

function StudentchangePrefernceTwo(params) {
    return mainWrapper.put(Constant.host + "/update-preferences/step-2" , params);
}


export default StudentProfileService;
