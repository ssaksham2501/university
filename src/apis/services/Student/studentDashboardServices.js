import Constant from "../../constants";
import { mainWrapper } from "../main";

const StudentDashboardService = {
    favUnivertiesListing,
    getMyApplication,
};

function favUnivertiesListing(params) {
    return mainWrapper.get(
        Constant.host + "/universities/favourite-listing",
        params
    );
}
function getMyApplication(params) {
    return mainWrapper.get(
        Constant.host + "/leads/my-application/student",
        params
    );
}

export default StudentDashboardService;
