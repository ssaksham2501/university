import Constant from "../constants";
import { mainWrapper } from "./main";

const FilterService = {
    getFeedback,
    getDemographics,
    getIndustry,
    getAffiliations,
    getUniversityFilters,
    getPrograms,
    getCountries,
    performSearch
};

function getFeedback() {
    return mainWrapper.get(Constant.host + "/feedback-categories");
}

function getDemographics() {
    return mainWrapper.get(Constant.host + "/class-demographics");
}

function getIndustry() {
    return mainWrapper.get(Constant.host + "/industry-types");
}

function getAffiliations() {
    return mainWrapper.get(Constant.host + "/diversity-affiliations");
}

function getUniversityFilters() {
    return mainWrapper.get(Constant.host + "/filters/others");
}

function getPrograms(data) {
    return mainWrapper.get(Constant.host + "/filters/programs", data);
}

function getCountries(data) {
    return mainWrapper.get(Constant.host + "/countries", data);
}

function performSearch(data) {
    return mainWrapper.getAxios(Constant.host + "/perform-search", data);
}



export default FilterService;
