import Constant from "../constants";
import { mainWrapper } from "./main";

const PagesService = {
    privacyPolicy,
    termsConditions,
    aboutUs,
    faq,
    feedback,
    helpSupport,
};

function privacyPolicy() {
    return mainWrapper.get(Constant.host + "/pages/privacy-policy");
}

function termsConditions() {
    return mainWrapper.get(Constant.host + "/pages/terms-condition");
}

function aboutUs() {
    return mainWrapper.get(Constant.host + "/pages/about-us");
}

function faq() {
    return mainWrapper.get(Constant.host + "/faq");
}

function feedback(params) {
    return mainWrapper.post(Constant.host + "/feedback",params);
}

function helpSupport(params) {
    return mainWrapper.post(Constant.host + "/help-support",params);
}

export default PagesService;
