import Constant from "../constants";
import { mainWrapper } from "./main";


const listing  =  (inputData) =>{
    return mainWrapper.get(Constant.host + "/articals", inputData);
}
const categoryList =() =>{
  return mainWrapper.get(Constant.host + "/articals-categories")
}

const knowledgeBaseDetail = (slug) =>{
  return mainWrapper.get(Constant.host + '/articals/detail/' + slug)
}


const KnowledgebaseData = {
  listing,
  categoryList,
  knowledgeBaseDetail,
};

export default KnowledgebaseData;