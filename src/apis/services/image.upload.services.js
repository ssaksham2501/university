import Constant from "../constants";
import { mainWrapper } from './main';


const baseUrl = `${Constant.host}`;

const UploadService = {
  fileupload,
  uploadUserImage,
  removeFile,
  removeUserFile
};

function fileupload(params, progressCallback) {
  return mainWrapper.upload(
    baseUrl + '/upload-file',
    params,
    progressCallback,
  );
}


function removeFile(params) {
  return mainWrapper.post(
    baseUrl + '/remove-file',
    params
  );
}

function removeUserFile() {
  return mainWrapper.post(
    baseUrl + '/users/remove-picture'
  );
}

function uploadUserImage(params, progressCallback) {
  return mainWrapper.upload(
    baseUrl + '/upload-picture',
    params,
    progressCallback,
  );
}

export default UploadService;
