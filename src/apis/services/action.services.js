import Constant from "../constants";
import { mainWrapper } from "./main";

const ActionService = {
    getSingupStepOne,
    getEducation,
    getDegrees,
    getStates,
    getProgramExpertise,
    getLocation,
    getScholarships,
    getSetting

};

function getSingupStepOne() {
    return mainWrapper.get(Constant.host + "/step-one-drops");
}

function getEducation() {
    return mainWrapper.get(Constant.host + "/education");
}

function getDegrees() {
    return mainWrapper.get(Constant.host + "/step-one-drops");
}

function getStates(params) {
    return mainWrapper.get(Constant.host + "/states",params);
}

function getProgramExpertise() {
    return mainWrapper.get(Constant.host + "/program-expertise");
}

function getLocation(params) {
    return mainWrapper.get(Constant.host + "/location",params);
}

function getScholarships() {
    return mainWrapper.get(Constant.host + "/scholarships");
}

function getSetting() {
    return mainWrapper.get(Constant.host + "/settings");
}

export default ActionService;
