import Constant from "../constants";
import { mainWrapper } from "./main";

const ModalService = {
    saveloginmodal,
    saveotpmodal,
    savestudenttypemodal,
    resendOtpRequest
};
function saveloginmodal(params) {
    return mainWrapper.post(Constant.host + "/auth/signup",params);
}
function  saveotpmodal(token, params) {
    return mainWrapper.put(Constant.host + "/auth/phone-verification/" + token, params);
}

function  savestudenttypemodal(token, params) {
    return mainWrapper.put(Constant.host + "/auth/register-user-type/" + token, params);
}

function resendOtpRequest(token) {
    return mainWrapper.post(Constant.host + "/auth/resend-otp/" + token, {});
}


export default ModalService;