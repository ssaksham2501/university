import React from "react";
import AgentDashboardService from "../../services/agent/agentDashboardServices";
import StudentDashboardService from "../../services/Student/studentDashboardServices";

class AgentDashboardController extends React.Component {
    constructor(props) {
        super(props);
    }

    async allLeads(data) {
        let response = await AgentDashboardService.allLeads(data);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async myLeads(data) {
        let response = await AgentDashboardService.myLeads(data);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

}
export default AgentDashboardController;
