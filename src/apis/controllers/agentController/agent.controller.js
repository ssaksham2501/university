import React from "react";
import AgentProfileService from "../../services/agent/agentProfile";
import StudentProfileService from "../../services/Student/studentProfile";

class AgentProfileController extends React.Component {
    constructor(props) {
        super(props);
    }

    async agentProfileDetails(slug) {
        let response = await AgentProfileService.profileDetails(slug);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async AgentUpdateProfile(data) {
        let post = {
            first_name: data.firstName,
            last_name: data.lastName,
            email: data.email,
            phonenumber: data.phoneNumber,
            zipcode: data.pincode,
            state_id: data.stateId ? data.stateId.id : null,
            //image: data.image,
        }
        let response = await AgentProfileService.updateProfile(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async changePasswords(data) {
        let post = {
            old_password: data.oldPassword,
            new_password: data.newPsssword,
        }
        let response = await AgentProfileService.changePasswords(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getListing(data) {
        let post = {
            page: data.page,
            slug: data.slug,
            modal: data.modal ? 1 : 0,
            sorting: data.sorting && data.sorting.value ? data.sorting.value : null
        }
        let response = await AgentProfileService.getListing(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async changePrefernce(data) {
        let post = {
            countries: data.country,
            universities: data.university.map(i => i.slug)
        }
        let response = await AgentProfileService.changePrefernce(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async reviewListing(data) {
        let post = {
            page: data.page,
            agent: data.agent,
            university: data.university,
        }
        let response = await AgentProfileService.reviewListing(post);
        return response;
    }

    async alreadySubmittedReview(type) {
        let response = await AgentProfileService.alreadySubmittedReview({type});
        return response
    }
   
    async addReview(data) {
        let post = {
            university: data.university ? data.university : null,
            agent: data.agent ? data.agent : null,
            review: data.review,
            rating: data.rating && data.rating > 20 ? (data.rating/20) : null
        };
        let response = await AgentProfileService.addReview(post);
        return response;
    }

    async isAdded(data) {
        let post = {
            type: data.type,
            slug: data.slug,
        };
        let response = await AgentProfileService.alreadySubmittedReview(post);
        return response;
    }
}
export default AgentProfileController;
