import React from "react";
import AuthService from "../services/auth.services";
import { setUserData } from "../../redux/actions/users";
import store from "../../redux/store";

class AuthController extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * To sign up the customer.
     * @param {Array} data
     * @return {Array} user
     */
    async studentSignup(token, data) {
        let put = {
            country: data.country,
            resumption_semster: data.resumption_semster,
            stream_id: data.stream_id,
        };
        let response = await AuthService.studentSignup(token, put);
        return response;
    }

    async studentSignupSecondStep(token, data) {
        let put = {
            education_id: data.education_id,
            gained_percentage: data.gained_percentage,
            year_of_graduation: data.year_of_graduation && data.year_of_graduation.value,
            tofel: data.tofel,
            ielts: data.ielts,
            pte: data.pte,
            ielts_score: data.ielts_score ? data.ielts_score : 0,
            tofel_score: data.tofel_score ? data.tofel_score : 0,
            pte_score: data.pte_score ? data.pte_score : 0,
        };
        let response = await AuthService.studentSignupSecondStep(token, put);
        return response;
    }

    async studentSignupFinalStep(token, data) {
        let put = {
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            password: data.password,
            pincode: data.zipcode,
            state_id: data.state_id.id,
            device_id: null,
            device_type: 'web',
            device_name: null,
            fcm_token: null
        };
        let response = await AuthService.studentSignupFinalStep(token, put);
        return response;
    }

    // async studentSignup(data) {
    //     let post = {
    //         first_name: data.firstName,
    //         middle_name: data.middleName,
    //         last_name: data.lastName,
    //         email: data.email,
    //         password: data.password,
    //         phonenumber: data.phoneNumber,
    //         education: data.education,
    //         industry_type_id: data.industryId,
    //         linkedin_url: data.linkdinUrl,
    //         profession: data.profession,
    //         profession_title: data.professionTitle,
    //         country: data.country,
    //         dob: data.dob,
    //         about_us: data.aboutUs,
    //         degree_id: data.degreeId,
    //         resumption_semster: data.resumptionSemster,
    //         address: data.address,
    //         device_id: "",
    //         device_type: "web",
    //         device_name: "",
    //         fcm_token: ""
    //     };
    //     let response = await AuthService.studentSignup(post);
    //     if (response && response.status) {
    //         return response;
    //     } else {
    //         //new Toaster().error(response.error);
    //         return null;
    //     }
    // }

    async agentSignup(data) {
        let post = {
            first_name: data.firstName,
            last_name: data.lastName,
            email: data.email,
            user_name: data.userName,
            password: data.password,
            image: data.image,
            about_us: data.aboutUs,
            name_of_institution: data.instituteName,
            year_of_graduation: data.graduationYear,
            pre_mba_industry: data.preMba,
            clubs_affiliation: data.clubsAffiliation,
            linkedin_url: data.linkdinUrl,
            industry_type_id: data.industryId,
            device_id: "",
            device_type: "web",
            device_name: "",
            fcm_token: ""
        };
        let response = await AuthService.agentSignup(post);
        return response;
    }

    async verfication(data) {
        let response = await AuthService.verfication(data);
        return response;
    }

    async studentLogin(data) {
        let post = {
            email: data.email,
            password: data.password,
            device_type: "web"
        };
        let response = await AuthService.studentLogin(post);
        return response;
    }

    async agentLogin(data) {
        let post = {
            email: data.email,
            password: data.password,
            device_type: "web"
        };
        let response = await AuthService.agentLogin(post);
        return response;
    }

    async forgetPassword(data) {
        let post = {
            email: data.email,
        };
        let response = await AuthService.forgetPassword(post);
        return response;
    }

    async verifyOtp(token, otp) {
        let response = await AuthService.verifyOTP(token, otp);
        return response;
    }

    async recoverdPassword(token, data) {
        let post = {
            new_password: data.new_password,
            confirm_password: data.confirm_password,
            otp: data.otp,
        };
        let response = await AuthService.recoverdPassword(token, post);
        return response;
    }


    getToken() {
        let user = store.getState().UserReducer.user;
        return user && user.access && user.access.token
            ? user.access.token
            : null;
    }

    getLoginUser() {
        let user = store.getState().UserReducer.user;
        return user && user.id ? user : null;
    }

    getLoginUserId() {
        let user = store.getState().UserReducer.user;
        return user && user !== null && user.id ? user.id : null;
    }

    async setUpLogin(user) {
        await store.dispatch(setUserData(user));
    }

    async logout() {
        store.dispatch(setUserData({}));
        localStorage.clear();
        await AuthService.logOut();
    }

    async agentSignupStepOne(token, data) {
        let put = {
            is_company: data.isCompany,
            company_name: data.companyName,
            first_name: data.firstName,
            last_name: data.lastName,
            email: data.email,
            state_id: data.stateId ? data.stateId.id : null,
            pincode: data.pinCode,
            password: data.password,
            bio: data.bio,
        };
        let response = await AuthService.agentSignupStepOne(token, put);
        return response
    }

    async agentSignupStepTwo(token, data) {
        let put = {
            countries: data.country,
            universities: data.university.map(i => i.slug)
        };
        let response = await AuthService.agentSignupStepTwo(token, put);
        return response;
    }

    async agentSignupStepThree(token, data) {
        let put = {
            range_of_success_cases: data.successCases ? data.successCases.label : null,
            range_of_success_rate: data.successRate ? data.successRate.label : null,
            accreditation: data.accreditation && data.accreditation.length > 0 ? data.accreditation : null,
            credentials: data.credentials && data.credentials.length > 0 ? data.credentials : null,
        };
        let response = await AuthService.agentSignupStepThree(token, put);
        return response;
    }

    async checkEmailExist(email)
    {
        let response = await AuthService.checkEmailExist(email);
        return response;
    }

    async checkLoginState() {
        let response = await AuthService.checkLoginState();
        return response;
    }
}
export default AuthController;
