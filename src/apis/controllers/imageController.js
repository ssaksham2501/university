import React from 'react';
import base64File from '../../helpers/base64file';
import UploadService from '../services/image.upload.services';

class ImageUploadController extends React.Component {
  constructor(props) {
    super(props);
  }

  /**
   *  Imageupload
   * @param {Array} data
   * @return {Array} image
   */
  async addImage(data) {
    let img = await base64File(data);
    console.log('controller===============',img);
    let post = {
      file: img,
      path: data.folder,
      name: data.file.name,
      file_type: 'image',
    };
    let response = await UploadService.fileupload(post, data.callback);
    if (response && response.status) {
      return response;
    } else {
      // new Toast().error(response.error);
      return null;
    }
  }

  async addDocument(data) {
    let img = await base64File(data.image.uri);
    let post = {
      file: img,
      path: data.folder,
      name: data.image.name,
      file_type: 'file',
    };
    let response = await UploadService.fileupload(post, data.callback);
    if (response && response.status) {
      return response;
    } else {
      // new Toaster().error(response.error);
      return null;
    }
  }
}

export default ImageUploadController;
