import React from "react";
import FilterService from "../services/filter.services";

class FilterController extends React.Component {
   
    async getFeedback() {
        let response = await FilterService.getFeedback();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getDemographics() {
        let response = await FilterService.getDemographics();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getIndustry() {
        let response = await FilterService.getIndustry();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getAffiliations() {
        let response = await FilterService.getAffiliations();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getUniversityFilters() {
        let response = await FilterService.getUniversityFilters();
        if (response && response.status) {
            return response;
        } else {
            return null;
        }
    }

    async getPrograms(data = null) {
        let response = await FilterService.getPrograms(data);
        if (response && response.status) {
            return response;
        } else {
            return null;
        }
    }

    async getCountries(data = null) {
        let response = await FilterService.getCountries(data);
        if (response && response.status)
        {
            let c = [];
            for (let i in response.listing) {
                c.push(
                    {
                        id: response.listing[i].id,
                        value: response.listing[i].name,
                        label: response.listing[i].name,
                    }
                );
            }
            return c;
        } else {
            return [];
        }
    }

    async performSearch(q) {
        let response = await FilterService.performSearch({q: q});
        if (response && response.status) {
            return response;
        } else {
            return null;
        }
    }
    async gridSearch(data) {
        let response = await FilterService.performSearch(data);
        if (response && response.status) {
            return response;
        } else {
            return null;
        }
    }

}
export default FilterController;
