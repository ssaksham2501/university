import KnowledgebaseData from '../services/knowledge_base.services';


const CollegesList =  async (inputData ) => {
        let data = {
            search: inputData && inputData.search ? inputData.search : ``,
            categories: inputData && inputData.categories ? inputData.categories : []
        }
        let response = await KnowledgebaseData.listing(data)
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }

    }

const CategoriesList =  async () => {
        let response = await KnowledgebaseData.categoryList()

        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }

    }
    
 const KnowledgeBaseDetail =  async (slug) => {
        let response = await KnowledgebaseData.knowledgeBaseDetail(slug)

        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }

    }


const Knowledgebase ={
        CollegesList,
        CategoriesList,
        KnowledgeBaseDetail,
    
    }

export default Knowledgebase;