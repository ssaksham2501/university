import React from "react";
import { getStoredItem } from "../../helpers/localstorage";
import { setApplyNowModal, setGetStartedModal, setUserData } from "../../redux/actions/users";
import store from "../../redux/store";
import AuthService from "../services/auth.services";
import ModalService from "../services/modal.services";

class ModalController extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * To sign up the customer.
     * @param {Array} data
     * @return {Array} user
     */


    async login(data) {
        let post = {
            phonenumber: data.phone_number ? data.phone_number : null,
            country_code: data.country_code ? data.country_code : null,
        };
        let response = await ModalService.saveloginmodal(post);
        return response;
    }

    async authLogin(data) {
        let post = {
            phone_number: data.phone_number ? data.phone_number : null,
            country_code: data.country_code ? data.country_code : null,
            password: data.password ? data.password : null,
            device_type: 'web'
        };
        let response = await AuthService.login(post);
        if (response && response.status && response.user) {
            this.setUpLogin(response.user);
        }
        
        return response;
    }

    setUpLogin(user) {
        store.dispatch(setUserData(user));
        return user;
    }


    async otpVerification(otp) {
        let post = {
            otp: otp,
        };
        let user= await getStoredItem("user");
        let response = await ModalService.saveotpmodal(user && user.token, post);
        return response;
    }

    async resendOtpRequest(token) {
        if (!token) {
            let user = await getStoredItem("user");
            token = user.token;
        }
        let response = await ModalService.resendOtpRequest(token);
        return response;
    }

    async userTypeSignup(usertype) {
        let post = {
            user_type: usertype,
          
        };
        let user= await getStoredItem("user");
        let response = await ModalService.savestudenttypemodal(user && user.token, post);
        return response;
    }

    async setStartModal(data) {
        await store.dispatch(setGetStartedModal(data));
    }

    async setApplyModal(data) {
        await store.dispatch(setApplyNowModal(data));
    }

}
export default ModalController;
