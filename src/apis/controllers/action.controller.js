import React from "react";
import Constant from "../constants";
import ActionService from "../services/action.services";
import UploadService from "../services/image.upload.services";

class ActionController extends React.Component {
    constructor(props) {
        super(props);
    }

    async getSingupStepOne() {
        let response = await ActionService.getSingupStepOne();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getSingupStepTwo() {
        let response = await ActionService.getEducation();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getSingupStepThree() {
        let response = await ActionService.getStates();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }
    

    async getDegrees() {
        let response = await ActionService.getDegrees();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getStates(data) {
        let response = await ActionService.getStates(data);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getProgramExpertise() {
        let response = await ActionService.getProgramExpertise();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getLocation(data) {
        let response = await ActionService.getLocation(data);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getScholarships() {
        let response = await ActionService.getScholarships();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async getSetting() {
        let response = await ActionService.getSetting();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async uploadFile(data, progressCallback) {
        let form = new FormData();
        if (data.size < Constant.maxFileSize) {
            form.append("file", data);
            let response = await UploadService.fileupload(form, progressCallback);
            return response;
        }
        else {
            return {
                status: false,
                error: 'File size exceeds maximum limit 25MB.'
            }
        }
    }

    async removeFile(file) {
        await UploadService.removeFile({file});
    }

    async removeUserFile() {
        let response = await UploadService.removeUserFile();
        return response;
    }

    async uploadUserImage(data, progressCallback) {
        let form = new FormData();
        if (data.size < Constant.maxFileSize) {
            form.append("file", data);
            let response = await UploadService.uploadUserImage(form, progressCallback);
            return response;
        }
        else {
            return {
                status: false,
                error: 'File size exceeds maximum limit 25MB.'
            }
        }
    }
}
export default ActionController;
