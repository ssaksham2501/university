import React from "react";
import StudentProfileService from "../../services/Student/studentProfile";

class StudentProfileController extends React.Component {
    constructor(props) {
        super(props);
    }

    async profileDetails() {
        let response = await StudentProfileService.profileDetails();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async updateProfile(data) {
        let post = {
            first_name: data.firstName,
            last_name: data.lastName,
            email: data.email,
            pincode: data.pincode,
            state_id: data.stateId ? data.stateId : null,
            bio: data.bio ? data.bio : null,
            // image: data.image,
        }
        let response = await StudentProfileService.updateProfile(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async StudentchangePrefernceOne(data) {
        let post = {
            country: data.country,
            resumption_semster: data.resumption_semster,
            stream_id: data.stream_id,
        }
        let response = await StudentProfileService.StudentchangePrefernceOne(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async StudentchangePrefernceTwo(data) {
        let post = {
            education_id: data.education_id,
            gained_percentage: data.gained_percentage,
            year_of_graduation: data.year_of_graduation,
            tofel: data.tofel,
            ielts: data.ielts,
            pte: data.pte,
            ielts_score: data.ielts_score ? data.ielts_score : 0,
            tofel_score: data.tofel_score ? data.tofel_score : 0,
            pte_score: data.pte_score ? data.pte_score : 0,
        }
        let response = await StudentProfileService.StudentchangePrefernceTwo(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

}
export default StudentProfileController;
