import React from "react";
import StudentDashboardService from "../../services/Student/studentDashboardServices";

class StudentDashboardController extends React.Component {
    constructor(props) {
        super(props);
    }

    async favUnivertiesListing(data) {
        let response = await StudentDashboardService.favUnivertiesListing(data);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }
    async getMyApplication(params) {
        let response = await StudentDashboardService.getMyApplication(params);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }
}
export default StudentDashboardController;
