import React from "react";
import AuthService from "../services/auth.services";
import PagesService from "../services/pages";

class PagesController extends React.Component {
    constructor(props) {
        super(props);
    }

    async privacyPolicy() {
        let response = await PagesService.privacyPolicy();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async termsConditions() {
        let response = await PagesService.termsConditions();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async aboutUs() {
        let response = await PagesService.aboutUs();
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }
    
    async feedback(data) {
        let post = {
            category_id: data.category.id,
            feedback: data.feedback
        };
        let response = await AuthService.feedback(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async helpSupport(data) {
        let post = {
            name: data.Name,
            email: data.email,
            subject: data.subject,
            description: data.description,
        };
        let response = await AuthService.helpSupport(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async faqs() {
        let response = await PagesService.faq();
        if (response && response.status) {
            return response;
        } else {
            return null;
        }
    }

}
export default PagesController;
