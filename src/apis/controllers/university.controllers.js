import React from "react";
import UniversityService from "../services/university.services";

class UniversityController extends React.Component {
    constructor(props) {
        super(props);
    }

    async getUniversity(data) {
        let allData = {
            tuition_cost: data && data.tuition_cost ? [data.tuition_cost.min, data.tuition_cost.max] : null,
            cost_of_living: data && data.cost_of_living ? [data.cost_of_living.min, data.cost_of_living.max] : null,
            scholarship: data && data.scholarship ? 1 : null,
            stream: data && data.stream ? data.stream : null,
            program: data && data.program ? data.program : null,
            gpa: data && data.gpa ? [data.gpa.min, data.gpa.max] : null,
            duration: data && data.duration ? [data.duration.min, data.duration.max] : null,
            ielts: data && data.ielts ? 1 : null,
            pte: data && data.pte ? 1 : null,
            toefl: data && data.toefl ? 1 : null,
            rating: data && data.rating ? data.rating / 20 : null,
            sort: data && data.sort ? data.sort : null,
            search: data.search ? data.search : null,
            country: data.country ? data.country : null,
            page: data.page ? data.page : 1,
        }
        try {
            let response = await UniversityService.getUniversity(allData);
            if (response && response.status) {
                return response;
            } else {
                //new Toaster().error(response.error);
                return null;
            }
        }
        catch (e) {
            
        }
    }

    async getUniversityDetails(id) {
        let response = await UniversityService.getUniversityDetails(id);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

    async favUniversity(data) {
        let post = {
            university_id: data
        };
        let response = await UniversityService.favUniversity(post);
        return response;
    }

    async appliedUniversity(data) {
        let post = {
            university_id: data
        };
        let response = await UniversityService.appliedUniversity(post);
        if (response && response.status) {
            return response;
        } else {
            //new Toaster().error(response.error);
            return null;
        }
    }

}
export default UniversityController;
