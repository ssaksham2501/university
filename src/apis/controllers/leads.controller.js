import React from "react";
import { getStoredItem } from "../../helpers/localstorage";
import { setApplyNowModal, setGetStartedModal, setUserData } from "../../redux/actions/users";
import store from "../../redux/store";
import LeadsService from "../services/leads.services";

class LeadsController extends React.Component {
    constructor(props) {
        super(props);
    }

    async getApplicationData(data) {
        let post = {
            type: data.type,
            university: data.university ? data.university : ``,
            agent: data.agent ? data.agent : ``
        }
        let response = await LeadsService.getApplicationData(post);
        return response;
    }

    async submitApplication(data) {
        let post = {
            message: data.message,
            type: data.type,
            university: data.university ? data.university : null,
            agent: data.agent ? data.agent : null,
            courses: data.courses ? data.courses : null
        }
        let response = await LeadsService.submitApplication(post);
        return response;
    }

    async getLeads() {
        let response = await LeadsService.getLeads();
        return response;
    }
}
export default LeadsController;