const Constant = {
    debug: true,
    image: 'http://127.0.0.1:8000',
    host: 'http://127.0.0.1:8000/api',
    maxFileSize: 5 * 1000 * 1000
  };
  
  export default Constant;